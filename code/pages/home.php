<?php
require_once('../config/init.php');

$logged_user = getLoggedinId();
if($logged_user !== -1)
    header('Location: dashboard.php');
else
{
    $smarty->display('content/home.tpl');
    unset($_SESSION['success_messages']);
    unset($_SESSION['error_messages']);
}
