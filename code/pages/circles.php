<?php
require_once('../config/init.php');

$circlesDB = $database->circlesDB;

$logged_user = getLoggedinId();
$logged_username = getLoggedinUsername();
if($logged_user === -1)
    header('Location: home.php');
else
{
    $logged_user_photo_id = $database->getProfilePicFromEmail(getLoggedinEmail());
    $smarty->assign('logged_user', $logged_user);
    $smarty->assign('logged_user_photo_id', $logged_user_photo_id);
    $smarty->assign('logged_username', $logged_username);

    $friends = $circlesDB->getFriends($logged_user);
    $love_matches = $circlesDB->getLoveMatches($logged_user);

    foreach($friends as &$friend)
    {
        if(isset($friend["city"]) && isset($friend["country"]) && ($friend["city"] != "") && ($friend["country"] != ""))
            $friend["location"] = $friend["city"] . ", " . $friend["country"];
        else
            $friend["location"] = $friend["city"] . $friend["country"];

        unset($friend["city"]);
        unset($friend["country"]);
    }

    foreach($love_matches as &$love_match)
    {
        if(isset($love_match["city"]) && isset($love_match["country"]) && ($love_match["city"] != "") && ($love_match["country"] != ""))
            $love_match["location"] = $love_match["city"] . ", " . $love_match["country"];
        else
            $love_match["location"] = $love_match["city"] . $love_match["country"];

        unset($love_match["city"]);
        unset($love_match["country"]);
    }

    $smarty->assign('friends', $friends);
    $smarty->assign('love_matches', $love_matches);
    $smarty->display('common/header.tpl');
    $smarty->display('content/circles.tpl');
    unset($_SESSION['success_messages']);
    unset($_SESSION['error_messages']);
    $smarty->display('common/footer.tpl');
}
