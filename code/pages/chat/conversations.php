<?php
require_once('../../config/init.php');

$logged_user = getLoggedinId();
$logged_username = getLoggedinUsername();
if($logged_user === -1)
    header('Location: ../home.php');
else
{
    $chatDB = $database->chatDB;

    $logged_user_photo_id = $database->getProfilePicFromEmail(getLoggedinEmail());

    $smarty->assign('logged_user', $logged_user);
    $smarty->assign('logged_user_photo_id', $logged_user_photo_id);
    $smarty->assign('logged_username', $logged_username);

    $conversations = $chatDB->getAllUserConversations($logged_user);
    if(count($conversations) == 0)
        $_SESSION['error_messages'][] = 'You have no conversations! Why don\'t you search for some new friends?';

    $smarty->assign('ERROR_MESSAGES', $_SESSION['error_messages']);
    $smarty->assign('conversations', $conversations);
    $smarty->display('common/header.tpl');
    $smarty->display('content/conversations.tpl');
    unset($_SESSION['success_messages']);
    unset($_SESSION['error_messages']);
    $smarty->display('common/footer.tpl');
}
