<?php
require_once('../../config/init.php');

$chatDB = $database->chatDB;

$logged_user = getLoggedinId();
$logged_username = getLoggedinUsername();

if($logged_user === -1 || !$_GET['id'])
    header('Location: ../home.php');
else
{
    //verify if logged in user is allowed to see conversation
    $conversation_id = $_GET['id'];

    if($chatDB->verifyConversation($logged_user,$conversation_id))
    {
        $logged_user_photo_id = $database->getProfilePicFromEmail(getLoggedinEmail());

        $other_user = $chatDB->getOtherUserFromConversation($conversation_id, $logged_user);

        $numMsg = 10;
        $maxId = $chatDB->getMaxIdMessage($conversation_id);
        $maxId++;
        $messages = $chatDB->getOldMessagesFromConversation($conversation_id, $numMsg, $maxId);

        $messages = array_reverse($messages);

        $smarty->assign('other_user', $other_user);
        $smarty->assign('messages', $messages);
        $smarty->assign('conversation_id', $conversation_id);

        $logged_user_photo_id = $database->getProfilePicFromEmail(getLoggedinEmail());
        $smarty->assign('logged_user_photo_id', $logged_user_photo_id);
        $smarty->assign('logged_user', $logged_user);
        $smarty->assign('logged_username', $logged_username);

        $smarty->display('common/header.tpl');
        $smarty->display('content/conversation.tpl');
        unset($_SESSION['success_messages']);
        unset($_SESSION['error_messages']);
        $smarty->display('common/footer.tpl');
    }
    else
    {
        header('Location: ../home.php');
    }
}
