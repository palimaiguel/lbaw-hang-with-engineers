<?php
require_once('../../config/init.php');

$profileDB = $database->profileDB;

$logged_user = getLoggedinId();
$logged_username = getLoggedinUsername();
if($logged_user === -1)
    header('Location: ../home.php');
else
{
    $admin = $profileDB->isAdmin($logged_user);
    $logged_user_photo_id = $database->getProfilePicFromEmail(getLoggedinEmail());
    $smarty->assign('logged_user', $logged_user);
    $smarty->assign('logged_user_photo_id', $logged_user_photo_id);
    $smarty->assign('logged_username', $logged_username);
    $smarty->assign('admin', $admin);

    $id = null;
    $own_profile = null;

    if(isset($_GET['id']))
    {
        if($_GET['id'] > 0)
        {
            $id = strip_tags($_GET['id']);

            $own_profile = ($id == $logged_user);
        }
        else
            header('Location: ../home.php');
    }
    else
    {
        $id = $logged_user;
        $own_profile = true;
    }

    $profile_admin = $profileDB->isAdmin($id);
    $banned = $profileDB->isBanned($id);
    $user = $profileDB->getUserInfo($id);
    if(!$user)
        header('Location: ../home.php');
    $timeline = $profileDB->getUserTimeline($id, $logged_user);
    $about_me = $profileDB->getUserAboutMe($id, $logged_user);
    $photos = $profileDB->getUserPhotos($id, $logged_user);
    $n_photos = count($photos);

    $smarty->assign('profile_admin', $profile_admin);
    $smarty->assign('banned', $banned);
    $smarty->assign('user', $user);
    $smarty->assign('timeline', $timeline);
    $smarty->assign('about_me', $about_me);
    $smarty->assign('photos', $photos);
    $smarty->assign('n_photos', $n_photos);
    $smarty->assign('own_profile', $own_profile);
    $smarty->assign('ERROR_MESSAGES', $_SESSION['error_messages']);
    $smarty->assign('SUCCESS_MESSAGES', $_SESSION['success_messages']);

    $smarty->display('common/header.tpl');
    $smarty->display('content/profile.tpl');
    unset($_SESSION['success_messages']);
    unset($_SESSION['error_messages']);
    $smarty->display('common/footer.tpl');
}
