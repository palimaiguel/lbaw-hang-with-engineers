<?php
require_once('../../config/init.php');

$logged_user = getLoggedinId();
$logged_username = getLoggedinUsername();
if($logged_user === -1)
    header('Location: ../home.php');
else
{
    $profileDB = $database->profileDB;

    $admin = $profileDB->isAdmin($logged_user);
    $logged_user_photo_id = $database->getProfilePicFromEmail(getLoggedinEmail());
    $smarty->assign('logged_user', $logged_user);
    $smarty->assign('logged_user_photo_id', $logged_user_photo_id);
    $smarty->assign('logged_username', $logged_username);
    $smarty->assign('admin', $admin);

    $countries = $profileDB->getCountryList();
    $user = $profileDB->getUserInfo($logged_user);
    $timeline = $profileDB->getUserTimeline($logged_user, $logged_user);
    $about_me = $profileDB->getUserAboutMe($logged_user, $logged_user);
    $photos = $profileDB->getUserPhotos($logged_user, $logged_user);
    $n_photos = count($photos);

    $smarty->assign('user', $user);
    $smarty->assign('countries', $countries);
    $smarty->assign('user', $user);
    $smarty->assign('timeline', $timeline);
    $smarty->assign('about_me', $about_me);
    $smarty->assign('photos', $photos);
    $smarty->assign('n_photos', $n_photos);
    $smarty->assign('ERROR_MESSAGES', $_SESSION['error_messages']);
    $smarty->assign('SUCCESS_MESSAGES', $_SESSION['success_messages']);

    $smarty->display('common/header.tpl');
    $smarty->display('content/edit_profile.tpl');
    unset($_SESSION['success_messages']);
    unset($_SESSION['error_messages']);
    $smarty->display('common/footer.tpl');
}

