<?php
require_once('../config/init.php');

$logged_user = getLoggedinId();
$logged_username = getLoggedinUsername();
if($logged_user === -1)
    header('Location: home.php');
else {
    $logged_user_photo_id = $database->getProfilePicFromEmail(getLoggedinEmail());

    $smarty->assign('logged_user', $logged_user);
    $smarty->assign('logged_user_photo_id', $logged_user_photo_id);
    $smarty->assign('logged_username', $logged_username);

    if(isset($_POST['keyword']))
        $smarty->assign('keyword', $_POST['keyword']);
    else
        $smarty->assign('keyword', "");

    $smarty->display('common/header.tpl');
    $smarty->display('content/search.tpl');
    unset($_SESSION['success_messages']);
    unset($_SESSION['error_messages']);
    $smarty->display('common/footer.tpl');

}