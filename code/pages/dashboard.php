<?php
require_once('../config/init.php');

$logged_user = getLoggedinId();
$logged_username = getLoggedinUsername();

if($logged_user === -1)
    header('Location: home.php');
else
{
    $logged_user_photo_id = $database->getProfilePicFromEmail(getLoggedinEmail());
    $smarty->assign('logged_user_photo_id', $logged_user_photo_id);
    $smarty->assign('logged_user', $logged_user);
    $smarty->assign('logged_username', $logged_username);

    $dashboardDB = $database->dashboardDB;
    $numMsg = 10;
    $offset = 0;
    $publications = $dashboardDB->getGlobalPublications($logged_user, $numMsg, $offset);
    $friendsPublications = $dashboardDB->getFriendsPublications($logged_user, $numMsg, $offset);
    $matchesPublications = $dashboardDB->getLoveMatchesPublications($logged_user, $numMsg, $offset);


    // CIRCLES STAT
    $friendsCount = $dashboardDB->getNumFriends($logged_user);
    $numTotalFriends = $friendsCount[0]["number_friends"];

    $lovedCount = $dashboardDB->getNumLoveMatches($logged_user);
    $numTotalLoved = $lovedCount[0]["number_loved"];

    $numTotalCircles = $numTotalFriends + $numTotalLoved;

    // LOVE MATCHES STAT
    $addedLoveMatchesCount = $dashboardDB->getAddedLoveMatches($logged_user);
    $numTotalLoveMatches = $addedLoveMatchesCount[0]["number_love_matches"];


    // MESSAGES STAT

    $messagesCount = $dashboardDB->getNumMessagesSent($logged_user);
    $numMessagesSent = $messagesCount[0]["number_messages"];

    // NOTIFICATIONS STAT

    $notificationsCount = $dashboardDB->getNumUnseenNotifications($logged_user);
    $numNotificationsUnseen = $notificationsCount[0]["number_notifications_unseen"];

    //var_dump($circlesCount[0]["number_love_matches"]);
    //var_dump($num);
    //flush();

    //variables assign
    $smarty->assign('publications', $publications);
    $smarty->assign('friendsPublications', $friendsPublications);
    $smarty->assign('matchesPublications', $matchesPublications);
    $smarty->assign('numTotalCircles', $numTotalCircles);
    $smarty->assign('numTotalLoveMatches', $numTotalLoveMatches);
    $smarty->assign('numMessagesSent', $numMessagesSent);
    $smarty->assign('numNotificationsUnseen', $numNotificationsUnseen);
    $smarty->assign('logged_username', $logged_username);

    //pages display
    $smarty->display('common/header.tpl');
    $smarty->display('content/dashboard.tpl');
    unset($_SESSION['success_messages']);
    unset($_SESSION['error_messages']);
    $smarty->display('common/footer.tpl');
}
