var relationshipsOpened = false;
var messagesOpened = false;
var gLastCirclesId = 0;
var gLastMessageId = 0;
var gBaseUrl = window.location.origin + "/~lbaw1412/final/";
var conversations = new Dictionary();

function Conversation(lastNotificationID, seen)
{
    this.lastNotificationID = lastNotificationID;
    this.seen = seen;
}

function Dictionary()
{
    this.keys = new Array();
    this.values = new Array();

    this.containsKey = function(key)
    {
        if(this.keys.indexOf(key) == -1)
            return false;

        return true;
    }

    this.add = function(key, value)
    {
        var index = this.keys.length;
        this.keys[index] = key;
        this.values[index] = value;
    }

    this.setValue = function(key, value)
    {
        var index = this.keys.indexOf(key);

        this.values[index] = value;
    }

    this.getValue = function(key)
    {
        var index = this.keys.indexOf(key);

        return this.values[index];
    }
}

function setUp()
{
    $(".relationships_notifications").click(function()
    {
        relationshipsOpened = !relationshipsOpened;

        if (relationshipsOpened)
        {
            readCircleNotifications(gLastCirclesId);

            updateCirclesBadge(0);
        }
    });

    $(".messages_notifications").click(function()
    {
        messagesOpened = !messagesOpened;

        if (messagesOpened)
        {
            readMessagesNotifications(gLastMessageId);

            updateMessagesBadge(0);
        }
    });

    getOldMessageNotifications(0, 4);
    getOldCircleNotifications(0, 4);
}

function updateCirclesBadge(unseenCount)
{
    if(unseenCount == 0)
        unseenCount = "";

    $("span#notification_badge").html(unseenCount);
}

function readCircleNotifications(lastId)
{
    $.ajax(
        {
            url: gBaseUrl + "api/notifications/read_circles_notifications.php",
            type: 'POST',
            data:
            {
                last_id: lastId
            },
            dataType: 'json'
        }
    );
}

function calculateTime(dateString)
{
    var regularExpression = /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/;
    var dateArray = regularExpression.exec(dateString);
    var dateObject = new Date(
        (+dateArray[1]),
        (+dateArray[2])-1, // Careful, month starts at 0!
        (+dateArray[3]),
        (+dateArray[4]),
        (+dateArray[5]),
        (+dateArray[6])
    );

    return dateObject.getTime() / 1000;
}

function calculateTimeString(dateString)
{
    var nowInSeconds = new Date().getTime() / 1000;
    var dateInSeconds = calculateTime(dateString);

    var diff = nowInSeconds - dateInSeconds;

    var timeMessage;

    if(diff < 0)
        timeMessage = "Now";
    else if(diff < 60)
        timeMessage = Math.floor(diff) + " seconds ago";
    else if(diff < 3600)
        timeMessage = Math.floor(diff / 60) + " minutes ago";
    else if(diff < 86400)
        timeMessage = Math.floor(diff / 3600) + " hours ago";
    else
        timeMessage = Math.floor(diff / 86400) + " days ago";

    return timeMessage;
}

function createLoveMatchNotification(timeMessage)
{
    var element = $(
        '<a href="#" class="list-group-item">' +
        '<div class="media">' +
        '<div class="media-body">' +
        '<h5 class="media-heading">Someone added you to its love matches!</h5>' +
        '<small class="text-muted">' + timeMessage + '</small>' +
        '</div>' +
        '</div>' +
        '</a>'
    );

    return element;
}

function createFriendNotification(notification, timeMessage)
{
    var link = gBaseUrl + 'images/avatar/' + notification.link;

    var element = $(
        '<a href="#" class="list-group-item">' +
        '<div class="media">' +
        '<div class="pull-left">' +
        '<img class="media-object img-circle pull-left" src="' + link + '" alt="user#1" width="40">' +
        '</div>' +
        '<div class="media-body">' +
        '<h5 class="media-heading">' + notification.user + ' added you to his friends.</h5>' +
        '<small class="text-muted">' + timeMessage + '</small>' +
        '</div>' +
        '</div>' +
        '</a>'
    );

    element.click(
        function()
        {
            window.location = gBaseUrl + 'pages/profile/profile.php?id=' + notification.id_user_sent;
        }
    );

    return element;
}

function createCircleNotifications(results)
{
    var unseenCount = 0;

    $.each(
        results,
        function(i, result)
        {
            var timeMessage = calculateTimeString(result.date);

            var element;
            if(result.type == "LOVE_MATCH")
                element = createLoveMatchNotification(timeMessage);
            else if(result.type == "FRIEND")
                element = createFriendNotification(result, timeMessage);
            else
                return;

            if(result.seen == false)
                unseenCount++;

            $("#relationships_notifications_list_group").prepend(element);
        }
    );

    updateCirclesBadge(unseenCount);
}

function getNewCircleNotifications(lastId)
{
    var t;

    $.ajax(
        {
            url: gBaseUrl + 'api/notifications/get_new_circles_notifications.php',
            type: 'GET',
            data:
            {
                last_id: lastId
            },
            dataType: 'json',
            success:
                function (payload)
                {
                    clearInterval( t );

                    if( payload.status == 'results' || payload.status == 'no-results')
                    {
                        lastId = payload.last_id;
                        gLastCirclesId = lastId;

                        if(payload.status == 'results')
                        {
                            createCircleNotifications((payload.results));
                            lastId = payload.results[payload.results.length - 1].id;
                            gLastCirclesId = lastId;
                        }

                        t = setTimeout(
                            function()
                            {
                                getNewCircleNotifications(lastId);
                            },
                            5000
                        );
                    }
                },
            error:
                function()
                {
                    clearInterval(t);
                    t = setTimeout(
                        function()
                        {
                            getNewCircleNotifications(lastId);
                        },
                        15000
                    );
                }
        }
    );
}

function getOldCircleNotifications(offset, limit)
{
    var t;

    $.ajax({
        url: gBaseUrl + 'api/notifications/get_old_circles_notifications.php',
        type: 'GET',
        data:
        {
            offset: offset,
            limit: limit
        },
        dataType: 'json',
        success:
            function( payload )
            {
                clearInterval( t );

                if(payload.length > 0)
                {
                    var lastId = payload[payload.length - 1].id;
                    createCircleNotifications(payload);
                    getNewCircleNotifications(lastId);
                }
                else
                {
                    getNewCircleNotifications(0);
                }
            },
        error:
            function()
            {
                clearInterval(t);
                t = setTimeout(
                    function()
                    {
                        getOldCircleNotifications(offset, limit);
                    },
                    15000
                );
            }
        }
    );
}

function updateMessagesBadge(unseenCount)
{
    if(unseenCount == 0)
        unseenCount = "";

    $("span#message_badge").html(unseenCount);
}

function readMessagesNotifications(lastId)
{
    $.ajax(
        {
            url: gBaseUrl + "api/notifications/read_message_notifications.php",
            type: 'POST',
            data:
            {
                last_id: lastId
            },
            dataType: 'json'
        }
    );
}

function createMessageNotification(notification, timeMessage)
{
    var targetId = notification.id_user_sent;

    // If conversation already exists, update id:
    if(conversations.containsKey(targetId))
    {
        var conversation = conversations.getValue(targetId);
        if(conversation.lastNotificationID > notification.id)
            return;

        // Update with new notification:
        conversations.setValue(targetId, new Conversation(notification.id, notification.seen));
    }

    var conversationID = notification.conversation_id;
    var lastMessage = notification.last_message;
    var element = createMessageNotificationTemplate(conversationID, notification, timeMessage, lastMessage);

    if(!conversations.containsKey(targetId))
    {
        conversations.add(targetId, new Conversation(notification.id, notification.seen));
        $("#messages_notifications_list_group").prepend(element);
    }
    else
    {
        // Remove element:
        $("#conversation_" + conversationID).remove();
        $("#messages_notifications_list_group").prepend(element);
    }
}

function createMessageNotificationTemplate(conversationID, notification, timeMessage, lastMessage)
{
    var chatLink = gBaseUrl + 'pages/chat/conversation.php?id=' + conversationID;
    var photoLink = gBaseUrl + 'images/avatar/' + notification.link;
    var text = notification.user + ' said ' + lastMessage;

    var element = $(
        '<a id="conversation_' + conversationID + '" href="' + chatLink + '" class="list-group-item">' +
        '<div class="media">' +
        '<div class="pull-left">' +
        '<img class="media-object img-circle pull-left" src="' + photoLink + '" alt="user#1" width="40">' +
        '</div>' +
        '<div class="media-body">' +
        '<h5 class="media-heading">' + text + '</h5>' +
        '<small class="text-muted">' + timeMessage + '</small>' +
        '</div>' +
        '</div>' +
        '</a>'
    );

    return element;
}

function createMessageNotifications(results)
{
    var lastNotifications = new Dictionary();

    $.each(
        results,
        function(i, result)
        {
            var userSentId = result.id_user_sent;

            if(!lastNotifications.containsKey(userSentId))
            {
                lastNotifications.add(userSentId, result);
            }

            else if(result.id > lastNotifications.getValue(userSentId).id)
            {
                lastNotifications.setValue(userSentId, result);
            }
        }
    );

    $.each(
        lastNotifications.values,
        function(i, result)
        {
            if(result.type != "MESSAGE")
                return;

            var timeMessage = calculateTimeString(result.date);
            createMessageNotification(result, timeMessage);
        }
    );

    // Remove old messages notifications:
    removeOldMessages(lastNotifications);

    // Update messages badge:
    var unseenCount = 0;
    for(var i = 0; i < conversations.values.length; i++)
        if(!conversations.values[i].seen)
            unseenCount++;

    updateMessagesBadge(unseenCount);

}

function removeOldMessages(lastNotifications)
{
    $.each(
        lastNotifications.values,
        function(i, result)
        {
            if(result.type != "MESSAGE")
                return;

            // Remove all but the last:
            var lastId = result.id - 1;

            var userSentId = result.id_user_sent;

            $.ajax(
                {
                    url: gBaseUrl + 'api/notifications/delete_message_notifications.php',
                    type: 'POST',
                    data:
                    {
                        last_id: lastId,
                        user_sent_id: userSentId
                    },
                    dataType: 'json'
                }
            )
        }
    );
}

function getNewMessageNotifications(lastId)
{
    var t;

    $.ajax(
        {
            url: gBaseUrl + 'api/notifications/get_new_messages_notifications.php',
            type: 'GET',
            data:
            {
                last_id: lastId
            },
            dataType: 'json',
            success:
                function (payload)
                {
                    clearInterval( t );

                    if( payload.status == 'results' || payload.status == 'no-results')
                    {
                        lastId = payload.last_id;
                        gLastMessageId = lastId;

                        if(payload.status == 'results')
                        {
                            createMessageNotifications(payload.results);
                            lastId = payload.results[payload.results.length - 1].id;
                            gLastMessageId = lastId;
                        }

                        t = setTimeout(
                            function()
                            {
                                getNewMessageNotifications(lastId);
                            },
                            5000
                        );
                    }
                },
            error:
                function()
                {
                    clearInterval(t);
                    t = setTimeout(
                        function()
                        {
                            getNewMessageNotifications(lastId);
                        },
                        15000
                    );
                }
        }
    );
}

function getOldMessageNotifications(offset, limit)
{
    var t;

    $.ajax(
        {
            url: gBaseUrl + 'api/notifications/get_old_messages_notifications.php',
            type: 'GET',
            data:
            {
                offset: offset,
                limit: limit
            },
            dataType: 'json',
            success:
                function( payload )
                {
                    clearInterval( t );

                    if(payload.length > 0)
                    {
                        var lastId = payload[payload.length - 1].id;
                        createMessageNotifications(payload);
                        getNewMessageNotifications(lastId);
                    }
                    else
                    {
                        getNewMessageNotifications(0);
                    }
                },
            error:
                function()
                {
                    clearInterval(t);
                    t = setTimeout(
                        function()
                        {
                            getOldMessageNotifications(offset, limit);
                        },
                        15000
                    );
                }
        }
    );
}

$(document).ready(setUp);