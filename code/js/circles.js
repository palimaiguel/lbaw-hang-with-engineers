var removeOrigin;

function setUp()
{
    $("a.remove").click(
        function()
        {
            removeOrigin = $(this);
            var circleType = removeOrigin.closest(".tab-pane").attr('id');
            var idUser = removeOrigin.attr('data-idUser');

            console.log(circleType);
            console.log(idUser);

            $.post("../api/circles/remove_from_circle.php", {"type": circleType, "idUser":idUser},
                function(data, textStatus, jqXHR)
                {
                    console.log("Received: " + jqXHR.status);

                    if (jqXHR.status === 200)
                        removeOrigin.closest('.person').remove();

                    else
                        appendError("Error: invalid user remove. Code: " + jqXHR.status);
                });

            return false;
        }
    )
}

function appendError(msg)
{
    $("div.page-header").after("<div class=\"row\"> <div class=\"col-lg-12 text-center\"><h3 class=\"message error\"><i class=\"fa fa-exclamation-triangle color\"></i>"
    + msg + "</h3> </div></div>");
}


$(document).ready(setUp);