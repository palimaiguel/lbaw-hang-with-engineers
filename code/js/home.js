function setUp()
{
    $("input#name").on("propertychange change click keyup input paste", function()
        {
            if ($(this).val().length > 0)
                $(this).eq(0).css("background-color","#E0FFD6");
            else
                $(this).eq(0).css("background-color","#FFB2B2");
        }
    );

    $("#datepicker").on("propertychange change click keyup input paste", function()
        {
            var reg = /^\d{4}\-\d{2}\-\d{2}$/;

            if (reg.test($("input#birthdate").eq(0).val()))
                $("input#birthdate").eq(0).css("background-color","#E0FFD6");
            else
                $("input#birthdate").eq(0).css("background-color","#FFB2B2");
        }
    );

    $("input#email_reg").on("propertychange change click keyup input paste", function()
        {
            var reg = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

            if (reg.test($(this).val()))
                $(this).eq(0).css("background-color","#E0FFD6");
            else
                $(this).eq(0).css("background-color","#FFB2B2");
        }
    );

    $("input#password_first").on("propertychange change click keyup input paste", function()
        {
            var original_pwd = $("input#password_first").eq(0).val();
            var repeated_pwd = $("input#password_repeated").eq(0).val();

            if (original_pwd.length < 8)
                $("input#password_first").eq(0).css("background-color","#FFB2B2");
            else
                $("input#password_first").eq(0).css("background-color","#E0FFD6");

            if (original_pwd != repeated_pwd)
                $("input#password_repeated").eq(0).css("background-color","#FFB2B2");
            else
                $("input#password_repeated").eq(0).css("background-color","#E0FFD6");
        }
    );

    $("input#password_repeated").on("propertychange change click keyup input paste", function()
        {
            var original_pwd = $("input#password_first").eq(0).val();
            var repeated_pwd = $("input#password_repeated").eq(0).val();

            if (original_pwd != repeated_pwd)
                $("input#password_repeated").eq(0).css("background-color","#FFB2B2");
            else
                $("input#password_repeated").eq(0).css("background-color","#E0FFD6");
        }
    );

}


$(document).ready(setUp);