var gResultsPerPage = 10;
var gBaseUrl = window.location.origin + "/~lbaw1412/final/";
var nextPage = 1;
var alreadyLoading = false;
var groupPublications = 1; //1 - global, 2 - friends, 3 - love_matches

$(document).ready(

    function(e)
    {
        $(window).scroll(function() {
            if ($('body').height() <= ($(window).height() + $(window).scrollTop())) {
                if (alreadyLoading == false) {
                    alreadyLoading = true;
                    searchUsers(groupPublications, gResultsPerPage, gResultsPerPage*nextPage);
                    alreadyLoading = false;
                    nextPage++;
                }

            }
        });

        $("[href='#home']").click( function() {
            groupPublications = 1;
        });

        $("[href='#friends']").click( function() {
            groupPublications = 2;
        });

        $("[href='#love_matches']").click( function() {
            groupPublications = 3;
        });
    }
)

function searchUsers(group_publications, limit, offset)
{
    $.ajax(
        {
            url: gBaseUrl + 'api/dashboard/dashboardMorePublications.php',
            method: 'GET',
            data: 'group=' + group_publications + '&limit=' + limit + '&offset=' + offset
        }
    ).done(
        function(dataJson)
        {
            //get the publications selector
            var parent_str = '#home';
            var class_str = ' .col-md-12';
            if(group_publications == 2)
                parent_str = '#friends';
            else if(group_publications == 3)
                parent_str = '#love_matches';

            var publication_list = $(parent_str + class_str);

            //debug
            //alert(JSON.stringify(dataJson));
            var data = JSON.parse(dataJson);
            var publications = data["publications"];

            for(var i = 0; i < publications.length; i++)
            {

                var publication = publications[i];

                var photo = publication["user_photo"];
                if(photo === null) {
                    photo = "default.jpg";
                }
                var photo = gBaseUrl + "images/avatar/" + photo;
                var id_user = publication["id_user"];
                var username = publication["username"];
                var time_stamp = publication["time_stamp"];
                var description = publication["description"];


                var html = "" +
                    "<div class=\"panel panel-default\"> " +
                        "<div class=\"panel-heading\">" +
                            "<img src=\" " + photo + "\" class=\"img-circle\" style=\"float:left; margin-right:1em;\"/>" +

                            "<span>" +
                                "<a href=\"" + gBaseUrl + "pages/profile/profile.php?id=" + id_user + "\"> " + username + "</a>" +
                                "<p> " + time_stamp + "</p>" +
                            "</span>" +
                        "</div>" +

                        "<div class=\"panel-body\">" +
                            description +
                        "</div>" +
                    "</div>";

                publication_list.append(html);
            }
        }
    ).fail(
        function()
        {

        }
    );
}
