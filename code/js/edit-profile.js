function setUp()
{
    $("input[name='element']").change(function(){
        // remove all link/photo form elements
        var form_extra = $(".form-group#extra");
        form_extra.empty();

        if ($(this).val() === "link")
        {
            form_extra.append("<input type=\"url\" class=\"form-control\" name=\"link\">");
        }
        else if ($(this).val() === "photo")
        {
            form_extra.append("<input type=\"file\" class=\"form-control\" name=\"photo_pub\"> Only JPG/PNG (max. 2MB)");
        }
    });
}

$(document).ready(setUp);