$( document ).ready(function() {
    var loc = window.location.pathname;
    var nav_element = $('a[href="' + loc + '"]');
    nav_element.parent().addClass('active');
    nav_element.parents('.has-submenu').addClass('active');
});