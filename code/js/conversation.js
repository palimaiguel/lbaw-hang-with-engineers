var gResultsPerPage = 10;
var gBaseUrl = window.location.origin + "/~lbaw1412/final/";
var lastIdOldMessage = 0;
var alreadyLoading = false;
var convo_id;

$( document ).ready(

    function(e) {

        //start at the bottom
        window.scrollTo(0, document.body.clientHeight);

        convo_id = window.location.search.replace("?", "").split("=")[1];

        function messages_longpolling( lastId ){
            var t;

            if( typeof lastId == 'undefined' ) {
                lastId = getLastIdSeen();
            }

            $.ajax({
                url: '../../api/chat/get_messages.php',
                type: 'GET',
                data:
                {
                    lastId: lastId,
                    conversation: convo_id
                },
                dataType: 'json',
                success: function( payload ){
                    clearInterval( t );
                    if( payload.status == 'results' || payload.status == 'no-results'){
                        t=setTimeout( function(){
                            messages_longpolling( payload.lastId );
                        }, 1000 );
                        if( payload.status == 'results' ){
                            $.each( payload.data, function(i,msg){
                                if( $('#' + msg.id).size() == 0 ){
                                    var pos;
                                    if(msg.id_emitter == payload.logged_user)
                                        pos = "right";
                                    else
                                        pos = "left";

                                    var photo = msg.user_photo;
                                    if(photo === null) {
                                        photo = "default.jpg";
                                    }
                                    $('.messages').append("<li class=\"media\" id=\"" + msg.id_message + "\"><a href=\"../profile/profile.php?id=" + msg.id_emitter + "\" class=\"pull-" + pos + "\"><img alt=\"user\" class=\"media-object\" src=\"../../images/avatar/" + photo + "\"></a><div class=\"media-body\"><h5 class=\"media-heading\"><strong>" + msg.username + "</strong></h5><p class=\"text-muted no-margn\">" + msg.content + "</p></div></li>");
                                }
                            });
                            window.scrollTo(0, document.body.clientHeight);
                        }
                    }
                },
                error: function(){
                    clearInterval( t );
                    t=setTimeout( function( ){
                        messages_longpolling( lastId );
                    }, 15000 );
                }
            });
            lastId = getLastIdSeen();
        }

        $("#send").submit(function( event ) {
            var msg = $('#send input').val();
            $.ajax({
                url: '../../api/chat/send_message.php',
                type: 'POST',
                data:
                {
                    content: msg,
                    conversation: convo_id
                },
                dataType: 'json',
                success: function( payload ){

                },
                error: function(){

                }
            });
            event.preventDefault();
            $('#send input').val('');
        });

        messages_longpolling();// refresh messages

        // on scroll up, load older messages
        $(window).scroll(function() {
            if ($(window).scrollTop() == 0) {
                lastIdOldMessage = getFirstIdSeen();

                if (alreadyLoading == false) {
                    alreadyLoading = true;
                    getOldMessages(lastIdOldMessage);
                    alreadyLoading = false;
                    nextPage++;
                }
            }
        });
    }

);

function getOldMessages(lastIdOldMessage)
{
    $.ajax({
        url: '../../api/chat/get_old_messages.php',
        type: 'GET',
        data:
        {
            lastId: lastIdOldMessage,
            conversation: convo_id
        },
        dataType: 'json',
        success: function( payload ){
            if( payload.status == 'results' ){
                $.each( payload.data, function(i,msg){
                    if( $('#' + msg.id).size() == 0 ){
                        var pos;
                        if(msg.id_emitter == payload.logged_user)
                            pos = "right";
                        else
                            pos = "left";

                        var photo = msg.user_photo;
                        if(photo === null) {
                            photo = "default.jpg";
                        }
                        $('.messages').prepend("<li class=\"media\" id=\"" + msg.id_message + "\"><a href=\"../profile/profile.php?id=" + msg.id_emitter + "\" class=\"pull-" + pos + "\"><img alt=\"user\" class=\"media-object\" src=\"../../images/avatar/" + photo+ "\"></a><div class=\"media-body\"><h5 class=\"media-heading\"><strong>" + msg.username + "</strong></h5><p class=\"text-muted no-margn\">" + msg.content + "</p></div></li>");
                    }
                });
            }
            if( payload.status == 'error' ){
                alert('We got confused, Please refresh the page!');
            }
        },
        error: function(){
        }
    });

}

function getLastIdSeen()
{
    var result = $('li.media:last').attr('id');
    if (typeof result === 'undefined')
        return 0;
    else return result;
}

function getFirstIdSeen()
{
    var result = $('li.media:first').attr('id');
    if (typeof result === 'undefined')
        return 0;
    else return result;
}