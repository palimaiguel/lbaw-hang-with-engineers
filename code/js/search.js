var gResultsPerPage = 5;
var gBaseUrl = window.location.origin + "/~lbaw1412/final/";
var gCurrentPage = 1;
var gCurrentUserCount;
var gCurrentKeyword;
var gCurrentPagesCount;

$(document).ready(
    function(e)
    {
        $('button#search_button_id').click(submit);

        $('#search_input_id').keydown(function (event) {
            var keypressed = event.keyCode || event.which;
            if (keypressed == 13) {
                submit();
            }
        });
    }
)

function submit()
{
    var keyword = $('input#search_input_id[name="keyword"]').val();
    var stripped_keyword = keyword.replace(/<\/?[^>]+(>|$)/g,"");
    search(stripped_keyword);
}

function search(keyword)
{
    gCurrentKeyword = keyword;
    searchUserCount(keyword);
    searchUsers(keyword, gResultsPerPage, 0);
}

function changePageButtonClick(page)
{
    if(gCurrentPage != page && page > 0 && page <= gCurrentPagesCount)
    {
        gCurrentPage = page;
        searchUsers(gCurrentKeyword, gResultsPerPage, (page - 1) * gResultsPerPage);
    }
}

function previousPageButtonClick()
{
    changePageButtonClick(gCurrentPage - 1);
}

function nextPageButtonClick()
{
    changePageButtonClick(gCurrentPage + 1);
}

function searchUserCount(keyword)
{
    $.ajax(
        {
            url: gBaseUrl + 'api/search/searchUserCount.php',
            method: 'GET',
            data: 'keyword=' + keyword
        }
    ).done(
        function(data)
        {
            var jsonObject = JSON.parse(data);
            var userCount = jsonObject["user_count"];
            gCurrentUserCount = userCount;

            var pagesCount = userCount % 5 == 0 ? userCount / 5 : userCount / 5 + 1;

            gCurrentPagesCount = pagesCount;
            createPagination(pagesCount);

            var searchResultsHeader = $('p#searchResultsHeader');
            searchResultsHeader.empty();
            searchResultsHeader.append('<strong>' + userCount + '</strong> results found for: <strong>' + keyword + '</strong>');

            var numberResultsSpan = $('span#numberResultsSpan');
            numberResultsSpan.empty();
            numberResultsSpan.append(userCount);
        }
    ).fail(
        function()
        {

        }
    );
}

function createPagination(count)
{
    var pagination = $("ul#pagination_id");
    pagination.empty();

    pagination.append("<li><a href=\"#users\" onClick=\"previousPageButtonClick()\">&laquo;</a></li>");

    for(var i = 1; i <= count; i++)
    {
        pagination.append("<li><a href=\"#users\" onClick=\"changePageButtonClick(" + i + ")\">" + i + "</a></li>");
    }

    pagination.append("<li><a href=\"#users\" onClick=\"nextPageButtonClick()\">&raquo;</a></li>");
}

function searchUsers(keyword, limit, offset)
{
    $.ajax(
        {
            url: gBaseUrl + 'api/search/searchUser.php',
            method: 'GET',
            data: 'keyword=' + keyword + '&limit=' + limit + '&offset=' + offset
        }
    ).done(
        function(data)
        {
            var parent = $("div#users_panel");
            parent.empty();

            createUsersHtml(parent, data);
        }
    ).fail(
        function()
        {

        }
    );
}

function createUsersHtml(parent, data)
{
    var jsonObject = JSON.parse(data);
    var keyword = jsonObject["keyword"];
    var baseProfileUrl = jsonObject["base_profile_url"];
    var usersJsonArray = jsonObject["auth"];
    var userCount = jsonObject["user_count"];

    for(var i = 0; i < userCount; i++)
    {
        var userJson = usersJsonArray[i];
        var userID = userJson["user_id"];
        var username = userJson["username"];
        var countryName = userJson["country_name"];
        var engineer = userJson["engineer"];
        var photoLink = userJson["photo_link"];


        var user = createUser(baseProfileUrl, userID, username, countryName, photoLink, engineer);

        parent.append(user);
        parent.append(document.createElement("hr"));
    }
}

function createUser(baseProfileUrl, userID, username, countryName, photoLink, engineer)
{
    var photo = createPhoto(baseProfileUrl, userID, photoLink);
    var description = createDescription(baseProfileUrl, userID, username, engineer, countryName)

    var li = document.createElement("li");
    li.className = "media";
    li.appendChild(photo);
    li.appendChild(description);

    var ul = document.createElement("ul");
    ul.className = "media-list messages nicescroll";
    ul.style = "margin-bottom: 0;max-height: none;";
    ul.appendChild(li);

    return ul;
};

function createPhoto(baseProfileUrl, userID, photoLink)
{
    if(photoLink === null) {
        photoLink = "default.jpg";
    }

    // Create image:
    var image = document.createElement("img");
    image.alt = "user";
    image.className = "media-object";
    image.src = gBaseUrl + "images/avatar/" + photoLink;

    // Create anchor:
    var anchor = document.createElement("a");
    anchor.href = baseProfileUrl + userID;
    anchor.className = "pull-left";
    anchor.appendChild(image);

    return anchor;
}

function createDescription(baseProfileUrl, userID, username, engineer, country_name)
{
    var anchor = document.createElement("a");
    anchor.href = baseProfileUrl + userID;
    anchor.textContent = username;

    var h5 = document.createElement("h5");
    h5.className = "media-heading";
    h5.appendChild(anchor);

    if(engineer == true)
    {
        var node = document.createTextNode(" - ");
        var i = document.createElement("i");
        i.className = "fa fa-gear";
        var node2 = document.createTextNode(" Engineer");

        h5.appendChild(node);
        h5.appendChild(i);
        h5.appendChild(node2);
    }

    var p = document.createElement("p");
    p.className = "text-muted no-margn";
    p.textContent = country_name;

    var div = document.createElement("div");
    div.className = "media-body";
    div.appendChild(h5);
    div.appendChild(p);

    return div;
}