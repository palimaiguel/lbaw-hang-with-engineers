<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Hang with Engineers</title>

    <link rel="stylesheet" href="{$BASE_URL}css/bootstrap/bootstrap.css" />
    <link rel="stylesheet" href="{$BASE_URL}css/app/timeline.css" />
    <link rel="stylesheet" href="{$BASE_URL}css/my-carousel.css" />
    <link rel="stylesheet" href="{$BASE_URL}css/switch-buttons/switch-buttons.css" />
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{$BASE_URL}css/app/app.v1.css" />
    <link rel="stylesheet" href="{$BASE_URL}css/messages.css" />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="{$BASE_URL}js/jquery/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="{$BASE_URL}js/plugins/underscore/underscore-min.js"></script>
    <script src="{$BASE_URL}js/bootstrap/bootstrap.min.js"></script>
    <script src="{$BASE_URL}js/globalize/globalize.min.js"></script>
    <script src="{$BASE_URL}js/plugins/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="{$BASE_URL}js/app/custom.js" type="text/javascript"></script>
    <script src="{$BASE_URL}js/notifications.js" type="text/javascript"></script>
    <script src="{$BASE_URL}js/nav.js" type="text/javascript"></script>

</head>
<body data-ng-app>

<div class="loading-container">
    <div class="loading">
        <div class="l1">
            <div></div>
        </div>
        <div class="l2">
            <div></div>
        </div>
        <div class="l3">
            <div></div>
        </div>
        <div class="l4">
            <div></div>
        </div>
    </div>
</div>

<aside class="left-panel"  style="padding-top: 0.5em;">
    <div class="user text-center">
        <h4 class="user-name" style="padding-bottom: 1em;"><img src="{$BASE_URL}images/lamp.png" style="width: 4rem; border:0;">Hang with Engineers</h4>
        <a href="{$BASE_URL}pages/profile/profile.php?id={$logged_user}"><img src="{$BASE_URL}images/avatar/{$logged_user_photo_id}" class="img-circle" alt="My photo"></a>
        <h4 class="user-name">{$logged_username}</h4>

        <div class="dropdown user-login">
            <a class="btn btn-xs btn-rounded" href="{$BASE_URL}actions/auth/logout.php">Logout</a>
        </div>
    </div>



    <nav class="navigation">
        <ul class="list-unstyled">
            <li><a href="{$BASE_URL}pages/dashboard.php"><i class="fa fa-home"></i><span class="nav-label">Dashboard</span></a></li>
            <li><a href="{$BASE_URL}pages/chat/conversations.php"><i class="fa fa-comment-o"></i> <span class="nav-label">Messages</span></a></li>
            <li class="has-submenu"><a><i class="fa fa-user"></i> <span class="nav-label">My profile</span></a>
                <ul class="list-unstyled">
                    <li><a href="{$BASE_URL}pages/profile/profile.php?id={$logged_user}">View</a></li>
                    <li><a href="{$BASE_URL}pages/profile/edit_profile.php">Edit</a></li>
                </ul>
            </li>
            <li><a href="{$BASE_URL}pages/circles.php"><i class="fa fa-circle-o"></i> <span class="nav-label">My circles</span></a></li>
            <li class="has-submenu"><a><i class="fa fa-heart-o"></i> <span class="nav-label">Meet people</span></a>
                <ul class="list-unstyled">
                    <li><a href="{$BASE_URL}pages/search.php">Find people</a></li>
                    <li><a href="{$BASE_URL}pages/match.php">Match suggestions (coming soon)</a></li>
                    <li><a href="{$BASE_URL}pages/showdown.php">Matchmaking Showdown (coming soon)</a></li>
                    <li><a href="{$BASE_URL}pages/dategen.php">Love Date Generator (coming soon)</a></li>
                </ul>
            </li>
        </ul>
    </nav>

</aside>


<section class="content">
    <header class="top-head container-fluid">
        <button type="button" class="navbar-toggle pull-left">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        <form role="search" class="navbar-left app-search pull-left hidden-xs" action="{$BASE_URL}pages/search.php" method="post">
            <input type="text" placeholder="Search" class="form-control form-control-circle" name="keyword">
        </form>

        <nav class=" navbar-default hidden-xs" role="navigation">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                </li>
            </ul>
        </nav>

        <ul class="nav-toolbar">
            <li class="dropdown messages_notifications"><a href="#" data-toggle="dropdown"><i class="fa fa-comments-o"></i> <span id="message_badge" class="badge bg-warning"></span></a>
                <div class="dropdown-menu md arrow pull-right panel panel-default arrow-top-right messages-dropdown">
                    <div class="panel-heading">
                        Messages
                    </div>

                    <div id="messages_notifications_list_group" class="list-group">
                        <a href="{$BASE_URL}pages/chat/conversations.php" class="btn btn-info btn-flat btn-block">View All Messages</a>
                    </div>

                </div>
            </li>
            <li class="dropdown relationships_notifications"><a href="#" data-toggle="dropdown"><i class="fa fa-heart-o"></i><span id="notification_badge" class="badge"></span></a>
                <div class="dropdown-menu arrow pull-right md panel panel-default arrow-top-right notifications">
                    <div class="panel-heading">
                        Recent notifications
                    </div>
                    <div id="relationships_notifications_list_group" class="list-group">
                    </div>
                </div>
            </li>
            <li class="dropdown relationships_notifications"><a href="{$BASE_URL}pages/help.php"><i class="fa fa-question"></i><span class="badge"></span></a></li>
        </ul>
    </header>