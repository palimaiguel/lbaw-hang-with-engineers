<div class="warper container-fluid">

    <div class="page-header"><h1>Help Center<small> Questions & answers and other tips.</small></h1></div>

    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        Dashboard
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <ul>
                        <li>Scroll down to find all the publications from other users.</li>
                        <li>You can choose to see only friends or love matches publications, by switch tabs.</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                        Messages
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <ul>
                        <li>Pick the conversation you want to open and exchange messages with other users.</li>
                        <li>Scroll up during a conversation to see older messages.</li>
                        <li>Get to know other users and stay in touch with your friends.</li>
                        <li>All conversations are private, only its members can send messages or see them.</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                        My profile
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <ul>
                        <li>Present yourself to other users.</li>
                        <li>Add new publications to your timeline with images or urls.</li>
                        <li>Edit your information and upload some nice photos of you.</li>
                        <li>You can choose the visibility for your "about me" tab and for each photo and publication.</li>
                        <li>Private (not global) publications will not be displayed in the dashboard.</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                        My circles
                    </a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                    <ul>
                        <li>Add other users to circles: friends if you're interested in following them or love matches for something more.</li>
                        <li>When you add someone to your friends circle they will be notificated. </li>
                        <li>The same occurs if you add someone to your love matches, but in this case your identity will be held secret to them.</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                        Meet people
                    </a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body">
                    <ul>
                        <li>Meet other users :) </li>
                        <li>Using search you can find users by their username or by keywords they have in their profile.</li>
                        <li>Enjoy and start meeting new people today.</li>
                        <li>See their profiles and add them to your friends and love matches circles. </li>
                        <li>Soon we will provide some other features to enhance your experience on our website, such as "the Date Generator". </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                        More help
                    </a>
                </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse">
                <div class="panel-body">
                    For more help please contact our friendly and helpful admins:
                    <ul>
                    {foreach $admins as $admin}
                        <li><a href="{$BASE_URL}pages/profile/profile.php?id={$admin.id}">{$admin.username}</a></li>
                        {/foreach}
                    </ul>
                </div>
            </div>
        </div>

    </div>
</div>

