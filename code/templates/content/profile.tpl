    <link rel="stylesheet" href="{$BASE_URL}css/profile.css" />

    <div class="warper container-fluid">
        {if $banned}
        <div class="page-header"><h1>This user has been banned!</h1></div>
        {else}
        <div class="page-header"><h1>{$user.username}'s profile</h1>

            {foreach $ERROR_MESSAGES as $message}
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h3 class="message error"><i class="fa fa-exclamation-triangle color"></i> {$message}</h3>
                    </div>
                </div>
            {/foreach}

            {foreach $SUCCESS_MESSAGES as $message}
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h3 class="message success"><i class="fa fa-check color"></i> {$message}</h3>
                    </div>
                </div>
            {/foreach}

            <ul class="media-list messages nicescroll" style="margin-bottom: 0;max-height: none;">
                <li class="media">
                    <a href="#" class="pull-left">
                        <img alt="user" class="media-object" src="{$BASE_URL}images/avatar/{$user.link}">
                    </a>
                    <div class="media-body">
                        <h5 class="media-heading"><strong>{$user.username}</strong></h5>
                        <p class="text-muted no-margn">{$about_me.city}{if isset($about_me.city) and isset($user.country) and ($about_me.city neq "") and ($user.country neq "")},{/if} {$user.country}</p>
                        {if $user.engineer}<p class="text-muted no-margn"><i class="fa fa-gear"></i> Engineer{if $profile_admin} / <i class="fa fa-user-secret"></i> Admin{/if}</p>{/if}
                    </div>
                </li>


                <ul role="tablist" class="nav nav-tabs" id="myTab">
                    <li class="active"><a data-toggle="tab" role="tab" href="#timeline"><i class="fa fa-clock-o"></i> Timeline</a></li>
                    <li><a data-toggle="tab" role="tab" href="#about"><i class="fa fa-user"></i> About me</a></li>
                    <li><a data-toggle="tab" role="tab" href="#photos"><i class="fa fa-camera"></i> Photos</a></li>
                    {if not $own_profile}
                    <li><a role="tab" href="../../actions/profile/send_message.php?id={$user.id}"> <i class="fa fa-envelope"></i> Send message</a></li>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" id="myTabDrop1" href="#"><i class="fa fa-plus"></i> Add to circles <span class="caret"></span></a>
                        <ul aria-labelledby="myTabDrop1" role="menu" class="dropdown-menu">
                            <li><a role="tab" tabindex="-1" href="../../actions/profile/add_to_circles.php?id={$user.id}&type=FRIEND">Friends</a></li>
                            <li><a role="tab" tabindex="-1" href="../../actions/profile/add_to_circles.php?id={$user.id}&type=LOVE_MATCH">Love matches</a></li>
                        </ul>
                    </li>
                    {/if}
                    {if $admin}<li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" id="adminOptions" href="#"><i class="fa fa-ban"></i> Admin options <span class="caret"></span></a>
                        <ul aria-labelledby="myTabDrop1" role="menu" class="dropdown-menu">
                            <li><a role="tab" id="ban" tabindex="-1">Ban user</a></li>
                        </ul>
                    </li>
                    {/if}
                </ul>
                <div class="tab-content" id="myTabContent">
                    {if $admin}
                    <div id="ban-warning" role="alert" class="alert alert-danger fade in" style="display: none;">
                        <h4>Are you sure you want to ban this user?</h4>
                        <p>Be aware that this action is irreversible!</p>
                        <p>
                            <a id="ban-confirm" href="../../actions/profile/ban_user.php?id={$user.id}" class="btn btn-danger" type="button">Yes, I'm sure</a>
                            <button class="btn btn-danger" onclick="$('#ban-warning').css('display','none');" type="button">No, I want to cancel</button>
                        </p>
                    </div>
                    {/if}
                    <div id="timeline" class="tab-pane tabs-up fade in active panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <ul class="timeline list-unstyled">
                                        {foreach $timeline as $publication}
                                            <li class="clearfix">
                                                <time class="tl-time">
                                                    <h3 class="text-purple">{$publication.date}</h3>
                                                    <p>{$publication.time}</p>
                                                </time>
                                                <i class="fa fa-comments-o bg-purple tl-icon text-white"></i>
                                                <div class="tl-content">
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading">{$publication.title}</div>
                                                        <div class="panel-body">
                                                            {$publication.content}
                                                            {if $publication.publication_type eq 'LINK'}
                                                                <p class="bold">Attached link:</p>
                                                                <a href="{$publication.publication_link}">{$publication.publication_link}</a>
                                                            {/if}
                                                            {if $publication.publication_type eq 'PHOTO'}
                                                                <p class="bold">Attached photo:</p>
                                                                <img alt="element photo" src="{$BASE_URL}images/publications/{$publication.publication_link}" />
                                                            {/if}
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        {/foreach}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="about" class="tab-pane tabs-up fade panel panel-default">
                        <div class="panel-body">
                            {if !empty($about_me)}
                            <p class="bold underline">Personal info</p>
                            <p><strong class="bold">Age:</strong> {$about_me.age}</p>
                            <p><strong class="bold">Gender:</strong> {if $about_me.gender=='F'}Female{elseif $about_me.gender=='M'}Male{elseif $about_me.gender=='O'}Other{/if}</p>
                            <p><strong class="bold">Sexual orientation:</strong> {if $about_me.sexual_orientation=='H'}Heterossexual{elseif $about_me.sexual_orientation=='G'}Homosexual{elseif $about_me.sexual_orientation=='B'}Bissexual{/if}</p>
                            <p><strong class="bold">Job:</strong> {$about_me.job}</p>
                            <p><strong class="bold">Education:</strong> {$about_me.course}</p>
                            <p><strong class="bold">School:</strong> {$about_me.university}</p>

                            <p class="bold underline">Personal info</p>
                            <p><strong class="bold">Music:</strong> {$about_me.musics}</p>
                            <p><strong class="bold">Movies:</strong> {$about_me.movies}</p>
                            <p><strong class="bold">Books:</strong> {$about_me.books}</p>
                            <p><strong class="bold">More information:</strong> {$about_me.others}</p>
                            {else}
                                <p>This user has this section hidden!</p>
                            {/if}
                        </div>
                    </div>
                    <div id="photos" class="tab-pane tabs-up fade panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3"></div>
                                <div class="col-md-12">
                                    {if $n_photos > 0}
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="background-color: #E7E7E7">

                                        <ol class="carousel-indicators">
                                            {for $var=0 to $n_photos-1}
                                            <li data-target="#carousel-example-generic" data-slide-to="{$var}"></li>
                                            {/for}
                                        </ol>


                                        <div class="carousel-inner">
                                            {foreach $photos as $photo}
                                            <div class="item">
                                                <img src="../../images/users/{$photo.link}" alt="{$photo.name}" class="img-responsive center-block">
                                                <div class="carousel-caption">
                                                    <h3>{$photo.name}</h3>
                                                    <p>{$photo.description}</p>
                                                </div>
                                            </div>
                                            {/foreach}
                                        </div>

                                        <script>$('.carousel-inner .item:first-child').addClass("active");
                                            $('.carousel-indicators li:first-child').addClass("active");</script>


                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <i class="fa fa-chevron-left" ></i>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <i class="fa fa-chevron-right" ></i>
                                        </a>
                                    </div>
                                    {else}
                                        <p>This user has no visible photos!</p>
                                    {/if}
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </ul>
        </div>
        {/if}


        <script src="{$BASE_URL}js/profile.js"></script>
