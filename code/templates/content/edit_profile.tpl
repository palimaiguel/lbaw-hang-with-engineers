
    <link rel="stylesheet" href="{$BASE_URL}css/edit-profile.css" />

    <div class="warper container-fluid">

        <div class="page-header"><h1>{$user.username}'s profile</h1></div>

        {foreach $ERROR_MESSAGES as $message}
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3 class="message error"><i class="fa fa-exclamation-triangle color-green"></i> {$message}</h3>
                </div>
            </div>
        {/foreach}

        {foreach $SUCCESS_MESSAGES as $message}
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3 class="message success"><i class="fa fa-check color-red"></i> {$message}</h3>
                </div>
            </div>
        {/foreach}

        <ul class="media-list messages nicescroll" style="margin-bottom: 0;max-height: none;">
            <li class="media">
                <a href="#" class="pull-left">
                    <img alt="user" class="media-object" src="{$BASE_URL}images/avatar/{$user.link}">
                </a>
                <div class="media-body">
                    <h5 class="media-heading"><strong>{$user.username}</strong></h5>
                    <p class="text-muted no-margn">{$about_me.city}{if isset($about_me.city) and isset($user.country) and ($about_me.city neq "") and ($user.country neq "")},{/if} {$user.country}</p>
                    {if $user.engineer}<p class="text-muted no-margn"><i class="fa fa-gear"></i> Engineer{if $admin} / <i class="fa fa-user-secret"></i> Admin{/if}</p>{/if}
                </div>
            </li>


            <ul role="tablist" class="nav nav-tabs" id="myTab">
                <li class="active"><a data-toggle="tab" role="tab" href="#timeline"><i class="fa fa-clock-o"></i> Timeline</a></li>
                <li><a data-toggle="tab" role="tab" href="#about"><i class="fa fa-user"></i> About me</a></li>
                <li><a data-toggle="tab" role="tab" href="#photos"><i class="fa fa-camera"></i> Photos</a></li>
                <li><a data-toggle="tab" role="tab" href="#profile-pic"><i class="fa fa-picture-o"></i> Profile picture</a></li>
                <li><a data-toggle="tab" role="tab" href="#privacy"><i class="fa fa-eye-slash"></i> Privacy</a></li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div id="timeline" class="tab-pane tabs-up fade in active panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Create new publication</div>
                                    <div class="panel-body">

                                        <form role="form" name="new-publication" id="new-publication" action="{$BASE_URL}actions/profile/insert_publication.php" method="post" enctype="multipart/form-data" autocomplete="off">
                                            <div class="form-group">
                                                <label>Title
                                                <input type="text" name="title" class="form-control">
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label>Text
                                                <textarea name="text" rows="4" cols="50" class="form-control"></textarea>
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label>Add extra element<br/>
                                                <input type="radio" name="element" value="none" checked/>None
                                                <input type="radio" name="element" value="link"/>Link
                                                <input type="radio" name="element" value="photo"/>Photo
                                                </label>
                                            </div>
                                            <div class="form-group" id="extra">
                                            </div>
                                            <div class="form-group">
                                                <label>Visibility<p></p>
                                                <input type="radio" name="visibility" value="SELF"/>Only me
                                                <input type="radio" name="visibility" value="LOVE_MATCHES"/>Love Matches
                                                <input type="radio" name="visibility" value="FRIENDS"/>Friends
                                                <input type="radio" name="visibility" value="GLOBAL" checked />Global
                                                </label>
                                            </div>
                                            <div class="form-group no-margn">
                                                <button type="submit" class="btn btn-success btn-block" data-toggle="modal" data-target="#timeline-event">Add New Event</button>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-9">

                                <ul class="timeline list-unstyled">
                                    {foreach $timeline as $publication}
                                        <li class="clearfix">
                                            <time class="tl-time">
                                                <h3 class="text-purple">{$publication.date}</h3>
                                                <p>{$publication.time}</p>
                                            </time>
                                            <i class="fa fa-comments-o bg-purple tl-icon text-white"></i>
                                            <div class="tl-content">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">{$publication.title}</div>
                                                    <div class="panel-body">
                                                        {$publication.content}
                                                        {if $publication.publication_type eq 'LINK'}
                                                            <p><strong style="font-weight:bold;">Attached link:</strong></p>
                                                            <a href="{$publication.publication_link}">{$publication.publication_link}</a>
                                                        {/if}
                                                        {if $publication.publication_type eq 'PHOTO'}
                                                            <p><strong style="font-weight:bold;">Attached photo:</strong></p>
                                                            <img alt="element photo" src="{$BASE_URL}images/publications/{$publication.publication_link}" />
                                                        {/if}
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    {/foreach}
                                </ul>

                            </div>

                        </div>

                    </div>
                </div>
                <div id="about" class="tab-pane tabs-up fade panel panel-default">
                    <div class="panel-body">
                        <form role="form" name="edit-profile" id="edit-profile" action="{$BASE_DIR}../../actions/profile/edit_profile.php" method="post" >
                            <div class="form-group">
                                <label>Gender<br/>
                                <input type="radio" name="gender" value="M" {if $about_me.gender eq 'M'}checked{/if}/>Male
                                <input type="radio" name="gender" value="F" {if $about_me.gender eq 'F'}checked{/if}/>Female
                                <input type="radio" name="gender" value="O" {if $about_me.gender eq 'O'}checked{/if}/>Other
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Sexual orientation<br/>
                                <input type="radio" name="sexual_orientation" value="H" {if $about_me.sexual_orientation eq 'H'}checked{/if}/>Heterosexual
                                <input type="radio" name="sexual_orientation" value="B" {if $about_me.sexual_orientation eq 'B'}checked{/if}/>Bisexual
                                <input type="radio" name="sexual_orientation" value="G" {if $about_me.sexual_orientation eq 'G'}checked{/if}/>Homosexual
                                </label>
                            </div>
                            <div class="form-group">
                                <label>City
                                <input type="text" name="city" class="form-control" value="{$about_me.city}">
                                </label>
                            </div>
                            <div class="form-group">
                                <label for="location">Country
                                <select class="form-control" name="country">
                                    <option value="null">-</option>
                                    {foreach $countries as $country}
                                        <option value="{$country.id}" {if $country.id == $about_me.country}selected{/if}>{$country.name}</option>
                                    {/foreach}
                                </select>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Job
                                <input type="text" name="job" class="form-control" value="{$about_me.job}">
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Education
                                <input type="text" name="education" class="form-control" value="{$about_me.course}">
                                </label>
                            </div>
                            <div class="form-group">
                                <label>School
                                <input type="text" name="school" class="form-control" value="{$about_me.university}">
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Music
                                <input type="text" name="music" class="form-control" value="{$about_me.musics}">
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Movies
                                <input type="text" name="movies" class="form-control" value="{$about_me.movies}">
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Books
                                <input type="text" name="books" class="form-control" value="{$about_me.books}">
                                </label>
                            </div>
                            <div class="form-group">
                                <label>More information
                                <textarea name="others" rows="4" cols="50" class="form-control">{$about_me.others}</textarea>
                                </label>
                            </div>
                            <div class="form-group no-margn">
                                <button type="submit" class="btn btn-success">OK</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="photos" class="tab-pane tabs-up fade panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Upload a new photo</div>
                                    <div class="panel-body">
                                        <form role="form" method="post" action="{$BASE_URL}actions/profile/insert_new_photo.php" enctype="multipart/form-data" autocomplete="off">
                                            <div class="form-group">
                                                <label for="photo">
                                                    <input type="file" name="photo">
                                                    Choose file (only JPG/PNG - max size: 2MB)</label>
                                            </div>
                                            <div class="form-group">
                                                <label for="title">Title</label>
                                                <input type="text" class="form-control" name="title" placeholder="Photo title">
                                            </div>
                                            <div class="form-group">
                                                <label for="description">Photo description</label>
                                                <input type="text" class="form-control" name="description" placeholder="Description">
                                            </div>
                                            <div class="form-group">
                                                <label>Visibility</label><p></p>
                                                <input type="radio" name="visibility" value="SELF"/>Only me
                                                <input type="radio" name="visibility" value="LOVE_MATCHES"/>Love Matches
                                                <input type="radio" name="visibility" value="FRIENDS"/>Friends
                                                <input type="radio" name="visibility" value="GLOBAL" checked />Global
                                            </div>
                                            <button type="submit" class="btn btn-success">Submit</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Delete existing photos</div>
                                    <div class="panel-body">
                                        {if $n_photos > 0}
                                        <form role="form" method="post" action="{$BASE_URL}actions/profile/delete_photo.php">
                                            <div class="form-group">
                                                {foreach $photos as $photo}
                                                <div class="photo_item">
                                                    <input type="checkbox" name="photos[]" value="{$photo.id_photo}" class="photo_item" />{$photo.name}
                                                    <img src="{$BASE_URL}images/users_thumbs/{$photo.link}" alt="{$photo.name}" class="photo_item">
                                                </div>
                                                {/foreach}
                                            </div>
                                            <button type="submit" class="btn btn-success">Submit</button>
                                        </form>
                                        {else}
                                            <p>You currently have no photos!</p>
                                        {/if}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="profile-pic" class="tab-pane tabs-up fade panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Change your profile picture</div>
                                    <div class="panel-body">
                                        <form role="form" method="post" action="{$BASE_URL}actions/profile/change_profile_picture.php" enctype="multipart/form-data" autocomplete="off">
                                            <div class="form-group">
                                                    <label>Choose file (only JPG/PNG - max size: 2MB)</label>
                                                    <input type="file" name="photo">
                                            </div>
                                            <button type="submit" class="btn btn-success">Submit</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="privacy" class="tab-pane tabs-up fade panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Change your profile visibility</div>
                                    <div class="panel-body">
                                        <form role="form" name="profile-pic" id="profile-pic" action="{$BASE_DIR}../../actions/profile/set_visibility.php" method="post">
                                            <div class="form-group">
                                                <label>Visibility<p></p>
                                                <input type="radio" name="visibility" value="SELF" {if $user.visibility eq "SELF"}checked{/if}/>Only me
                                                <input type="radio" name="visibility" value="LOVE_MATCHES" {if $user.visibility eq "LOVE_MATCHES"}checked{/if}/>Love Matches
                                                <input type="radio" name="visibility" value="FRIENDS" {if $user.visibility eq "FRIENDS"}checked{/if}/>Friends
                                                <input type="radio" name="visibility" value="GLOBAL" {if $user.visibility eq "GLOBAL"}checked{/if}/>Global
                                                </label>
                                            </div>
                                            <button type="submit" class="btn btn-success">OK</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </ul>

    </div>


    <script src="{$BASE_URL}js/edit-profile.js"></script>