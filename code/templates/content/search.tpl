<div class="warper container-fluid">

    <div class="page-header"><h1>Search <small>Find users</small></h1></div>

    {foreach $ERROR_MESSAGES as $message}
        <div class="row">
            <div class="col-lg-12 text-center">
                <h3 class="message error"><i class="fa fa-exclamation-triangle color"></i> {$message}</h3>
            </div>
        </div>
    {/foreach}

    {foreach $SUCCESS_MESSAGES as $message}
        <div class="row">
            <div class="col-lg-12 text-center">
                <h3 class="message success"><i class="fa fa-check color"></i> {$message}</h3>
            </div>
        </div>
    {/foreach}

    <div class="row">
        <div class="col-lg-8">
            <p id="searchResultsHeader" class="form-control-static"></p>
        </div>
        <div class="col-lg-4">
            <div class="input-group">
                <input id="search_input_id" type="text" class="form-control" placeholder="Search Keywords" name="keyword">
                <div class="input-group-btn">
                    <button id="search_button_id" type="submit" value="Submit" class="btn btn-default">Search</button>
                </div>
            </div>
        </div>
    </div>


    <hr class="clean">



    <ul class="nav nav-tabs" role="tablist">

        <li role="presentation" class="active"><a href="#users" role="tab" data-toggle="tab">Users <span id="numberResultsSpan" class="badge bg-purple"></span></a></li>
    </ul>


    <div class="tab-content">
        <div role="tabpanel" class="panel panel-default tab-pane tabs-up active" id="users">
            <div id="users_panel" class="panel-body">

            </div>
        </div>
    </div>

    <nav>
        <ul id="pagination_id" class="pagination">

        </ul>
    </nav>

</div>


<script src="{$BASE_URL}js/search.js"></script>
{if $keyword != ""}
    <script>search("{$keyword}");</script>
{/if}
