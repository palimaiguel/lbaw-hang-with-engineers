    <div class="warper container-fluid">

        <div class="page-header"><h1>My Circles<small> View and remove users from your circles.</small></h1></div>

        {foreach $ERROR_MESSAGES as $message}
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3 class="message error"><i class="fa fa-exclamation-triangle color"></i> {$message}</h3>
                </div>
            </div>
        {/foreach}

        {foreach $SUCCESS_MESSAGES as $message}
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3 class="message success"><i class="fa fa-check color"></i> {$message}</h3>
                </div>
            </div>
        {/foreach}

        <ul role="tablist" class="nav nav-tabs" id="myTab">
            <li class="active"><a data-toggle="tab" role="tab" href="#friends">Friends</a></li>
            <li><a data-toggle="tab" role="tab" href="#love_matches">Love matches</a></li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div id="friends" class="tab-pane tabs-up fade in active panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            {foreach $friends as $friend}
                            <div class="panel panel-default person">
                                <div class="panel-heading">
                                    <ul class="media-list messages nicescroll" style="margin-bottom: 0;max-height: none;">
                                        <li class="media">
                                            <a href="{$BASE_URL}pages/profile/profile.php?id={$friend.id}" class="pull-left">
                                                <img alt="user" class="media-object" src="{if is_null($friend.profile_pic)}{$BASE_URL}images/avatar/default.jpg{else}{$BASE_URL}images/avatar/{$friend.profile_pic} {/if}">
                                            </a>
                                            <div class="media-body">
                                                <h5 class="media-heading"><a href="{$BASE_URL}pages/profile/profile.php?id={$friend.id}">{$friend.username}</a>{if $friend.engineer} - <i class="fa fa-gear"></i> Engineer {/if}</h5>
                                                <p class="text-muted no-margn">{$friend.location}</p>
                                                <p class="text-muted no-margn"><a href="{$BASE_URL}actions/profile/send_message.php?id={$friend.id}">Send message</a> | <a href="" class="remove" data-idUser="{$friend.id}">Remove</a></p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            {/foreach}
                        </div>
                    </div>
                </div>
            </div>
            <div id="love_matches" class="tab-pane tabs-up fade panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            {foreach $love_matches as $love_match}
                                <div class="panel panel-default person">
                                    <div class="panel-heading">
                                        <ul class="media-list messages nicescroll" style="margin-bottom: 0;max-height: none;">
                                            <li class="media">
                                                <a href="{$BASE_URL}pages/profile/profile.php?id={$love_match.id}" class="pull-left">
                                                    <img alt="user" class="media-object" src="{if is_null($love_match.profile_pic)}{$BASE_URL}images/avatar/default.jpg{else}{$BASE_URL}images/avatar/{$love_match.profile_pic} {/if}">
                                                </a>
                                                <div class="media-body">
                                                    <h5 class="media-heading"><a href="{$BASE_URL}pages/profile/profile.php?id={$love_match.id}">{$love_match.username}</a>{if $love_match.engineer} - <i class="fa fa-gear"></i> Engineer {/if}</h5>
                                                    <p class="text-muted no-margn">{$love_match.location}</p>
                                                    <p class="text-muted no-margn"><a href="{$BASE_URL}actions/profile/send_message.php?id={$love_match.id}">Send message</a> | <a href="" class="remove" data-idUser="{$love_match.id}">Remove</a></p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            {/foreach}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="{$BASE_URL}js/circles.js"></script>

