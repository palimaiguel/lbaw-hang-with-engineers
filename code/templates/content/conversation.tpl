<div class="warper container-fluid">

    <div class="page-header"><h1>Conversation with {$other_user}</h1></div>
    <div class="col-md-12">
        <div class="panel-body">
            <ul class="media-list messages nicescroll" style="max-height: none;">
                 {foreach $messages as $message}
                <li class="media" id="{$message.id_message}">
                    <a href="{$BASE_URL}pages/profile/profile.php?id={$message.id_emitter}" class="pull-{if $message.id_emitter == $logged_user}right{else}left{/if}">
                        <img alt="user" src="{$BASE_URL}images/avatar/{$message.user_photo}" class="media-object"/>
                    </a>
                    <div class="media-body">
                        <h5 class="media-heading"><strong>{$message.username}</strong></h5>
                        <p class="text-muted no-margn">{$message.content}</p>
                    </div>
                </li>
                {/foreach}

            </ul>

        </div>
        <div class="panel-footer">
            <form role="form" id="send">
                <div class="input-group">
                    <input type="text" placeholder="Write your message here..." class="form-control" data-ng-model="todoText">
                      <span class="input-group-btn">
                        <button type="submit" class=" btn-success btn">Send</button>
                      </span>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-4"></div>
</div>



<script src="{$BASE_URL}js/conversation.js"></script>