    <div class="warper container-fluid">

        <div class="page-header"><h1>Dashboard<small> News feed and some statistics.</small></h1></div>

        {foreach $ERROR_MESSAGES as $message}
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3 class="message error"><i class="fa fa-exclamation-triangle color"></i> {$message}</h3>
                </div>
            </div>
        {/foreach}

        {foreach $SUCCESS_MESSAGES as $message}
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3 class="message success"><i class="fa fa-check color"></i> {$message}</h3>
                </div>
            </div>
        {/foreach}

        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="panel panel-default clearfix dashboard-stats rounded">
                    <span id="dashboard-stats-sparkline1" class="sparkline transit"></span>
                    <i class="fa fa-users bg-danger transit stats-icon"></i>
                    <h3 class="transit">{$numTotalCircles} <small class="text-green"><i class="fa fa-caret-up"></i></small></h3>
                    <p class="text-muted transit">On your circles</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="panel panel-default clearfix dashboard-stats rounded">
                    <span id="dashboard-stats-sparkline2" class="sparkline transit"></span>
                    <i class="fa fa-envelope bg-info transit stats-icon"></i>
                    <h3 class="transit">{$numMessagesSent} <small class="text-green"><i class="fa fa-caret-up"></i></small></h3>
                    <p class="text-muted transit">Messages sent</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="panel panel-default clearfix dashboard-stats rounded">
                    <span id="dashboard-stats-sparkline3" class="sparkline transit"></span>
                    <i class="fa fa-user-plus bg-success transit stats-icon"></i>
                    <h3 class="transit">{$numTotalLoveMatches} <small class="text-green"><i class="fa fa-caret-up"></i></small></h3>
                    <p class="text-muted transit">Added you to "love matches"</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="panel panel-default clearfix dashboard-stats rounded">
                    <span id="dashboard-stats-sparkline4" class="sparkline transit"></span>
                    <i class="fa fa-eye bg-warning transit stats-icon"></i>
                    <h3 class="transit">{$numNotificationsUnseen} <small class="text-green"><i class="fa fa-caret-up"></i></small></h3>
                    <p class="text-muted transit">Unseen notifications</p>
                </div>
            </div>
        </div>

        <ul role="tablist" class="nav nav-tabs" id="myTab">
            <li class="active"><a data-toggle="tab" role="tab" href="#home">Global feed</a></li>
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" id="myTabDrop1" href="#">My Circles feed <span class="caret"></span></a>
                <ul aria-labelledby="myTabDrop1" role="menu" class="dropdown-menu">
                    <li><a data-toggle="tab" role="tab" tabindex="-1" href="#friends">Friends</a></li>
                    <li><a data-toggle="tab" role="tab" tabindex="-1" href="#matches">Love matches</a></li>
                </ul>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div id="home" class="tab-pane tabs-up fade in active panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            {foreach $publications as $publication}
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <img src="{if is_null($publication.user_photo)}{$BASE_URL}images/avatar/default.jpg{else}{$BASE_URL}images/avatar/{$publication.user_photo} {/if}" class="img-circle" style="float:left; margin-right:1em;"/>
                                        <span>
                                            <a href="{$BASE_URL}pages/profile/profile.php?id={$publication.id_user}">{$publication.username}</a>
                                            <p>{$publication.time_stamp}</p>
                                        </span>
                                    </div>
                                    <div class="panel-body">
                                        {$publication.description}
                                        {if $publication.publication_type eq 'LINK'}
                                            <a href="{$publication.publication_link}">{$publication.publication_link}</a>
                                        {/if}
                                        {if $publication.publication_type eq 'PHOTO'}
                                            <img alt="element photo" src="{$BASE_URL}images/publications/{$publication.publication_link}" />
                                        {/if}
                                    </div>
                                </div>
                            {/foreach}
                        </div>
                    </div>
                </div>
            </div>
            <div id="friends" class="tab-pane tabs-up fade panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">

                            {foreach $friendsPublications as $publication}
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <img src="{if is_null($publication.user_photo)}{$BASE_URL}images/avatar/default.jpg{else}{$BASE_URL}images/avatar/{$publication.user_photo} {/if}" class="img-circle" style="float:left; margin-right:1em;"/>
                                        <span>
                                            <a href="{$BASE_URL}pages/profile/profile.php?id={$publication.id_user}">{$publication.username}</a>
                                            <p>{$publication.time_stamp}</p>
                                        </span>
                                    </div>
                                    <div class="panel-body">
                                        {$publication.description}
                                        {if $publication.publication_type eq 'LINK'}
                                            <a href="{$publication.publication_link}">{$publication.publication_link}</a>
                                        {/if}
                                        {if $publication.publication_type eq 'PHOTO'}
                                            <img alt="element photo" src="{$BASE_URL}images/publications/{$publication.publication_link}" />
                                        {/if}
                                    </div>
                                </div>
                            {/foreach}
                        </div>
                    </div>
                </div>
            </div>
            <div id="matches" class="tab-pane tabs-up fade panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">

                            {foreach $matchesPublications as $publication}
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <img src="{if is_null($publication.user_photo)}{$BASE_URL}images/avatar/default.jpg{else}{$BASE_URL}images/avatar/{$publication.user_photo} {/if}" class="img-circle" style="float:left; margin-right:1em;"/>
                                        <span>
                                            <a href="{$BASE_URL}pages/profile/profile.php?id={$publication.id_user}">{$publication.username}</a>
                                            <p>{$publication.time_stamp}</p>
                                        </span>
                                    </div>
                                    <div class="panel-body">
                                        {$publication.description}
                                        {if $publication.publication_type eq 'LINK'}
                                            <a href="{$publication.publication_link}">{$publication.publication_link}</a>
                                        {/if}
                                        {if $publication.publication_type eq 'PHOTO'}
                                            <img alt="element photo" src="{$BASE_URL}images/publications/{$publication.publication_link}" />
                                        {/if}
                                    </div>
                                </div>
                            {/foreach}


                        </div>
                    </div>
                </div>
            </div>
        </div>


        <script src="{$BASE_URL}js/dashboard.js"></script>