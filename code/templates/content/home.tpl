<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hang with Engineers</title>


    <link href="{$BASE_URL}css/bootstrap/bootstrap.min.css" rel="stylesheet">


    <link href="{$BASE_URL}css/agency/agency.css" rel="stylesheet">


    <link href="{$BASE_URL}css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet">


    <link href="{$BASE_URL}css/home.css" rel="stylesheet">


    <link href="{$BASE_URL}font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>



    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">


<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">

        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">Hang with Engineers</a>
        </div>


        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <li>
                    <a class="page-scroll" href="#join">Login / Register</a>
                </li>
                <li>
                    <a class="page-scroll" href="#service">Hang with us!</a>
                </li>
                <li>
                    <a class="page-scroll" href="#features">Features</a>
                </li>
                <li>
                    <a class="page-scroll" href="#team">Our team</a>
                </li>
            </ul>
        </div>

    </div>

</nav>


<header>
    <div class="container">
        <div class="intro-text">
            <div class="intro-lead-in">Wanna hang with your engineering friends?
                <br>Or maybe meet some interesting engineering love matches?</div>
            <div class="intro-heading">1,000,000 people have already joined us!</div>
            <a href="#service" class="page-scroll btn btn-xl">I wanna know more!</a>
        </div>
    </div>
</header>


<section id="join">
    <div class="container">
        {foreach $ERROR_MESSAGES as $message}
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3 class="message error"><i class="fa fa-exclamation-triangle color-red"></i> {$message}</h3>
                </div>
            </div>
        {/foreach}

        {foreach $SUCCESS_MESSAGES as $message}
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3 class="message success"><i class="fa fa-check color-green"></i> {$message}</h3>
                </div>
            </div>
        {/foreach}

        <div class="row">
            <div class="col-lg-6 col-xs-6 text-center">
                <h2 class="section-heading">Login</h2>
                <h3 class="section-subheading text-muted">Let's find new people and have some fun!</h3>
            </div>
            <div class="col-lg-6 col-xs-6 text-center">
                <h2 class="section-heading">Register</h2>
                <h3 class="section-subheading text-muted">Create an account.</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-xs-6">
                <form name="login" id="login" action="{$BASE_DIR}../actions/auth/login.php" method="post">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Email address" name="email" required data-validation-required-message="Please enter your registration email address.">
                        <p class="help-block text-danger"></p>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Password" name="password" required data-validation-required-message="Please enter your password.">
                        <p class="help-block text-danger"></p>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12 col-xs-12 text-center">
                        <div id="success"></div>
                        <button type="submit" class="btn btn-xl">Login</button>
                    </div>
                </form>
            </div>
            <div class="col-lg-6 col-xs-6">
                <form name="register" id="register" action="{$BASE_DIR}../actions/auth/register.php" method="post">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Your name" name="name" id="name" required data-validation-required-message="Please enter your name.">
                        <p class="help-block text-danger"></p>
                    </div>
                    <div class="form-group">
                            <div class='input-group date' id="datepicker" >
                                <input type='text' class="form-control" placeholder="Your birthdate" name="birthdate" id="birthdate" required data-validation-required-message="Please enter your birthdate" data-date-format="YYYY-MM-DD">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i>
                                            </span>
                            </div>
                    </div>
                    <div class="form-group">
                        <label>
                        <input type="checkbox" name="engineer" value="y"> I'm an engineer
                        </label>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Email address" name="email" id="email_reg" required data-validation-required-message="Please enter your registration email address.">
                        <p class="help-block text-danger"></p>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Password (minimum 8 characters)" name="password" id="password_first" required data-validation-required-message="Please enter your password (minimum 8 characters).">
                        <p class="help-block text-danger"></p>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Password (again)" name="password_repeated" id="password_repeated" required data-validation-required-message="Please enter your password again.">
                        <p class="help-block text-danger"></p>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12 col-xs-6 text-center">
                        <div id="success"></div>
                        <button type="submit" class="btn btn-xl">Register</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>


<section id="service" class="bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Hang with us & meet engineers!</h2>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-users fa-stack-1x fa-inverse"></i>
                    </span>
                <h4 class="service-heading">Friendship</h4>
            </div>
            <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-heart fa-stack-1x fa-inverse"></i>
                    </span>
                <h4 class="service-heading">Love</h4>
            </div>
            <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-cogs fa-stack-1x fa-inverse"></i>
                    </span>
                <h4 class="service-heading">Engineering</h4>
            </div>
        </div>
    </div>
</section>


<section id="features">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Features</h2>
                <h3 class="section-subheading text-muted">Amazing features. By our engineers.</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6 portfolio-item">
                <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">
                            <i class="fa fa-plus fa-3x"></i>
                        </div>
                    </div>
                    <img src="{$BASE_URL}images/landing-page/portfolio/perfil-preview.png" class="img-responsive" alt="">
                </a>
                <div class="portfolio-caption">
                    <h4>Create a profile</h4>
                    <p class="text-muted">Let other users know more about you.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 portfolio-item">
                <a href="#portfolioModal2" class="portfolio-link" data-toggle="modal">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">
                            <i class="fa fa-plus fa-3x"></i>
                        </div>
                    </div>
                    <img src="{$BASE_URL}images/landing-page/portfolio/search-preview.png" class="img-responsive" alt="">
                </a>
                <div class="portfolio-caption">
                    <h4>Meet people</h4>
                    <p class="text-muted">Find engineers & other people with similar interests.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 portfolio-item">
                <a href="#portfolioModal3" class="portfolio-link" data-toggle="modal">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">
                            <i class="fa fa-plus fa-3x"></i>
                        </div>
                    </div>
                    <img src="{$BASE_URL}images/landing-page/portfolio/conv-preview.png" class="img-responsive" alt="">
                </a>
                <div class="portfolio-caption">
                    <h4>Chat</h4>
                    <p class="text-muted">Exchange private messages with other users.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 portfolio-item">
                <a href="#portfolioModal4" class="portfolio-link" data-toggle="modal">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">
                            <i class="fa fa-plus fa-3x"></i>
                        </div>
                    </div>
                    <img src="{$BASE_URL}images/landing-page/portfolio/circles-preview.png" class="img-responsive" alt="">
                </a>
                <div class="portfolio-caption">
                    <h4>Circles</h4>
                    <p class="text-muted">Add other users to your friends & love matches.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 portfolio-item">
                <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">
                            <i class="fa fa-plus fa-3x"></i>
                        </div>
                    </div>
                    <img src="{$BASE_URL}images/landing-page/portfolio/privacy-preview.png" class="img-responsive" alt="">
                </a>
                <div class="portfolio-caption">
                    <h4>Privacy</h4>
                    <p class="text-muted">It's your information, so you decide on its visibility.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 portfolio-item">
                <a href="#portfolioModal6" class="portfolio-link" data-toggle="modal">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">
                            <i class="fa fa-plus fa-3x"></i>
                        </div>
                    </div>
                    <img src="{$BASE_URL}images/landing-page/portfolio/roundicons.png" class="img-responsive" alt="">
                </a>
                <div class="portfolio-caption">
                    <h4>And much more...</h4>
                    <p class="text-muted">Coming soon!</p>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="team" class="bg-light-gray"    >
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Meet our team: Team Palimaiguel</h2>
                <h3 class="section-subheading text-muted">No code, no fun!</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="team-member">
                    <img src="{$BASE_URL}images/landing-page/team/dawiwa.jpg" class="img-responsive img-circle" alt="">
                    <h4>Dawiwa</h4>
                    <p class="text-muted">CEO, UI Designer, Security Analyst</p>
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="team-member">
                    <img src="{$BASE_URL}images/landing-page/team/maia.jpg" class="img-responsive img-circle" alt="">
                    <h4>Maiah</h4>
                    <p class="text-muted">CEO, Agile Methodology Master, Pattern Guru</p>
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="team-member">
                    <img src="{$BASE_URL}images/landing-page/team/miguew.jpg" class="img-responsive img-circle" alt="">
                    <h4>Miguew</h4>
                    <p class="text-muted">CEO, Machine Learning Specialist, Social Network Interaction Analyst</p>
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="team-member">
                    <img src="{$BASE_URL}images/landing-page/team/pawa.jpg" class="img-responsive img-circle" alt="">
                    <h4>Pawa</h4>
                    <p class="text-muted">CEO, Machine Learning Specialist, Database Engineer, Social Psychologist</p>
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <p class="large text-muted"></p>
            </div>
        </div>
    </div>
</section>

<section id="end">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">So, what are you waiting for?</h2>
                <a href="#join" class="page-scroll btn btn-xl">Join us!</a>
            </div>
        </div>
    </div>
</section>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <span class="copyright">Copyright &copy; Hang With Engineers 2015</span>
            </div>
            <div class="col-md-4">
                <ul class="list-inline social-buttons">
                    <li><a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul class="list-inline quicklinks">
                    <li><a href="#">Privacy Policy</a>
                    </li>
                    <li><a href="#">Terms of Use</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>





<div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">

                        <h2>Create a profile</h2>
                        <p class="item-intro text-muted">Let other users know more about you.</p>
                        <img class="img-responsive" src="{$BASE_URL}images/landing-page/portfolio/perfil-def.png" alt="">
                        <p>Your publications, personal information, photos.</p>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Feature</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <h2>Meet people</h2>
                        <p class="item-intro text-muted">Find engineers & other people with similar interests.</p>
                        <img class="img-responsive img-centered" src="{$BASE_URL}images/landing-page/portfolio/search-def.png" alt="">
                        <p>Search by username, keywords (interests, school, etc).</p>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">

                        <h2>Chat</h2>
                        <p class="item-intro text-muted">Exchange private messages with other users.</p>
                        <img class="img-responsive img-centered" src="{$BASE_URL}images/landing-page/portfolio/conv-def.png" alt="">
                        <p>Talk with other people.</p>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <h2>Circles</h2>
                        <p class="item-intro text-muted">Add other users to your friends & love matches.</p>
                        <img class="img-responsive img-centered" src="{$BASE_URL}images/landing-page/portfolio/circles-def.png" alt="">
                        <p>If you add another user to your love matches they won't know it was you!</p>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <h2>Privacy</h2>
                        <p class="item-intro text-muted">It's your information, so you decide on its visibility.</p>
                        <img class="img-responsive img-centered" src="{$BASE_URL}images/landing-page/portfolio/privacy-def.png" alt="">
                        <p>Keep your publications, personal info and photos private, if you don't want others to see them.</p>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                        <h2>And much more...</h2>
                        <p class="item-intro text-muted">Coming soon!</p>
                        <img class="img-responsive img-centered" src="{$BASE_URL}images/landing-page/portfolio/roundicons-free.png" alt="">
                        <p>More features coming soon so you can find your best match!</p>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{$BASE_URL}js/jquery/jquery.js"></script>
<script src="{$BASE_URL}js/bootstrap/bootstrap.min.js"></script>
<script src="{$BASE_URL}js/jquery/jquery.easing.min.js"></script>
<script src="{$BASE_URL}js/classie/classie.js"></script>
<script src="{$BASE_URL}js/cbp/cbpAnimatedHeader.js"></script>
<script src="{$BASE_URL}js/moment/moment.js"></script>
<script src="{$BASE_URL}js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
<script src="{$BASE_URL}js/app/custom.js" type="text/javascript"></script>
<script src="{$BASE_URL}js/agency/agency.js"></script>
<script src="{$BASE_URL}js/home.js"></script>

</body>
</html>
