    <div class="warper container-fluid">

        <div class="page-header"><h1>Your conversations</h1></div>

        {foreach $ERROR_MESSAGES as $message}
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3 class="message"><i class="fa fa-exclamation-triangle color"></i> {$message}</h3>
                </div>
            </div>
        {/foreach}

        {foreach $SUCCESS_MESSAGES as $message}
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3 class="message success"><i class="fa fa-check color"></i> {$message}</h3>
                </div>
            </div>
        {/foreach}

        <div class="panel panel-default">
            <div class="panel-body">
                <ul class="media-list messages nicescroll" style="margin-bottom: 0;">
                    {foreach $conversations as $conversation}
                    <li class="media">
                        <a href="{$BASE_URL}pages/profile/profile.php?id={$conversation.id_user}" class="pull-left">
                            <img alt="user" class="media-object" src="{if is_null($conversation.link)}{$BASE_URL}images/avatar/default.jpg{else}{$BASE_URL}images/avatar/{$conversation.link} {/if}">
                        </a>
                        <div class="media-body">
                            <h5 class="media-heading"><strong><a href="{$BASE_URL}pages/profile/profile.php?id={$conversation.id_user}">{$conversation.username}</a></strong></h5>
                            <p class="text-muted no-margn"><a href="{$BASE_URL}pages/chat/conversation.php?id={$conversation.id_conversation}">{$conversation.content}</a></p>
                            <p class="text-muted no-margn"><small>Sent at {$conversation.time_stamp}</small></p>
                        </div>
                    </li>
                    {/foreach}
                </ul>

            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
