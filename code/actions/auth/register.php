<?php
require_once('../../config/init.php');

if (!$_POST['name'] || !$_POST['birthdate'] || !$_POST['email'] || !$_POST['password'] || !$_POST['password_repeated']) {
    $_SESSION['error_messages'][] = 'All fields are mandatory';
    $_SESSION['form_values'] = $_POST;
    header("Location: $BASE_URL" . 'pages/home.php' . "#join");
    exit;
}

$name = strip_tags($_POST['name']);
$birthdate = strip_tags($_POST['birthdate']);
$email = strip_tags($_POST['email']);

if ($_POST['engineer'] == "y")
    $engineer = "TRUE";
else
    $engineer = "FALSE";

$password = $_POST['password'];
$password_r = $_POST['password_repeated'];

if (strlen($password) < 8)
{
    $_SESSION['error_messages'][] = 'Your password has to be at least 8 characters.';
    header("Location: $BASE_URL" . 'pages/home.php' . "#join");
}

if($password === $password_r)
{
    try
    {
        $authDB = $database->authDB;
        $authDB->createUser($name, $password, $email, $engineer, $birthdate);
    } catch (PDOException $e) {
        if (strpos($e->getMessage(), 'email_unique_constraint'))
        {
            $_SESSION['error_messages'][] = 'This email already exists! Try another one.';
            $_SESSION['field_errors']['email'] = $email;
        }
        else if (strpos($e->getMessage(), 'birth_date_adult_constraint'))
            $_SESSION['error_messages'][] = 'This service is limited to adults (minimum age: 18).';
        else
            $_SESSION['error_messages'][] = 'Oops, seems like you filled some of the fields in the wrong way!';

        header("Location: $BASE_URL" . 'pages/home.php' . "#join");
        exit;
    }
    $_SESSION['success_messages'][] = 'User registered successfully.';
    header("Location: $BASE_URL" . '#join');
}
else
{
    $_SESSION['error_messages'][] = 'Passwords do not match.';
    header("Location: $BASE_URL" . 'pages/home.php' . "#join");
}
