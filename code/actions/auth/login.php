<?php
require_once('../../config/init.php');

$authDB = $database->authDB;

if (!$_POST['email'] || !$_POST['password']) {
    $_SESSION['error_messages'][] = 'Invalid login';
    $_SESSION['form_values'] = $_POST;
    header('Location: ' . $_SERVER['HTTP_REFERER']);
    exit;
}

$email = $_POST['email'];
$password = $_POST['password'];

if ($authDB->isLoginCorrect($email, $password)) {
    startSession(); // to prevent session fixation
    $id = $authDB->getLoggedInId($email);
    $username = $authDB->getLoggedInUsername($email);
    setLoggedinUser($id, $email, $username);

    $_SESSION['success_messages'][] = 'Login successful.';
    header("Location: " .  $BASE_URL . 'pages/dashboard.php');
} else {
    $_SESSION['error_messages'][] = 'Login failed.';
    header("Location: " . $_SERVER['HTTP_REFERER'] . "#join");
}
