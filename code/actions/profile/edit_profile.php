<?php
require_once('../../config/init.php');

    if (getLoggedinId() === -1)
    {
        header("Location: " .  $BASE_URL . 'pages/home.php');
        exit;
    }

        $profileDB= $database->profileDB;

        $gender = (isset($_POST['gender'])) ? strip_tags($_POST['gender']) : '';
        $sexual_orientation = (isset($_POST['sexual_orientation'])) ? strip_tags($_POST['sexual_orientation']) : '';
        $city = (isset($_POST['city'])) ? strip_tags($_POST['city']) : '';
        $country = (isset($_POST['country'])) ? strip_tags($_POST['country']) : 'null';
        $job = (isset($_POST['job'])) ? strip_tags($_POST['job']) : '';
        $education = (isset($_POST['education'])) ? strip_tags($_POST['education']) : '';
        $school = (isset($_POST['school'])) ? strip_tags($_POST['school']) : '';
        $music = (isset($_POST['music'])) ? strip_tags($_POST['music']) : '';
        $movies = (isset($_POST['movies'])) ? strip_tags($_POST['movies']) : '';
        $books = (isset($_POST['books'])) ? strip_tags($_POST['books']) : '';
        $others = (isset($_POST['others'])) ? strip_tags($_POST['others']) : '';

        $logged_user = getLoggedinId();

        if($profileDB->updateAboutMe($logged_user,$gender,$sexual_orientation,$city,$country,$job,$education,$school,$music,$movies,$books,$others))
            $_SESSION['success_messages'][] = '"About Me" successfully edited.';
        else
            $_SESSION['error_messages'][] = 'Error editing "About Me".';

        header("Location: " .  $BASE_URL . 'pages/profile/edit_profile.php');
