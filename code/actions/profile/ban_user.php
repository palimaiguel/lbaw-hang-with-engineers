<?php
require_once('../../config/init.php');

if (getLoggedinId() === -1)
{
    header("Location: " .  $BASE_URL . 'pages/home.php');
    exit;
}

if(isset($_GET['id']))
{
    $profileDB = $database->profileDB;

    $id_user = strip_tags($_GET['id']);
    $id_myuser = getLoggedinID();

    if($profileDB->banUser($id_myuser, $id_user))
    {
        header("Location: " .  $BASE_URL . 'pages/profile/profile.php?id=' . $id_user);
    }
    else
        header("Location: " .  $BASE_URL . 'pages/dashboard.php');
}
else
{
    header("Location: " .  $BASE_URL . 'pages/dashboard.php');
}