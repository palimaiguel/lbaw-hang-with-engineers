<?php
require_once('../../config/init.php');

if (getLoggedinId() === -1)
{
    header("Location: " .  $BASE_URL . 'pages/home.php');
    exit;
}

$profileDB= $database->profileDB;

if (isset($_POST['photos']) && count($_POST['photos']) > 0)
{
    foreach ($_POST['photos'] as $photo)
    {
        $photo = strip_tags($photo);

        $photoName = $profileDB->getGalleryPhotoLink($photo);

        if ($photoName === false)
        {
            $_SESSION['error_messages'][] = 'Error in the database deleting the gallery photo(s) you wanted :(. ';
            break;
        }

        if ($profileDB->deleteGalleryPhoto($photo))
        {
            $imagePath = $BASE_DIR . "images/users/$photoName";
            $thumbnailPath = $BASE_DIR . "images/users_thumbs/$photoName";
            unlink($imagePath);
            unlink($thumbnailPath);
            $_SESSION['success_messages'][] = 'Photo deleted successfully.';
        }
        else
        {
            $_SESSION['error_messages'][] = 'Error in the database deleting the gallery photo(s) you wanted :(. ';
            break;
        }
    }
}
else
    $_SESSION['error_messages'][] = 'Please select the photos you want to remove!';

header("Location: " .  $BASE_URL . 'pages/profile/edit_profile.php');
