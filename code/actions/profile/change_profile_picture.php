<?php

require_once('../../config/init.php');
require_once('../../config/files.php');

if (getLoggedinId() === -1)
{
    header('Location: home.php');
}
else
{
    $profileDB = $database->profileDB;

    $processReturn = processImage($_FILES["photo"]);

    if ($processReturn === 0)
    {
        $imageName = md5(getLoggedinId() . " " . microtime()) . ".jpg";
        $imagePath = $BASE_DIR ."images/avatar/$imageName";

        if (generateProfileImage($_FILES["photo"]["tmp_name"], $imagePath))
        {
            if($profileDB->updateProfilePhoto(getLoggedinId(), $imageName))
                $_SESSION['success_messages'][] = 'New profile picture successfully added!';
            else
                $_SESSION['error_messages'][] = 'Error adding new profile picture.';
        }
    }

    header("Location: $BASE_URL" . "pages/profile/edit_profile.php");
}
