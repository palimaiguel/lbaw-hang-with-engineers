<?php
require_once('../../config/init.php');
require_once('../../config/files.php');

if (getLoggedinId() === -1)
{
    header("Location: " .  $BASE_URL . 'pages/home.php');
    exit;
}

if(isset($_POST["element"]) && isset($_POST["title"]) && isset($_POST["visibility"]) && isset($_POST["text"]))
{
    $profileDB = $database->profileDB;

    $logged_user = getLoggedinId();

    $element = strip_tags($_POST["element"]);
    $title = strip_tags($_POST["title"]);
    $text = strip_tags($_POST["text"]);
    $visibility = strip_tags($_POST["visibility"]);

    if($element === "none")
    {

        if($profileDB->newPublication($logged_user, $title, $text, $visibility))
            $_SESSION['success_messages'][] = 'New publication successfully created.';
        else
            $_SESSION['error_messages'][] = 'Error adding new publication.';

    }
    else if ($element === "link")
    {
        if (isset($_POST["link"]))
        {
            $link = strip_tags($_POST["link"]);

            if (strpos($link,'http://') === false)
                $link = "http://".$link;

            if($profileDB->newPublicationWithElem($logged_user, $title, $text, $visibility, 'LINK', $link))
                $_SESSION['success_messages'][] = 'New publication successfully created.';
            else
                $_SESSION['error_messages'][] = 'Error adding new publication.';
        }
        else
        {
            $_SESSION['error_messages'][] = 'You didn\'t attach a link to the publication!';
        }

    }
    else if ($element === "photo")
    {
        if (processImage($_FILES["photo_pub"]) === 0)
        {
            $imageName = md5(getLoggedinId() . ' ' . microtime()) . ".jpg";
            $imagePath = $BASE_DIR ."images/publications/$imageName";

            if (generateElementImage($_FILES["photo_pub"]["tmp_name"], $imagePath))
            {
                if($profileDB->newPublicationWithElem($logged_user, $title, $text, $visibility, 'PHOTO', $imageName))
                    $_SESSION['success_messages'][] = 'New publication successfully created.';
                else
                    $_SESSION['error_messages'][] = 'Error adding new publication.';
            }
        }
    }
    else
        $_SESSION['error_messages'][] = "Invalid publication element!";


    header("Location: " .  $BASE_URL . 'pages/profile/edit_profile.php');
}
else
    $_SESSION['error_messages'][] = "You must fill all the publication insertion fields!";
header("Location: " .  $BASE_URL . 'pages/profile/edit_profile.php');