<?php
require_once('../../config/init.php');

    if (getLoggedinId() === -1)
    {
        header("Location: " .  $BASE_URL . 'pages/home.php');
        exit;
    }

    if(isset($_GET['id']))
    {
        $chatDB = $database->chatDB;

        $id_user = strip_tags($_GET['id']);
        $id_myuser = getLoggedinID();
        $id_conversation = $chatDB->getConversationID($id_myuser, $id_user);

        if(!$id_conversation)
        {
            $id_conversation = $chatDB->newConversation($id_myuser, $id_user);

            if(!$id_conversation)
                header("Location: " .  $BASE_URL . 'pages/conversations.php');
            else
                header("Location: " .  $BASE_URL . 'pages/chat/conversation.php?id=' . $id_conversation);
        }
        else
            header("Location: " .  $BASE_URL . 'pages/chat/conversation.php?id=' . $id_conversation);
    }
    else
    {
        header("Location: " .  $BASE_URL . 'pages/conversations.php');
    }