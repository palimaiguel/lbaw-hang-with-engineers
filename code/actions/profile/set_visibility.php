<?php
require_once('../../config/init.php');

    if (getLoggedinId() === -1)
    {
        header("Location: " .  $BASE_URL . 'pages/home.php');
        exit;
    }

    $profileDB= $database->profileDB;

    $visibility = (isset($_POST['visibility'])) ? strip_tags($_POST['visibility']) : '';

    $logged_user = getLoggedinId();

    if($profileDB->updateProfileVisibility($logged_user, $visibility))
        $_SESSION['success_messages'][] = 'Profile visibility successfully changed.';
    else
        $_SESSION['error_messages'][] = 'Error changing profile visibility';

    header("Location: " .  $BASE_URL . 'pages/profile/edit_profile.php');

