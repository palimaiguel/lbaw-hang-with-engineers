<?php
require_once('../../config/init.php');

    if (getLoggedinId() === -1)
    {
        header("Location: " .  $BASE_URL . 'pages/home.php');
        exit;
    }

    if(isset($_GET['id']) && isset($_GET['type']))
    {
        $circlesDB = $database->circlesDB;

        $id_user = strip_tags($_GET['id']);
        $circle = strip_tags($_GET['type']);
        $id_myuser = getLoggedinId();

        if ($id_myuser === -1)
        header("Location: " .  $BASE_URL);

        //prepare notifications
        $notificationsDB = $database->notificationsDB;

        if($circle=="FRIEND")
        {
            if($circlesDB->addFriends($id_myuser, $id_user))
            {
                $_SESSION['success_messages'][] = 'Friend added.';
                $notificationsDB->insertCircleNotification($circle,$id_user, $id_myuser);
            }
            else
                $_SESSION['error_messages'][] = 'You already added this person as a friend.';
        }
        if($circle=="LOVE_MATCH")
        {
            if($circlesDB->addLovematch($id_myuser, $id_user))
            {
                $_SESSION['success_messages'][] = 'Love match added.';
                $notificationsDB->insertCircleNotification($circle,$id_user, $id_myuser);
            }
            else
                $_SESSION['error_messages'][] = 'You already added this person as a love match.';
        }

        header("Location: " .  $BASE_URL . 'pages/profile/profile.php?id=' . $id_user);
    }
    else
        header("Location: " .  $BASE_URL . 'pages/home.php');
