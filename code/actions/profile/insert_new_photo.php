<?php
require_once('../../config/init.php');
require_once('../../config/files.php');

if (getLoggedinId() === -1)
{
    header("Location: " .  $BASE_URL . 'pages/home.php');
    exit;
}
else
{
    if (isset($_POST["title"]) && isset($_POST["description"]) && isset($_POST["visibility"]))
    {
        $profileDB = $database->profileDB;

        $title = strip_tags($_POST["title"]);
        $description = strip_tags($_POST["description"]);
        $visibility = strip_tags($_POST["visibility"]);

        $processReturn = processImage($_FILES["photo"]);

        if ($processReturn === 0) {
            $imageName = md5(getLoggedinId() . " " . microtime()) . ".jpg";
            $imagePath = $BASE_DIR . "images/users/$imageName";
            $thumbnailPath = $BASE_DIR . "images/users_thumbs/$imageName";

            if (generateGalleryImage($_FILES["photo"]["tmp_name"], $imagePath) &&
                generateGalleryThumb($_FILES["photo"]["tmp_name"], $thumbnailPath))
            {
                if ($profileDB->newGalleryPhoto(getLoggedinId(), $title, $description, $visibility, $imageName))
                    $_SESSION['success_messages'][] = 'New photo successfully added!';
                else
                    $_SESSION['error_messages'][] = 'Error adding new photo.';
            }
        }
    }
    else
        $_SESSION['error_messages'][] = 'All fields must be filled!';

    header("Location: $BASE_URL" . "pages/profile/edit_profile.php");
}
