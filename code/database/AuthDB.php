<?php

class AuthDB
{
    var $conn;

    function AuthDB($conn)
    {
        $this->conn = $conn;
    }

    function isLoginCorrect($email, $password)
    {
        try{
            $stmt = $this->conn->prepare("SELECT * FROM users WHERE email = :email AND password = :passwordHash AND banned = FALSE;");
            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':passwordHash', sha1($password));

            $stmt->execute();

            $result = $stmt->fetch() == true;

            return $result;
        }
        catch(PDOException $e)
        {
            error_log("AuthDB::isLoginCorrect() " . $email."|".$password. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function createUser($username, $password, $email, $engineer, $birthdate)
    {
        try {
            $hash = sha1($password);

            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $this->conn->beginTransaction();
            $stmt = $this->conn->prepare("INSERT INTO user_photos (link) SELECT 'default.jpg'");
            $stmt->execute(array());
            $stmt = $this->conn->prepare('INSERT INTO users (username, password, email, birth_date, engineer, id_user_photo) SELECT ?, ?, ?, ?, ?, MAX(id_user_photo) FROM user_photos');
            $stmt->execute(array($username, $hash, $email, $birthdate, $engineer));
            $stmt = $this->conn->prepare('INSERT INTO profiles (id_user) SELECT MAX(id_user) FROM users');
            $stmt->execute(array());

            $this->conn->commit();

        } catch (Exception $e) {
            $this->conn->rollBack();
            error_log("AuthDB::createUser() " . $username . "," . $email . "," . $engineer . "," . $birthdate." --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
        }
    }

    function getLoggedInUsername($email)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT username FROM users WHERE email = :email");
            $stmt->bindParam(':email', $email);

            $stmt->execute();

            $user = $stmt->fetch();

            return $user['username'];
        }
        catch(PDOException $e)
        {
            error_log("AuthDB::getLoggedInUsername() " . $email. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }

    }

    function getLoggedInId($email)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT id_user FROM users WHERE email = :email");
            $stmt->bindParam(':email', $email);

            $stmt->execute();

            $user = $stmt->fetch();

            return $user['id_user'];
        }
        catch(PDOException $e)
        {
            error_log("AuthDB::getLoggedInId() " . $email. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function getLoggedInPhoto($id_user)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT link FROM user_photos, users
                                      WHERE users.id_user = :id_user
                                      AND users.id_user_photo = user_photos.id_user_photo");
            $stmt->bindParam(':id_user', $id_user);

            $stmt->execute();

            $photo = $stmt->fetch()['link'];

            return $photo;
        }
        catch(PDOException $e)
        {
            error_log("AuthDB::getLoggedInPhoto() " . $id_user. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }
}