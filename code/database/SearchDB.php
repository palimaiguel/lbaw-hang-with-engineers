<?php

class SearchDB
{
    var $conn;

    function SearchDB($conn)
    {
        $this->conn = $conn;
    }

    function searchUsersCount($keyword)
    {
        try
        {
            $keyword = '.*' . $keyword . '.*';

            $stmt = $this->conn->prepare('SELECT * FROM search_user_count(:keyword);');
            $stmt->bindParam(':keyword', $keyword, PDO::PARAM_STR);
            $stmt->execute();
            $result = $stmt->fetch();

            return $result["search_user_count"];
        }
        catch(PDOException $e)
        {
            error_log("SearchDB::searchUsersCount() " . $keyword. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function searchUsers($keyword, $limit, $offset)
    {
        try
        {
            $keyword = '.*' . $keyword . '.*';

            $this->conn->beginTransaction();

            $refcursor = 'cursor';
            $stmt = $this->conn->prepare('SELECT * FROM search_user(:keyword, :limit, :offset, :refcursor);');
            $stmt->bindParam(':keyword', $keyword, PDO::PARAM_STR);
            $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
            $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
            $stmt->bindParam(':refcursor', $refcursor, PDO::PARAM_STR);
            $stmt->execute();
            $stmt->fetchAll();

            $stmt = $this->conn->query('FETCH ALL IN "' . $refcursor . '";');
            $result = $stmt->fetchAll();
            $stmt->closeCursor();

            $this->conn->commit();

            return $result;
        }
        catch(PDOException $e)
        {
            error_log("SearchDB::searchUsers() " . $keyword."|".$limit."|".$offset. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }
}