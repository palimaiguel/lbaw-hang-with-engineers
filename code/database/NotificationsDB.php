<?php

class NotificationsDB
{
    var $conn;

    function NotificationsDB($conn)
    {
        $this->conn = $conn;
    }

    function getMessageNotifications($idUserNotificated, $offset, $limit)
    {
        try
        {
            // check if we have unseen notifications and return all
            $stmt = $this->conn->prepare(
                "SELECT notifications.id_notification AS id, notifications.id_user_sent, notification_type AS type, username AS user, time_stamp AS date, seen, link
            FROM final.notifications, final.users, final.user_photos
            WHERE id_user_notificated = :p_id_user
            AND notifications.id_user_sent = users.id_user
            AND users.id_user_photo = user_photos.id_user_photo
            AND notifications.notification_type = 'MESSAGE'
            AND seen = FALSE
            ORDER BY time_stamp ASC
            ;"
            );

            $stmt->bindParam("p_id_user", $idUserNotificated, PDO::PARAM_INT);
            $stmt->execute();

            $result = $stmt->fetchAll();

            // if there are no unseen notifications than give the last N
            if(count($result) == 0){

                $stmt = $this->conn->prepare(
                    "SELECT notifications.id_notification AS id, notifications.id_user_sent, notification_type AS type, username AS user, time_stamp AS date, seen, link
                 FROM final.notifications, final.users, final.user_photos
                 WHERE id_user_notificated = :p_id_user
                   AND notifications.id_user_sent = users.id_user
                   AND users.id_user_photo = user_photos.id_user_photo
                   AND notifications.notification_type = 'MESSAGE'
                 ORDER BY time_stamp ASC
                 LIMIT :p_limit
                 OFFSET :p_offset;
                "
                );

                $stmt->bindParam("p_id_user", $idUserNotificated, PDO::PARAM_INT);
                $stmt->bindParam("p_limit", $limit, PDO::PARAM_INT);
                $stmt->bindParam("p_offset", $offset, PDO::PARAM_INT);

                $stmt->execute();

                $result = $stmt->fetchAll();
            }

            return $result;
        }
        catch(PDOException $e)
        {
            error_log("NotificationsDB::getMessageNotifications() " . $idUserNotificated."|".$offset."|".$limit. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function getNewMessageNotifications($idUserNotificated, $lastId)
    {
        try
        {
            $stmt = $this->conn->prepare(
                "SELECT notifications.id_notification AS id, notifications.id_user_sent, notification_type AS type, username AS user, time_stamp AS date, seen, link
            FROM final.notifications, final.users, final.user_photos
            WHERE id_user_notificated = :p_id_user
            AND notifications.id_user_sent = users.id_user
            AND users.id_user_photo = user_photos.id_user_photo
            AND notifications.notification_type = 'MESSAGE'
            AND seen = FALSE
            AND notifications.id_notification > :last_id
            ORDER BY time_stamp ASC
            ;"
            );

            $stmt->bindParam("p_id_user", $idUserNotificated, PDO::PARAM_INT);
            $stmt->bindParam(':last_id', $lastId);

            $stmt->execute();

            $result = $stmt->fetchAll();

            return $result;
        }
        catch(PDOException $e)
        {
            error_log("NotificationsDB::getNewMessageNotifications() " . $idUserNotificated."|".$lastId. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function getCirclesNotifications($idUserNotificated, $offset, $limit)
    {
        try
        {
            // check if we have unseen notifications and return all
            $stmt = $this->conn->prepare(
			"SELECT notifications.id_notification AS id, notifications.id_user_sent, notification_type AS type, username AS user, time_stamp AS date, seen, link
            FROM final.notifications, final.users, final.user_photos
            WHERE id_user_notificated = :p_id_user
            AND notifications.id_user_sent = users.id_user
            AND users.id_user_photo = user_photos.id_user_photo
            AND (notifications.notification_type = 'FRIEND' OR notifications.notification_type = 'LOVE_MATCH')
            AND seen = FALSE
            ORDER BY time_stamp ASC
            ;"
            );

            $stmt->bindParam("p_id_user", $idUserNotificated, PDO::PARAM_INT);
            $stmt->execute();

            $result = $stmt->fetchAll();

            // if there are no unseen notifications than give the last N
            if(count($result) == 0){

                $stmt = $this->conn->prepare(
                    "
                SELECT notifications.id_notification AS id, notifications.id_user_sent, notification_type AS type, username AS user, time_stamp AS date, seen, link
                FROM final.notifications, final.users, final.user_photos
                WHERE id_user_notificated = :p_id_user
                  AND notifications.id_user_sent = users.id_user
                  AND users.id_user_photo = user_photos.id_user_photo
                  AND (notifications.notification_type = 'FRIEND' OR notifications.notification_type = 'LOVE_MATCH')
                ORDER BY time_stamp ASC
                LIMIT :p_limit
                OFFSET :p_offset;
                "
                );

                $stmt->bindParam("p_id_user", $idUserNotificated, PDO::PARAM_INT);
                $stmt->bindParam("p_limit", $limit, PDO::PARAM_INT);
                $stmt->bindParam("p_offset", $offset, PDO::PARAM_INT);

                $stmt->execute();

                $result = $stmt->fetchAll();
            }

            return $result;
        }
        catch(PDOException $e)
        {
            error_log("NotificationsDB::getCirclesNotifications() " . $idUserNotificated."|".$offset."|".$limit. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function getNewCirclesNotifications($idUserNotificated, $lastId)
    {
        try
        {
            $stmt = $this->conn->prepare(
                "SELECT notifications.id_notification AS id, notifications.id_user_sent, notification_type AS type, username AS user, time_stamp AS date, seen, link
            FROM final.notifications, final.users, final.user_photos
            WHERE id_user_notificated = :p_id_user
            AND notifications.id_user_sent = users.id_user
            AND users.id_user_photo = user_photos.id_user_photo
            AND (notifications.notification_type = 'FRIEND' OR notifications.notification_type = 'LOVE_MATCH')
            AND seen = FALSE
            AND notifications.id_notification > :last_id
            ORDER BY time_stamp ASC
            ;"
            );

            $stmt->bindParam("p_id_user", $idUserNotificated, PDO::PARAM_INT);
            $stmt->bindParam(':last_id', $lastId);

            $stmt->execute();

            $result = $stmt->fetchAll();

            return $result;
        }
        catch(PDOException $e)
        {
            error_log("NotificationsDB::getNewCirclesNotifications() " . $idUserNotificated."|".$lastId. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function readCirclesNotifications($idUserNotificated, $lastId)
    {
        try
        {
            $stmt = $this->conn->prepare(
                " UPDATE notifications
          SET seen = TRUE
          WHERE seen = FALSE
            AND id_user_notificated = :id_user_notificated
            AND (notifications.notification_type = 'FRIEND' OR notifications.notification_type = 'LOVE_MATCH')
            AND id_notification <= :last_id;"
            );

            $stmt->bindParam("id_user_notificated", $idUserNotificated, PDO::PARAM_INT);
            $stmt->bindParam("last_id", $lastId, PDO::PARAM_INT);
            $stmt->execute();
            return true;
        }
        catch(PDOException $e)
        {
            error_log("NotificationsDB::readCirclesNotifications() " . $idUserNotificated."|".$lastId. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function readMessagesNotifications($idUserNotificated, $lastId)
    {
        try
        {
            $stmt = $this->conn->prepare(
                " UPDATE notifications
                  SET seen = TRUE
                  WHERE seen = FALSE
                  AND id_user_notificated = :id_user_notificated
                  AND notifications.notification_type = 'MESSAGE'
                  AND id_notification <= :last_id;"
            );

            $stmt->bindParam("id_user_notificated", $idUserNotificated, PDO::PARAM_INT);
            $stmt->bindParam("last_id", $lastId, PDO::PARAM_INT);
            $stmt->execute();

            return true;
        }
        catch(PDOException $e)
        {
            error_log("NotificationsDB::readMessagesNotifications() " . $idUserNotificated."|".$lastId. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function insertMessageNotification($user_notificated, $logged_in)
    {
        try
        {
            $stmt = $this->conn->prepare("INSERT INTO notifications VALUES (DEFAULT, false, CURRENT_TIMESTAMP, 'MESSAGE', :user_notificated, :logged_in);");
            $stmt->bindParam(':user_notificated', $user_notificated);
            $stmt->bindParam(':logged_in', $logged_in);

            $stmt->execute();
            return true;
        }
        catch(PDOException $e)
        {
            error_log("NotificationsDB::insertMessageNotification() " . $user_notificated."|".$logged_in. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function insertCircleNotification($type_notification, $user_notificated, $logged_in)
    {
        try
        {
            $stmt = $this->conn->prepare("INSERT INTO notifications VALUES (DEFAULT, false, CURRENT_TIMESTAMP, :type_notification, :user_notificated, :logged_in);");
            $stmt->bindParam(':type_notification', $type_notification);
            $stmt->bindParam(':user_notificated', $user_notificated);
            $stmt->bindParam(':logged_in', $logged_in);

            $stmt->execute();
            return true;
        }
        catch(PDOException $e)
        {
            error_log("NotificationsDB::insertCircleNotification() " . $type_notification."|".$user_notificated."|".$logged_in. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function removeOldMessageNotifications($userNotificatedID, $userSentID, $lastID)
    {
        try
        {
            $stmt = $this->conn->prepare(
                "
                DELETE FROM final.notifications
                WHERE id_user_notificated = :id_user_notificated
                  AND id_user_sent = :id_user_sent
                  AND id_notification <= :last_id
                "
            );

            $stmt->bindParam(':id_user_notificated', $userNotificatedID);
            $stmt->bindParam(':id_user_sent', $userSentID);
            $stmt->bindParam(':last_id', $lastID);

            $stmt->execute();
            return true;
        }
        catch(PDOException $e)
        {
            error_log("NotificationsDB::removeOldMessageNotifications() " . $userNotificatedID."|".$userSentID."|".$lastID. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }
}