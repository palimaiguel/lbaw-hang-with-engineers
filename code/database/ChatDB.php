<?php

class ChatDB
{
    var $conn;

    function ChatDB($conn)
    {
        $this->conn = $conn;
    }

    function getOtherUserFromConversation($conversation_id, $logged_user)
    {
        try
        {
            $stmt = $this->conn->prepare("
          SELECT users.username
          FROM conversations, users
          WHERE conversations.id_conversation = :id_conversation
                AND conversations.id_user_2 = :id_user
                AND users.id_user = conversations.id_user_1 UNION SELECT users.username FROM conversations, users WHERE conversations.id_conversation = :id_conversation AND conversations.id_user_1 = :id_user AND users.id_user = conversations.id_user_2");
            $stmt->bindParam(':id_user', $logged_user);
            $stmt->bindParam(':id_conversation', $conversation_id);

            $stmt->execute();

            $other_user = $stmt->fetch();

            return $other_user['username'];
        }
        catch(PDOException $e)
        {
            error_log("ChatDB::getOtherUserFromConversation() " . $conversation_id . "|" . $logged_user . " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function getOtherUserIdFromConversation($conversation_id, $logged_user)
    {
        try
        {
            $stmt = $this->conn->prepare("
          SELECT conversations.id_user_1 as user_res
          FROM conversations
          WHERE conversations.id_conversation = :id_conversation
                AND conversations.id_user_2 = :id_user
            UNION
            SELECT conversations.id_user_2 as user_res
          FROM conversations
          WHERE conversations.id_conversation = :id_conversation
                AND conversations.id_user_1 = :id_user
            ");
            $stmt->bindParam(':id_user', $logged_user);
            $stmt->bindParam(':id_conversation', $conversation_id);

            $stmt->execute();

            $other_user = $stmt->fetch();

            return $other_user['user_res'];
        }
        catch(PDOException $e)
        {
            error_log("ChatDB::getOtherUserIdFromConversation() " . $conversation_id . "|" . $logged_user . " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function getAllUserConversations($user_id)
    {
        try
        {
            $stmt = $this->conn->prepare(
                "
                SELECT result.id_conversation, result.id_user, COALESCE(result.link, 'default.jpg') as link, result.username, messages.content, messages.id_emitter, to_char(messages.time_stamp, 'HH24:MI, FMDD FMMonth YYYY') as time_stamp, time_stamp as time_stamp_2
                FROM
                (
                    SELECT t1.id_conversation, t2.id_user, t2.link, t2.username, MAX(t3.id_message) as id_message
                    FROM
                    (
                        SELECT id_conversation, id_user_2
                        FROM conversations
                        WHERE id_user_1 = :user_logged_in
                        UNION
                        SELECT id_conversation, id_user_1
                        FROM conversations
                        WHERE id_user_2 = :user_logged_in
                    ) t1
                    LEFT JOIN
                    (
                        SELECT users.id_user, user_photos.link, users.username
                        FROM users
                        LEFT JOIN profiles
                        ON (profiles.id_user = users.id_user)
                        LEFT JOIN user_photos
                        ON (users.id_user_photo = user_photos.id_user_photo)
                    ) t2
                    ON t1.id_user_2 = t2.id_user
                    LEFT JOIN
                    (
                        SELECT messages.id_conversation, messages.id_message
                        FROM messages
                    ) t3
                    ON t1.id_conversation = t3.id_conversation
                    GROUP BY t1.id_conversation, t2.id_user, t2.link, t2.username
                ) as result
                INNER JOIN messages
                ON result.id_message = messages.id_message
                ORDER BY time_stamp_2 DESC
                ;
                ");

            $stmt->bindParam(':user_logged_in', $user_id);
            $stmt->execute();

            $conversations = $stmt->fetchAll();
            return $conversations;
        }
        catch(PDOException $e)
        {
            error_log("ChatDB::getAllUserConversations() " . $user_id . " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return array();
        }
    }

    function getOldMessagesFromConversation($conversation_id, $numberMsg, $last_id)
    {
        try
        {
            $stmt = $this->conn->prepare("
          SELECT messages.id_message,
                 messages.content,
                 time_stamp as time_stamp_2,
                 to_char(time_stamp, 'HH24:MI, FMDD FMMonth') as time_stamp,
                 messages.id_emitter,
                 users.username,
                 COALESCE(user_photos.link, 'default.jpg') as user_photo
          FROM messages
          LEFT JOIN users
          ON (users.id_user = messages.id_emitter)
          LEFT JOIN user_photos
          ON (user_photos.id_user_photo = users.id_user_photo)
          WHERE messages.id_conversation = :conversation_id
            AND id_message < :last_id
          ORDER BY time_stamp_2 DESC
          LIMIT :numberMsg
          ;
          ");

            $stmt->bindParam(':conversation_id', $conversation_id);
            $stmt->bindParam(':numberMsg', $numberMsg);
            $stmt->bindParam(':last_id', $last_id);

            $stmt->execute();

            $messages = $stmt->fetchAll();

            return $messages;
        }
        catch(PDOException $e)
        {
            error_log("ChatDB::getOldMessagesFromConversation() " . $conversation_id ."|".$numberMsg."|".$last_id. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return array();
        }
    }

    function getNewMessagesFromConversation($conversation_id, $last_id)
    {
        try
        {

            $stmt = $this->conn->prepare("
          SELECT messages.id_message,
                 messages.content,
                 to_char(time_stamp, 'HH24:MI, FMDD FMMonth') as time_stamp,
                 time_stamp as time_stamp_2,
                 messages.id_emitter,
                 users.username,
                 COALESCE(user_photos.link, 'default.jpg') as user_photo
          FROM messages
          LEFT JOIN users
          ON (users.id_user = messages.id_emitter)
          LEFT JOIN user_photos
          ON (user_photos.id_user_photo = users.id_user_photo)
          WHERE messages.id_conversation = :conversation_id
            AND id_message > :last_id
          ORDER BY time_stamp_2 ASC
          ;
          ");

            $stmt->bindParam(':conversation_id', $conversation_id);
            $stmt->bindParam(':last_id', $last_id);

            $stmt->execute();

            $messages = $stmt->fetchAll();

            return $messages;
        }
        catch(PDOException $e)
        {
            error_log("ChatDB::getNewMessagesFromConversation() " . $conversation_id ."|".$last_id. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return array();
        }

    }

    function getMaxIdMessage($conversation_id)
    {
        try
        {
            $stmt = $this->conn->prepare("
          SELECT MAX(messages.id_message) as max_id
          FROM messages
          WHERE messages.id_conversation = :conversation_id
          ;");
            $stmt->bindParam(':conversation_id', $conversation_id);
            $stmt->execute();

            $max = $stmt->fetchAll();


            return $max[0]["max_id"];
        }
        catch(PDOException $e)
        {
            error_log("ChatDB::getMaxIdMessage() " . $conversation_id . " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }

    }

    function sendMessage($convo_id, $emitter_id, $msg)
    {
        if($this->verifyConversation($emitter_id, $convo_id))
        {
            try
            {
                $stmt = $this->conn->prepare("INSERT INTO messages VALUES (DEFAULT, :content, CURRENT_TIMESTAMP, :id_conversation, :id_user_logged_in);");
                $stmt->bindParam(':id_conversation', $convo_id);
                $stmt->bindParam(':content', $msg);
                $stmt->bindParam(':id_user_logged_in', $emitter_id);

                $stmt->execute();

                return true;
            }
            catch(PDOException $e)
            {
                error_log("ChatDB::sendMessage() " . $convo_id."|".$emitter_id."|".$msg . " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
                return false;
            }
        }


    }

    function getConversationID($logged_user, $target)
    {
        try{
            $stmt = $this->conn->prepare("SELECT id_conversation FROM conversations WHERE id_user_1 = :logged_user AND id_user_2 = :target OR id_user_2 = :logged_user AND id_user_1 = :target");
            $stmt->bindParam(':logged_user', $logged_user);
            $stmt->bindParam(':target', $target);

            $stmt->execute();

            $conversation = $stmt->fetch();

            return $conversation['id_conversation'];
        }
        catch(PDOException $e)
        {
            error_log("ChatDB::getConversationID() " . $logged_user."|".$target. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function newConversation($logged_user, $target)
    {
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $this->conn->beginTransaction();
            $stmt = $this->conn->prepare('INSERT INTO conversations(id_user_1, id_user_2) VALUES (:logged_user, :target);');
            $stmt->execute(array($logged_user, $target));
            $stmt = $this->conn->prepare('SELECT MAX(id_conversation) as id_conversation FROM conversations;');
            $stmt->execute(array());
            $this->conn->commit();
            $conversation = $stmt->fetch();
            return $conversation['id_conversation'];

        } catch (Exception $e) {
            $this->conn->rollBack();
            error_log("ChatDB::newConversation() " . $logged_user."|".$target. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function verifyConversation($logged_user, $current_conversation)
    {
        try
        {
            $stmt = $this->conn->prepare("
              SELECT id_user_1, id_user_2
              FROM conversations
              WHERE id_conversation = :current_conversation
              ;
              ");
            $stmt->bindParam(':current_conversation', $current_conversation);
            $stmt->execute();

            $users = $stmt->fetchAll();

            if($logged_user == $users[0]["id_user_1"] || $logged_user == $users[0]["id_user_2"])
                return true;
        }
        catch(PDOException $e)
        {
            error_log("ChatDB::verifyConversation() " . $logged_user."|".$current_conversation. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function getLastMessageFromConversation($conversation_id)
    {
        try
        {
            $stmt = $this->conn->prepare("
          SELECT messages.content
          FROM final.messages
          WHERE messages.id_conversation = :conversation_id
          ORDER BY messages.time_stamp DESC
          LIMIT 1
          ;
          ");

            $stmt->bindParam(':conversation_id', $conversation_id);

            $stmt->execute();

            $messages = $stmt->fetch()["content"];

            return $messages;
        }
        catch(PDOException $e)
        {
            error_log("ChatDB::getLastMessageFromConversation() " . $conversation_id. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }

    }
}