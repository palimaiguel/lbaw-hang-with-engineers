<?php
/**
 * Created by PhpStorm.
 * User: João
 * Date: 15/05/2015
 * Time: 10:36
 */

class DashboardDB
{
    var $conn;

    function DashboardDB($conn)
    {
        $this->conn = $conn;
    }

    function getGlobalPublications($idUserLoggedIn, $numberMsg, $lastBlock)
    {
        try
        {
            $stmt = $this->conn->prepare(
                "SELECT id_publication,
                    name,
                    description,
                    visibility,
                    to_char(time_stamp, 'FMDD FMMonth YYYY @ HH24:MI') as time_stamp,
                    time_stamp as time_stamp_2,
                    users.id_user,
                    publications.id_publication_element,
                    publication_elements.link as publication_link,
                    publication_type,
                    username,
                    user_photos.link as user_photo
             FROM publications LEFT JOIN publication_elements
              ON (publications.id_publication_element = publication_elements.id_publication_element)
             LEFT JOIN users
             ON (publications.id_user = users.id_user)
             LEFT JOIN user_photos
             ON (users.id_user_photo = user_photos.id_user_photo)
             WHERE publications.id_user != :idUserLoggedIn
              AND (publications.visibility = 'GLOBAL'
              OR (publications.visibility = 'LOVE_MATCHES'
              AND :idUserLoggedIn IN (
                        SELECT id_loved
		                FROM love_matches
			            WHERE publications.id_user = love_matches.id_owner
			                          ))
	          OR (publications.visibility = 'FRIENDS'
              AND :idUserLoggedIn IN (
                        SELECT id_friend
                        FROM friends
				        WHERE publications.id_user = friends.id_owner
			                            ))
                )
            ORDER BY time_stamp_2 DESC
            LIMIT :numberMsg
            OFFSET :lastBlock;
            "
            );

            $stmt->bindParam(':idUserLoggedIn', $idUserLoggedIn);
            $stmt->bindParam(':numberMsg', $numberMsg);
            $stmt->bindParam(':lastBlock', $lastBlock);
            $stmt->execute();
            $result = $stmt->fetchall();

            return $result;
        }
        catch(PDOException $e)
        {
            error_log("DashboardDB::getGlobalPublications() " . $idUserLoggedIn."|".$numberMsg."|".$lastBlock. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function getFriendsPublications($idUserLoggedIn, $numberMsg, $lastBlock)
    {
        try
        {

            $stmt = $this->conn->prepare(
                "SELECT id_publication,
                    name,
                    description,
                    visibility,
                    to_char(time_stamp, 'FMDD FMMonth YYYY @ HH24:MI') as time_stamp,
                    time_stamp as time_stamp_2,
                    users.id_user,
                    publications.id_publication_element,
                    publication_elements.link as publication_link,
                    publication_type,
                    username,
                    user_photos.link as user_photo
             FROM publications LEFT JOIN publication_elements
              ON (publications.id_publication_element = publication_elements.id_publication_element)
             LEFT JOIN users
             ON (publications.id_user = users.id_user)
             LEFT JOIN user_photos
             ON (users.id_user_photo = user_photos.id_user_photo)
             WHERE publications.id_user != :idUserLoggedIn
              AND (publications.visibility = 'GLOBAL'
              OR (publications.visibility = 'LOVE_MATCHES'
              AND :idUserLoggedIn IN (
                        SELECT id_loved
		                FROM love_matches
			            WHERE publications.id_user = love_matches.id_owner
			                          ))
	          OR (publications.visibility = 'FRIENDS'
              AND :idUserLoggedIn IN (
                        SELECT id_friend
                        FROM friends
				        WHERE publications.id_user = friends.id_owner
			                            ))
                )
                AND publications.id_user IN (
                SELECT id_friend
                FROM friends
                WHERE friends.id_owner = :idUserLoggedIn
                )
            ORDER BY time_stamp_2 DESC
            LIMIT :numberMsg
            OFFSET :lastBlock;
            "
            );

            $stmt->bindParam(':idUserLoggedIn', $idUserLoggedIn);
            $stmt->bindParam(':numberMsg', $numberMsg);
            $stmt->bindParam(':lastBlock', $lastBlock);
            $stmt->execute();
            $result = $stmt->fetchall();

            return $result;
        }
        catch(PDOException $e)
        {
            error_log("DashboardDB::getFriendsPublications() " . $idUserLoggedIn."|".$numberMsg."|".$lastBlock. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function getLoveMatchesPublications($idUserLoggedIn, $numberMsg, $lastBlock)
    {
        try
        {

            $stmt = $this->conn->prepare(
                "SELECT id_publication,
                    name,
                    description,
                    visibility,
                    to_char(time_stamp, 'FMDD FMMonth YYYY @ HH24:MI') as time_stamp,
                    time_stamp as time_stamp_2,
                    users.id_user,
                    publications.id_publication_element,
                    publication_elements.link as publication_link,
                    publication_type,
                    username,
                    user_photos.link as user_photo
             FROM publications LEFT JOIN publication_elements
              ON (publications.id_publication_element = publication_elements.id_publication_element)
             LEFT JOIN users
             ON (publications.id_user = users.id_user)
             LEFT JOIN user_photos
             ON (users.id_user_photo = user_photos.id_user_photo)
             WHERE publications.id_user != :idUserLoggedIn
              AND (publications.visibility = 'GLOBAL'
              OR (publications.visibility = 'LOVE_MATCHES'
              AND :idUserLoggedIn IN (
                        SELECT id_loved
		                FROM love_matches
			            WHERE publications.id_user = love_matches.id_owner
			                          ))
	          OR (publications.visibility = 'FRIENDS'
              AND :idUserLoggedIn IN (
                        SELECT id_friend
                        FROM friends
				        WHERE publications.id_user = friends.id_owner
			                            ))
                )
                AND publications.id_user IN (
                SELECT id_loved
                FROM love_matches
                WHERE love_matches.id_owner = :idUserLoggedIn
                )
            ORDER BY time_stamp_2 DESC
            LIMIT :numberMsg
            OFFSET :lastBlock;
            "
            );

            $stmt->bindParam(':idUserLoggedIn', $idUserLoggedIn);
            $stmt->bindParam(':numberMsg', $numberMsg);
            $stmt->bindParam(':lastBlock', $lastBlock);
            $stmt->execute();
            $result = $stmt->fetchall();

            return $result;
        }
        catch(PDOException $e)
        {
            error_log("DashboardDB::getLoveMatchesPublications() " . $idUserLoggedIn."|".$numberMsg."|".$lastBlock. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function getNumFriends($idUserLoggedIn)
    {
        try
        {
            $stmt = $this->conn->prepare(
                "SELECT COUNT(*) as number_friends
             FROM friends
             WHERE id_owner = :idUserLoggedIn;"
            );

            $stmt->bindParam(':idUserLoggedIn', $idUserLoggedIn);
            $stmt->execute();
            $result = $stmt->fetchall();

            return $result;
        }
        catch(PDOException $e)
        {
            error_log("DashboardDB::getNumFriends() " . $idUserLoggedIn. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function getNumLoveMatches($idUserLoggedIn)
    {
        try
        {
            $stmt = $this->conn->prepare(
                "SELECT COUNT(*) as number_loved
             FROM love_matches
             WHERE id_owner = :idUserLoggedIn;"
            );

            $stmt->bindParam(':idUserLoggedIn', $idUserLoggedIn);
            $stmt->execute();
            $result = $stmt->fetchall();

            return $result;
        }
        catch(PDOException $e)
        {
            error_log("DashboardDB::getNumLoveMatches() " . $idUserLoggedIn. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function getNumMessagesSent($idUserLoggedIn)
    {
        try
        {
            $stmt = $this->conn->prepare(
                "SELECT COUNT(messages.id_message) as number_messages
             FROM conversations, messages
             WHERE (conversations.id_user_1 = :idUserLoggedIn OR conversations.id_user_2 = :idUserLoggedIn)
             AND conversations.id_conversation = messages.id_conversation
             AND messages.id_emitter = :idUserLoggedIn;
             "
            );

            $stmt->bindParam(':idUserLoggedIn', $idUserLoggedIn);
            $stmt->execute();
            $result = $stmt->fetchall();

            return $result;
        }
        catch(PDOException $e)
        {
            error_log("DashboardDB::getNumMessagesSent() " . $idUserLoggedIn. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function getAddedLoveMatches($idUserLoggedIn)
    {
        try
        {
            $stmt = $this->conn->prepare(
                "SELECT COUNT(*) as number_love_matches
             FROM love_matches
             WHERE id_loved = :idUserLoggedIn;"
            );

            $stmt->bindParam(':idUserLoggedIn', $idUserLoggedIn);
            $stmt->execute();
            $result = $stmt->fetchall();

            return $result;
        }
        catch(PDOException $e)
        {
            error_log("DashboardDB::getAddedLoveMatches() " . $idUserLoggedIn. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function getNumUnseenNotifications($idUserLoggedIn)
    {
        try
        {
            $stmt = $this->conn->prepare(
                "SELECT COUNT(*) as number_notifications_unseen
             FROM notifications
             WHERE id_user_notificated = :idUserLoggedIn
             AND seen = 'false'
             ;"
            );

            $stmt->bindParam(':idUserLoggedIn', $idUserLoggedIn);
            $stmt->execute();
            $result = $stmt->fetchall();

            return $result;
        }
        catch(PDOException $e)
        {
            error_log("DashboardDB::getNumUnseenNotifications() " . $idUserLoggedIn. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

}

