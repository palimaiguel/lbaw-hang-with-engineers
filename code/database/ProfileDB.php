<?php

class ProfileDB
{
    var $conn;

    function ProfileDB($conn)
    {
        $this->conn = $conn;
    }


    function getUserInfo($user_id)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT users.id_user AS id, users.username, COALESCE(user_photos.link, 'default.jpg') as link, countries.name AS country, users.engineer, users.admin, profiles.city, profiles.visibility FROM users LEFT JOIN profiles ON (users.id_user = profiles.id_user)  LEFT JOIN user_photos ON (users.id_user_photo = user_photos.id_user_photo) LEFT JOIN countries ON (countries.id_country = profiles.id_country) WHERE users.id_user = :user_target");
            $stmt->bindParam(":user_target", $user_id);
            $stmt->execute();

            $user = $stmt->fetch();
            return $user;
        }
        catch(PDOException $e)
        {
            error_log("ProfileDB::getUserInfo() " . $user_id. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function getUserTimeline($target, $logged_user)
    {
        try
        {
            if($target == $logged_user)
            {
                $stmt = $this->conn->prepare("SELECT name AS title, description AS content, to_char(time_stamp, 'HH24:MI') AS time, to_char(time_stamp, 'FMDD FMMonth YYYY') AS date, publication_elements.link AS publication_link, publication_elements.publication_type AS publication_type
                                              FROM publications LEFT JOIN publication_elements ON (publications.id_publication_element = publication_elements.id_publication_element)
                                              WHERE publications.id_user = :id_user
                                              ORDER BY time_stamp DESC");
                $stmt->bindParam(":id_user", $target);

                $stmt->execute();
            }
            else
            {
                $stmt = $this->conn->prepare("SELECT name AS title, description AS content, to_char(time_stamp, 'HH24:MI') AS time, to_char(time_stamp, 'FMDD FMMonth YYYY') AS date, publication_elements.link AS publication_link, publication_elements.publication_type AS publication_type
                                              FROM publications LEFT JOIN publication_elements ON (publications.id_publication_element = publication_elements.id_publication_element)
                                              WHERE publications.id_user = :id_target AND (publications.visibility = 'GLOBAL' OR (publications.id_user = :user_logged_in) OR (publications.visibility = 'LOVE_MATCHES'
                                              AND :user_logged_in IN (SELECT id_loved FROM love_matches WHERE publications.id_user = love_matches.id_owner)) OR (publications.visibility = 'FRIENDS'
                                              AND :user_logged_in IN (SELECT id_friend FROM friends WHERE publications.id_user = friends.id_owner))) ORDER BY time_stamp DESC");
                $stmt->bindParam(":id_target", $target);
                $stmt->bindParam(":user_logged_in", $logged_user);

                $stmt->execute();
            }

            $timeline = $stmt->fetchAll();
            return $timeline;
        }
        catch(PDOException $e)
        {
            error_log("ProfileDB::getUserTimeline() " . $target."|".$logged_user. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function getUserAboutMe($target, $logged_user)
    {
        try
        {
            if($target == $logged_user)
            {
                $stmt = $this->conn->prepare("SELECT date_part('year',age(birth_date)) AS age, gender, city, id_country AS country, sexual_orientation, job, course, university, musics, movies, books, others FROM profiles, users WHERE profiles.id_user = :id_user AND users.id_user = :id_user");
                $stmt->bindParam(":id_user", $target);

                $stmt->execute();
            }
            else
            {
                $stmt = $this->conn->prepare("SELECT date_part('year',age(birth_date)) AS age, gender, city, id_country AS country, sexual_orientation, job, course, university, musics, movies, books, others FROM profiles, users WHERE profiles.id_user = :id_user AND users.id_user = :id_user AND (profiles.visibility = 'GLOBAL' OR (profiles.visibility = 'LOVE_MATCHES' AND :user_logged_in IN (SELECT id_loved FROM love_matches WHERE profiles.id_user = love_matches.id_owner)) OR (profiles.visibility = 'FRIENDS' AND :user_logged_in IN (SELECT id_friend FROM friends WHERE profiles.id_user = friends.id_owner)))");
                $stmt->bindParam(":id_user", $target);
                $stmt->bindParam(":user_logged_in", $logged_user);

                $stmt->execute();
            }

            $about_me = $stmt->fetch();
            return $about_me;
        }
        catch(PDOException $e)
        {
            error_log("ProfileDB::getUserAboutMe() " . $target."|".$logged_user. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }

    }

    function getGalleryPhotoLink($id)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT link FROM photos WHERE id_photo = :id;");
            $stmt->bindParam(":id", $id);

            $stmt->execute();
            return $stmt->fetch()['link'];
        }
        catch(PDOException $e)
        {
            error_log("ProfileDB::getGalleryPhotoLink() " . $id. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function getUserPhotos($target, $logged_user)
    {
        try
        {
            if($target == $logged_user)
            {
                $stmt = $this->conn->prepare("SELECT * FROM photos WHERE photos.id_user = :id_user");
                $stmt->bindParam(":id_user", $target);

                $stmt->execute();
            }
            else
            {
                $stmt = $this->conn->prepare("SELECT * FROM photos WHERE photos.id_user = :id_user AND (photos.visibility = 'GLOBAL' OR (photos.visibility = 'LOVE_MATCHES'  AND :user_logged_in IN (SELECT id_loved FROM love_matches WHERE photos.id_user = love_matches.id_owner)) OR (photos.visibility = 'FRIENDS'  AND :user_logged_in IN (SELECT id_friend FROM friends WHERE photos.id_user = friends.id_owner)))");
                $stmt->bindParam(":id_user", $target);
                $stmt->bindParam(":user_logged_in", $logged_user);

                $stmt->execute();
            }

            $photos = $stmt->fetchAll();
            return $photos;
        }
        catch(PDOException $e)
        {
            error_log("ProfileDB::getUserPhotos() " . $target."|".$logged_user. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return array();
        }
    }

    function newConversation($id_myuser, $id_user)
    {
        try
        {
            $stmt = $this->conn->prepare("INSERT INTO conversations(id_user_1, id_user_2) VALUES (:id_user_1, :id_user_2);");
            $stmt->bindParam(":id_user_1", $id_myuser);
            $stmt->bindParam(":id_user_2", $id_user);

            $stmt->execute();

            return true;
        }
        catch(PDOException $e)
        {
            error_log("ProfileDB::newConversation() " . $id_myuser."|".$id_user. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function banUser($logged_user, $target)
    {
        if($this->isAdmin($logged_user)) {
            try {
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                $this->conn->beginTransaction();
                $stmt = $this->conn->prepare('UPDATE users SET banned = TRUE WHERE id_user = ?;');
                $stmt->execute(array($target));
                $stmt = $this->conn->prepare('DELETE FROM friends WHERE id_owner = ? OR id_friend = ?;');
                $stmt->execute(array($target, $target));
                $stmt = $this->conn->prepare('DELETE FROM love_matches WHERE id_owner = ? OR id_loved = ?;');
                $stmt->execute(array($target, $target));
                $stmt = $this->conn->prepare('DELETE FROM notifications WHERE id_user_notificated = ? OR id_user_sent = ?;');
                $stmt->execute(array($target, $target));
                $stmt = $this->conn->prepare('DELETE FROM photos WHERE id_user = ?;');
                $stmt->execute(array($target));
                $stmt = $this->conn->prepare('DELETE FROM profiles WHERE id_user = ?;');
                $stmt->execute(array($target));
                $stmt = $this->conn->prepare('DELETE FROM publication_elements WHERE id_publication_element IN (SELECT id_publication_element FROM publications WHERE id_user = ?);');
                $stmt->execute(array($target));
                $stmt = $this->conn->prepare('DELETE FROM publications WHERE id_user = ?;');
                $stmt->execute(array($target));
                $stmt = $this->conn->prepare('DELETE FROM messages WHERE id_conversation IN (SELECT id_conversation FROM conversations WHERE id_user_1 = ? OR id_user_2 = ?);');
                $stmt->execute(array($target, $target));
                $stmt = $this->conn->prepare('DELETE FROM conversations WHERE id_user_1 = ? OR id_user_2 = ?;');
                $stmt->execute(array($target, $target));
                $this->conn->commit();
                return true;

            } catch (Exception $e) {
                $this->conn->rollBack();
                error_log("ProfileDB::banUser() " . $logged_user."|".$target. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
                return false;
            }
        }
    }

    function isAdmin($user)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM users WHERE id_user = :user AND admin = TRUE;");
            $stmt->bindParam(':user', $user);
            $stmt->execute();

            $result = $stmt->fetch() == true;

            return $result;
        }
        catch(PDOException $e)
        {
            error_log("ProfileDB::isAdmin() " . $user. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function getAdmins()
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT id_user as id, username FROM users WHERE admin = TRUE AND banned = FALSE;");
            $stmt->execute();

            $result = $stmt->fetchAll();

            return $result;
        }
        catch(PDOException $e)
        {
            error_log("ProfileDB::getAdmins() "." --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function isBanned($user)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM users WHERE id_user = :user AND banned = TRUE;");
            $stmt->bindParam(':user', $user);
            $stmt->execute();

            $result = $stmt->fetch() == true;

            return $result;
        }
        catch(PDOException $e)
        {
            error_log("ProfileDB::isBanned() ". $user ." --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function getCountryList()
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT id_country as id, name FROM countries");
            $stmt->execute();

            $countries = $stmt->fetchAll();

            return $countries;
        }
        catch(PDOException $e)
        {
            error_log("ProfileDB::getCountryList() ". " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function newPublication($logged_user, $title, $text, $visibility)
    {
        try
        {
            $stmt = $this->conn->prepare("INSERT INTO publications (name, description, visibility, id_user) VALUES (:title, :text, :visibility, :id_myuser);");
            $stmt->bindParam(":id_myuser", $logged_user);
            $stmt->bindParam(":title", $title);
            $stmt->bindParam(":text", $text);
            $stmt->bindParam(":visibility", $visibility);

            $stmt->execute();

            return true;
        }
        catch(PDOException $e)
        {
            error_log("ProfileDB::newPublication() ". $logged_user."|".$title."|".$text."|".$visibility."|"." --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function newPublicationWithElem($logged_user, $title, $text, $visibility, $type, $link)
    {
        try
        {
            $this->conn->beginTransaction();

            $stmt = $this->conn->prepare("INSERT INTO publication_elements (link, publication_type)
                                          VALUES (:link, :type);");
            $stmt->bindParam(":link", $link);
            $stmt->bindParam(":type", $type);

            $stmt->execute();

            $stmt = $this->conn->prepare("INSERT INTO publications (name, description, visibility, id_user, id_publication_element)
                                          SELECT :title, :text, :visibility, :id_myuser, MAX(publication_elements.id_publication_element)
                                          FROM publication_elements;");
            $stmt->bindParam(":id_myuser", $logged_user);
            $stmt->bindParam(":title", $title);
            $stmt->bindParam(":text", $text);
            $stmt->bindParam(":visibility", $visibility);

            $stmt->execute();

            $this->conn->commit();

            return true;
        }
        catch(PDOException $e)
        {
            $this->conn->rollBack();
            error_log("ProfileDB::newPublicationWithElem() ". $logged_user."|".$title."|".$text."|".$visibility."|".$type."|".$link." --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function newGalleryPhoto($logged_user, $title, $description, $visibility, $imageName)
    {
        try
        {
            $stmt = $this->conn->prepare("INSERT INTO photos(name, description, link, visibility, id_user) VALUES (:name, :description, :link, :visibility, :id_myuser);");
            $stmt->bindParam(":name", $title);
            $stmt->bindParam(":description", $description);
            $stmt->bindParam(":link", $imageName);
            $stmt->bindParam(":visibility", $visibility);
            $stmt->bindParam(":id_myuser", $logged_user);

            $stmt->execute();

            return true;
        }
        catch(PDOException $e)
        {
            $this->conn->rollBack();
            error_log("ProfileDB::newGalleryPhoto() ". $logged_user."|".$title."|".$description."|".$visibility."|".$imageName . " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function deleteGalleryPhoto($id)
    {
        try
        {
            $stmt = $this->conn->prepare("DELETE FROM photos WHERE id_photo = :id;");
            $stmt->bindParam(":id", $id);
            $stmt->execute();

            return true;
        }
        catch(PDOException $e)
        {
            $this->conn->rollBack();
            error_log("ProfileDB::deleteGalleryPhoto() ". $id. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function updateAboutMe($logged_user,$gender,$sexual_orientation,$city,$country,$job,$education,$school,$music,$movies,$books,$others)
    {
        try
        {
            $stmt = $this->conn->prepare("UPDATE profiles SET gender=:gender, sexual_orientation=:sexual_orientation, job=:job, city=:city, id_country=:id_country, course=:education, university=:school, movies=:movies, books=:books, musics=:music, others=:others WHERE id_user = :id_user;");
            $stmt->bindParam(":id_user", $logged_user);
            $stmt->bindParam(":gender", $gender);
            $stmt->bindParam(":sexual_orientation", $sexual_orientation);
            $stmt->bindParam(":job", $job);
            $stmt->bindParam(":city", $city);
            if ($country === 'null')
            {
                $myNull = null;
                $stmt->bindParam(":id_country", $myNull, PDO::PARAM_NULL);
            }
            else
                $stmt->bindParam(":id_country", $country);
            $stmt->bindParam(":education", $education);
            $stmt->bindParam(":school", $school);
            $stmt->bindParam(":movies", $movies);
            $stmt->bindParam(":music", $music);
            $stmt->bindParam(":books", $books);
            $stmt->bindParam(":others", $others);

            $stmt->execute();

            return true;
        }
        catch(PDOException $e)
        {
            error_log("ProfileDB::updateAboutMe() ". $logged_user."|".$gender."|".$sexual_orientation."|".$city."|".$country."|".$job."|".$education."|".$school."|".$music."|".$movies."|".$books."|".$others. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function updateProfileVisibility($id_user, $visibility)
    {
        try
        {
            $stmt = $this->conn->prepare("UPDATE profiles SET visibility=:visibility WHERE id_user = :id_user;");
            $stmt->bindParam(":visibility", $visibility);
            $stmt->bindParam(":id_user", $id_user);

            $stmt->execute();

            return true;
        }
        catch(PDOException $e)
        {
            error_log("ProfileDB::updateProfileVisibility() ". $id_user . "|" . $visibility. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function updateProfilePhoto($id_user, $newPhoto)
    {

        try
        {
            $stmt = $this->conn->prepare("UPDATE user_photos SET link=:link WHERE id_user_photo IN
                              (SELECT users.id_user_photo FROM users WHERE id_user = :id_user)");
            $stmt->bindParam(":link", $newPhoto);
            $stmt->bindParam(":id_user", $id_user);

            $stmt->execute();

            return true;
        }
        catch(PDOException $e)
        {
            error_log("ProfileDB::updateProfileVisibility() ". $id_user . "|" . $newPhoto. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }
}

