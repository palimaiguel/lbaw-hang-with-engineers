<?php
require_once('AuthDB.php');
require_once('ChatDB.php');
require_once('CirclesDB.php');
require_once('DashboardDB.php');
require_once('NotificationsDB.php');
require_once('ProfileDB.php');
require_once('SearchDB.php');

class Database
{
    var $conn;
    var $authDB;
    var $chatDB;
    var $circlesDB;
    var $dashboardDB;
    var $notificationsDB;
    var $profileDB;
    var $searchDB;

    function Database()
    {
        $this->conn = new PDO('pgsql:host=vdbm;dbname=lbaw1412', 'lbaw1412', 'tD739fo1');
        $this->conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $this->conn->exec('SET SCHEMA \'final\'');

        $this->authDB = new AuthDB($this->conn);
        $this->chatDB = new ChatDB($this->conn);
        $this->circlesDB = new CirclesDB($this->conn);
        $this->dashboardDB = new DashboardDB($this->conn);
        $this->notificationsDB = new NotificationsDB($this->conn);
        $this->profileDB = new ProfileDB($this->conn);
        $this->searchDB = new SearchDB($this->conn);
    }

    function getProfilePicFromEmail($email)
    {
        $stmt = $this->conn->prepare("SELECT user_photos.link FROM users, user_photos
                                      WHERE users.email = :email AND users.id_user_photo = user_photos.id_user_photo");
        $stmt->bindParam(':email', $email);

        $stmt->execute();

        $photo = $stmt->fetch()['link'];

        return $photo;
    }

    function getConversationID($id_myuser, $id_user)
    {
        $stmt = $this->conn->prepare("SELECT id_conversation FROM conversations WHERE (id_user_1 = :id_user_1 AND id_user_2 = :id_user_2) OR (id_user_1 = :id_user_2 OR id_user_2 = :id_user_1)");
        $stmt->bindParam(":id_user_1", $id_myuser, PDO::PARAM_INT);
        $stmt->bindParam(":id_user_2", $id_user, PDO::PARAM_INT);

        $stmt->execute();

        $conversationId = $stmt->fetch()["id_conversation"];

        return $conversationId;
    }

    function isAdmin($id_user)
    {
        $stmt = $this->conn->prepare("SELECT admin FROM users WHERE id_user = :id_user;");
        $stmt->bindParam(":id_user", $id_user, PDO::PARAM_INT);

        $stmt->execute();

        $isAdmin = $stmt->fetch()["admin"];

        return $isAdmin;
    }
}
