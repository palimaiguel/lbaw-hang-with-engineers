<?php

class CirclesDB
{
    var $conn;

    function CirclesDB($conn)
    {
        $this->conn = $conn;
    }

    function getFriends($id_user)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT id_friend AS id,
                                    users.username AS username,
                                    user_photos.link AS profile_pic,
                                    countries.name AS country,
                                    city,
                                    users.engineer AS engineer
                                    FROM friends
                                    LEFT JOIN users ON friends.id_friend = users.id_user
                                    LEFT JOIN profiles ON users.id_user = profiles.id_user
                                    LEFT JOIN user_photos ON users.id_user_photo = user_photos.id_user_photo
                                    LEFT JOIN countries ON profiles.id_country = countries.id_country
                                    WHERE id_owner = :id_user;");
            $stmt->bindParam("id_user", $id_user, PDO::PARAM_INT);

            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        }
        catch(PDOException $e)
        {
            error_log("CirclesDB::getFriends() " . $id_user. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function getLoveMatches($id_user)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT id_loved AS id,
                            users.username AS username,
                            user_photos.link AS profile_pic,
                            countries.name AS country,
                            city,
                            users.engineer AS engineer
                            FROM love_matches INNER JOIN users
                            ON love_matches.id_loved = users.id_user
                            LEFT JOIN profiles
                            ON users.id_user = profiles.id_user
                            LEFT JOIN user_photos
                            ON users.id_user_photo = user_photos.id_user_photo
                            LEFT JOIN countries
                            ON profiles.id_country = countries.id_country
                            WHERE id_owner = :id_user;");
            $stmt->bindParam("id_user", $id_user, PDO::PARAM_INT);

            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        }
        catch(PDOException $e)
        {
            error_log("CirclesDB::getLoveMatches() " . $id_user. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function removeFromCircle($circleType, $loggedUser, $idUser)
    {
        try
        {
            if ($circleType === 'friends')
                $stmt = $this->conn->prepare("DELETE FROM friends
                            WHERE id_owner = :loggedUser AND id_friend = :idUser;");
            else if ($circleType === 'love_matches')
                $stmt = $this->conn->prepare("DELETE FROM love_matches
                            WHERE id_owner = :loggedUser AND id_loved = :idUser;");
            else
                throw new PDOException("CirclesDB::removeFromCircle - Invalid circleType: " . $circleType);
            $stmt->bindParam("loggedUser", $loggedUser, PDO::PARAM_INT);
            $stmt->bindParam("idUser", $idUser, PDO::PARAM_INT);

            $stmt->execute();

            return true;
        }
        catch (PDOException $e) {
            error_log("CirclesDB::removeFromCircle() " . $circleType ."|". $loggedUser ."|". $idUser. " --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function addFriends($logged_user, $target)
    {
        try {
            $stmt = $this->conn->prepare("INSERT INTO friends VALUES (:logged_user, :target);");
            $stmt->bindParam(':logged_user', $logged_user);
            $stmt->bindParam(':target', $target);

            $stmt->execute();

            return true;
        } catch (PDOException $e) {
            error_log("CirclesDB::addFriends() " . $logged_user ."|". $target ." --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

    function addLovematch($logged_user, $target)
    {
        try {
            $stmt = $this->conn->prepare("INSERT INTO love_matches VALUES (:logged_user, :target);");
            $stmt->bindParam(':logged_user', $logged_user);
            $stmt->bindParam(':target', $target);

            $stmt->execute();

            return true;
        } catch (PDOException $e) {
            error_log("CirclesDB::addLovematch() " . $logged_user ."|". $target ." --> " . $e->getMessage() . "\n", 3, "/opt/lbaw/lbaw1412/error.log");
            return false;
        }
    }

}