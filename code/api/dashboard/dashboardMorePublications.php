<?php
require_once('../../config/init.php');

if(!isset($_GET['group']) || !isset($_GET['limit']) || !isset($_GET['offset']))
{
    $answer = array();
    $answer["error"] = "Missing paremeter";
    echo json_encode($answer);
    exit;
}

$group = $_GET['group'];
$limit = $_GET['limit'];
$offset = $_GET['offset'];

$logged_user = getLoggedinId();

if($logged_user === -1)
    header('Location: home.php');
else {

// Find users in the database:
    $dashboardDB = $database->dashboardDB;
    $publications = array();

    if ($group == 1)
        $publications = $dashboardDB->getGlobalPublications($logged_user, $limit, $offset);
    else if ($group == 2)
        $publications = $dashboardDB->getFriendsPublications($logged_user, $limit, $offset);
    else if ($group == 3)
        $publications = $dashboardDB->getLoveMatchesPublications($logged_user, $limit, $offset);

// Transform result in json format:
    $answer = array();
    $answer["publications"] = $publications;

    $answer = json_encode($answer);

    echo $answer;
}