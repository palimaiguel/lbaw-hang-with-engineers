<?php
require_once('../../config/init.php');

if (getLoggedinId() === -1)
{
    header("Location: " .  $BASE_URL . 'pages/home.php');
    exit;
}

if(!isset($_GET['keyword']))
{
    $answer = array();
    $answer["error"] = "Missing paremeter";
    echo json_encode($answer);
    exit;
}

$keyword = $_GET['keyword'];

// Find auth in the database:
$searchDB = $database->searchDB;
$userCount = $searchDB->searchUsersCount($keyword);

// Transform result in json format:
$answer = array();
$answer["user_count"] = $userCount;

$answer = json_encode($answer);

echo $answer;