<?php
require_once('../../config/init.php');

if (getLoggedinId() === -1)
{
    header("Location: " .  $BASE_URL . 'pages/home.php');
    exit;
}

if(!isset($_GET['keyword']) || !isset($_GET['limit']) || !isset($_GET['offset']))
{
    $answer = array();
    $answer["error"] = "Missing paremeter";
    echo json_encode($answer);
    exit;
}

$keyword = $_GET['keyword'];
$limit = $_GET['limit'];
$offset = $_GET['offset'];

// Find users in the database:
$searchDB = $database->searchDB;
$users = $searchDB->searchUsers($keyword, $limit, $offset);
$userCount = count($users);

$baseProfileUrl = $BASE_URL . 'pages/profile/profile.php?id=';

// Transform result in json format:
$answer = array();
$answer["keyword"] = $keyword;
$answer["base_profile_url"] = $baseProfileUrl;
$answer["auth"] = $users;
$answer["user_count"] = $userCount;

$answer = json_encode($answer);

echo $answer;