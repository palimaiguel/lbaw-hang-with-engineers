<?php
require_once('../../config/init.php');

$circlesDB = $database->circlesDB;

$loggedUser = getLoggedinId();

if ($loggedUser === -1)
{
    http_response_code(401);
    exit;
}

if (!isset($_POST['type']) || !isset($_POST['idUser']))
{
    http_response_code(400);
    exit;
}

$circleType = $_POST['type'];
$idUserInCircle = $_POST['idUser'];

if ($circleType === 'love_matches' || $circleType === 'friends')
{
    if ($circlesDB->removeFromCircle($circleType, $loggedUser, $idUserInCircle))
        http_response_code(200);
    else
        http_response_code(404);
}
else
    http_response_code(400);

?>