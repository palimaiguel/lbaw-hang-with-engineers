<?php
require_once('../../config/init.php');

if (getLoggedinId() === -1)
{
    header("Location: " .  $BASE_URL . 'pages/home.php');
    exit;
}

$chatDB = $database->chatDB;
$notificationsDB = $database->notificationsDB;


if(isset($_POST["content"]) && isset($_POST["conversation"])) {
    $content = strip_tags($_POST["content"]);
    $conversationID = strip_tags($_POST["conversation"]);
    $logged_user = getLoggedinId();

    $chatDB->sendMessage($conversationID, $logged_user, $content);
    $logged_user = getLoggedinId();

    $other_user = $chatDB->getOtherUserIdFromConversation($conversationID, $logged_user);
    $notificationsDB->insertMessageNotification($other_user, $logged_user);

    die(json_encode(array('status' => 'success')));
}
else
    die( json_encode( array( 'status' => 'error' ) ) );
