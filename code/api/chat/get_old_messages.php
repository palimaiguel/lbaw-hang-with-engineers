<?php
require_once('../../config/init.php');

if (getLoggedinId() === -1)
{
    header("Location: " .  $BASE_URL . 'pages/home.php');
    exit;
}

$chatDB = $database->chatDB;

if(isset($_GET["lastId"]) && isset($_GET["conversation"]))
{
    $lastID = strip_tags($_GET["lastId"]);
    $conversationID = strip_tags($_GET["conversation"]);
    $logged_user = getLoggedinId();

    $numberMessages = 10;

    $messages = $chatDB->getOldMessagesFromConversation($conversationID, $numberMessages, $lastID);

    if(count($messages) == 0)
        die( json_encode( array( 'status' => 'no-results', 'lastId' => $lastID) ));
    else
    {
        $newLastID = $messages[count($messages) - 1]['id_message'];
        die( json_encode( array( 'status' => 'results', 'logged_user' => $logged_user, 'lastId' => $newLastID, 'data' => $messages)) );

    }

}
else
    die( json_encode( array( 'status' => 'error' ) ) );
