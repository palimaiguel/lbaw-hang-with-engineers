<?php
require_once('../../config/init.php');

$logged_user = getLoggedinId();
if($logged_user === -1)
{
    header('Location: home.php');
    exit;
}

if (!isset($_GET['last_id']))
{
    http_response_code(400);
    exit;
}

$lastId = strip_tags($_GET['last_id']);

$notificationsDB = $database->notificationsDB;
$results = $notificationsDB->getNewMessageNotifications($logged_user, $lastId);
if($results === false)
{
    http_response_code(400);
    exit;
}

$chatDB = $database->chatDB;
foreach($results as &$result)
{
    $target = $result['id_user_sent'];

    $conversationID = $chatDB->getConversationID($logged_user, $target);
    $lastMessage = $chatDB->getLastMessageFromConversation($conversationID);

    $result['conversation_id'] = $conversationID;
    $result['last_message'] = $lastMessage;
}

if(count($results) == 0)
{
    echo json_encode(
        array(
            'status' => 'no-results',
            'last_id' => $lastId
        )
    );
}
else
{
    echo json_encode(
        array(
            'status' => 'results',
            'last_id' => $lastId,
            'results' => $results
        )
    );
}