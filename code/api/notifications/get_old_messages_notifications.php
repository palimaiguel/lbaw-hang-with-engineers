<?php
require_once('../../config/init.php');

$logged_user = getLoggedinId();
if($logged_user === -1)
{
    header('Location: home.php');
    exit;
}

if (!isset($_GET['offset']) || !isset($_GET['limit']))
{
    http_response_code(400);
    exit;
}

$offset = strip_tags($_GET['offset']);
$limit = strip_tags($_GET['limit']);

$notificationsDB = $database->notificationsDB;
$results = $notificationsDB->getMessageNotifications($logged_user, $offset, $limit);
if($results === false)
{
    http_response_code(400);
    exit;
}

$chatDB = $database->chatDB;
foreach($results as &$result)
{
    $target = $result['id_user_sent'];

    $conversationID = $chatDB->getConversationID($logged_user, $target);
    $lastMessage = $chatDB->getLastMessageFromConversation($conversationID);

    $result['conversation_id'] = $conversationID;
    $result['last_message'] = $lastMessage;
}

echo json_encode($results);