<?php
require_once('../../config/init.php');

$logged_user = getLoggedinId();
if($logged_user === -1)
{
    header('Location: home.php');
    exit;
}

if (!isset($_GET['offset']) || !isset($_GET['limit']))
{
    http_response_code(400);
    exit;
}

$offset = strip_tags($_GET['offset']);
$limit = strip_tags($_GET['limit']);

$notificationsDB = $database->notificationsDB;
$result = $notificationsDB->getCirclesNotifications($logged_user, $offset, $limit);

echo json_encode($result);