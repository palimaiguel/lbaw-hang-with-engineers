<?php
require_once('../../config/init.php');

$logged_user = getLoggedinId();
if($logged_user === -1)
{
    header('Location: home.php');
    exit;
}

if (!isset($_POST['last_id']))
{
    http_response_code(400);
    exit;
}

$lastId = strip_tags($_POST['last_id']);

$notificationsDB = $database->notificationsDB;
$notificationsDB->readMessagesNotifications($logged_user, $lastId);