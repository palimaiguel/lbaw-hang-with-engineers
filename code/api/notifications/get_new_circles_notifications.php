<?php
require_once('../../config/init.php');

$logged_user = getLoggedinId();
if($logged_user === -1)
{
    header('Location: home.php');
    exit;
}

if (!isset($_GET['last_id']))
{
    http_response_code(400);
    exit;
}

$lastId = strip_tags($_GET['last_id']);

$notificationsDB = $database->notificationsDB;
$result = $notificationsDB->getNewCirclesNotifications($logged_user, $lastId);

if(count($result) == 0)
{
    echo json_encode(
        array(
            'status' => 'no-results',
            'last_id' => $lastId
        )
    );
}
else
{
    echo json_encode(
        array(
            'status' => 'results',
            'last_id' => $lastId,
            'results' => $result
        )
    );
}