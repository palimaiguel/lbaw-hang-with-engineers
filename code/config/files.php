<?php

// Returns:
// -> -2 error (invalid image sent)
// -> -1 error (image too big)
// -> 0 OK
// -> 1 No photo
function processImage($image)
{
    if ($image["error"] !== UPLOAD_ERR_OK && $image["error"] !== UPLOAD_ERR_NO_FILE)
    {
        $_SESSION['error_messages'][] = "Error during file sending (maybe it's too big or you didn't sent one?)!";
        return -1;
    }

    // There is a file uploaded! Lets analyze it:
    if (is_uploaded_file($image["tmp_name"]))
    {
        if (getimagesize($image["tmp_name"]) === false)
        {
            $_SESSION['error_messages'][] = "Invalid image file sent!";
            return -2;
        }

        if (exif_imagetype($image["tmp_name"]) !== IMAGETYPE_JPEG &&
            exif_imagetype($image["tmp_name"]) !== IMAGETYPE_PNG)
        {
            $_SESSION['error_messages'][] = "Unsupported file type!";
            return -2;
        }

        if ($image["size"] > 2000000)
        {
            $_SESSION['error_messages'][] = "Image is too big!";
            return -1;
        }
    }
    else
        return 1;

    return 0;
}


function generateProfileImage($tmpPath, $filePath)
{
    if (exif_imagetype($tmpPath) == IMAGETYPE_JPEG)
        $imageOriginal = imagecreatefromjpeg($tmpPath);
    else if (exif_imagetype($tmpPath) == IMAGETYPE_PNG)
        $imageOriginal = imagecreatefrompng($tmpPath);
    else
    {
        $_SESSION['error_messages'][] = "Image type not supported!";
        return false;
    }

    $width = imagesx($imageOriginal);
    $height = imagesy($imageOriginal);
    $square = min($width, $height);

    $thumbnail = imagecreatetruecolor(80, 80);
    imagecopyresampled($thumbnail, $imageOriginal, 0, 0, ($width>$square)?($width-$square)/2:0, ($height>$square)?($height-$square)/2:0, 80, 80, $square, $square);
    imagejpeg($thumbnail, $filePath);

    return true;
}

function generateElementImage($tmpPath, $filePath)
{
    if (exif_imagetype($tmpPath) == IMAGETYPE_JPEG)
        $imageOriginal = imagecreatefromjpeg($tmpPath);
    else if (exif_imagetype($tmpPath) == IMAGETYPE_PNG)
        $imageOriginal = imagecreatefrompng($tmpPath);
    else
    {
        $_SESSION['error_messages'][] = "Image type not supported!";
        return false;
    }

    $width = imagesx($imageOriginal);
    $height = imagesy($imageOriginal);

    $mediumwidth = $width;
    $mediumheight = $height;

    if ($mediumwidth > 400)
    {
        $mediumwidth = 400;
        $mediumheight = $mediumheight * ( $mediumwidth / $width );
    }

    $finalImage = imagecreatetruecolor($mediumwidth, $mediumheight);
    imagecopyresampled($finalImage, $imageOriginal, 0, 0, 0, 0, $mediumwidth, $mediumheight, $width, $height);
    imagejpeg($finalImage, $filePath);

    return true;
}

function generateGalleryImage($tmpPath, $filePath)
{
    if (exif_imagetype($tmpPath) == IMAGETYPE_JPEG)
        $imageOriginal = imagecreatefromjpeg($tmpPath);
    else if (exif_imagetype($tmpPath) == IMAGETYPE_PNG)
        $imageOriginal = imagecreatefrompng($tmpPath);
    else
    {
        $_SESSION['error_messages'][] = "Image type not supported!";
        return false;
    }

    $width = imagesx($imageOriginal);
    $height = imagesy($imageOriginal);

    $mediumwidth = $width;
    $mediumheight = $height;

    if ($mediumwidth > 800)
    {
        $mediumwidth = 800;
        $mediumheight = $mediumheight * ( $mediumwidth / $width );
    }

    $finalImage = imagecreatetruecolor($mediumwidth, $mediumheight);
    imagecopyresampled($finalImage, $imageOriginal, 0, 0, 0, 0, $mediumwidth, $mediumheight, $width, $height);
    imagejpeg($finalImage, $filePath);

    return true;
}

function generateGalleryThumb($tmpPath, $filePath)
{
    if (exif_imagetype($tmpPath) == IMAGETYPE_JPEG)
        $imageOriginal = imagecreatefromjpeg($tmpPath);
    else if (exif_imagetype($tmpPath) == IMAGETYPE_PNG)
        $imageOriginal = imagecreatefrompng($tmpPath);
    else
    {
        $_SESSION['error_messages'][] = "Image type not supported!";
        return false;
    }

    $width = imagesx($imageOriginal);
    $height = imagesy($imageOriginal);

    $mediumwidth = $width;
    $mediumheight = $height;

    if ($mediumwidth > 200)
    {
        $mediumwidth = 200;
        $mediumheight = $mediumheight * ( $mediumwidth / $width );
    }

    $finalImage = imagecreatetruecolor($mediumwidth, $mediumheight);
    imagecopyresampled($finalImage, $imageOriginal, 0, 0, 0, 0, $mediumwidth, $mediumheight, $width, $height);
    imagejpeg($finalImage, $filePath);

    return true;
}