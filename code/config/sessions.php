<?php

function startSession()
{
//    session_name("Hang with Engineers");
//    session_set_cookie_params(0, '/~lbaw1412/final/', 'gnomo.up.pt', false, true);
//    session_start();
    session_set_cookie_params(0, '/~lbaw1412');
    session_start();
}

startSession();

function destroySession()
{
    session_unset();
    session_destroy();
}

function getLoggedinId()
{
    if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true)
        return $_SESSION['id'];
    else
        return -1;
}

function getLoggedinUsername()
{
    if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true)
        return $_SESSION['username'];
    else
        return -1;
}

function getLoggedinEmail()
{
    if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true)
        return $_SESSION['email'];
    else
        return -1;
}

function setLoggedinUser($id, $email, $username)
{
    $_SESSION['loggedin'] = true;
    $_SESSION['id'] = $id;
    $_SESSION['email'] = $email;
    $_SESSION['username'] = $username;
}
