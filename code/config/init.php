<?php
//session_set_cookie_params(3600, '/~lbaw1412');
//session_start();

// Initiate global variables:
error_reporting(E_ERROR | E_WARNING); // E_NOTICE by default
$BASE_DIR = '/opt/lbaw/lbaw1412/public_html/final/';
$BASE_URL = '/~lbaw1412/final/';

// Initiate sessions:
require_once($BASE_DIR . 'config/sessions.php');

// Initiate DataBase:
require_once($BASE_DIR . "database/Database.php");
$database = new Database();

require_once($BASE_DIR . 'lib/smarty/Smarty.class.php');

// Smarty initiation => global variables in templates:
$smarty = new Smarty;
$smarty->template_dir = $BASE_DIR . 'templates/';
$smarty->compile_dir = $BASE_DIR . 'templates_c/';
$smarty->assign('BASE_URL', $BASE_URL);
  
$smarty->assign('ERROR_MESSAGES', $_SESSION['error_messages']);
$smarty->assign('FIELD_ERRORS', $_SESSION['field_errors']);
$smarty->assign('SUCCESS_MESSAGES', $_SESSION['success_messages']);
$smarty->assign('FORM_VALUES', $_SESSION['form_values']);
$smarty->assign('USERNAME', $_SESSION['username']);

unset($_SESSION['field_errors']);
unset($_SESSION['form_values']);