-- Number of registered users:
CREATE OR REPLACE FUNCTION count_registered_users()
RETURNS INTEGER AS $number_registered_users$
DECLARE
	number_registered_users INTEGER;
BEGIN
	SELECT COUNT(*) INTO number_registered_users FROM users;
	RETURN number_registered_users;
END;
$number_registered_users$ LANGUAGE plpgsql;

-- Count how many users add another to love_matches
CREATE OR REPLACE FUNCTION get_love_matches_counter(id_user_logged_in INTEGER)
RETURNS INTEGER AS $number_love_matches$
DECLARE
	number_love_matches INTEGER;
BEGIN
	SELECT COUNT(*) INTO number_love_matches 
	FROM love_matches
	WHERE id_loved = id_user_logged_in;
	RETURN number_love_matches;
END;
$number_love_matches$ LANGUAGE plpgsql;

-- BEGIN; -- Start a transaction
-- SELECT show_cities2('cities_cur'); -- Returns: cities_cur
-- FETCH ALL IN "cities_cur";
-- COMMIT;

-- Complete User Information of an user
CREATE OR REPLACE FUNCTION get_user_information(id_user_logged_in INTEGER, ref refcursor) RETURNS refcursor AS $$
BEGIN
	OPEN ref FOR 
		SELECT *
    FROM profiles INNER JOIN users
      ON (profiles.id_user = users.id_user)
     INNER JOIN users_photo
      ON (users.id_user_photo = users_photo.id_user_photo)
      WHERE photos.user_id = logged_in_user;
	RETURN ref;
END;
$$ LANGUAGE plpgsql;

-- Give the notifications unseen from an user
CREATE OR REPLACE FUNCTION get_unseen_notifications(id_user_logged_in INTEGER, ref refcursor) RETURNS refcursor AS $$
BEGIN
	OPEN ref FOR 
		SELECT *
		FROM notifications
		WHERE id_user_notificated = id_user_logged_in and seen = 'FALSE'
		ORDER BY time DESC;
	RETURN ref;
END;
$$ LANGUAGE plpgsql;


-- Give the friends from an user
CREATE OR REPLACE FUNCTION get_user_friends(id_user_logged_in INTEGER, ref refcursor) RETURNS refcursor AS $$
BEGIN
	OPEN ref FOR 
		SELECT id_friend
		FROM friends
		WHERE id_owner = id_user_logged_in;
	RETURN ref;
END;
$$ LANGUAGE plpgsql;

-- Get love matches from a specific user
CREATE OR REPLACE FUNCTION get_user_love_matches(id_user_logged_in INTEGER, ref refcursor) RETURNS refcursor AS $$
BEGIN
	OPEN ref FOR 
		SELECT id_loved
		FROM love_matches
		WHERE id_owner = id_user_logged_in;
	RETURN ref;
END;
$$ LANGUAGE plpgsql;

-- Give photos from a user
CREATE OR REPLACE FUNCTION get_user_photos(id_user_logged_in INTEGER, ref refcursor) RETURNS refcursor AS $$
BEGIN
	OPEN ref FOR 
		SELECT *
		FROM photos
		WHERE id_user = id_user_logged_in;
	RETURN ref;
END;
$$ LANGUAGE plpgsql;

-- Give conversations from an user. 
-- the id will be usefull when to show the messages
CREATE OR REPLACE FUNCTION get_user_conversations(id_user_logged_in INTEGER, ref refcursor) RETURNS refcursor AS $$
BEGIN
	OPEN ref FOR 
		SELECT *
		FROM conversations
		WHERE id_user_1 = id_user_logged_in OR id_user_2 = id_user_logged_in;
	RETURN ref;
END;
$$ LANGUAGE plpgsql;

-- Get messages from a specific conversation
CREATE OR REPLACE FUNCTION get_conversation_messages(id_user_conversation INTEGER, ref refcursor) RETURNS refcursor AS $$
BEGIN
	OPEN ref FOR 
		SELECT *
		FROM messages 
		WHERE id_conversation = id_user_conversation
		ORDER BY time DESC;
	RETURN ref;
END;
$$ LANGUAGE plpgsql;

-- Give publications from an user
CREATE OR REPLACE FUNCTION get_user_publications(id_user_logged_in INTEGER, ref refcursor) RETURNS refcursor AS $$
BEGIN
	OPEN ref FOR 
		SELECT *
		FROM publications INNER JOIN publication_elements
		ON (publications.id_publication_element = publication_elements.id_publication_element)
		WHERE publications.id_user = id_user_logged_in
		ORDER BY time DESC;
	RETURN ref;
END;
$$ LANGUAGE plpgsql;

-- Give all the publications a user can access
CREATE OR REPLACE FUNCTION get_publications(id_user_logged_in INTEGER, ref refcursor) RETURNS refcursor AS $$
BEGIN
	OPEN ref FOR 
		SELECT *
		FROM publications INNER JOIN publication_elements
		ON (publications.id_publication_element = publication_elements.id_publication_element)
		WHERE publications.visibility = 'GLOBAL'
			OR (publications.visibility = 'SELF' AND publications.id_user = id_user_logged_in)
			OR (publications.visibility = 'LOVE_MATCHES' AND id_user_logged_in IN (
																	SELECT id_loved
																	FROM love_matches
																	WHERE publications.id_user = love_matches.id_owner
																	))
			OR (publications.visibility = 'FRIENDS' AND id_user_logged_in IN (
																	SELECT id_friend
																	FROM friends
																	WHERE publications.id_user = friends.id_owner
																	))
		ORDER BY time DESC;
	RETURN ref;
END;
$$ LANGUAGE plpgsql;