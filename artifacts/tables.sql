DROP SCHEMA IF EXISTS proto CASCADE;
CREATE SCHEMA proto;

-- DROP TABLE IF EXISTS messages;
-- DROP TABLE IF EXISTS conversations;
-- DROP TABLE IF EXISTS notifications;
-- DROP DOMAIN IF EXISTS d_notification_type;
-- DROP TABLE IF EXISTS friends;
-- DROP TABLE IF EXISTS love_matches;
-- DROP TABLE IF EXISTS publications;
-- DROP TABLE IF EXISTS publication_elements;
-- DROP DOMAIN IF EXISTS d_publication_type;
-- DROP TABLE IF EXISTS photos;
-- DROP TABLE IF EXISTS galleries;
-- DROP TABLE IF EXISTS profiles;
-- DROP TABLE IF EXISTS users;
-- DROP DOMAIN IF EXISTS d_visibility_type;
-- DROP TABLE IF EXISTS user_photos;
-- DROP TABLE IF EXISTS universities;
-- DROP TABLE IF EXISTS courses;
-- DROP TABLE IF EXISTS jobs;
-- DROP TABLE IF EXISTS cities;
-- DROP TABLE IF EXISTS countries;

CREATE TABLE countries (
	id_country SERIAL PRIMARY KEY,
	name TEXT UNIQUE NOT NULL
);

CREATE TABLE cities (
	id_country SERIAL PRIMARY KEY,
	name TEXT NOT NULL
);

CREATE TABLE jobs ( 
	id_job SERIAL PRIMARY KEY,
	name TEXT NOT NULL
);

CREATE TABLE courses ( 
	id_course SERIAL PRIMARY KEY,
	name TEXT NOT NULL
);

CREATE TABLE universities ( 
	id_university SERIAL PRIMARY KEY,
	name TEXT UNIQUE NOT NULL
);

CREATE TABLE user_photos (
	id_user_photo SERIAL PRIMARY KEY,
	link TEXT NOT NULL
);

CREATE DOMAIN d_visibility_type TEXT
	CONSTRAINT visibility_constraint CHECK (UPPER(VALUE) IN ('SELF', 'LOVE_MATCHES', 'FRIENDS', 'GLOBAL'));

CREATE TABLE users (
	id_user SERIAL PRIMARY KEY,
	username TEXT NOT NULL,
	password TEXT NOT NULL,
	email TEXT CONSTRAINT email_unique_constraint UNIQUE NOT NULL,
	birth_date TIMESTAMP CONSTRAINT birth_date_adult_constraint CHECK(EXTRACT(year from AGE(NOW(), birth_date)) >= 18),
	banned BOOLEAN NOT NULL DEFAULT FALSE,
	admin BOOLEAN NOT NULL DEFAULT FALSE,
	engineer BOOLEAN NOT NULL DEFAULT FALSE,

	id_user_photo INTEGER REFERENCES user_photos ON DELETE CASCADE
);

CREATE TABLE profiles ( 
	id_profile SERIAL PRIMARY KEY,
	visibility d_visibility_type NOT NULL,
	gender CHAR(1),
	sexual_orientation CHAR(1),
	movies TEXT,
	books TEXT,
	musics TEXT,
	others TEXT,
	
	id_user INTEGER NOT NULL REFERENCES users ON DELETE CASCADE,
	id_country INTEGER NOT NULL REFERENCES countries ON DELETE CASCADE,
	id_course INTEGER NOT NULL REFERENCES courses ON DELETE CASCADE,
	id_university INTEGER NOT NULL REFERENCES universities ON DELETE CASCADE,
	id_job INTEGER NOT NULL REFERENCES jobs ON DELETE CASCADE
);

CREATE TABLE photos ( 
	id_photo SERIAL PRIMARY KEY,
	name TEXT,
	description TEXT,
	link TEXT,
	visibility d_visibility_type NOT NULL,

	id_user INTEGER NOT NULL REFERENCES users ON DELETE CASCADE
);

CREATE DOMAIN d_publication_type TEXT
	CONSTRAINT publication_constraint CHECK (UPPER(VALUE) IN ('PHOTO', 'LINK'));

CREATE TABLE publication_elements (
	id_publication_element SERIAL PRIMARY KEY,
	name TEXT,
	publication_type d_publication_type NOT NULL
);

CREATE TABLE publications (
	id_publication SERIAL PRIMARY KEY,
	name TEXT NOT NULL,
	description TEXT NOT NULL,
	visibility d_visibility_type NOT NULL,
	time_stamp TIMESTAMP NOT NULL,

	id_user INTEGER NOT NULL REFERENCES users ON DELETE CASCADE,
	id_publication_element INTEGER REFERENCES publication_elements ON DELETE CASCADE
);

CREATE TABLE love_matches (
	id_owner INTEGER NOT NULL REFERENCES users ON DELETE CASCADE,
	id_loved INTEGER NOT NULL REFERENCES users ON DELETE CASCADE CONSTRAINT love_matches_not_self_constraint CHECK(id_owner != id_loved),

	PRIMARY KEY(id_owner, id_loved)
);

CREATE TABLE friends (
	id_owner INTEGER NOT NULL REFERENCES users ON DELETE CASCADE,
	id_friend INTEGER NOT NULL REFERENCES users ON DELETE CASCADE CONSTRAINT friends_not_self_constraint CHECK(id_owner != id_friend),

	PRIMARY KEY(id_owner, id_friend)
);

CREATE DOMAIN d_notification_type TEXT
 CONSTRAINT notification_constraint CHECK (UPPER(VALUE) IN ('MESSAGE', 'FRIEND', 'LOVE_MATCH'));

CREATE TABLE notifications (
	id_notification SERIAL PRIMARY KEY,
	seen BOOLEAN DEFAULT FALSE,
	time_stamp TIMESTAMP NOT NULL, -- current time 
	notification_type d_notification_type NOT NULL,

	id_user_notificated INTEGER NOT NULL REFERENCES users ON DELETE CASCADE,
	id_user_sent INTEGER NOT NULL REFERENCES users ON DELETE CASCADE CONSTRAINT notifications_not_self_constraint CHECK(id_user_sent != id_user_notificated)
);

CREATE TABLE conversations (
	id_conversation SERIAL PRIMARY KEY,

	id_user_1 INTEGER NOT NULL REFERENCES users ON DELETE CASCADE,
	id_user_2 INTEGER NOT NULL REFERENCES users ON DELETE CASCADE CONSTRAINT conversations_not_self_constraint CHECK (id_user_2 != id_user_1),
	
	CONSTRAINT unique_id_users UNIQUE(id_user_1, id_user_2)
);

CREATE TABLE messages (
	id_message SERIAL PRIMARY KEY,
	content TEXT,
	time_stamp TIMESTAMP NOT NULL,

	id_conversation INTEGER NOT NULL REFERENCES conversations ON DELETE CASCADE,
	id_emitter INTEGER NOT NULL REFERENCES users ON DELETE CASCADE
);

CREATE RULE conversation_rule AS ON INSERT TO conversations WHERE (
	SELECT (EXISTS (SELECT conversations.id_user_1, conversations.id_user_2 FROM conversations
	WHERE ((NEW.id_user_2 = conversations.id_user_1) AND (NEW.id_user_1 = conversations.id_user_2))))
	AS bool
)
DO INSTEAD NOTHING;
 
CREATE RULE conversation_update_rule AS ON UPDATE TO conversations WHERE (
	SELECT (EXISTS (SELECT conversations.id_user_1, conversations.id_user_2 FROM conversations
	WHERE ((NEW.id_user_2 = conversations.id_user_1) AND (NEW.id_user_1 = conversations.id_user_2))))
	AS bool
)
DO INSTEAD NOTHING;

CREATE OR REPLACE FUNCTION valid_message_emitter() RETURNS TRIGGER AS $$
BEGIN
 IF
 NOT EXISTS (SELECT *
  FROM messages, conversations
  WHERE NEW.id_conversation = conversations.id_conversation AND 
   (
    NEW.id_emitter = conversations.id_user_1 OR 
    NEW.id_emitter = conversations.id_user_2
   )
  )
 THEN RAISE 'erro';
 END IF;
 RETURN NEW;
END;
$$ LANGUAGE plpgsql;
 
 
DROP TRIGGER IF EXISTS  trigger_messages_emitter ON messages;
CREATE TRIGGER trigger_messages_emitter
AFTER INSERT ON messages
FOR EACH ROW
EXECUTE PROCEDURE valid_message_emitter();

------------------------ INDEXES -------------------------------------------------

--DROP INDEX IF EXISTS index_users_id_user_email;
--DROP INDEX IF EXISTS index_notifications_id_user_date;
--DROP INDEX IF EXISTS index_publications_id_user_date;
--DROP INDEX IF EXISTS index_messages_conversation_date;
--DROP INDEX IF EXISTS index_friends_id_user;
--DROP INDEX IF EXISTS index_love_matches_id_user;

CREATE INDEX index_users_id_user_email ON users (email);
CREATE INDEX index_profiles_id_user ON profiles(id_user);
CREATE INDEX index_photos_userOwner ON photos(id_user);
CREATE INDEX index_notifications_id_user_date ON notifications (id_user_notificated, time_stamp DESC);
CREATE INDEX index_publications_id_user_date ON publications (id_user, time_stamp DESC);
CREATE INDEX index_messages_conversation_date ON messages (id_conversation, time_stamp DESC);
CREATE INDEX index_friends_id_user ON friends (id_owner);
CREATE INDEX index_love_matches_id_user ON love_matches (id_owner);

CLUSTER profiles USING index_profiles_id_user;
CLUSTER messages USING index_messages_conversation_date;
CLUSTER friends USING index_friends_id_user;
CLUSTER love_matches USING index_love_matches_id_user;
CLUSTER photos USING index_photos_userOwner;
CLUSTER notifications USING index_notifications_id_user_date;

-- This indexes are automatically created since postgresql generates indexes automatically for primary keys
--CREATE INDEX index_users_id_user ON users (id_user);
--CREATE INDEX index_profiles_id_user ON profiles (id_user);
--CREATE INDEX index_photo_id_user ON photos (id_user);

-- CREATE INDEX index_users_search ON users USING gist(username gist_trgm_ops);
CREATE INDEX index_users_search ON users(username);

CREATE OR REPLACE FUNCTION search_user(keyword TEXT, limit_parameter INTEGER, offset_parameter INTEGER, ref refcursor)
  RETURNS refcursor AS
  $BODY$
BEGIN
  OPEN ref FOR
    SELECT users.id_user AS user_id, users.username, countries.name AS country_name, user_photos.link AS photo_link, users.engineer AS engineer
    FROM users, profiles, countries, user_photos
    WHERE users.username ~* keyword
      AND users.id_user = profiles.id_user
      AND profiles.id_country = countries.id_country
      AND users.id_user_photo = user_photos.id_user_photo
    LIMIT limit_parameter
    OFFSET offset_parameter;

  RETURN ref;
END;
$BODY$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION search_user_count(keyword TEXT)
  RETURNS INTEGER AS
  $BODY$
  DECLARE
    user_count INTEGER;
BEGIN
  SELECT count(*) INTO user_count
  FROM users
  WHERE users.username ~* keyword;

  RETURN user_count;
END;
$BODY$
LANGUAGE plpgsql;

-- Number of registered users:
CREATE OR REPLACE FUNCTION count_registered_users()
RETURNS INTEGER AS $number_registered_users$
DECLARE
	number_registered_users INTEGER;
BEGIN
	SELECT COUNT(*) INTO number_registered_users FROM users;
	RETURN number_registered_users;
END;
$number_registered_users$ LANGUAGE plpgsql;

-- Count how many users add another to love_matches
CREATE OR REPLACE FUNCTION get_love_matches_counter(id_user_logged_in INTEGER)
RETURNS INTEGER AS $number_love_matches$
DECLARE
	number_love_matches INTEGER;
BEGIN
	SELECT COUNT(*) INTO number_love_matches
	FROM love_matches
	WHERE id_loved = id_user_logged_in;
	RETURN number_love_matches;
END;
$number_love_matches$ LANGUAGE plpgsql;

-- BEGIN; -- Start a transaction
-- SELECT show_cities2('cities_cur'); -- Returns: cities_cur
-- FETCH ALL IN "cities_cur";
-- COMMIT;

-- Complete User Information of an user
CREATE OR REPLACE FUNCTION get_user_information(id_user_logged_in INTEGER, ref refcursor) RETURNS refcursor AS $$
BEGIN
	OPEN ref FOR
		SELECT *
    FROM profiles INNER JOIN users
      ON (profiles.id_user = users.id_user)
     INNER JOIN users_photo
      ON (users.id_user_photo = users_photo.id_user_photo)
      WHERE photos.user_id = logged_in_user;
	RETURN ref;
END;
$$ LANGUAGE plpgsql;

-- Give the notifications unseen from an user
CREATE OR REPLACE FUNCTION get_unseen_notifications(id_user_logged_in INTEGER, ref refcursor) RETURNS refcursor AS $$
BEGIN
	OPEN ref FOR
		SELECT *
		FROM notifications
		WHERE id_user_notificated = id_user_logged_in and seen = 'FALSE'
		ORDER BY time_stamp DESC;
	RETURN ref;
END;
$$ LANGUAGE plpgsql;


-- Give the friends from an user
CREATE OR REPLACE FUNCTION get_user_friends(id_user_logged_in INTEGER, ref refcursor) RETURNS refcursor AS $$
BEGIN
	OPEN ref FOR
		SELECT id_friend
		FROM friends
		WHERE id_owner = id_user_logged_in;
	RETURN ref;
END;
$$ LANGUAGE plpgsql;

-- Get love matches from a specific user
CREATE OR REPLACE FUNCTION get_user_love_matches(id_user_logged_in INTEGER, ref refcursor) RETURNS refcursor AS $$
BEGIN
	OPEN ref FOR
		SELECT id_loved
		FROM love_matches
		WHERE id_owner = id_user_logged_in;
	RETURN ref;
END;
$$ LANGUAGE plpgsql;

-- Give photos from a user
CREATE OR REPLACE FUNCTION get_user_photos(id_user_logged_in INTEGER, ref refcursor) RETURNS refcursor AS $$
BEGIN
	OPEN ref FOR
		SELECT *
		FROM photos
		WHERE id_user = id_user_logged_in;
	RETURN ref;
END;
$$ LANGUAGE plpgsql;

-- Give conversations from an user.
-- the id will be usefull when to show the messages
CREATE OR REPLACE FUNCTION get_user_conversations(id_user_logged_in INTEGER, ref refcursor) RETURNS refcursor AS $$
BEGIN
	OPEN ref FOR
		SELECT *
		FROM conversations
		WHERE id_user_1 = id_user_logged_in OR id_user_2 = id_user_logged_in;
	RETURN ref;
END;
$$ LANGUAGE plpgsql;

-- Get messages from a specific conversation
CREATE OR REPLACE FUNCTION get_conversation_messages(id_user_conversation INTEGER, ref refcursor) RETURNS refcursor AS $$
BEGIN
	OPEN ref FOR
		SELECT *
		FROM messages
		WHERE id_conversation = id_user_conversation
		ORDER BY time_stamp DESC;
	RETURN ref;
END;
$$ LANGUAGE plpgsql;

-- Give publications from an user
CREATE OR REPLACE FUNCTION get_user_publications(id_user_logged_in INTEGER, ref refcursor) RETURNS refcursor AS $$
BEGIN
	OPEN ref FOR
		SELECT *
		FROM publications INNER JOIN publication_elements
		ON (publications.id_publication_element = publication_elements.id_publication_element)
		WHERE publications.id_user = id_user_logged_in
		ORDER BY time_stamp DESC;
	RETURN ref;
END;
$$ LANGUAGE plpgsql;

-- Give all the publications a user can access
CREATE OR REPLACE FUNCTION get_publications(id_user_logged_in INTEGER, ref refcursor) RETURNS refcursor AS $$
BEGIN
	OPEN ref FOR
		SELECT *
		FROM publications INNER JOIN publication_elements
		ON (publications.id_publication_element = publication_elements.id_publication_element)
		WHERE publications.visibility = 'GLOBAL'
			OR (publications.visibility = 'SELF' AND publications.id_user = id_user_logged_in)
			OR (publications.visibility = 'LOVE_MATCHES' AND id_user_logged_in IN (
																	SELECT id_loved
																	FROM love_matches
																	WHERE publications.id_user = love_matches.id_owner
																	))
			OR (publications.visibility = 'FRIENDS' AND id_user_logged_in IN (
																	SELECT id_friend
																	FROM friends
																	WHERE publications.id_user = friends.id_owner
																	))
		ORDER BY time_stamp DESC;
	RETURN ref;
END;
$$ LANGUAGE plpgsql;


-----------------------------------------------------------------------------------
------------------------ INSERTS --------------------------------------------------
-----------------------------------------------------------------------------------

INSERT INTO courses VALUES (1, ' Mestrado Integrado em Engenharia InformÃ¡tica e ComputaÃ§Ã£o ');
INSERT INTO courses VALUES (2, ' Mestrado Integrado em Engenharia Civil ');
INSERT INTO courses VALUES (3, ' Mestrado Integrado em Engenharia MetalÃºrgica e de Materiais ');
INSERT INTO courses VALUES (4, ' Mestrado Integrado em Engenharia do Ambiente ');
INSERT INTO courses VALUES (5, ' Mestrado Integrado em Engenharia MecÃ¢nica ');
INSERT INTO courses VALUES (6, ' Mestrado Integrado em Engenharia QuÃ­mica');
INSERT INTO courses VALUES (7, ' Mestrado Integrado em Engenharia ElectrotÃ©cnica e de Computadores ');

INSERT INTO cities VALUES (1, 'London');
INSERT INTO cities VALUES (2, 'Sao Paulo');
INSERT INTO cities VALUES (3, 'Barcelona');
INSERT INTO cities VALUES (4, 'Paris');
INSERT INTO cities VALUES (5, 'Lisbon');
INSERT INTO cities VALUES (6, 'Porto');

INSERT INTO countries VALUES (1, 'United Kingdom');
INSERT INTO countries VALUES (2, 'France');
INSERT INTO countries VALUES (3, 'Portugal');
INSERT INTO countries VALUES (4, 'Brazil');
INSERT INTO countries VALUES (5, 'Germany');
INSERT INTO countries VALUES (6, 'Spain');

INSERT INTO jobs VALUES (1, 'Used Car Renovator');
INSERT INTO jobs VALUES (2, 'Foreman Construction');
INSERT INTO jobs VALUES (3, 'Machinist Setup Operator');
INSERT INTO jobs VALUES (4, 'Steam Table Attendant');
INSERT INTO jobs VALUES (5, 'Building Materials Salesperson');
INSERT INTO jobs VALUES (6, 'Tailor Alteration');
INSERT INTO jobs VALUES (7, 'Repairer Furniture');
INSERT INTO jobs VALUES (8, 'Surveyor Helper');
INSERT INTO jobs VALUES (9, 'Planning Manager Long-Range');
INSERT INTO jobs VALUES (10, 'Supervisor Painting');
INSERT INTO jobs VALUES (11, 'Clerk Inventory Control');
INSERT INTO jobs VALUES (12, 'Human Resources Clerk');
INSERT INTO jobs VALUES (13, 'Systems Administrator');
INSERT INTO jobs VALUES (14, 'Automotive Service Technician');
INSERT INTO jobs VALUES (15, 'Clergy Member Head');
INSERT INTO jobs VALUES (16, 'Top Corporate Shareholder Relations Executive');
INSERT INTO jobs VALUES (17, 'Volunteer Services Director');
INSERT INTO jobs VALUES (18, 'Technician Cardiac Monitor');
INSERT INTO jobs VALUES (19, 'Traffic Manager');
INSERT INTO jobs VALUES (20, 'Operator Conveyor System');
INSERT INTO jobs VALUES (21, 'Terrazzo Finisher');
INSERT INTO jobs VALUES (22, 'Technician Automotive Service');
INSERT INTO jobs VALUES (23, 'Helper Carpenter');
INSERT INTO jobs VALUES (24, 'Clerk Information');
INSERT INTO jobs VALUES (25, 'Technologist EEG');
INSERT INTO jobs VALUES (26, 'Case Aide');
INSERT INTO jobs VALUES (27, 'PC Maintenance Technician');
INSERT INTO jobs VALUES (28, 'Helper Machine Operator');
INSERT INTO jobs VALUES (29, 'Repairer Bicycle');
INSERT INTO jobs VALUES (30, 'Public Relations Coordinator');
INSERT INTO jobs VALUES (31, 'Engineer Transportation');
INSERT INTO jobs VALUES (32, 'Painter Helper');
INSERT INTO jobs VALUES (33, 'Foreman Assistant');
INSERT INTO jobs VALUES (34, 'Equipment Appraiser');
INSERT INTO jobs VALUES (35, 'Marketing Associate');
INSERT INTO jobs VALUES (36, 'Materials Management Director');
INSERT INTO jobs VALUES (37, 'Survey Worker Marketing');
INSERT INTO jobs VALUES (38, 'Clerk Stock Retail');
INSERT INTO jobs VALUES (39, 'Mechanical Building Maintenance Supervisor');
INSERT INTO jobs VALUES (40, 'Banking Finance Sales Representative');

INSERT INTO universities VALUES (1, ' Instituto Superior Tecnico ');
INSERT INTO universities VALUES (2, ' Faculdade de Engenharia da Universidade do Porto ');
INSERT INTO universities VALUES (3, ' Universidade do Minho');
INSERT INTO universities VALUES (4, ' Instituto Superior de Engenharia do Porto ');

INSERT INTO user_photos VALUES (1, 'www.myhelp.com');
INSERT INTO user_photos VALUES (2, 'www.drinktool.nl');
INSERT INTO user_photos VALUES (3, 'www.startwerk.co.uk');
INSERT INTO user_photos VALUES (4, 'www.contactweb.be');
INSERT INTO user_photos VALUES (5, 'www.newsfill.gr');
INSERT INTO user_photos VALUES (6, 'www.compatrust.ch');
INSERT INTO user_photos VALUES (7, 'www.schoenenplaats.de');
INSERT INTO user_photos VALUES (8, 'www.blacktrust.fr');
INSERT INTO user_photos VALUES (9, 'www.retailmagics.es');
INSERT INTO user_photos VALUES (10, 'www.allepagina.gr');
INSERT INTO user_photos VALUES (11, 'www.magicfill.ch');
INSERT INTO user_photos VALUES (12, 'www.coffeeinfo.es');
INSERT INTO user_photos VALUES (13, 'www.zooffice.de');
INSERT INTO user_photos VALUES (14, 'www.coffeeoffice.fr');
INSERT INTO user_photos VALUES (15, 'www.coolactive.es');
INSERT INTO user_photos VALUES (16, 'www.voedingbode.com');
INSERT INTO user_photos VALUES (17, 'www.kooptargets.be');
INSERT INTO user_photos VALUES (18, 'www.newspeople.fr');
INSERT INTO user_photos VALUES (19, 'www.magicmasters.gr');
INSERT INTO user_photos VALUES (20, 'www.koopsite.be');
INSERT INTO user_photos VALUES (21, 'www.marktplaza.com');
INSERT INTO user_photos VALUES (22, 'www.stockthings.de');
INSERT INTO user_photos VALUES (23, 'www.linkfill.ch');
INSERT INTO user_photos VALUES (24, 'www.waterplaats.es');
INSERT INTO user_photos VALUES (25, 'www.homeinfo.de');
INSERT INTO user_photos VALUES (26, 'www.gohouse.es');
INSERT INTO user_photos VALUES (27, 'www.downloadinfo.nl');
INSERT INTO user_photos VALUES (28, 'www.keuzeweb.gr');
INSERT INTO user_photos VALUES (29, 'www.trefplaats.nl');
INSERT INTO user_photos VALUES (30, 'www.trainsoftware.gr');
INSERT INTO user_photos VALUES (31, 'www.cooltrust.com');
INSERT INTO user_photos VALUES (32, 'www.linkfill.nl');
INSERT INTO user_photos VALUES (33, 'www.webschool.nl');
INSERT INTO user_photos VALUES (34, 'www.tasplein.org');
INSERT INTO user_photos VALUES (35, 'www.myhouse.de');
INSERT INTO user_photos VALUES (36, 'www.ziecompany.gr');
INSERT INTO user_photos VALUES (37, 'www.gezondheidpagina.fr');
INSERT INTO user_photos VALUES (38, 'www.maximaalwinkel.fr');
INSERT INTO user_photos VALUES (39, 'www.contactbode.de');
INSERT INTO user_photos VALUES (40, 'www.tasmarkt.co.uk');
INSERT INTO user_photos VALUES (41, 'www.webtool.fr');
INSERT INTO user_photos VALUES (42, 'www.seekhouse.org');
INSERT INTO user_photos VALUES (43, 'www.digitaalplaats.com');
INSERT INTO user_photos VALUES (44, 'www.allmoon.it');
INSERT INTO user_photos VALUES (45, 'www.zoekbaan.fr');
INSERT INTO user_photos VALUES (46, 'www.etenschool.be');
INSERT INTO user_photos VALUES (47, 'www.koopcompany.it');
INSERT INTO user_photos VALUES (48, 'www.goinfo.co.uk');
INSERT INTO user_photos VALUES (49, 'www.webplein.be');
INSERT INTO user_photos VALUES (50, 'www.keuzevacatures.de');
INSERT INTO user_photos VALUES (51, 'www.watersite.ch');
INSERT INTO user_photos VALUES (52, 'www.keuzevacatures.co.uk');
INSERT INTO user_photos VALUES (53, 'www.greeninform.be');
INSERT INTO user_photos VALUES (54, 'www.compatargets.it');
INSERT INTO user_photos VALUES (55, 'www.trefplaats.be');
INSERT INTO user_photos VALUES (56, 'www.mythings.ch');
INSERT INTO user_photos VALUES (57, 'www.voedingbanen.de');
INSERT INTO user_photos VALUES (58, 'www.alltrust.de');
INSERT INTO user_photos VALUES (59, 'www.nedweb.com');
INSERT INTO user_photos VALUES (60, 'www.tassenmarkt.ch');
INSERT INTO user_photos VALUES (61, 'www.gezondheidpagina.ch');
INSERT INTO user_photos VALUES (62, 'www.etenschool.nl');
INSERT INTO user_photos VALUES (63, 'www.newfill.co.uk');
INSERT INTO user_photos VALUES (64, 'www.contactvak.nl');
INSERT INTO user_photos VALUES (65, 'www.whitethings.fr');
INSERT INTO user_photos VALUES (66, 'www.seekraak.es');
INSERT INTO user_photos VALUES (67, 'www.bettertrace.be');
INSERT INTO user_photos VALUES (68, 'www.kooppeople.nl');
INSERT INTO user_photos VALUES (69, 'www.etensite.org');
INSERT INTO user_photos VALUES (70, 'www.elkebaan.nl');
INSERT INTO user_photos VALUES (71, 'www.mastertrust.fr');
INSERT INTO user_photos VALUES (72, 'www.greeninform.be');
INSERT INTO user_photos VALUES (73, 'www.startpagina.fr');
INSERT INTO user_photos VALUES (74, 'www.inktool.nl');
INSERT INTO user_photos VALUES (75, 'www.allactive.com');
INSERT INTO user_photos VALUES (76, 'www.webtool.de');
INSERT INTO user_photos VALUES (77, 'www.gomagics.fr');
INSERT INTO user_photos VALUES (78, 'www.digipagina.nl');
INSERT INTO user_photos VALUES (79, 'www.digitaalwereld.it');
INSERT INTO user_photos VALUES (80, 'www.cooltargets.it');
INSERT INTO user_photos VALUES (81, 'www.webplaza.es');
INSERT INTO user_photos VALUES (82, 'www.newsoffice.be');
INSERT INTO user_photos VALUES (83, 'www.webactive.de');
INSERT INTO user_photos VALUES (84, 'www.schoenplaats.es');
INSERT INTO user_photos VALUES (85, 'www.seekhelp.es');
INSERT INTO user_photos VALUES (86, 'www.trainsoftware.gr');
INSERT INTO user_photos VALUES (87, 'www.schoensite.ch');
INSERT INTO user_photos VALUES (88, 'www.zohouse.nl');
INSERT INTO user_photos VALUES (89, 'www.allmaster.fr');
INSERT INTO user_photos VALUES (90, 'www.allebode.ch');
INSERT INTO user_photos VALUES (91, 'www.schoenenmarkt.nl');
INSERT INTO user_photos VALUES (92, 'www.newssite.com');
INSERT INTO user_photos VALUES (93, 'www.contactwereld.fr');
INSERT INTO user_photos VALUES (94, 'www.traintarget.co.uk');
INSERT INTO user_photos VALUES (95, 'www.contactwerk.nl');
INSERT INTO user_photos VALUES (96, 'www.choiceinfo.co.uk');
INSERT INTO user_photos VALUES (97, 'www.restaurantwerk.co.uk');
INSERT INTO user_photos VALUES (98, 'www.linkinform.de');
INSERT INTO user_photos VALUES (99, 'www.goinfo.fr');
INSERT INTO user_photos VALUES (100, 'www.myinform.es');

INSERT INTO publication_elements VALUES (1, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (2, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (3, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (4, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (5, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (6, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (7, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (8, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (9, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (10, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (11, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (12, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (13, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (14, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (15, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (16, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (17, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (18, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (19, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (20, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (21, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (22, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (23, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (24, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (25, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (26, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (27, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (28, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (29, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (30, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (31, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (32, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (33, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (34, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (35, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (36, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (37, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (38, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (39, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (40, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (41, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (42, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (43, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (44, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (45, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (46, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (47, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (48, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (49, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (50, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (51, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (52, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (53, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (54, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (55, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (56, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (57, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (58, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (59, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (60, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (61, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (62, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (63, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (64, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (65, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (66, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (67, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (68, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (69, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (70, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (71, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (72, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (73, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (74, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (75, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (76, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (77, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (78, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (79, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (80, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (81, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (82, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (83, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (84, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (85, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (86, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (87, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (88, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (89, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (90, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (91, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (92, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (93, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (94, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (95, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (96, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (97, 'Element name', 'LINK');
INSERT INTO publication_elements VALUES (98, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (99, 'Element name', 'PHOTO');
INSERT INTO publication_elements VALUES (100, 'Element name', 'LINK');

INSERT INTO users VALUES (DEFAULT, 'Sogood71', 'e4bI5a2UZqDelwaNKppSpg', 'William.Little@libero.fr', '1916-06-10 03:10:00', FALSE, FALSE, FALSE, 8);
INSERT INTO users VALUES (DEFAULT, 'Mads871', 'KjumRTB26sjSmeAND21KVoL6jADTKeDHeB6VpAMTIsmByY', 'JennyGunter4@mobileme.es', '1940-02-26 05:55:00', FALSE, TRUE, FALSE, 13);
INSERT INTO users VALUES (DEFAULT, 'Ann908', 'KxMkG4Eg467Eyp5GhVy6gJIJfSbUZXBvm1txisOGaIjtprRlukKXkMog0NBTrSL8BJb5HClN2z4GPwUCTzbdyUevbjSyaSQsFFzPpFFm0uYdSpy6', 'TonGieske@web.es', '1923-09-04 01:28:00', FALSE, TRUE, FALSE, 13);
INSERT INTO users VALUES (DEFAULT, 'Rachel0', 'e6bX5rFAMbqaei0zVOkQM6OVjdxvBog2x6sd3cOlCPtxpt6XFVMlKucbm7WaI32EHGQoLNKIb0AUiq2HXfUq0scgI2RjaxBVibqV1q5TMr1aTSbFhY7PVWqHH0hS5IpknDXWll2iWSZkL8WI5UBuzrS05RuV8pkV2ySUlo4jKZOWoZxAjxJZZoaVSw14vfMmAJTEO1fPsca083PzkaxCGXEhiR5K8gXS', 'Freddy.Helfrich4@web.ca', '1992-12-14 07:30:00', FALSE, FALSE, TRUE, 13);
INSERT INTO users VALUES (DEFAULT, 'GoPlaat75', 'Ui8r2eAsWHsfp05rfFgwv8tV6xVFA7qOnOVjatLq8oEGaFmFcVzGldTu2Z0M28IA1CuabrwzB75VutJy83hNhTqBTn8slmdf', 'LAlspaugh2@yahoo.cn', '1991-06-26 02:32:00', TRUE, TRUE, FALSE, 17);
INSERT INTO users VALUES (DEFAULT, 'Hero14', 'o', 'YMoore@telefonica.co.uk', '1902-01-11 00:32:00', TRUE, TRUE, FALSE, 17);
INSERT INTO users VALUES (DEFAULT, 'Siem', 'F2DgxtcUTk8imaSFFHorrUlojeu4bwcbt5trTDIOY2QpufRKdth5uKgPhmbBfTEnVBdy0irlW2Cg0bRL82JVxKOW58vfGB0DlMrWRFHTvEglJVR4YYLLHwu0oXgNbdhQD4PZDABtcMmgQvYgV1EkzEvrJZTgNVENjPA84KTsEck3G0JkcT7TLsEOWeuqsRRUj1MJarEsMnhtJMxmfYoMDGpyh0nGEwn8dW4VRewAPEyTHL1F8AVf7KMT8E', 'Nick.Foreman@telefonica.fr', '1921-05-08 01:30:00', FALSE, FALSE, FALSE, 17);
INSERT INTO users VALUES (DEFAULT, 'Edwin', '1WC0ZLwDwrWA4OdlRWVuWA2YjweE5Z6n0GreowSftcctH2TYE5FBNX7JCBYf7RLbrLQg77BtFHfSg10D42DkLdJyqLWzGgffeVFEugAIRtoscE04suDxiySZad2lOskGkBh0t4O3C0An3DptEpc3FeND36scEP2NkTFHpK1laPxTexRo6cciHgrPv4ejzDtdEkqvkXgCZoEbtOHABN8shxIcucEYlO3KsmbEpX8keAmPYzDhi', 'BasKingslan@telfort.nl', '1920-08-02 06:08:00', TRUE, TRUE, TRUE, 25);
INSERT INTO users VALUES (DEFAULT, 'Ron61', '8Syc7giDkv5CMAMAcZ6BZyngnDEYEBf5ZYzesdBUKOjk4yRJykRShY1YGVOXb2jtjPFPT2KrrMtuQB6P7YyWJUdMCAd4XwLrdlmoDQakRFhs0JMYpSF1KVAUj0IuZEXpXtEpHUZzzLHsONnjnFmJkSm4H0HycSjaJpexwOod2zaAJmFowDoqL3kbJdDTmqBvvX1FbOxuH6RDjNg43bwaKl8kw4nHSxKAZ3ox0aEi', 'E.Bruno@gmail.co.uk', '1969-09-03 06:55:00', TRUE, FALSE, TRUE, 25);
INSERT INTO users VALUES (DEFAULT, 'Niklas449', 'mQ4plIQ2VJ2n6LEj0kZe7szXnMp45OaSoha2aI1y3raiZ7QaqZOIMnvfrbdTbE4252Ns8D76gMtQhRMg12TccX3AyZxoMXweVA82wnKBahUIcNfUOVXMkSvPXLxlWmfgTXTnubUsnaZG44t1yFj2qiHyJZhWbVgmH3NDD8x45yrq6tN0dVD4RmdvN0jt5GZ', 'WilliamArcadi@libero.gov', '1912-11-03 05:26:00', FALSE, TRUE, TRUE, 34);
INSERT INTO users VALUES (DEFAULT, 'Ricardo71', 'fEX1mB0FEacbo6TQIuHpHyUzCZA0OtBsxaDct4AKnMF3NSvbD1jDAbQN3Mh2IAbNRkUNLdcXgDo1VCoMOC4Wsy6AhaiUAfhh4RGOAjpcPMxyBFVkFHNXp1R5s7nfF7IRYkDylnfrusFtESuJZlIqEiAao4jiWruqFsUgKY8QUVRofsWSzza0WCuDGrBaSBWMxE3tyaoyJuablwqenfdjXR8FcNutIJB', 'Jack.Alspaugh4@mail.cn', '1905-06-23 05:28:00', TRUE, TRUE, TRUE, 34);
INSERT INTO users VALUES (DEFAULT, 'Teun55', 'DRbYWA4eaxTxopaeTnlEtS52dEDXKdyzM7AX20p58pIWehdWtvhQaZrkwlUGFIFqXRtX4NMiTOA724URB6bOfH8kPjtNzKSDHeZLx26AIxv4LXw6YOPA', 'ARoche@libero.org', '1989-05-15 07:37:00', FALSE, FALSE, TRUE, 34);
INSERT INTO users VALUES (DEFAULT, 'Herman1', 'TZ78QEk2pUQ1w5rIfzk5EUfmeWuHsWOuYcP0s4e56XlNjSX1dnUXAdNKtjOiUCwodAy7NXireojPsxjXzfF44bqcHl0xdfiyVedsmOb3S76D4bt4yVObowa8trSHBpJQPDO', 'William.Williams@live.es', '1971-03-16 05:25:00', TRUE, FALSE, FALSE, 36);
INSERT INTO users VALUES (DEFAULT, 'Marco158', '80LlAXH7KXtLaEr15', 'Hans.Fox@aol.com', '1923-08-31 01:10:00', TRUE, TRUE, TRUE, 36);
INSERT INTO users VALUES (DEFAULT, 'Aoife0', '3hIxQwvgG0ThNfzttb4A16FKH04QwIkBgnT6UJ0Twzd8MjisGBDRaQecohBpTTMvsqgy2gLT5N4Y72U26CwU4OiSWCI6ShAXwHSk', 'Freddy.Stevens3@telefonica.ca', '1986-03-18 08:34:00', FALSE, FALSE, FALSE, 42);
INSERT INTO users VALUES (DEFAULT, 'Ciara50', 'fBFtK23lSGpj7uhQ5ER0I7IFGMm3O18dQqRoGRe4KDh4oloYMI7GDwnxNg7vzGURYshJZPSiR6gppf', 'H.Shapiro@aol.dk', '1902-07-18 04:44:00', FALSE, FALSE, TRUE, 42);
INSERT INTO users VALUES (DEFAULT, 'Marie5', 'ApkpI1vWtlK4RyOVFjUqikQi8IAv7ASDennITElVlYzb7rMEJbZt4hiRuGRAyOozli6E66HvOF', 'VictorKuehn@libero.us', '1984-05-24 09:49:00', TRUE, FALSE, TRUE, 42);
INSERT INTO users VALUES (DEFAULT, 'Jorge69', 'Kl87dVyoveJ87Sr3MH0TSlF0utXbz60vxqoVjlpassGh5G08XO8hm2pKEW5TzQeyK4rHAc7tcaELgPYSw4uCHrQps3pSwqaPwU0SsvRLhxDmKuROAMT6EH5Cid1zsHwyEwYB', 'Ronald.Nelson@gawab.org', '1932-04-12 10:55:00', FALSE, FALSE, FALSE, 50);
INSERT INTO users VALUES (DEFAULT, 'Rick', 'tSjMVKQWgpbNUXRrQyNXSK2KHoIQzDFlnQba3Zkbupbnjj7BLtpmx8HcmCzJ3Lv4eVekoAhMuYTGPudFl2SBG6irG8GH3Nbzabw4FNb3rsr2dVacUs2uVYqHpYWM2OsMNzvpaoZZairdqOczDy3BALodtqnQOhCn6fvmpTnArfxTqcCTC', 'RickCappello1@mail.it', '1953-02-16 07:56:00', TRUE, TRUE, TRUE, 54);
INSERT INTO users VALUES (DEFAULT, 'Camila46', 'WBKlWOv', 'L.Brennan@myspace.nl', '1911-01-24 10:44:00', TRUE, FALSE, FALSE, 54);
INSERT INTO users VALUES (DEFAULT, 'Cameron3', 'p0E5hjy8XTpX6cp7pohIi7xYBCivMP2or6gEkEgo5fIvynIvQyJe2FaaqZi7xDLlQfMYhpde2wppfyKfYYy0EVabrVHSq4KwtqoXs660kWGLnorofwgIjuDuvnaUt8vmbDnuEoFLVRFUYnZdqyW0i3rfWW5CSR6ukjnHBAF0hmHKx1DrMyWMiaaJ1hjpRgQdc7c6h0YWRwhdcHjNljQMLT2ExEnfmdWcHJ3AouwJRb1K8u2dPVwm', 'Mattijs.Naff1@telfort.nl', '1933-06-04 05:19:00', TRUE, TRUE, FALSE, 60);
INSERT INTO users VALUES (DEFAULT, 'Cristina', 'Mgw4KWORzvKzmswN0kXZITkdrTIBpxReo2M', 'MickBlacher2@live.ca', '1954-12-28 10:55:00', TRUE, TRUE, FALSE, 62);
INSERT INTO users VALUES (DEFAULT, 'Francisco207', 'uwLCqayMPoV0C63yczJnSigvaU7DidUHgjEg072fSOdXdmaG3OfnmxNPlIwYuRF52H4U2jfPlfjHOqjNKjv0nZCsrflPNH88FAT8mtefrIp3dBGcv6T6', 'Bianca.Lawton@msn.cn', '1913-09-17 08:44:00', FALSE, TRUE, FALSE, 62);
INSERT INTO users VALUES (DEFAULT, 'Alva', 'rPEnmwRluURu8midrcBjIdLQFv7KT', 'LucasHummel1@web.be', '1983-12-02 09:03:00', FALSE, TRUE, FALSE, 69);
INSERT INTO users VALUES (DEFAULT, 'Katie', 'yhqnaT7BLH8yOOrhnYaau6BK27fRuoV1Ty4QmbJ6zZ2JfTkGkzc8PtTYDn5mQNRW6wL2jS3J45eqgjupmSPp3gWCkrTwOyGOZbUvrMcJavhtg6bqFSkY2xIV0', 'Lindsy.Paul@hotmail.com', '1953-04-28 10:48:00', TRUE, TRUE, TRUE, 69);
INSERT INTO users VALUES (DEFAULT, 'Lisa885', 'oj0EK1yLhWKQYqpYiMGL2Ma3CVd4MCAhNdGrYX18COUhNaSHnprKZKtDSipnOcrqWNJs0QwHJuwKcFOvjDBDTzaFHbhT4Rjh6iAkXzLoHqMxSxQ7Fy76T2QjQilPUAstJljQm3goQ7mVVFsV1S10xG6fhL3oJDxETB8zkK5MPTt1MYnqLPfuC20PTkUCm8VlDQ', 'IJessen@mymail.no', '1932-11-06 07:49:00', FALSE, FALSE, TRUE, 69);
INSERT INTO users VALUES (DEFAULT, 'Maja01', '0vMRHWkQHjvNplbl8JMh8N2SkzNBDP6SMV1QOXIopqlhhtszI0mVZGoKFeg2ZPzOoT5OvmFJmMHIjg8hQbsLF7GzEhKxneOhr6JrgpX27d4BqJJOhrgWqf72jn0dkhGR5HdqiAdkNebPOL01OGyl2SKOuTdqc1ggil2RZoEyI7jNgMHwqiAOpjHpTssLN1IL8YAYPepgauJqG', 'C.Suszantor@telefonica.com', '1919-01-26 05:23:00', TRUE, TRUE, FALSE, 71);
INSERT INTO users VALUES (DEFAULT, 'Philippa39', 'YrMvqbhdHwJzklKSpl7', 'Martvan het Hof3@msn.nl', '1966-01-26 00:12:00', FALSE, TRUE, FALSE, 71);
INSERT INTO users VALUES (DEFAULT, 'Anne09', '4UygEQFokN2urw6xjQmC15dIRK3vYC8ZZqZQndMfIkZrIyOwJr31TM678LZ0fozZMtz1iBkWajOayhExa7EYA6bad5Sk8Yfz0CckABq01pIRiIkvlj1bwdPUSYGb2isD5KzNqeGLvnx711NNjPCFN1zJGKE0XqBUZuOuNKSUbU4Yk58fZoK1vP8E0N7vseBPWKcdXzC6GiKwujNG2CoigfA2JhYRU4t', 'WillJones4@yahoo.ca', '1987-09-20 08:46:00', FALSE, FALSE, TRUE, 71);
INSERT INTO users VALUES (DEFAULT, 'Georgina04', 'c2cyMsJE0C6aS32I0bku0lyAWiHeR6AbPiBXqVnquLxaSNIfU1F62SVtpgZHiDu14heorxxUVNztxa87A8Kynj3fOzBqytdEnuLTAGuKBWc4gq6RLtUJI5heeqnLi4G01wVvMBjcW8DU3oX7Y3mfpYGDz11zoAXxoZWHHwQzmZuAreolQZ8YDi8iYRliaakZMYaFSZsIC8TAO2QN0jtKyeoZsxOl', 'BillZia1@telfort.cn', '1918-12-02 07:49:00', FALSE, FALSE, TRUE, 80);
INSERT INTO users VALUES (DEFAULT, 'Lara45', 'FVjhX5B', 'Richard.Waddell5@dolfijn.ca', '1995-10-05 04:42:00', FALSE, FALSE, TRUE, 90);
INSERT INTO users VALUES (DEFAULT, 'Giood757', 'U8yWU8EwcJ6RVO4Hg1ZecKCYTv4H5SMPYzyX6qXJg2tUzikZVdXdxpyNoATGP3paVf6t6XC3P3DcZ77HYQ5zJxG5J7KChrgsfqRcZrV8lbpaCQ2Vk2kkUm1Y02qqjYCedYnwL7ZQvwEfNSrtYRJYp6ZmvMdzKPFNHfl4G7d7CP0GpTFguJ8MaC1', 'T.Pengilly@msn.es', '1912-08-27 04:48:00', TRUE, FALSE, TRUE, 94);
INSERT INTO users VALUES (DEFAULT, 'Nicolas9', 'DL07JYSNPmyPLcsWGr1mshM2gTDihOqNskzEOHNCWrvX2c5yv6BLRVIgDpC3Q8B1cxlKHyAxJWR82ZEVmkNosA81kge2DNQ4osuEIKKN5kPWSzDlI5xf4sYNzBTNPiedW0BObrUUlWaqa2nCmSSFqOwtzaOpd71zaacriiwIvariNlVCFLXhRhwmz272GQkgaJErmEJ4u1n5tn5gXUHwrkOAMVCXlFDcfCX1OuHAwlGfDeeTx6u', 'H.Mayberry@web.no', '1969-03-14 00:22:00', FALSE, FALSE, FALSE, 94);
INSERT INTO users VALUES (DEFAULT, 'Megan96', 'GcemHcbyxhBXFPeiTBS2RNTXfHQVcMcu22YTInWEaDSWJenA3LitpSX6DS5h5kpigBkeXeirqiolE5MK4lDWXKDRnadkzudVyPDMuS4BlDJce6SiUG2dqwQRlLVtflcZEAk5HrIGHQAbk4H4g3oX5rXsf7fUaeiu5', 'BillForeman@hotmail.net', '1996-01-13 06:45:00', TRUE, TRUE, TRUE, 94);
INSERT INTO users VALUES (DEFAULT, 'Sigrid', 'wB3wbBye5bOIBOGhZOJKsRx8kRUA52eXGzAKhO5WvySkchbujY8rDmajm8WGO2y6DYwSOJ1y11xVm1nOPSkOVca1Ox1zz8sd53UBHTyhBZDCbFATHhcnnxzeZs0uF1XqtgBeadMSQEptkjEPqrgnnnGbq5fHZBNB7UMH0oEg6IEF47eWE5n83Oz5jLJBWY62qt7CHAvSAz1SXU4PQyOkj4YiETZRvjzWwWM703tORBPj8scbL3B8wIYJE6XZC', 'BartSanders@telefonica.es', '1933-11-28 06:46:00', TRUE, TRUE, TRUE, 99);
INSERT INTO users VALUES (DEFAULT, 'Tobias', 'PoYY4IH1rU1MFEkgAaiUrWsAjeK', 'William.Hoyt@mail.dk', '1901-07-07 08:25:00', FALSE, FALSE, FALSE, 99);
INSERT INTO users VALUES (DEFAULT, 'Luke', 'Gquly8EQWm8xfZD5jsT5b5SFiynL', 'Hans.Imhoff@web.no', '1945-09-03 06:29:00', TRUE, TRUE, TRUE, 99);
INSERT INTO users VALUES (DEFAULT, 'John408', '0gDrihQd54rZ6i8vmABYLPQA1Fiz84FYjW8iwQSR2HiqfiBCrNv0Un7fYZzOO4dY2sf1DUaeXTcGEe8fAxT60vtUYn5zeUE6NxA55NYyYSOjg3JCkT7opp7zJriYo4hd2j4Nb2lTkfHKwO2FqfaScLz8tstOSwdVO3D4MIPZRYHxjP', 'Hank.King5@hotmail.es', '1984-06-17 08:17:00', FALSE, FALSE, FALSE, 5);
INSERT INTO users VALUES (DEFAULT, 'Zofia0', 'HIG4JqFCFNczLNGTyHcxb8YCWAlrdNcRG1FxxePgKkF2Wj5QmagP7cSjFjJKHq7BH8eQThgpysQpVh3HLGfeKLwgsaMIyekSRMIiUZBMym7V1H7Jh0Eq0XCgFAfcgakbckxUusJPygRsDShRNkAq8LyNJjkHJZLupy8us4EHU', 'TreesHoyt4@mymail.gov', '1982-06-01 06:52:00', FALSE, TRUE, FALSE, 6);
INSERT INTO users VALUES (DEFAULT, 'Isak8', 'jwzv0SYi54T8J6Ce1U6nYcSsKnrFp4Momle8pHZO1rcwBuAJwja60YDeR23jZiDW7nNGbs7E8rDvRPYehClJOSTRH4Z4ckZX10KIMULpaxKC5vLpcG2xSAyLCXsJSOABs3zQZSZh8R7yBcj5dkes4COx2M2JAolauYJDsXeLSxNgrW2oMHcoj7kpXMIoZiXgwlRyoL2101KfuRokQS4FzQ4QVDBx3IGgmFVlEC0', 'G.Lawton@gmail.dk', '1950-03-17 02:19:00', FALSE, TRUE, TRUE, 6);
INSERT INTO users VALUES (DEFAULT, 'Ester6', 'hbFlWQd3Oj', 'PeterOstanik3@libero.no', '1940-11-01 01:19:00', TRUE, TRUE, TRUE, 16);
INSERT INTO users VALUES (DEFAULT, 'Amy50', 'a88YUChelMt0flDoxB8nLXYIMPdZ21yBTlvRFWiWR40uMT1mglUxF36LNavDvtmQ81go8vX6U7N1hhtZTPEkaQstXOuelF8mgPbanzG5F0HOXeivN1lZe3coGZG3KOt7TTPxeyuApaFoPNo1YFPhvNcjHbLLYgaPD3SRRQy4UoQpMwIsYRMQHGNGi', 'LeoVanderoever1@telfort.com', '1951-09-26 00:10:00', TRUE, FALSE, FALSE, 16);
INSERT INTO users VALUES (DEFAULT, 'Nienke7', 'LYRqIQhH82Vdzmd7dv11AlvQE8WJMOKrOTYRYTqFLkSJyphpXJ70FVq3vpvsD0b34P2aqO6cRIQhDrE4t', 'BrendToreau@gmail.fr', '1902-01-14 05:22:00', FALSE, FALSE, TRUE, 24);
INSERT INTO users VALUES (DEFAULT, 'Lauren521', 'nfXTAE8eSOU3e4zOdZ8LeYg2ZzI8kVWYQ5ntf15M8dGSdZakVXui0XNw0CEYRYfAyXxlbrrRQIRXYxqadB512wAnbJT7cF34wNVzu7FhVLeWSVMpvvfck6o6af0nQKAL4mQfaQuonWjpnPzlf7q', 'T.Brendjens@mymail.gov', '1956-08-15 01:40:00', TRUE, FALSE, FALSE, 24);
INSERT INTO users VALUES (DEFAULT, 'Pieter0', 'yq0s870uTZ7HhVSIYFGAJ7OJWWMbIXL', 'Brent.Aldritch4@kpn.org', '1903-09-20 09:57:00', TRUE, TRUE, TRUE, 24);
INSERT INTO users VALUES (DEFAULT, 'Lucille555', 'U1FDk08Hv7HkXDiilFIcdmXtvGfMgs', 'Brent.Bloom@kpn.fr', '1917-07-18 09:38:00', TRUE, FALSE, TRUE, 34);
INSERT INTO users VALUES (DEFAULT, 'Dave92', 'zlFiaOWCPTuQVmOs4x8ElImtQXjzHYUiGGn8rcposi6sICXzdCmSMjVuJkbMHxBGANuMtLBLJSa1Q06xvSZfviFXyu4aRWVkVUTVZXRntjwh6F6SrRoYC20CxikNJzBr', 'RickNewman2@mobileme.cc', '1961-03-30 04:20:00', FALSE, FALSE, TRUE, 34);
INSERT INTO users VALUES (DEFAULT, 'Chloe3', 'izpnvztqXYH7V2werBfeHpi52DP0QV5zEiCTg56gQtW4dGGKcxuQC0IvAVgMb1K4vrpqIPY7dEKgorNKD2', 'Lucas.Arden4@mymail.dk', '1911-03-18 03:56:00', TRUE, FALSE, TRUE, 43);
INSERT INTO users VALUES (DEFAULT, 'Rando552', 'MV5mgKEikM2k5CuumI1CkPgiw7pXmALZ3zjPKi7zrpy8e5JbdoGvsLl5LmVV', 'Suus.Anderson@mobileme.es', '1920-06-16 05:16:00', TRUE, TRUE, FALSE, 44);
INSERT INTO users VALUES (DEFAULT, 'Ada727', 'Q8FIs0M2aCnKzjBHhFDHE1b63LYEX7NxxXtIaFaLNGQpsS4LKjmkYleSwivim0lCgzTt8cN0RbhEpBrbMljPID75zWWYaBH4HCEnNkh5w3TkEW3Slbz', 'KPerilloux1@gmail.co.uk', '1948-03-23 10:41:00', FALSE, FALSE, TRUE, 44);
INSERT INTO users VALUES (DEFAULT, 'Guus', 'zwgsPs3HlMrYEHg5Liv5HCoEVnEO3Ol7PQ1TRfziNu0zv0zAOEI32j2QEXowzZdaoSAmhqAWoyYnVomuXunyOFi6NPsoUPZoq4puDPZ2jWekdf57BVAhjdyZoAzZPZmPDeFvY6CtfIzFXz4GFPoXVdo0FvrBjThu4MRFtd0q7Pqgxs4', 'EMitchell@yahoo.nl', '1916-06-19 10:41:00', TRUE, FALSE, FALSE, 53);
INSERT INTO users VALUES (DEFAULT, 'Elin', 'mtFvaiQWDrUyiEDN3IWMhz4tI7ijRgnYTxZIqdW1UoACh14HVBapvZ42TLxz1UFYpMoMLm0k2LUOFDgvlx2gKJPgmkzrbgLYyAXcCbm3HsqgsPCgyZbRqs1Q6lzk7mBLvgCfHe', 'AnnBrendjens@telefonica.nl', '1911-09-21 03:29:00', FALSE, TRUE, FALSE, 53);
INSERT INTO users VALUES (DEFAULT, 'Anne76', 'SxxoqTGbyUOqXBPZM2tdqAxJJ5wZMoLKsye802yNrPPEl72oEsncGUPQDSirq1vbo5XsKYeXZJmVC27gBVqe28yzplwwLC5SHqv7pcBIvNkNXu1d54je8EIAuBsZ7PHdgujaZM71XovEoiW7tTtLs0320sr', 'Trees.Toler@excite.fr', '1986-02-24 07:21:00', FALSE, TRUE, FALSE, 63);
INSERT INTO users VALUES (DEFAULT, 'Taylor', 'gvUuMeOUvoUSun2BoVIrttVBeSOwoSCdSqBtTRiznSBU06ETrIBexlLmne14zxNwHzInTsZEC3ePBIbJUpqkPrX6', 'TreesDeWald3@live.com', '1949-07-13 09:07:00', TRUE, TRUE, TRUE, 71);
INSERT INTO users VALUES (DEFAULT, 'Marta254', 'P4IIgiBeNp4yQOPpBXd6AK0DokN7qIQ6vF3TLITIBioJW1j52iKz0hHWiXlPDLdwWxoSMlDWQYi4sft2VveWTgVLPgZHwAbF5cOzuBZfcvPyFnlIUDu4Mddm0hfRLZIFzwh0ad02tdWNWncYO7FdcnQysBtO6rx4THezNzUeeq6oM', 'Freddy.Royal3@aol.nl', '1989-12-07 05:38:00', TRUE, FALSE, TRUE, 77);
INSERT INTO users VALUES (DEFAULT, 'Niklas4', 'IOD8JV1OaAlZ1jgf3V3R0pSkXOE5TakGNpYZOjpNAu7Kc4SGnFzZulR7mdekcYOnG7Ul1qg4AYu3G4YrXWERIAnYigWxCrYxa4uwkFwN6u8sBJ7VNZ3M3oMOul1EDQmcnHQnilwQ3mg8wGtGnMeaFAMJuc0Z6bFFbGPtWSzRpjBQKl6NsBt33y8', 'KKing@mobileme.de', '1908-11-03 06:38:00', FALSE, TRUE, FALSE, 77);
INSERT INTO users VALUES (DEFAULT, 'Will68', '8jRt0M3ErFOlY4hmhebM56b3HYlc7WTdc2sEiHsm0oansStFvcSBpbs10jEFvchfMzB8uHDesgqtxOkfWcSl53zEcjclpFOJKgDalOVWO8qI3VMmHkRtyLI6cMIOkOnCO5eYhRaOxI0InffXUeT4QnAv1Rzpzfa0lbHSA4gqKs5z3Fhw0ipH01aYZVDzvSK', 'M.Trainor@libero.de', '1937-01-17 09:22:00', FALSE, TRUE, FALSE, 77);
INSERT INTO users VALUES (DEFAULT, 'Victor2', 'KionU24HUsNoAv8ZIhjvW7EuJVHaup12Alpiuc6aSx6481d4cWwmYcADMNc8fec2gVOaKuHUysYIWRJ5iE0pyZnCJA36QGP6K2jX2D65uUVRYMgxq5QAFmc0jwawqsJOZuogObmbwMFLGtVqVLcMwYJeHGRXydrYJot5wCQJllm2fCFp52zrYsEkfMHkKZz4P2qb6paRYM5im7cxY', 'L.Wood3@kpn.es', '1930-05-02 04:19:00', TRUE, FALSE, FALSE, 78);
INSERT INTO users VALUES (DEFAULT, 'Marta048', 'jQeAaboGmfOwTJYsrORXos7YjvgODerkz5dlbofJPQfaE0swJDe4SIwtuRGuCUtCXrcQGOSZcoRo1qRqSbxIdYvVs7EEjswhP6a4REonA8EHzvkw5Cse4OxGkeAF7HeLI5yd3YoIRhQYQUQG', 'L.Brown@myspace.com', '1916-08-30 07:53:00', TRUE, TRUE, FALSE, 88);
INSERT INTO users VALUES (DEFAULT, 'Ivan2', 'OzzzU0MW4RXlQgkwLlGrh4YRBJYxgycTKz2tPvnhAP8u77J4Zm41kvafrE8kF', 'NadineChwatal@lycos.co.uk', '1969-07-11 05:46:00', FALSE, TRUE, TRUE, 96);
INSERT INTO users VALUES (DEFAULT, 'Tim574', 'mJD02JOICFVUuifcTFIHGuK7S0skwJWx71oUSqSrGpLK7ZY2k7UV55EqmIsNZwiiiO4xQkOnT4OguRCak5tVv4XRDJJlBMgQJBJSE5MxSUOmYQ5Hh84bNk76dwNiZCmhBeC30EsSibslVlVzo7TRdjV23oMQGrp', 'PJulieze@libero.dk', '1973-01-01 04:47:00', TRUE, TRUE, FALSE, 96);
INSERT INTO users VALUES (DEFAULT, 'Juana', 'f80avfcJfmiruTMmvAmVdbd7AGG1ZQj68TGChPoXQufPsEC4vc0RTpvzSeuoi67tjzgVCMemOhCfI5og63vmXESYuSklX3UItIP61Px6QXF4kW7PPzEad', 'E.Dulisse@lycos.fr', '1954-11-27 09:47:00', TRUE, TRUE, FALSE, 96);
INSERT INTO users VALUES (DEFAULT, 'Daniela47', 'AcXvyyhA55cYexGUezOs3eyXBvOUFhfObIf3tAMm8ENnY1yfGevohulhaMvbjLx73LjA6dWS62cwM40UyoknYnXO3RsjmV15eujs4VLdcPeZzGjBcWec2VjKmed2rQyYbJXaHCaG4dDpdc8xaJWruXIox', 'Ann.Paul@live.gov', '1967-12-31 07:21:00', FALSE, FALSE, FALSE, 4);
INSERT INTO users VALUES (DEFAULT, 'Kees0', 'xtzicUknvfZOFLlyGQQiC6F7Uab6PGvvew5EBcYYfX3kNCOOS7FAEnSUtd0kzVcl5Gcjgf4LYPQ5juYBCesRCBM1TnFE6sv2DgFvDYbBr5Pfcy32IN2f1nxloZTSNBwUXb1cNCTrswtyBXH5c1UrZUVA2yAtIfuOeiqPRHUkM0yBJZ8yLdB7fKPigdPWuFDbk2qpy', 'BasDeans1@telefonica.org', '1945-02-23 09:23:00', FALSE, FALSE, FALSE, 4);
INSERT INTO users VALUES (DEFAULT, 'Tomasz576', 'LPGV7VnnEC873tYbPDeDINgPQ6Mm5gSRdoMgJqqzraLjkGKwqXcQ1HPYg', 'FransHarder3@aol.ca', '1937-06-11 02:02:00', TRUE, TRUE, TRUE, 4);
INSERT INTO users VALUES (DEFAULT, 'Catherine', 'V6WvlLvDuaIJJ0yfrG2THhOnqUIhB2wwvMV8GzoH0UWeCY35wUCQr47RFk2y4ML6nONwKdXVWOrurT8GycHVnan1TPtGEjpRDIgJ0cTz7AYQXMrVAZt3DUVbrHVv4TQsTuN3rcIrVzSNpKdFHpSCsyrlrsmMM7yJkcUkZz8hYZChTx3Od0rowI5vYw23LdyR8fWo27VSrfe1y8rCB8lbFpoLKQ3SiCLtbRBR8DVkObiift3T4WnHqLawaqHaA', 'HansKellock@gawab.cn', '1987-07-17 00:02:00', FALSE, FALSE, FALSE, 7);
INSERT INTO users VALUES (DEFAULT, 'Alexander', 'LwIBFJTcKGarKLskhQRkI8lTWoX5vbKrokyzPrQhnA31rHADyV6lOa7AEH4lyCat2GRQxbfKPmyYgyMfn3d4SlssZ1u0J2z6Wmxi7V7s07I728slBKoKXAhGXeg7PRxHrAn7reJOyUXIFnxrF3M2AVv4kXMhon1aLoPvHQhsQAMGoEkGyuUmdgDklriUCP3zdL2xWNR1htOqrmUdeGO4s0qUia3B4wM4', 'PHardoon2@kpn.be', '1929-04-06 00:52:00', FALSE, FALSE, TRUE, 7);
INSERT INTO users VALUES (DEFAULT, 'Esther184', 'Jov2lXFjwVEBOgNJnyQ5zbu62aPzMFXqcsTtDZEvYt4JVZsJcAfsJAF4gVh0vqcblDessmuK3yBkAYgRwWHgNwhZB51lFAyvgLU0xc6uFatdPELSObyaQo2obs1TQ6rixjmuHYeSjHetthTvHCIYYBo5KhwzdRQYlFqWupemkiWmgQAJPIuhxdhjO1YRhzPuvMU', 'Hvan Dijk@libero.us', '1935-04-25 00:25:00', FALSE, FALSE, FALSE, 14);
INSERT INTO users VALUES (DEFAULT, 'Henk8', 'bZ23XAU6agbtG8TG4DeYI3dIEa', 'John.van Dijk1@kpn.us', '1981-01-03 08:03:00', FALSE, TRUE, FALSE, 16);
INSERT INTO users VALUES (DEFAULT, 'Lena85', 'Mut0AdRqiv0yMxCbCpx1Hzufez7DaPzrqIpj7y3OYZcgZQW7Obn3oNp2mE1lbZIQrTWixToAmynrdAoyePiEJ2jkcfJl6YJKRn', 'FrankyMcgrew3@lycos.co.uk', '1954-07-10 07:34:00', TRUE, TRUE, TRUE, 16);
INSERT INTO users VALUES (DEFAULT, 'Harry', 'B2qlOwxLrWIREE4aeLqGBvi78ybgK7g8fSS2LXxNPxq4PV31dtRJEPkFS4ZaYI36uspmOwbzR4Qjh2ncrtlwIAvd7', 'E.Haynes@libero.cn', '1902-02-08 01:53:00', TRUE, FALSE, FALSE, 16);
INSERT INTO users VALUES (DEFAULT, 'Benjamin480', 'MHjMcUmM0bwNO', 'MartinDaley5@kpn.cc', '1970-01-27 01:02:00', TRUE, TRUE, FALSE, 17);
INSERT INTO users VALUES (DEFAULT, 'Sjaak', 'vdz36gW83q8miKwvFN5U2R5BXQv7XgTCNFdFkbQRRHRDEdAmuRRHKLnpSZdMWWTmd0GIs3WOmRm4dwEkMatbXsfJnF2K3xZSp3msMLcnJfgkNEPCuNLYJ7bdEU1FNdyO6ugdMHZ4QQ2cLxf5KPQqIsDxhUykVVGZU31zsOq44xNkpM0haZgMXNTTxEqMhl08Q8izhiz8ONwMkGlmA', 'Bob.Korkovski@lycos.org', '1913-04-02 10:26:00', TRUE, FALSE, FALSE, 17);
INSERT INTO users VALUES (DEFAULT, 'Mathilde271', 'yIO2I7cp6OUCJkr2z32W1WYwaqZHCHD6ub4DBJZLj7wipHMF8zibUE5KdtOcMD3zVloOFHTBHyAHbQBbZrWvng6GT7t87RtVKEoKBqJoGZxoLFbRvQEK0VFBIWVnNulZb1GpKf7cprD0ZQqKSAPM3JL7gET5cb2MkjzpYvO3ObsVs5jhsbqxlwQN8lzJSdpgMoEyerj', 'Hans.Fox@telfort.de', '1943-05-12 03:47:00', TRUE, TRUE, TRUE, 24);
INSERT INTO users VALUES (DEFAULT, 'Christa47', '4RYnQCcUNbEA83DmcXl0Ms7nD0L2xJIaBoYhbdrip6idtaSjSnPduViNUyL67u0d8uJr8gWJ54XydPwGxET', 'J.Newman4@weboffice.no', '1995-04-21 02:12:00', FALSE, FALSE, FALSE, 24);
INSERT INTO users VALUES (DEFAULT, 'Jose44', 'gs13YfeMfZjaqSreTiD3zRt2y0wMvSri8KkuyEK42cmZilD0zJwzBikK2DcPm20mrTgsLUXey66Q7nu0cPFo8l0U51NOXp8aF4YxlthHuFgRL', 'William.Antonucci@telefonica.ca', '1971-03-15 06:42:00', FALSE, TRUE, FALSE, 28);
INSERT INTO users VALUES (DEFAULT, 'Sanne08', 'OjgADDFpMdFzwdhdJxYTT6swmxx8bcc4OdUYN2K1P0r27BvC7HbB5u2dPfZsScq6b3VogXDMmXnvMdukpx6zQvMbeYBHt4jISSpFJVrJUhaACmD8nOWbNaijxzNgGCRCYub', 'BasWilson@gawab.it', '1928-09-06 07:54:00', TRUE, FALSE, TRUE, 28);
INSERT INTO users VALUES (DEFAULT, 'Dave33', 'ogfJotavB0yGCPv4H4WzzHpm5Vcm1jCTDzFCdLb2ha8uhNYEMMDFyaDp', 'TonDulisse@gmail.org', '1972-08-02 02:13:00', TRUE, TRUE, FALSE, 28);
INSERT INTO users VALUES (DEFAULT, 'Al6', 'FLjFPemK38eZgSktxHMxc3Yr2SL0ZPSDin2HHIKnEXrs2JWGh0ajfzw6', 'Victor.Trainor@web.cn', '1974-03-05 07:11:00', FALSE, FALSE, FALSE, 33);
INSERT INTO users VALUES (DEFAULT, 'Theodore1', 'We17teCCvTsmKqTr63uMWXtnShk3kzg0KyUh3y2UCLvFu1GjOntDqJS04UBF76IGWqd1Imvq1PjI8WMwYkUCCaXqd5YyBVjWvXInY4PDO14PZ6rAfikSnAW5BJxkqzFworqEwwUHdLG23FcKuj4Dp2fHPjO58qrRbb016GVcORsjjX1W', 'RichardPerilloux@freeweb.cc', '1977-03-16 01:04:00', FALSE, FALSE, FALSE, 33);
INSERT INTO users VALUES (DEFAULT, 'Ron6', 'hA6jRK3CToNO0lZXP12n44uu4Le7ZJI0yKz0zL5T4IZcApeS7C8rB0CGfE7zoJENaWy1ShBBpdnmEhSNefYEFWnSj3m8Zu2bta8zgeoipWny4jDAIbkoNiuZ78NUxwd1hnQZDL8ohkYJThd0uSMtcgbpOgNOK6Kv4COoMM0BilXPBDnxybmMB8gaA1jGVCZKdudtEBLcKCxuIhgEVk4TzYon7KdRIXIR8z3AK4ehcDSFRlJYhywPSB6J', 'CarlaPekagnan2@hotmail.co.uk', '1987-04-02 03:05:00', FALSE, FALSE, FALSE, 36);
INSERT INTO users VALUES (DEFAULT, 'Scottie2', 'EQJRmHrdqXtd7OCCbkpG1VijDlMmPh6vnSup7ur6bphxwWUPNzgy5DgiPFR7WeMnzgXeTYC6sJVZjkqLFUAeWRA6GkZT0fyikuEEbhHMR3bvgij3dog7Q8gW68cVX3y8Eb6R0X3X0ksbVDP06ZotV44', 'Emma.Browne@mail.es', '1961-02-28 09:28:00', TRUE, TRUE, TRUE, 36);
INSERT INTO users VALUES (DEFAULT, 'Rosa3', 'aMkiimlWbdzgsuTbtPlWQyyRV6mYARMgCbCZPE7hWzmj063IWNECH5legLlkIF3vjb1TOPDtLk2locHwNJzGjjFfHCKCTVLMDCBOv0tGn38TcMeCWiqJYRGPgPIxSAtpBqi', 'RickWakefield@lycos.de', '1968-01-01 10:45:00', TRUE, FALSE, FALSE, 46);
INSERT INTO users VALUES (DEFAULT, 'Vincent2', 'XWARasmamP6S7RgEAP78zD35MKCCsnc3Wfj4jGxXhF7xkYTeMmZfEyoTqfaEE12kQM4rTSoo5ANP2JjFJkCVMgCGtZEfTGyDAq21TXltj2C8ARGplkGBXo3g552TOjnIMVEtV7q2G3YvI76Q181un6zUqvoraMWHgFNpJjpny2s0d1FcZjMzXwTbxEl0fsiGNEsbvXz5u7nMvIENjWqNFVFuEA7d5ZEoNR', 'DBraconi3@mobileme.it', '1916-01-10 09:13:00', FALSE, FALSE, TRUE, 46);
INSERT INTO users VALUES (DEFAULT, 'Tonnie', 'BJ2FAS7sDMPZBExlyLGDw7Iupr5kolJhpbeudTLm3Bmnvyrd32eENMCrPBPJM6x2OwoxBE1SirSiTuyzIKDZplATHO1y0A71RZsx0mlpPtaX58fAtRhI4CSjsLbasw4xiTnBhNN33LLmQxG4vz2JOlxKyyx0ByVnRteamCuo6fOxKZqpwsHK', 'Bas.Otto2@msn.dk', '1963-07-07 06:17:00', FALSE, FALSE, FALSE, 52);
INSERT INTO users VALUES (DEFAULT, 'Jane768', 'AqO7hu', 'RichardZurich4@excite.cc', '1955-10-17 02:08:00', TRUE, TRUE, TRUE, 52);
INSERT INTO users VALUES (DEFAULT, 'Giel09', 'LmbaO6qniXBXTJ652Xt0wV1R2SXnVowMkJtCA8lpOioltV2vATAenkftxHWzARzpnHlBMNWKnW3eEv40h85KNeTXthdk34VOqcev1GfO6PpUz0ag2XSLcSEAfQvlOrddXhNYJ7BNJc8HpTITgMJCR0sjo', 'YStockton@web.nl', '1916-03-30 04:57:00', TRUE, FALSE, TRUE, 56);
INSERT INTO users VALUES (DEFAULT, 'Scott85', 'CXZBBTp1euvwPxuT6IJEUieiAlXxjL3BWgYJdbCwZuHHGlnV7gFmnt0sZ5', 'I.Foreman@gawab.dk', '1965-05-08 10:20:00', TRUE, FALSE, TRUE, 65);
INSERT INTO users VALUES (DEFAULT, 'Albert2', '6Lfsl1fDtdP3DEgUxzODqsApHUhFUY75vKAgC7nj661Y2ZWNy', 'YJohnson1@kpn.net', '1933-10-29 05:02:00', TRUE, FALSE, TRUE, 65);
INSERT INTO users VALUES (DEFAULT, 'Theo', 'Nn5l5chEZLOyODjgtY2af4EGeYCglZ5jEBcwmporGaAWjSu6P2V1AmSzzlMlHGtE2LhVqNhzbrtBh0Vrb7PCLBS4GOZR2iw8lOAXmnJtwzcpvVTSMqzI3NeHuLHqewotX3tF7NqEqE5RTjkTpvXb7o3t63wplHCKuQwFI4d0tGPVSUgxvOb8cgJ240xd5ZsGe8YEFnL2xLsaCdzIzhvAtUxoq5mQllSWyIxxk1R', 'H.Slater5@yahoo.com', '1923-07-05 03:31:00', TRUE, TRUE, TRUE, 68);
INSERT INTO users VALUES (DEFAULT, 'Catharine12', 'VNgbAg6wyMNXQ3rfCgXWwEIzbNpbuhwJ3osTLzIC', 'EMcCrary@freeweb.ca', '1984-12-26 02:04:00', TRUE, TRUE, FALSE, 73);
INSERT INTO users VALUES (DEFAULT, 'Joop', 'ZVQonfYPGYaqnSGwIYlyhTpJHJuZFOo0l0KsDnOmQXv2aE80KYlG1z47QfuDLhVIm6D7LpzLapOT2XHh6iTryirW8qTD7WBdJrQRuzdEqaSM4eKNXK', 'Frank.Bryant@mail.no', '1925-11-01 01:43:00', FALSE, TRUE, TRUE, 73);
INSERT INTO users VALUES (DEFAULT, 'Albert9', 'pZqTNoWPyv4H48j6NihGx1go4sQsIBBLkm4KlasoZmexWwcCHXdA4Lk4NIquHwqR6vO3NkySCWHCGpGtVflp6Wpmvi1yimmqAL20AjYVOlcQuurqvQRBg8rOmggrBOQzHJkhIZqswcRdS1q4Sbewxax4m1fb6WpGHIRCQafqOSxWlNHQCiJaDPEMjIV1elhQyDFeJN6aoSi5TDMkuBUSgHuzWcNNBLihQvlFS0dlsU2', 'Pete.Ijukop@telefonica.ca', '1974-04-03 07:02:00', TRUE, FALSE, TRUE, 75);
INSERT INTO users VALUES (DEFAULT, 'Nico6', '8glnysaqJJrDohf7pv7yUA4eYuDlQHcHFwqh0sOSbiTsK08KL1i6EQyzVrd53SvkEboPmaSbyRKlhyDWBZPkDHeqKhyRWMSW20WEuWCMhlsDMFEdpj1oukvQBKlhSEcLOrCmkujFqA3J8EoACNS576Rtl8vRbNuzxPqzj6dOp3U2FCD00XRU', 'HankRay1@dolfijn.nl', '1978-08-09 05:16:00', FALSE, FALSE, FALSE, 75);
INSERT INTO users VALUES (DEFAULT, 'Cathy2', 'RUjzdOhrg5BWhi5a1P8LxF7vFbIusQYEMFPbL1JtScgzVx55oypMgRrOdWJnzboG0yqfn6jMJl5LqpikpyJtyOhTzqvAXKht4ggmsXVDBA5o28xLRVcIxABNfWnvCj3bjPAuOaEqUKQ8IAvOTEP3quQEmB1QX', 'Ton.Boyer@gawab.nl', '1975-02-22 10:36:00', TRUE, TRUE, TRUE, 75);
INSERT INTO users VALUES (DEFAULT, 'Tomasz', '50QSUkZAzFBCeTpc2fVUDr8ZUqqTBN0kXEzVtxLvrNIGnkR', 'TreesForsberg1@mobileme.cc', '1906-09-29 01:54:00', TRUE, FALSE, FALSE, 76);
INSERT INTO users VALUES (DEFAULT, 'Leontien9', 'XXjLJLJ', 'BiancaAngarano1@myspace.nl', '1995-02-02 06:50:00', FALSE, FALSE, FALSE, 76);
INSERT INTO users VALUES (DEFAULT, 'Leah62', 'ypkFLjb0XuJMlYGYrmwhYMv0Wlolch2pjPg12OHxwCbn0XDhPRvNs2oZAuckAoGU032DxjN0ChHN3ndt3vZr16Y3OsdXJpWPmZZnZPiVkDb8hj3R08ySb3ulOHKAzO1pRBJOUT7gkOsg5Y2ogcXojmGDnjZuQ5akH2tT21q6rkHclF763YYg18ujxhyrdasgOMUUVb3QbDzP2WAeKFRQ', 'Peter.Weaver@dolfijn.es', '1944-02-03 04:38:00', FALSE, TRUE, TRUE, 76);
INSERT INTO users VALUES (DEFAULT, 'Daniel945', 'P8ESSyMHY6afcW5RfvhP5EiU5dQ20I5uEcnSIqmbfXYtHYNcl6y6D68nHLdSXKgCvghayeLNKENvJ4StC0gvEdYyJvZgJ6HsoBtx2kZ64YCEUc1ORd6N8LLMSvGBQyZzpCXD0do1CWEJuhnnBXvM8nboaewnIZ60lmpwdPa1g2vlu5w3F0WcImNFeNf8xnrg6A52xUVThIcT8IBynboL8lr', 'P.Depew@kpn.net', '1975-04-30 03:50:00', TRUE, FALSE, TRUE, 82);
INSERT INTO users VALUES (DEFAULT, 'Magnus164', 'q4D70fN24L6HZN0tJvAw6rtofLJ2GqzwoF7Nlv5iWyu5rw1RmK3VZfEeNtlU4ci3IWf3bpNNbyaT3GbYjpJNxIIaKoDZ7a4oG3sJztHbPjBptiik1u1iEa8xGefP1hH7CXno2WaWs04KQsVLGuyoEp5OLICyMMcR72yCRIb6KHA768yU30D3kQ8aztkxyUBEajaAeY3t7diCGyPiHdO6b0pKaG0W5l2TpykpucZkuP77Hm0K2yAlMi', 'A.Ditmanen@mobileme.cc', '1945-12-20 08:32:00', TRUE, TRUE, FALSE, 82);

INSERT INTO publications VALUES (1, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-07-25 03:36:00', 5, 5);
INSERT INTO publications VALUES (2, 'Publication title', 'My publication here.', 'FRIENDS', '2014-05-14 03:51:00', 14, 5);
INSERT INTO publications VALUES (3, 'Publication title', 'My publication here.', 'SELF', '2014-10-31 00:50:00', 14, 8);
INSERT INTO publications VALUES (4, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-08-12 06:43:00', 20, NULL);
INSERT INTO publications VALUES (5, 'Publication title', 'My publication here.', 'GLOBAL', '2014-02-03 06:35:00', 20, NULL);
INSERT INTO publications VALUES (6, 'Publication title', 'My publication here.', 'FRIENDS', '2014-11-13 10:36:00', 20, NULL);
INSERT INTO publications VALUES (7, 'Publication title', 'My publication here.', 'SELF', '2014-10-28 10:14:00', 21, NULL);
INSERT INTO publications VALUES (8, 'Publication title', 'My publication here.', 'GLOBAL', '2014-11-15 09:14:00', 21, NULL);
INSERT INTO publications VALUES (9, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2015-01-29 03:49:00', 21, 17);
INSERT INTO publications VALUES (10, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2015-04-06 02:16:00', 30, 17);
INSERT INTO publications VALUES (11, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-02-23 05:03:00', 30, 17);
INSERT INTO publications VALUES (12, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-01-09 10:12:00', 32, 19);
INSERT INTO publications VALUES (13, 'Publication title', 'My publication here.', 'FRIENDS', '2014-01-10 05:56:00', 32, 19);
INSERT INTO publications VALUES (14, 'Publication title', 'My publication here.', 'SELF', '2014-09-28 06:38:00', 32, 23);
INSERT INTO publications VALUES (15, 'Publication title', 'My publication here.', 'SELF', '2014-07-07 01:18:00', 41, 28);
INSERT INTO publications VALUES (16, 'Publication title', 'My publication here.', 'FRIENDS', '2014-03-03 04:31:00', 41, 28);
INSERT INTO publications VALUES (17, 'Publication title', 'My publication here.', 'FRIENDS', '2014-07-21 09:12:00', 41, 28);
INSERT INTO publications VALUES (18, 'Publication title', 'My publication here.', 'GLOBAL', '2015-01-13 00:49:00', 42, 35);
INSERT INTO publications VALUES (19, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-09-04 08:49:00', 42, 35);
INSERT INTO publications VALUES (20, 'Publication title', 'My publication here.', 'GLOBAL', '2014-01-28 03:43:00', 47, 44);
INSERT INTO publications VALUES (21, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-01-13 09:17:00', 52, 50);
INSERT INTO publications VALUES (22, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-05-15 00:31:00', 55, 50);
INSERT INTO publications VALUES (23, 'Publication title', 'My publication here.', 'FRIENDS', '2014-12-13 10:55:00', 64, 59);
INSERT INTO publications VALUES (24, 'Publication title', 'My publication here.', 'GLOBAL', '2014-09-07 04:21:00', 65, 68);
INSERT INTO publications VALUES (25, 'Publication title', 'My publication here.', 'SELF', '2014-07-15 05:15:00', 65, 69);
INSERT INTO publications VALUES (26, 'Publication title', 'My publication here.', 'SELF', '2014-06-19 08:48:00', 74, 69);
INSERT INTO publications VALUES (27, 'Publication title', 'My publication here.', 'SELF', '2014-12-12 06:07:00', 74, 69);
INSERT INTO publications VALUES (28, 'Publication title', 'My publication here.', 'FRIENDS', '2014-12-06 00:20:00', 84, NULL);
INSERT INTO publications VALUES (29, 'Publication title', 'My publication here.', 'GLOBAL', '2014-01-23 00:36:00', 84, NULL);
INSERT INTO publications VALUES (30, 'Publication title', 'My publication here.', 'SELF', '2014-06-08 05:09:00', 92, 74);
INSERT INTO publications VALUES (31, 'Publication title', 'My publication here.', 'SELF', '2014-04-12 03:27:00', 92, 74);
INSERT INTO publications VALUES (32, 'Publication title', 'My publication here.', 'SELF', '2014-06-30 01:46:00', 99, 74);
INSERT INTO publications VALUES (33, 'Publication title', 'My publication here.', 'SELF', '2014-10-20 09:42:00', 99, 77);
INSERT INTO publications VALUES (34, 'Publication title', 'My publication here.', 'SELF', '2014-05-17 08:30:00', 99, NULL);
INSERT INTO publications VALUES (35, 'Publication title', 'My publication here.', 'FRIENDS', '2015-03-14 08:57:00', 3, NULL);
INSERT INTO publications VALUES (36, 'Publication title', 'My publication here.', 'FRIENDS', '2014-11-03 02:27:00', 9, NULL);
INSERT INTO publications VALUES (37, 'Publication title', 'My publication here.', 'SELF', '2014-06-26 00:30:00', 9, 81);
INSERT INTO publications VALUES (38, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-04-01 03:15:00', 18, NULL);
INSERT INTO publications VALUES (39, 'Publication title', 'My publication here.', 'FRIENDS', '2015-02-25 03:02:00', 18, NULL);
INSERT INTO publications VALUES (40, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2015-03-22 01:58:00', 18, NULL);
INSERT INTO publications VALUES (41, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-10-26 01:27:00', 24, 83);
INSERT INTO publications VALUES (42, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-01-14 07:45:00', 24, 83);
INSERT INTO publications VALUES (43, 'Publication title', 'My publication here.', 'GLOBAL', '2014-09-08 07:09:00', 24, NULL);
INSERT INTO publications VALUES (44, 'Publication title', 'My publication here.', 'GLOBAL', '2015-01-28 04:42:00', 25, NULL);
INSERT INTO publications VALUES (45, 'Publication title', 'My publication here.', 'GLOBAL', '2015-03-15 04:14:00', 28, 88);
INSERT INTO publications VALUES (46, 'Publication title', 'My publication here.', 'SELF', '2014-05-03 06:17:00', 34, 88);
INSERT INTO publications VALUES (47, 'Publication title', 'My publication here.', 'GLOBAL', '2014-08-30 06:11:00', 41, 91);
INSERT INTO publications VALUES (48, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-12-22 02:28:00', 41, NULL);
INSERT INTO publications VALUES (49, 'Publication title', 'My publication here.', 'SELF', '2014-08-07 00:40:00', 44, NULL);
INSERT INTO publications VALUES (50, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-04-21 08:28:00', 49, 98);
INSERT INTO publications VALUES (51, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2015-03-05 09:57:00', 49, NULL);
INSERT INTO publications VALUES (52, 'Publication title', 'My publication here.', 'FRIENDS', '2014-04-30 03:37:00', 49, NULL);
INSERT INTO publications VALUES (53, 'Publication title', 'My publication here.', 'SELF', '2015-03-06 01:58:00', 54, NULL);
INSERT INTO publications VALUES (54, 'Publication title', 'My publication here.', 'FRIENDS', '2014-07-21 06:50:00', 55, NULL);
INSERT INTO publications VALUES (55, 'Publication title', 'My publication here.', 'FRIENDS', '2014-12-30 01:14:00', 55, NULL);
INSERT INTO publications VALUES (56, 'Publication title', 'My publication here.', 'FRIENDS', '2015-04-07 01:33:00', 60, 2);
INSERT INTO publications VALUES (57, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-04-20 06:03:00', 60, 2);
INSERT INTO publications VALUES (58, 'Publication title', 'My publication here.', 'SELF', '2015-03-08 08:11:00', 60, 2);
INSERT INTO publications VALUES (59, 'Publication title', 'My publication here.', 'SELF', '2014-09-19 03:06:00', 69, 11);
INSERT INTO publications VALUES (60, 'Publication title', 'My publication here.', 'GLOBAL', '2015-04-02 04:50:00', 77, 11);
INSERT INTO publications VALUES (61, 'Publication title', 'My publication here.', 'GLOBAL', '2014-04-23 06:50:00', 87, NULL);
INSERT INTO publications VALUES (62, 'Publication title', 'My publication here.', 'GLOBAL', '2015-03-18 10:44:00', 87, NULL);
INSERT INTO publications VALUES (63, 'Publication title', 'My publication here.', 'FRIENDS', '2015-01-14 03:41:00', 87, 12);
INSERT INTO publications VALUES (64, 'Publication title', 'My publication here.', 'FRIENDS', '2014-10-13 07:35:00', 91, 15);
INSERT INTO publications VALUES (65, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-09-05 04:46:00', 91, 15);
INSERT INTO publications VALUES (66, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2015-01-01 06:11:00', 99, 22);
INSERT INTO publications VALUES (67, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-12-13 09:19:00', 99, 22);
INSERT INTO publications VALUES (68, 'Publication title', 'My publication here.', 'SELF', '2014-03-21 07:41:00', 4, 23);
INSERT INTO publications VALUES (69, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-12-25 09:51:00', 4, 31);
INSERT INTO publications VALUES (70, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-04-06 10:37:00', 12, 31);
INSERT INTO publications VALUES (71, 'Publication title', 'My publication here.', 'SELF', '2014-11-14 10:30:00', 12, 31);
INSERT INTO publications VALUES (72, 'Publication title', 'My publication here.', 'FRIENDS', '2014-12-24 00:28:00', 21, 36);
INSERT INTO publications VALUES (73, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2015-03-29 09:39:00', 26, 36);
INSERT INTO publications VALUES (74, 'Publication title', 'My publication here.', 'FRIENDS', '2014-01-24 07:28:00', 26, NULL);
INSERT INTO publications VALUES (75, 'Publication title', 'My publication here.', 'SELF', '2014-11-17 01:01:00', 36, NULL);
INSERT INTO publications VALUES (76, 'Publication title', 'My publication here.', 'SELF', '2014-11-06 03:22:00', 36, NULL);
INSERT INTO publications VALUES (77, 'Publication title', 'My publication here.', 'GLOBAL', '2014-09-26 02:27:00', 45, NULL);
INSERT INTO publications VALUES (78, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2015-03-23 09:12:00', 45, NULL);
INSERT INTO publications VALUES (79, 'Publication title', 'My publication here.', 'FRIENDS', '2014-08-27 05:01:00', 46, NULL);
INSERT INTO publications VALUES (80, 'Publication title', 'My publication here.', 'GLOBAL', '2014-03-22 03:47:00', 50, 42);
INSERT INTO publications VALUES (81, 'Publication title', 'My publication here.', 'SELF', '2014-02-26 01:08:00', 50, 42);
INSERT INTO publications VALUES (82, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-09-08 04:39:00', 60, 50);
INSERT INTO publications VALUES (83, 'Publication title', 'My publication here.', 'SELF', '2014-04-20 01:56:00', 60, NULL);
INSERT INTO publications VALUES (84, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-02-25 01:18:00', 60, NULL);
INSERT INTO publications VALUES (85, 'Publication title', 'My publication here.', 'GLOBAL', '2014-10-01 09:06:00', 68, 56);
INSERT INTO publications VALUES (86, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-05-27 04:14:00', 68, 56);
INSERT INTO publications VALUES (87, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-10-15 06:56:00', 74, 56);
INSERT INTO publications VALUES (88, 'Publication title', 'My publication here.', 'SELF', '2014-12-25 07:35:00', 81, NULL);
INSERT INTO publications VALUES (89, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-07-11 08:32:00', 81, NULL);
INSERT INTO publications VALUES (90, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-07-01 05:35:00', 85, NULL);
INSERT INTO publications VALUES (91, 'Publication title', 'My publication here.', 'GLOBAL', '2015-04-10 10:29:00', 85, NULL);
INSERT INTO publications VALUES (92, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-11-25 00:35:00', 85, NULL);
INSERT INTO publications VALUES (93, 'Publication title', 'My publication here.', 'SELF', '2014-04-24 10:00:00', 86, 64);
INSERT INTO publications VALUES (94, 'Publication title', 'My publication here.', 'SELF', '2014-02-13 05:21:00', 86, NULL);
INSERT INTO publications VALUES (95, 'Publication title', 'My publication here.', 'FRIENDS', '2014-02-18 10:20:00', 95, NULL);
INSERT INTO publications VALUES (96, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-06-30 09:31:00', 95, 69);
INSERT INTO publications VALUES (97, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-12-29 09:10:00', 1, 69);
INSERT INTO publications VALUES (98, 'Publication title', 'My publication here.', 'SELF', '2014-07-20 07:08:00', 1, NULL);
INSERT INTO publications VALUES (99, 'Publication title', 'My publication here.', 'GLOBAL', '2014-03-02 01:53:00', 1, NULL);
INSERT INTO publications VALUES (100, 'Publication title', 'My publication here.', 'GLOBAL', '2014-11-14 00:36:00', 3, NULL);
INSERT INTO publications VALUES (101, 'Publication title', 'My publication here.', 'GLOBAL', '2014-05-05 00:48:00', 3, NULL);
INSERT INTO publications VALUES (102, 'Publication title', 'My publication here.', 'SELF', '2014-03-30 07:42:00', 3, NULL);
INSERT INTO publications VALUES (103, 'Publication title', 'My publication here.', 'GLOBAL', '2014-03-26 10:06:00', 5, NULL);
INSERT INTO publications VALUES (104, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-08-31 00:26:00', 5, NULL);
INSERT INTO publications VALUES (105, 'Publication title', 'My publication here.', 'FRIENDS', '2014-02-19 01:42:00', 5, 77);
INSERT INTO publications VALUES (106, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-02-26 10:50:00', 12, 77);
INSERT INTO publications VALUES (107, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-06-08 04:34:00', 12, 77);
INSERT INTO publications VALUES (108, 'Publication title', 'My publication here.', 'FRIENDS', '2014-04-26 01:25:00', 12, 78);
INSERT INTO publications VALUES (109, 'Publication title', 'My publication here.', 'FRIENDS', '2014-01-21 01:03:00', 18, 78);
INSERT INTO publications VALUES (110, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-09-16 02:27:00', 18, 82);
INSERT INTO publications VALUES (111, 'Publication title', 'My publication here.', 'GLOBAL', '2015-03-16 10:41:00', 23, 82);
INSERT INTO publications VALUES (112, 'Publication title', 'My publication here.', 'FRIENDS', '2015-03-05 02:00:00', 30, 84);
INSERT INTO publications VALUES (113, 'Publication title', 'My publication here.', 'SELF', '2014-02-16 01:43:00', 34, 84);
INSERT INTO publications VALUES (114, 'Publication title', 'My publication here.', 'SELF', '2014-12-21 07:54:00', 34, NULL);
INSERT INTO publications VALUES (115, 'Publication title', 'My publication here.', 'FRIENDS', '2014-03-05 01:17:00', 34, NULL);
INSERT INTO publications VALUES (116, 'Publication title', 'My publication here.', 'SELF', '2014-10-15 09:47:00', 44, 90);
INSERT INTO publications VALUES (117, 'Publication title', 'My publication here.', 'FRIENDS', '2015-03-20 07:02:00', 44, 90);
INSERT INTO publications VALUES (118, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-02-27 09:00:00', 52, NULL);
INSERT INTO publications VALUES (119, 'Publication title', 'My publication here.', 'FRIENDS', '2014-01-28 00:02:00', 59, NULL);
INSERT INTO publications VALUES (120, 'Publication title', 'My publication here.', 'FRIENDS', '2014-09-17 06:55:00', 66, NULL);
INSERT INTO publications VALUES (121, 'Publication title', 'My publication here.', 'SELF', '2015-04-12 03:02:00', 66, 93);
INSERT INTO publications VALUES (122, 'Publication title', 'My publication here.', 'FRIENDS', '2015-03-11 04:10:00', 74, NULL);
INSERT INTO publications VALUES (123, 'Publication title', 'My publication here.', 'GLOBAL', '2014-07-16 10:05:00', 74, NULL);
INSERT INTO publications VALUES (124, 'Publication title', 'My publication here.', 'FRIENDS', '2014-08-11 08:01:00', 75, NULL);
INSERT INTO publications VALUES (125, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-10-14 06:06:00', 75, 3);
INSERT INTO publications VALUES (126, 'Publication title', 'My publication here.', 'SELF', '2015-02-12 10:11:00', 77, 8);
INSERT INTO publications VALUES (127, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-10-07 10:22:00', 77, 12);
INSERT INTO publications VALUES (128, 'Publication title', 'My publication here.', 'GLOBAL', '2014-01-26 00:39:00', 77, 16);
INSERT INTO publications VALUES (129, 'Publication title', 'My publication here.', 'SELF', '2015-01-05 02:57:00', 82, 19);
INSERT INTO publications VALUES (130, 'Publication title', 'My publication here.', 'SELF', '2014-08-13 00:08:00', 82, 19);
INSERT INTO publications VALUES (131, 'Publication title', 'My publication here.', 'SELF', '2014-04-15 10:55:00', 82, 19);
INSERT INTO publications VALUES (132, 'Publication title', 'My publication here.', 'FRIENDS', '2014-06-17 08:09:00', 84, 20);
INSERT INTO publications VALUES (133, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2015-04-08 04:14:00', 84, 20);
INSERT INTO publications VALUES (134, 'Publication title', 'My publication here.', 'GLOBAL', '2014-12-06 02:18:00', 93, 30);
INSERT INTO publications VALUES (135, 'Publication title', 'My publication here.', 'GLOBAL', '2014-04-29 00:06:00', 100, 30);
INSERT INTO publications VALUES (136, 'Publication title', 'My publication here.', 'SELF', '2014-12-13 04:02:00', 100, 30);
INSERT INTO publications VALUES (137, 'Publication title', 'My publication here.', 'GLOBAL', '2014-10-10 10:28:00', 100, 32);
INSERT INTO publications VALUES (138, 'Publication title', 'My publication here.', 'GLOBAL', '2014-09-04 07:27:00', 5, 32);
INSERT INTO publications VALUES (139, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-11-15 05:30:00', 5, 32);
INSERT INTO publications VALUES (140, 'Publication title', 'My publication here.', 'FRIENDS', '2014-11-22 06:52:00', 7, 38);
INSERT INTO publications VALUES (141, 'Publication title', 'My publication here.', 'SELF', '2014-01-28 01:20:00', 8, 38);
INSERT INTO publications VALUES (142, 'Publication title', 'My publication here.', 'FRIENDS', '2014-06-22 00:55:00', 8, 41);
INSERT INTO publications VALUES (143, 'Publication title', 'My publication here.', 'GLOBAL', '2014-04-07 07:25:00', 17, 41);
INSERT INTO publications VALUES (144, 'Publication title', 'My publication here.', 'FRIENDS', '2014-11-13 06:48:00', 17, 41);
INSERT INTO publications VALUES (145, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-07-19 10:57:00', 19, 43);
INSERT INTO publications VALUES (146, 'Publication title', 'My publication here.', 'SELF', '2014-08-26 10:52:00', 19, 43);
INSERT INTO publications VALUES (147, 'Publication title', 'My publication here.', 'FRIENDS', '2014-07-11 10:15:00', 22, 47);
INSERT INTO publications VALUES (148, 'Publication title', 'My publication here.', 'GLOBAL', '2014-12-26 09:37:00', 22, 47);
INSERT INTO publications VALUES (149, 'Publication title', 'My publication here.', 'SELF', '2014-08-20 07:48:00', 22, 47);
INSERT INTO publications VALUES (150, 'Publication title', 'My publication here.', 'GLOBAL', '2014-05-01 06:29:00', 31, 54);
INSERT INTO publications VALUES (151, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-08-01 10:15:00', 31, 61);
INSERT INTO publications VALUES (152, 'Publication title', 'My publication here.', 'SELF', '2015-01-07 00:41:00', 33, 61);
INSERT INTO publications VALUES (153, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-01-27 03:10:00', 41, 61);
INSERT INTO publications VALUES (154, 'Publication title', 'My publication here.', 'GLOBAL', '2014-05-17 01:43:00', 48, 65);
INSERT INTO publications VALUES (155, 'Publication title', 'My publication here.', 'GLOBAL', '2014-08-23 10:09:00', 58, 65);
INSERT INTO publications VALUES (156, 'Publication title', 'My publication here.', 'SELF', '2015-01-11 08:57:00', 64, 75);
INSERT INTO publications VALUES (157, 'Publication title', 'My publication here.', 'SELF', '2014-10-11 09:32:00', 64, 75);
INSERT INTO publications VALUES (158, 'Publication title', 'My publication here.', 'SELF', '2014-06-02 10:24:00', 64, 75);
INSERT INTO publications VALUES (159, 'Publication title', 'My publication here.', 'FRIENDS', '2015-04-11 00:02:00', 73, 76);
INSERT INTO publications VALUES (160, 'Publication title', 'My publication here.', 'FRIENDS', '2014-12-12 10:37:00', 73, 82);
INSERT INTO publications VALUES (161, 'Publication title', 'My publication here.', 'SELF', '2014-12-25 02:42:00', 73, 91);
INSERT INTO publications VALUES (162, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-01-31 00:40:00', 82, 91);
INSERT INTO publications VALUES (163, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-04-01 02:11:00', 82, 91);
INSERT INTO publications VALUES (164, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-06-04 06:19:00', 91, 1);
INSERT INTO publications VALUES (165, 'Publication title', 'My publication here.', 'FRIENDS', '2015-01-21 02:25:00', 91, 7);
INSERT INTO publications VALUES (166, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-11-21 01:14:00', 91, 7);
INSERT INTO publications VALUES (167, 'Publication title', 'My publication here.', 'SELF', '2014-09-22 03:00:00', 93, 7);
INSERT INTO publications VALUES (168, 'Publication title', 'My publication here.', 'FRIENDS', '2014-04-18 04:47:00', 93, 15);
INSERT INTO publications VALUES (169, 'Publication title', 'My publication here.', 'FRIENDS', '2014-09-05 08:08:00', 99, 15);
INSERT INTO publications VALUES (170, 'Publication title', 'My publication here.', 'FRIENDS', '2015-03-22 06:30:00', 99, 15);
INSERT INTO publications VALUES (171, 'Publication title', 'My publication here.', 'SELF', '2014-11-25 08:44:00', 8, 17);
INSERT INTO publications VALUES (172, 'Publication title', 'My publication here.', 'GLOBAL', '2015-03-08 01:15:00', 15, 22);
INSERT INTO publications VALUES (173, 'Publication title', 'My publication here.', 'GLOBAL', '2015-04-06 00:35:00', 15, 22);
INSERT INTO publications VALUES (174, 'Publication title', 'My publication here.', 'GLOBAL', '2014-02-27 00:06:00', 17, 22);
INSERT INTO publications VALUES (175, 'Publication title', 'My publication here.', 'FRIENDS', '2014-10-10 05:31:00', 17, 28);
INSERT INTO publications VALUES (176, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-12-04 03:57:00', 22, 30);
INSERT INTO publications VALUES (177, 'Publication title', 'My publication here.', 'FRIENDS', '2015-03-06 06:16:00', 31, NULL);
INSERT INTO publications VALUES (178, 'Publication title', 'My publication here.', 'SELF', '2014-09-13 02:09:00', 34, NULL);
INSERT INTO publications VALUES (179, 'Publication title', 'My publication here.', 'FRIENDS', '2015-02-06 07:04:00', 43, NULL);
INSERT INTO publications VALUES (180, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-11-20 06:49:00', 43, 35);
INSERT INTO publications VALUES (181, 'Publication title', 'My publication here.', 'FRIENDS', '2014-02-03 00:01:00', 46, 41);
INSERT INTO publications VALUES (182, 'Publication title', 'My publication here.', 'FRIENDS', '2015-02-01 06:33:00', 46, 41);
INSERT INTO publications VALUES (183, 'Publication title', 'My publication here.', 'FRIENDS', '2014-05-21 01:44:00', 46, 41);
INSERT INTO publications VALUES (184, 'Publication title', 'My publication here.', 'GLOBAL', '2014-02-20 00:44:00', 48, 49);
INSERT INTO publications VALUES (185, 'Publication title', 'My publication here.', 'SELF', '2014-04-20 05:42:00', 48, 49);
INSERT INTO publications VALUES (186, 'Publication title', 'My publication here.', 'GLOBAL', '2014-03-19 06:35:00', 48, 55);
INSERT INTO publications VALUES (187, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2015-03-08 04:53:00', 53, 55);
INSERT INTO publications VALUES (188, 'Publication title', 'My publication here.', 'SELF', '2014-08-09 01:01:00', 63, 55);
INSERT INTO publications VALUES (189, 'Publication title', 'My publication here.', 'GLOBAL', '2014-07-07 08:03:00', 63, 59);
INSERT INTO publications VALUES (190, 'Publication title', 'My publication here.', 'GLOBAL', '2015-03-18 10:52:00', 71, 59);
INSERT INTO publications VALUES (191, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-08-17 05:01:00', 71, 69);
INSERT INTO publications VALUES (192, 'Publication title', 'My publication here.', 'SELF', '2014-05-25 02:54:00', 74, 69);
INSERT INTO publications VALUES (193, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-07-30 10:34:00', 74, 77);
INSERT INTO publications VALUES (194, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-09-07 07:53:00', 74, 77);
INSERT INTO publications VALUES (195, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-12-26 03:58:00', 81, NULL);
INSERT INTO publications VALUES (196, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-04-13 01:58:00', 90, NULL);
INSERT INTO publications VALUES (197, 'Publication title', 'My publication here.', 'SELF', '2015-02-08 10:17:00', 90, NULL);
INSERT INTO publications VALUES (198, 'Publication title', 'My publication here.', 'GLOBAL', '2014-04-20 04:34:00', 97, 87);
INSERT INTO publications VALUES (199, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-01-09 05:00:00', 97, 88);
INSERT INTO publications VALUES (200, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-01-18 05:12:00', 97, 88);
INSERT INTO publications VALUES (201, 'Publication title', 'My publication here.', 'GLOBAL', '2014-12-30 05:37:00', 1, 92);
INSERT INTO publications VALUES (202, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-08-20 05:21:00', 1, 92);
INSERT INTO publications VALUES (203, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-08-18 02:54:00', 9, 93);
INSERT INTO publications VALUES (204, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-01-11 00:38:00', 9, 93);
INSERT INTO publications VALUES (205, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-09-13 09:40:00', 9, 94);
INSERT INTO publications VALUES (206, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2015-01-01 07:24:00', 19, 94);
INSERT INTO publications VALUES (207, 'Publication title', 'My publication here.', 'SELF', '2015-02-05 06:53:00', 19, 94);
INSERT INTO publications VALUES (208, 'Publication title', 'My publication here.', 'FRIENDS', '2015-01-14 09:38:00', 26, 3);
INSERT INTO publications VALUES (209, 'Publication title', 'My publication here.', 'SELF', '2015-02-20 01:41:00', 26, 3);
INSERT INTO publications VALUES (210, 'Publication title', 'My publication here.', 'SELF', '2014-06-28 08:33:00', 26, 3);
INSERT INTO publications VALUES (211, 'Publication title', 'My publication here.', 'FRIENDS', '2014-05-27 01:19:00', 34, NULL);
INSERT INTO publications VALUES (212, 'Publication title', 'My publication here.', 'SELF', '2014-08-29 07:17:00', 34, NULL);
INSERT INTO publications VALUES (213, 'Publication title', 'My publication here.', 'FRIENDS', '2015-04-12 03:14:00', 34, NULL);
INSERT INTO publications VALUES (214, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-09-09 09:39:00', 39, NULL);
INSERT INTO publications VALUES (215, 'Publication title', 'My publication here.', 'FRIENDS', '2014-09-06 04:19:00', 42, 9);
INSERT INTO publications VALUES (216, 'Publication title', 'My publication here.', 'GLOBAL', '2014-05-12 00:04:00', 42, 9);
INSERT INTO publications VALUES (217, 'Publication title', 'My publication here.', 'FRIENDS', '2014-12-19 03:50:00', 42, NULL);
INSERT INTO publications VALUES (218, 'Publication title', 'My publication here.', 'SELF', '2015-03-04 07:13:00', 43, NULL);
INSERT INTO publications VALUES (219, 'Publication title', 'My publication here.', 'GLOBAL', '2015-04-07 02:39:00', 43, 13);
INSERT INTO publications VALUES (220, 'Publication title', 'My publication here.', 'SELF', '2014-01-14 06:39:00', 49, NULL);
INSERT INTO publications VALUES (221, 'Publication title', 'My publication here.', 'SELF', '2015-04-07 09:00:00', 50, NULL);
INSERT INTO publications VALUES (222, 'Publication title', 'My publication here.', 'GLOBAL', '2014-04-04 04:38:00', 50, 18);
INSERT INTO publications VALUES (223, 'Publication title', 'My publication here.', 'GLOBAL', '2015-01-12 01:23:00', 54, 18);
INSERT INTO publications VALUES (224, 'Publication title', 'My publication here.', 'FRIENDS', '2014-10-27 10:31:00', 55, 18);
INSERT INTO publications VALUES (225, 'Publication title', 'My publication here.', 'FRIENDS', '2014-10-30 07:10:00', 55, 24);
INSERT INTO publications VALUES (226, 'Publication title', 'My publication here.', 'FRIENDS', '2014-08-01 07:49:00', 55, 24);
INSERT INTO publications VALUES (227, 'Publication title', 'My publication here.', 'GLOBAL', '2015-01-25 09:17:00', 58, 32);
INSERT INTO publications VALUES (228, 'Publication title', 'My publication here.', 'SELF', '2014-01-28 03:42:00', 58, 37);
INSERT INTO publications VALUES (229, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-07-07 07:49:00', 67, 37);
INSERT INTO publications VALUES (230, 'Publication title', 'My publication here.', 'FRIENDS', '2015-02-23 04:43:00', 67, 37);
INSERT INTO publications VALUES (231, 'Publication title', 'My publication here.', 'SELF', '2014-08-29 09:29:00', 67, NULL);
INSERT INTO publications VALUES (232, 'Publication title', 'My publication here.', 'FRIENDS', '2014-01-01 07:13:00', 70, 41);
INSERT INTO publications VALUES (233, 'Publication title', 'My publication here.', 'GLOBAL', '2014-02-06 07:22:00', 70, 41);
INSERT INTO publications VALUES (234, 'Publication title', 'My publication here.', 'GLOBAL', '2014-05-05 05:05:00', 70, 43);
INSERT INTO publications VALUES (235, 'Publication title', 'My publication here.', 'SELF', '2015-02-14 00:06:00', 77, 43);
INSERT INTO publications VALUES (236, 'Publication title', 'My publication here.', 'GLOBAL', '2014-02-20 08:25:00', 77, 45);
INSERT INTO publications VALUES (237, 'Publication title', 'My publication here.', 'FRIENDS', '2014-07-29 02:26:00', 81, 53);
INSERT INTO publications VALUES (238, 'Publication title', 'My publication here.', 'GLOBAL', '2014-04-01 06:00:00', 81, 53);
INSERT INTO publications VALUES (239, 'Publication title', 'My publication here.', 'SELF', '2014-06-13 05:47:00', 89, NULL);
INSERT INTO publications VALUES (240, 'Publication title', 'My publication here.', 'SELF', '2014-05-16 04:39:00', 89, NULL);
INSERT INTO publications VALUES (241, 'Publication title', 'My publication here.', 'FRIENDS', '2015-03-22 02:20:00', 89, NULL);
INSERT INTO publications VALUES (242, 'Publication title', 'My publication here.', 'FRIENDS', '2014-10-11 00:32:00', 92, NULL);
INSERT INTO publications VALUES (243, 'Publication title', 'My publication here.', 'LOVE_MATCHES', '2014-05-27 06:54:00', 92, NULL);
INSERT INTO publications VALUES (244, 'Publication title', 'My publication here.', 'FRIENDS', '2015-03-21 09:41:00', 94, NULL);
INSERT INTO publications VALUES (245, 'Publication title', 'My publication here.', 'SELF', '2014-05-30 04:41:00', 94, 56);
INSERT INTO publications VALUES (246, 'Publication title', 'My publication here.', 'SELF', '2015-02-23 01:28:00', 4, 56);
INSERT INTO publications VALUES (247, 'Publication title', 'My publication here.', 'GLOBAL', '2014-05-08 05:33:00', 4, 62);
INSERT INTO publications VALUES (248, 'Publication title', 'My publication here.', 'SELF', '2015-04-08 10:15:00', 8, NULL);
INSERT INTO publications VALUES (249, 'Publication title', 'My publication here.', 'FRIENDS', '2014-06-26 09:49:00', 8, NULL);
INSERT INTO publications VALUES (250, 'Publication title', 'My publication here.', 'GLOBAL', '2015-04-02 01:20:00', 8, NULL);

INSERT INTO profiles VALUES (1, 'SELF', 'F', NULL, 'Trainspotting', 'Fernando Pessoa', 'Tayti', 'More details here.', 4, 4, 3, 1, 7);
INSERT INTO profiles VALUES (2, 'LOVE_MATCHES', NULL, 'G', 'Match Point', 'Os Maias', 'Spice Girls', NULL, 13, 4, 3, 1, 16);
INSERT INTO profiles VALUES (3, 'LOVE_MATCHES', 'F', 'H', 'The Imitation Game', 'Amor de PerdiÃ§Ã£o', 'Emanuel', 'More details here.', 16, 1, 5, 1, 16);
INSERT INTO profiles VALUES (4, 'LOVE_MATCHES', 'M', 'G', 'Gran Torino', 'The Notebook', 'Tayti', 'More details here.', 26, 1, 3, 3, 18);
INSERT INTO profiles VALUES (5, 'GLOBAL', NULL, 'H', 'Million Dollar Baby', 'Fernando Pessoa', 'Darude - Sandstorm', 'More details here.', 33, 3, 2, 1, 18);
INSERT INTO profiles VALUES (6, 'FRIENDS', 'F', 'G', NULL, 'The Notebook', 'One Direction', 'More details here.', 43, 3, 2, 1, 18);
INSERT INTO profiles VALUES (7, 'LOVE_MATCHES', 'F', 'H', 'Se7en', 'Amor de PerdiÃ§Ã£o', NULL, 'More details here.', 47, 3, 4, 1, 21);
INSERT INTO profiles VALUES (8, 'GLOBAL', 'M', 'B', 'Gran Torino', 'Fernando Pessoa', 'Nucha', 'More details here.', 51, 2, 4, 4, 21);
INSERT INTO profiles VALUES (9, 'GLOBAL', 'F', NULL, 'Se7en', 'Amor de PerdiÃ§Ã£o', 'Nucha', NULL, 57, 2, 7, 4, 25);
INSERT INTO profiles VALUES (10, 'FRIENDS', NULL, 'G', 'Match Point', 'Windows for Dummies', 'Darude - Sandstorm', 'More details here.', 60, 6, 7, 1, 25);
INSERT INTO profiles VALUES (11, 'FRIENDS', 'M', 'G', 'Boyhood', 'Amor de PerdiÃ§Ã£o', 'One Direction', 'More details here.', 67, 5, 1, 1, 25);
INSERT INTO profiles VALUES (12, 'LOVE_MATCHES', NULL, 'H', NULL, NULL, NULL, 'More details here.', 69, 4, 1, 1, 27);
INSERT INTO profiles VALUES (13, 'GLOBAL', 'M', 'G', 'Boys Don''t Cry', 'Marley & Me', 'Emanuel', NULL, 70, 4, 5, 1, 27);
INSERT INTO profiles VALUES (14, 'LOVE_MATCHES', NULL, NULL, 'Trainspotting', '50 Shades of Gray', 'Darude - Sandstorm', 'More details here.', 73, 3, 5, 3, 27);
INSERT INTO profiles VALUES (15, 'FRIENDS', 'M', 'G', 'Trainspotting', 'Os Maias', 'Rosinha', 'More details here.', 81, 3, 4, 3, 32);
INSERT INTO profiles VALUES (16, 'LOVE_MATCHES', 'F', NULL, 'Boyhood', '50 Shades of Gray', NULL, 'More details here.', 83, 3, 4, 4, 39);
INSERT INTO profiles VALUES (17, 'LOVE_MATCHES', 'M', 'B', 'Boys Don''t Cry', NULL, NULL, NULL, 87, 4, 7, 4, 39);
INSERT INTO profiles VALUES (18, 'FRIENDS', 'M', NULL, 'Boys Don''t Cry', 'Fernando Pessoa', NULL, 'More details here.', 89, 4, 7, 3, 39);
INSERT INTO profiles VALUES (19, 'FRIENDS', NULL, 'B', 'Se7en', 'Os Maias', 'Rosinha', 'More details here.', 93, 4, 1, 3, 5);
INSERT INTO profiles VALUES (20, 'FRIENDS', 'M', 'H', NULL, 'Windows for Dummies', 'Ana Malhoa', 'More details here.', 3, 5, 4, 3, 5);
INSERT INTO profiles VALUES (21, 'LOVE_MATCHES', NULL, 'B', 'Million Dollar Baby', 'Marley & Me', 'Darude - Sandstorm', 'More details here.', 19, 5, 4, 2, 9);
INSERT INTO profiles VALUES (22, 'FRIENDS', 'M', 'G', 'Million Dollar Baby', 'Revista Maria', NULL, 'More details here.', 29, 3, 4, 2, 9);
INSERT INTO profiles VALUES (23, 'SELF', 'F', 'G', 'Gran Torino', NULL, 'Backstreet Boys', NULL, 32, 3, 6, 2, 16);
INSERT INTO profiles VALUES (24, 'LOVE_MATCHES', 'M', 'H', 'The Imitation Game', 'Windows for Dummies', NULL, 'More details here.', 37, 3, 3, 4, 16);
INSERT INTO profiles VALUES (25, 'GLOBAL', 'F', 'H', 'Million Dollar Baby', NULL, 'JosÃ© Malhoa', NULL, 40, 1, 3, 4, 16);
INSERT INTO profiles VALUES (26, 'LOVE_MATCHES', NULL, 'G', 'Match Point', 'Fernando Pessoa', NULL, 'More details here.', 49, 1, 3, 4, 21);
INSERT INTO profiles VALUES (27, 'LOVE_MATCHES', NULL, 'H', 'Boys Don''t Cry', 'Windows for Dummies', 'Nucha', 'More details here.', 59, 1, 5, 2, 21);
INSERT INTO profiles VALUES (28, 'FRIENDS', 'M', 'H', 'The Imitation Game', 'Os Maias', 'Spice Girls', 'More details here.', 71, 1, 5, 1, 22);
INSERT INTO profiles VALUES (29, 'GLOBAL', NULL, 'G', 'Boys Don''t Cry', 'Windows for Dummies', 'Rosinha', 'More details here.', 91, 1, 5, 1, 25);
INSERT INTO profiles VALUES (30, 'SELF', NULL, NULL, 'Match Point', 'Marley & Me', 'Tayti', 'More details here.', 95, 5, 6, 2, 35);
INSERT INTO profiles VALUES (31, 'SELF', 'F', 'H', 'Boys Don''t Cry', 'Amor de PerdiÃ§Ã£o', 'JosÃ© Malhoa', NULL, 1, 6, 2, 2, 35);
INSERT INTO profiles VALUES (32, 'GLOBAL', NULL, 'G', 'Million Dollar Baby', 'Os Maias', 'JosÃ© Malhoa', 'More details here.', 7, 6, 1, 2, 35);
INSERT INTO profiles VALUES (33, 'LOVE_MATCHES', 'M', 'B', 'Trainspotting', '50 Shades of Gray', 'Darude - Sandstorm', 'More details here.', 9, 6, 1, 2, 2);
INSERT INTO profiles VALUES (34, 'SELF', 'F', 'B', 'Boyhood', 'Fernando Pessoa', 'One Direction', 'More details here.', 28, 4, 3, 1, 2);
INSERT INTO profiles VALUES (35, 'GLOBAL', NULL, NULL, 'Boyhood', NULL, 'Nucha', 'More details here.', 31, 1, 4, 2, 10);
INSERT INTO profiles VALUES (36, 'SELF', 'M', 'G', 'Boyhood', 'The Notebook', NULL, NULL, 35, 1, 4, 3, 11);
INSERT INTO profiles VALUES (37, 'FRIENDS', 'M', 'G', 'Gran Torino', 'Fernando Pessoa', NULL, 'More details here.', 38, 5, 4, 3, 20);
INSERT INTO profiles VALUES (38, 'GLOBAL', NULL, 'B', 'Trainspotting', 'Windows for Dummies', NULL, 'More details here.', 45, 3, 4, 2, 20);
INSERT INTO profiles VALUES (39, 'FRIENDS', 'F', 'B', NULL, 'Windows for Dummies', NULL, NULL, 50, 1, 1, 2, 26);
INSERT INTO profiles VALUES (40, 'GLOBAL', 'F', 'B', 'Trainspotting', 'Revista Maria', 'Ana Malhoa', 'More details here.', 58, 1, 1, 2, 26);
INSERT INTO profiles VALUES (41, 'FRIENDS', NULL, 'H', 'Match Point', 'Os Maias', 'JosÃ© Malhoa', 'More details here.', 65, 1, 1, 2, 26);
INSERT INTO profiles VALUES (42, 'SELF', 'F', 'G', 'Match Point', 'The Notebook', 'Emanuel', NULL, 84, 2, 6, 1, 33);
INSERT INTO profiles VALUES (43, 'SELF', 'M', 'G', 'Boys Don''t Cry', 'Marley & Me', 'JosÃ© Cid', 'More details here.', 94, 6, 6, 1, 33);
INSERT INTO profiles VALUES (44, 'SELF', NULL, 'B', 'The Imitation Game', NULL, NULL, NULL, 99, 6, 7, 1, 37);
INSERT INTO profiles VALUES (45, 'FRIENDS', 'M', 'G', 'Boyhood', 'Amor de PerdiÃ§Ã£o', 'Emanuel', NULL, 8, 6, 1, 2, 37);
INSERT INTO profiles VALUES (46, 'LOVE_MATCHES', NULL, 'H', 'Million Dollar Baby', 'The Notebook', 'Nucha', 'More details here.', 15, 5, 1, 2, 39);
INSERT INTO profiles VALUES (47, 'FRIENDS', NULL, 'H', 'Se7en', 'Windows for Dummies', NULL, NULL, 23, 5, 1, 1, 39);
INSERT INTO profiles VALUES (48, 'FRIENDS', 'F', 'G', NULL, 'Os Maias', NULL, 'More details here.', 30, 5, 4, 1, 40);
INSERT INTO profiles VALUES (49, 'GLOBAL', 'M', 'H', 'Trainspotting', '50 Shades of Gray', 'Spice Girls', 'More details here.', 44, 1, 5, 1, 40);
INSERT INTO profiles VALUES (50, 'GLOBAL', 'F', NULL, 'Match Point', 'Revista Maria', 'Backstreet Boys', 'More details here.', 48, 1, 5, 1, 40);
INSERT INTO profiles VALUES (51, 'SELF', 'M', 'G', 'Match Point', NULL, 'Backstreet Boys', 'More details here.', 61, 5, 1, 1, 5);
INSERT INTO profiles VALUES (52, 'FRIENDS', 'F', 'H', NULL, 'Os Maias', 'One Direction', 'More details here.', 74, 5, 1, 3, 5);
INSERT INTO profiles VALUES (53, 'GLOBAL', 'M', 'G', 'Boys Don''t Cry', NULL, 'Emanuel', 'More details here.', 82, 5, 1, 3, 12);
INSERT INTO profiles VALUES (54, 'SELF', 'M', 'H', 'Trainspotting', 'Windows for Dummies', 'Spice Girls', 'More details here.', 97, 1, 3, 3, 12);
INSERT INTO profiles VALUES (55, 'FRIENDS', 'F', 'G', 'Gran Torino', 'Amor de PerdiÃ§Ã£o', 'Ana Malhoa', 'More details here.', 6, 5, 3, 4, 13);
INSERT INTO profiles VALUES (56, 'SELF', 'F', 'B', 'Se7en', 'Windows for Dummies', 'JosÃ© Malhoa', NULL, 24, 5, 2, 2, 13);
INSERT INTO profiles VALUES (57, 'GLOBAL', 'M', 'B', NULL, 'Fernando Pessoa', NULL, NULL, 34, 6, 2, 2, 13);
INSERT INTO profiles VALUES (58, 'LOVE_MATCHES', 'F', 'G', 'Gran Torino', NULL, 'Quim Barreiros', 'More details here.', 39, 6, 1, 2, 18);
INSERT INTO profiles VALUES (59, 'LOVE_MATCHES', 'M', 'H', NULL, NULL, 'JosÃ© Malhoa', 'More details here.', 46, 6, 1, 4, 18);
INSERT INTO profiles VALUES (60, 'FRIENDS', 'F', 'G', 'Se7en', '50 Shades of Gray', 'Darude - Sandstorm', NULL, 55, 4, 4, 4, 18);
INSERT INTO profiles VALUES (61, 'GLOBAL', 'F', 'H', 'Boys Don''t Cry', 'Fernando Pessoa', 'JosÃ© Malhoa', 'More details here.', 56, 4, 4, 4, 26);
INSERT INTO profiles VALUES (62, 'SELF', NULL, 'B', 'Million Dollar Baby', 'Windows for Dummies', 'JosÃ© Malhoa', 'More details here.', 64, 5, 6, 1, 26);
INSERT INTO profiles VALUES (63, 'LOVE_MATCHES', 'M', 'H', NULL, 'Revista Maria', 'JosÃ© Malhoa', 'More details here.', 77, 2, 6, 1, 26);
INSERT INTO profiles VALUES (64, 'LOVE_MATCHES', 'M', 'H', 'Match Point', NULL, 'Spice Girls', NULL, 100, 2, 6, 1, 32);
INSERT INTO profiles VALUES (65, 'LOVE_MATCHES', NULL, 'H', 'Se7en', 'Fernando Pessoa', 'Backstreet Boys', 'More details here.', 2, 2, 7, 4, 32);
INSERT INTO profiles VALUES (66, 'SELF', 'M', 'H', 'Se7en', NULL, 'Darude - Sandstorm', NULL, 5, 6, 4, 4, 1);
INSERT INTO profiles VALUES (67, 'GLOBAL', NULL, 'G', NULL, NULL, 'Darude - Sandstorm', 'More details here.', 10, 2, 4, 4, 1);
INSERT INTO profiles VALUES (68, 'FRIENDS', NULL, 'G', 'Trainspotting', 'Os Maias', NULL, NULL, 18, 2, 4, 4, 7);
INSERT INTO profiles VALUES (69, 'GLOBAL', 'F', 'H', NULL, 'Fernando Pessoa', 'One Direction', 'More details here.', 27, 2, 7, 4, 7);
INSERT INTO profiles VALUES (70, 'FRIENDS', NULL, NULL, 'Match Point', NULL, 'Spice Girls', 'More details here.', 54, 6, 7, 2, 13);
INSERT INTO profiles VALUES (71, 'GLOBAL', NULL, 'H', NULL, NULL, NULL, 'More details here.', 62, 4, 7, 2, 13);
INSERT INTO profiles VALUES (72, 'FRIENDS', 'M', 'B', NULL, '50 Shades of Gray', 'Ana Malhoa', 'More details here.', 66, 5, 1, 2, 13);
INSERT INTO profiles VALUES (73, 'SELF', NULL, 'B', 'Se7en', NULL, NULL, NULL, 90, 5, 1, 2, 19);
INSERT INTO profiles VALUES (74, 'GLOBAL', 'M', NULL, 'Trainspotting', 'Revista Maria', 'Tayti', NULL, 96, 1, 7, 2, 19);
INSERT INTO profiles VALUES (75, 'GLOBAL', 'F', 'B', 'Million Dollar Baby', 'The Notebook', 'One Direction', 'More details here.', 17, 5, 7, 4, 19);
INSERT INTO profiles VALUES (76, 'SELF', 'F', 'H', 'Match Point', NULL, NULL, 'More details here.', 25, 5, 3, 4, 26);
INSERT INTO profiles VALUES (77, 'SELF', NULL, 'B', 'Gran Torino', 'Fernando Pessoa', 'One Direction', 'More details here.', 41, 5, 3, 4, 26);
INSERT INTO profiles VALUES (78, 'SELF', 'F', 'B', 'Match Point', '50 Shades of Gray', 'Spice Girls', 'More details here.', 68, 6, 7, 1, 26);
INSERT INTO profiles VALUES (79, 'LOVE_MATCHES', NULL, 'H', 'Se7en', NULL, NULL, 'More details here.', 72, 6, 1, 1, 34);
INSERT INTO profiles VALUES (80, 'LOVE_MATCHES', NULL, 'G', NULL, NULL, 'JosÃ© Malhoa', 'More details here.', 86, 6, 1, 1, 34);
INSERT INTO profiles VALUES (81, 'FRIENDS', 'M', 'G', 'Boys Don''t Cry', 'Amor de PerdiÃ§Ã£o', 'Rosinha', 'More details here.', 98, 1, 3, 2, 35);
INSERT INTO profiles VALUES (82, 'FRIENDS', NULL, 'G', 'Boyhood', 'Windows for Dummies', 'Rosinha', 'More details here.', 22, 1, 3, 2, 35);
INSERT INTO profiles VALUES (83, 'GLOBAL', 'F', 'G', 'Match Point', 'The Notebook', 'Tayti', NULL, 52, 1, 3, 2, 35);
INSERT INTO profiles VALUES (84, 'FRIENDS', 'M', 'H', NULL, 'Os Maias', 'Tayti', 'More details here.', 53, 5, 7, 2, 38);
INSERT INTO profiles VALUES (85, 'LOVE_MATCHES', 'F', 'G', 'Boyhood', 'Marley & Me', 'Spice Girls', 'More details here.', 80, 5, 7, 3, 39);
INSERT INTO profiles VALUES (86, 'LOVE_MATCHES', 'F', 'G', 'Trainspotting', 'Amor de PerdiÃ§Ã£o', 'Spice Girls', 'More details here.', 88, 6, 7, 3, 4);
INSERT INTO profiles VALUES (87, 'GLOBAL', 'F', 'H', NULL, 'Amor de PerdiÃ§Ã£o', NULL, 'More details here.', 78, 2, 3, 4, 11);
INSERT INTO profiles VALUES (88, 'LOVE_MATCHES', 'M', 'H', 'Boys Don''t Cry', 'Fernando Pessoa', 'Backstreet Boys', 'More details here.', 14, 3, 7, 4, 11);
INSERT INTO profiles VALUES (89, 'GLOBAL', NULL, 'B', NULL, 'Windows for Dummies', 'Ana Malhoa', 'More details here.', 36, 3, 1, 4, 11);
INSERT INTO profiles VALUES (90, 'LOVE_MATCHES', NULL, 'H', NULL, 'Amor de PerdiÃ§Ã£o', 'Tayti', 'More details here.', 42, 3, 1, 3, 17);
INSERT INTO profiles VALUES (91, 'FRIENDS', 'F', 'H', 'Million Dollar Baby', 'Windows for Dummies', 'Backstreet Boys', 'More details here.', 79, 6, 7, 3, 17);
INSERT INTO profiles VALUES (92, 'LOVE_MATCHES', 'F', 'B', 'Gran Torino', 'Revista Maria', NULL, NULL, 85, 6, 7, 3, 17);
INSERT INTO profiles VALUES (93, 'GLOBAL', 'F', 'G', 'Se7en', 'Fernando Pessoa', NULL, 'More details here.', 63, 3, 7, 1, 23);
INSERT INTO profiles VALUES (94, 'SELF', 'F', 'H', 'The Imitation Game', NULL, 'Rosinha', 'More details here.', 92, 3, 7, 1, 32);
INSERT INTO profiles VALUES (95, 'FRIENDS', 'F', 'B', 'The Imitation Game', 'Amor de PerdiÃ§Ã£o', 'Spice Girls', NULL, 21, 1, 3, 4, 32);
INSERT INTO profiles VALUES (96, 'SELF', 'M', 'H', 'Boyhood', 'The Notebook', 'Rosinha', 'More details here.', 76, 1, 3, 4, 32);
INSERT INTO profiles VALUES (97, 'FRIENDS', NULL, 'H', 'Boys Don''t Cry', 'Fernando Pessoa', 'Tayti', NULL, 75, 1, 6, 2, 35);
INSERT INTO profiles VALUES (98, 'GLOBAL', NULL, 'B', 'Trainspotting', 'Windows for Dummies', 'Nucha', 'More details here.', 20, 5, 6, 2, 39);
INSERT INTO profiles VALUES (99, 'LOVE_MATCHES', 'M', 'H', 'Boys Don''t Cry', 'Os Maias', 'Quim Barreiros', 'More details here.', 12, 5, 3, 1, 39);
INSERT INTO profiles VALUES (100, 'LOVE_MATCHES', 'M', 'G', NULL, 'Amor de PerdiÃ§Ã£o', 'JosÃ© Cid', 'More details here.', 11, 5, 3, 4, 39);

INSERT INTO notifications VALUES (DEFAULT, TRUE, '2014-10-07 07:00:00', 'MESSAGE', 11, 12);
INSERT INTO notifications VALUES (DEFAULT, NULL, '2014-11-15 03:07:00', 'MESSAGE', 18, 19);
INSERT INTO notifications VALUES (DEFAULT, NULL, '2014-11-15 05:19:00', 'MESSAGE', 18, 20);
INSERT INTO notifications VALUES (DEFAULT, FALSE, '2014-11-16 07:27:00', 'MESSAGE', 18, 21);
INSERT INTO notifications VALUES (DEFAULT, TRUE, '2014-11-17 02:54:00', 'FRIEND', 25, 27);
INSERT INTO notifications VALUES (DEFAULT, FALSE, '2014-11-18 10:33:00', 'MESSAGE', 25, 27);
INSERT INTO notifications VALUES (DEFAULT, NULL, '2014-11-19 09:05:00', 'LOVE_MATCH', 25, 28);
INSERT INTO notifications VALUES (DEFAULT, FALSE, '2014-11-20 08:55:00', 'MESSAGE', 34, 36);
INSERT INTO notifications VALUES (DEFAULT, TRUE, '2014-11-21 07:36:00', 'LOVE_MATCH', 34, 37);
INSERT INTO notifications VALUES (DEFAULT, TRUE, '2014-11-22 01:18:00', 'LOVE_MATCH', 35, 50);
INSERT INTO notifications VALUES (DEFAULT, NULL, '2014-11-23 03:43:00', 'FRIEND', 35, 39);
INSERT INTO notifications VALUES (DEFAULT, TRUE, '2014-11-24 07:49:00', 'MESSAGE', 37, 40);
INSERT INTO notifications VALUES (DEFAULT, TRUE, '2014-11-25 04:05:00', 'LOVE_MATCH', 37, 3);
INSERT INTO notifications VALUES (DEFAULT, FALSE, '2014-11-26 07:20:00', 'LOVE_MATCH', 43, 39);
INSERT INTO notifications VALUES (DEFAULT, FALSE, '2014-11-27 07:19:00', 'LOVE_MATCH', 43, 40);
INSERT INTO notifications VALUES (DEFAULT, FALSE, '2014-11-28 09:47:00', 'FRIEND', 46, 47);
INSERT INTO notifications VALUES (DEFAULT, TRUE, '2015-01-10 03:26:00', 'LOVE_MATCH', 51, 52);
INSERT INTO notifications VALUES (DEFAULT, TRUE, '2015-01-11 03:00:00', 'FRIEND', 55, 56);
INSERT INTO notifications VALUES (DEFAULT, FALSE, '2015-01-12 04:14:00', 'MESSAGE', 55, 57);
INSERT INTO notifications VALUES (DEFAULT, TRUE, '2015-01-13 09:32:00', 'LOVE_MATCH', 62, 63);
INSERT INTO notifications VALUES (DEFAULT, FALSE, '2015-01-14 02:17:00', 'MESSAGE', 62, 64);
INSERT INTO notifications VALUES (DEFAULT, TRUE, '2015-01-15 10:08:00', 'FRIEND', 62, 65);
INSERT INTO notifications VALUES (DEFAULT, FALSE, '2015-01-16 02:48:00', 'FRIEND', 72, 73);
INSERT INTO notifications VALUES (DEFAULT, TRUE, '2015-01-17 06:01:00', 'LOVE_MATCH', 74, 75);
INSERT INTO notifications VALUES (DEFAULT, NULL, '2015-01-18 08:47:00', 'LOVE_MATCH', 80, 81);
INSERT INTO notifications VALUES (DEFAULT, NULL, '2015-01-19 07:01:00', 'LOVE_MATCH', 85, 86);
INSERT INTO notifications VALUES (DEFAULT, FALSE, '2015-01-23 06:12:00', 'FRIEND', 85, 87);
INSERT INTO notifications VALUES (DEFAULT, NULL, '2015-01-24 01:20:00', 'MESSAGE', 85, 88);
INSERT INTO notifications VALUES (DEFAULT, FALSE, '2015-01-25 08:46:00', 'FRIEND', 90, 91);
INSERT INTO notifications VALUES (DEFAULT, TRUE, '2015-01-26 05:08:00', 'LOVE_MATCH', 94, 95);
INSERT INTO notifications VALUES (DEFAULT, FALSE, '2015-01-27 09:50:00', 'FRIEND', 94, 96);
INSERT INTO notifications VALUES (DEFAULT, NULL, '2015-01-28 06:39:00', 'FRIEND', 94, 97);
INSERT INTO notifications VALUES (DEFAULT, TRUE, '2015-01-29 06:04:00', 'FRIEND', 96, 98);
INSERT INTO notifications VALUES (DEFAULT, TRUE, '2015-01-30 06:28:00', 'FRIEND', 96, 99);
INSERT INTO notifications VALUES (DEFAULT, FALSE, '2015-02-02 03:09:00', 'FRIEND', 3, 4);
INSERT INTO notifications VALUES (DEFAULT, TRUE, '2015-02-03 00:02:00', 'MESSAGE', 5, 6);
INSERT INTO notifications VALUES (DEFAULT, TRUE, '2015-02-04 00:06:00', 'FRIEND', 14, 15);
INSERT INTO notifications VALUES (DEFAULT, TRUE, '2015-02-05 02:52:00', 'LOVE_MATCH', 50, 57);
INSERT INTO notifications VALUES (DEFAULT, FALSE, '2015-02-06 07:29:00', 'LOVE_MATCH', 51, 58);
INSERT INTO notifications VALUES (DEFAULT, NULL, '2015-02-07 01:41:00', 'LOVE_MATCH', 52, 59);
INSERT INTO notifications VALUES (DEFAULT, TRUE, '2015-02-08 10:58:00', 'FRIEND', 28, 29);
INSERT INTO notifications VALUES (DEFAULT, FALSE, '2015-02-09 07:03:00', 'FRIEND', 28, 30);
INSERT INTO notifications VALUES (DEFAULT, FALSE, '2015-02-10 01:34:00', 'MESSAGE', 28, 31);
INSERT INTO notifications VALUES (DEFAULT, FALSE, '2015-02-11 04:35:00', 'MESSAGE', 31, 32);
INSERT INTO notifications VALUES (DEFAULT, TRUE, '2015-02-12 10:45:00', 'FRIEND', 31, 33);
INSERT INTO notifications VALUES (DEFAULT, FALSE, '2015-02-13 04:01:00', 'FRIEND', 39, 40);
INSERT INTO notifications VALUES (DEFAULT, NULL, '2015-02-13 06:32:00', 'LOVE_MATCH', 39, 22);
INSERT INTO notifications VALUES (DEFAULT, TRUE, '2015-02-14 09:34:00', 'MESSAGE', 45, 55);
INSERT INTO notifications VALUES (DEFAULT, NULL, '2015-02-15 06:27:00', 'LOVE_MATCH', 52, 62);
INSERT INTO notifications VALUES (DEFAULT, FALSE, '2015-02-16 04:57:00', 'LOVE_MATCH', 58, 59);


INSERT INTO conversations VALUES (1, 8, 39);
INSERT INTO conversations VALUES (2, 92, 56);
INSERT INTO conversations VALUES (3, 38, 24);
INSERT INTO conversations VALUES (4, 6, 53);
INSERT INTO conversations VALUES (5, 69, 94);
INSERT INTO conversations VALUES (6, 10, 63);
INSERT INTO conversations VALUES (7, 14, 98);
INSERT INTO conversations VALUES (8, 87, 22);
INSERT INTO conversations VALUES (9, 30, 49);
INSERT INTO conversations VALUES (10, 66, 32);
INSERT INTO conversations VALUES (11, 77, 52);
INSERT INTO conversations VALUES (12, 33, 46);
INSERT INTO conversations VALUES (13, 71, 8);
INSERT INTO conversations VALUES (14, 57, 86);
INSERT INTO conversations VALUES (15, 75, 27);
INSERT INTO conversations VALUES (16, 47, 79);
INSERT INTO conversations VALUES (17, 86, 100);
INSERT INTO conversations VALUES (18, 89, 84);
INSERT INTO conversations VALUES (19, 9, 19);
INSERT INTO conversations VALUES (20, 100, 14);
INSERT INTO conversations VALUES (21, 98, 3);
INSERT INTO conversations VALUES (22, 21, 41);
INSERT INTO conversations VALUES (23, 74, 10);
INSERT INTO conversations VALUES (24, 12, 38);
INSERT INTO conversations VALUES (25, 32, 71);
INSERT INTO conversations VALUES (26, 15, 26);
INSERT INTO conversations VALUES (27, 97, 35);
INSERT INTO conversations VALUES (28, 72, 77);
INSERT INTO conversations VALUES (29, 60, 76);
INSERT INTO conversations VALUES (30, 11, 36);
INSERT INTO conversations VALUES (31, 34, 21);
INSERT INTO conversations VALUES (32, 40, 69);
INSERT INTO conversations VALUES (33, 99, 13);
INSERT INTO conversations VALUES (34, 4, 55);
INSERT INTO conversations VALUES (35, 2, 6);
INSERT INTO conversations VALUES (36, 26, 25);
INSERT INTO conversations VALUES (37, 20, 74);
INSERT INTO conversations VALUES (38, 13, 2);
INSERT INTO conversations VALUES (39, 19, 33);
INSERT INTO conversations VALUES (40, 88, 64);
INSERT INTO conversations VALUES (41, 48, 18);
INSERT INTO conversations VALUES (42, 7, 90);
INSERT INTO conversations VALUES (43, 18, 62);
INSERT INTO conversations VALUES (45, 24, 12);
INSERT INTO conversations VALUES (46, 29, 60);
INSERT INTO conversations VALUES (47, 96, 91);
INSERT INTO conversations VALUES (48, 37, 70);
INSERT INTO conversations VALUES (49, 50, 93);
INSERT INTO conversations VALUES (50, 64, 44);
INSERT INTO conversations VALUES (51, 39, 34);
INSERT INTO conversations VALUES (52, 82, 7);
INSERT INTO conversations VALUES (53, 67, 83);
INSERT INTO conversations VALUES (54, 81, 45);
INSERT INTO conversations VALUES (55, 65, 81);
INSERT INTO conversations VALUES (56, 59, 85);
INSERT INTO conversations VALUES (57, 5, 29);
INSERT INTO conversations VALUES (58, 28, 80);
INSERT INTO conversations VALUES (59, 93, 67);
INSERT INTO conversations VALUES (60, 62, 73);
INSERT INTO conversations VALUES (61, 91, 88);
INSERT INTO conversations VALUES (62, 83, 37);
INSERT INTO conversations VALUES (63, 16, 42);
INSERT INTO conversations VALUES (64, 73, 11);
INSERT INTO conversations VALUES (65, 3, 58);
INSERT INTO conversations VALUES (67, 85, 28);
INSERT INTO conversations VALUES (68, 43, 97);
INSERT INTO conversations VALUES (69, 1, 9);
INSERT INTO conversations VALUES (70, 44, 54);
INSERT INTO conversations VALUES (71, 31, 95);

INSERT INTO messages VALUES (23626, '[Message content here]', '2014-09-25 04:19:00', 19, 9);
INSERT INTO messages VALUES (2101, '[Message content here]', '2014-07-12 05:05:00', 31, 21);
INSERT INTO messages VALUES (21442, '[Message content here]', '2014-10-29 04:25:00', 41, 48);
INSERT INTO messages VALUES (18191, '[Message content here]', '2014-09-26 00:37:00', 12, 33);
INSERT INTO messages VALUES (10367, '[Message content here]', '2014-04-28 09:03:00', 54, 45);
INSERT INTO messages VALUES (10375, '[Message content here]', '2014-11-08 00:52:00', 35, 2);
INSERT INTO messages VALUES (8733, '[Message content here]', '2014-01-18 02:19:00', 16, 79);
INSERT INTO messages VALUES (7110, '[Message content here]', '2014-11-17 00:55:00', 56, 85);
INSERT INTO messages VALUES (4010, '[Message content here]', '2014-09-15 02:39:00', 19, 9);
INSERT INTO messages VALUES (23645, '[Message content here]', '2014-07-16 03:20:00', 25, 32);
INSERT INTO messages VALUES (23060, '[Message content here]', '2014-12-31 03:41:00', 37, 74);
INSERT INTO messages VALUES (7114, '[Message content here]', '2015-01-17 03:40:00', 65, 3);
INSERT INTO messages VALUES (21461, '[Message content here]', '2014-09-12 05:52:00', 41, 48);
INSERT INTO messages VALUES (10396, '[Message content here]', '2015-01-09 06:42:00', 65, 3);
INSERT INTO messages VALUES (5419, '[Message content here]', '2014-04-23 04:36:00', 15, 27);
INSERT INTO messages VALUES (23073, '[Message content here]', '2014-04-11 01:13:00', 37, 74);
INSERT INTO messages VALUES (15031, '[Message content here]', '2014-11-20 07:28:00', 31, 34);
INSERT INTO messages VALUES (16370, '[Message content here]', '2014-07-29 10:18:00', 15, 75);
INSERT INTO messages VALUES (23662, '[Message content here]', '2014-01-30 01:01:00', 10, 32);
INSERT INTO messages VALUES (8764, '[Message content here]', '2014-05-21 07:28:00', 16, 79);
INSERT INTO messages VALUES (23670, '[Message content here]', '2014-05-02 06:15:00', 25, 32);
INSERT INTO messages VALUES (15048, '[Message content here]', '2015-01-04 09:06:00', 51, 34);
INSERT INTO messages VALUES (7156, '[Message content here]', '2014-12-21 04:25:00', 21, 3);
INSERT INTO messages VALUES (19635, '[Message content here]', '2014-05-17 10:51:00', 3, 38);
INSERT INTO messages VALUES (15059, '[Message content here]', '2014-06-07 09:43:00', 69, 1);
INSERT INTO messages VALUES (19640, '[Message content here]', '2015-03-10 00:21:00', 24, 38);
INSERT INTO messages VALUES (2181, '[Message content here]', '2014-04-04 00:25:00', 31, 21);
INSERT INTO messages VALUES (23690, '[Message content here]', '2015-01-14 00:44:00', 13, 71);
INSERT INTO messages VALUES (13198, '[Message content here]', '2014-11-14 01:25:00', 51, 34);
INSERT INTO messages VALUES (15071, '[Message content here]', '2014-12-03 06:52:00', 6, 63);
INSERT INTO messages VALUES (19657, '[Message content here]', '2014-07-16 01:36:00', 37, 20);
INSERT INTO messages VALUES (23707, '[Message content here]', '2015-03-12 00:28:00', 25, 71);
INSERT INTO messages VALUES (2215, '[Message content here]', '2014-09-17 09:36:00', 1, 8);
INSERT INTO messages VALUES (23140, '[Message content here]', '2014-02-06 07:20:00', 39, 19);
INSERT INTO messages VALUES (13232, '[Message content here]', '2014-11-18 06:43:00', 51, 34);
INSERT INTO messages VALUES (13237, '[Message content here]', '2014-09-24 07:35:00', 6, 63);
INSERT INTO messages VALUES (15110, '[Message content here]', '2014-10-24 01:47:00', 6, 63);
INSERT INTO messages VALUES (7217, '[Message content here]', '2014-11-26 03:45:00', 43, 62);
INSERT INTO messages VALUES (2234, '[Message content here]', '2014-05-01 00:13:00', 13, 8);
INSERT INTO messages VALUES (23732, '[Message content here]', '2015-01-01 04:11:00', 25, 71);
INSERT INTO messages VALUES (18303, '[Message content here]', '2014-02-13 06:58:00', 39, 33);
INSERT INTO messages VALUES (2243, '[Message content here]', '2014-12-02 00:22:00', 46, 60);
INSERT INTO messages VALUES (13250, '[Message content here]', '2014-06-23 05:33:00', 6, 63);
INSERT INTO messages VALUES (7231, '[Message content here]', '2014-09-05 03:39:00', 60, 62);
INSERT INTO messages VALUES (10500, '[Message content here]', '2014-06-19 10:09:00', 21, 3);
INSERT INTO messages VALUES (7237, '[Message content here]', '2014-06-01 06:37:00', 60, 62);
INSERT INTO messages VALUES (5533, '[Message content here]', '2015-03-30 09:11:00', 15, 27);
INSERT INTO messages VALUES (7242, '[Message content here]', '2014-04-18 06:04:00', 47, 91);
INSERT INTO messages VALUES (15134, '[Message content here]', '2014-02-04 01:18:00', 31, 21);
INSERT INTO messages VALUES (21569, '[Message content here]', '2014-02-10 09:47:00', 34, 4);
INSERT INTO messages VALUES (23174, '[Message content here]', '2014-10-02 06:47:00', 69, 1);
INSERT INTO messages VALUES (8876, '[Message content here]', '2014-07-20 00:22:00', 22, 41);
INSERT INTO messages VALUES (18331, '[Message content here]', '2014-04-11 03:04:00', 12, 33);
INSERT INTO messages VALUES (20566, '[Message content here]', '2014-02-26 00:47:00', 71, 31);
INSERT INTO messages VALUES (8889, '[Message content here]', '2014-08-16 07:46:00', 22, 41);
INSERT INTO messages VALUES (18342, '[Message content here]', '2014-01-17 08:04:00', 21, 98);
INSERT INTO messages VALUES (2292, '[Message content here]', '2014-06-30 07:40:00', 38, 13);
INSERT INTO messages VALUES (20579, '[Message content here]', '2015-01-08 00:07:00', 38, 2);
INSERT INTO messages VALUES (15167, '[Message content here]', '2014-04-05 05:10:00', 31, 21);
INSERT INTO messages VALUES (208, '[Message content here]', '2015-03-23 04:52:00', 49, 50);
INSERT INTO messages VALUES (4188, '[Message content here]', '2014-05-16 03:53:00', 34, 55);
INSERT INTO messages VALUES (216, '[Message content here]', '2014-11-21 04:24:00', 49, 50);
INSERT INTO messages VALUES (7291, '[Message content here]', '2014-07-03 07:42:00', 47, 91);
INSERT INTO messages VALUES (23789, '[Message content here]', '2014-07-18 09:45:00', 33, 99);
INSERT INTO messages VALUES (8914, '[Message content here]', '2014-04-04 08:57:00', 61, 88);
INSERT INTO messages VALUES (19751, '[Message content here]', '2014-09-23 00:11:00', 37, 20);
INSERT INTO messages VALUES (18367, '[Message content here]', '2014-03-22 01:47:00', 21, 98);
INSERT INTO messages VALUES (15185, '[Message content here]', '2014-05-01 00:41:00', 37, 20);
INSERT INTO messages VALUES (19754, '[Message content here]', '2014-07-08 10:53:00', 70, 44);
INSERT INTO messages VALUES (16517, '[Message content here]', '2014-07-16 00:13:00', 15, 75);
INSERT INTO messages VALUES (10563, '[Message content here]', '2014-04-29 03:36:00', 26, 26);
INSERT INTO messages VALUES (7311, '[Message content here]', '2014-04-03 07:58:00', 39, 19);
INSERT INTO messages VALUES (7313, '[Message content here]', '2014-04-07 07:36:00', 12, 33);
INSERT INTO messages VALUES (18377, '[Message content here]', '2014-12-08 00:14:00', 21, 98);
INSERT INTO messages VALUES (2337, '[Message content here]', '2014-04-10 03:42:00', 38, 13);
INSERT INTO messages VALUES (2342, '[Message content here]', '2014-05-30 00:08:00', 50, 44);
INSERT INTO messages VALUES (4228, '[Message content here]', '2014-10-30 06:19:00', 34, 55);
INSERT INTO messages VALUES (257, '[Message content here]', '2014-05-09 04:32:00', 49, 50);
INSERT INTO messages VALUES (258, '[Message content here]', '2014-05-10 00:58:00', 11, 52);
INSERT INTO messages VALUES (2351, '[Message content here]', '2014-12-10 01:10:00', 70, 44);
INSERT INTO messages VALUES (15209, '[Message content here]', '2014-01-05 09:11:00', 37, 20);
INSERT INTO messages VALUES (10590, '[Message content here]', '2014-11-30 01:52:00', 28, 77);
INSERT INTO messages VALUES (20622, '[Message content here]', '2014-04-23 09:15:00', 35, 2);
INSERT INTO messages VALUES (4244, '[Message content here]', '2014-07-20 00:29:00', 34, 55);
INSERT INTO messages VALUES (4251, '[Message content here]', '2014-07-25 08:10:00', 62, 83);
INSERT INTO messages VALUES (22599, '[Message content here]', '2014-11-20 07:44:00', 23, 10);
INSERT INTO messages VALUES (10605, '[Message content here]', '2015-04-09 03:55:00', 11, 77);
INSERT INTO messages VALUES (13361, '[Message content here]', '2014-10-02 01:31:00', 48, 70);
INSERT INTO messages VALUES (23832, '[Message content here]', '2014-03-18 00:27:00', 46, 60);
INSERT INTO messages VALUES (19799, '[Message content here]', '2014-05-24 06:39:00', 50, 44);
INSERT INTO messages VALUES (293, '[Message content here]', '2014-12-08 10:46:00', 11, 52);
INSERT INTO messages VALUES (20642, '[Message content here]', '2014-08-07 00:15:00', 64, 11);
INSERT INTO messages VALUES (13370, '[Message content here]', '2014-11-01 04:45:00', 48, 70);
INSERT INTO messages VALUES (4268, '[Message content here]', '2014-09-08 05:51:00', 53, 83);
INSERT INTO messages VALUES (22614, '[Message content here]', '2014-10-02 00:37:00', 58, 80);
INSERT INTO messages VALUES (15249, '[Message content here]', '2014-11-13 07:22:00', 15, 27);
INSERT INTO messages VALUES (13390, '[Message content here]', '2015-03-20 05:51:00', 56, 85);
INSERT INTO messages VALUES (8999, '[Message content here]', '2014-03-13 05:50:00', 25, 32);
INSERT INTO messages VALUES (9002, '[Message content here]', '2015-01-26 06:40:00', 9, 49);
INSERT INTO messages VALUES (324, '[Message content here]', '2014-04-07 03:57:00', 11, 52);
INSERT INTO messages VALUES (19825, '[Message content here]', '2014-12-11 10:39:00', 50, 44);
INSERT INTO messages VALUES (23279, '[Message content here]', '2015-02-01 08:35:00', 6, 63);
INSERT INTO messages VALUES (13402, '[Message content here]', '2014-10-17 00:54:00', 56, 85);
INSERT INTO messages VALUES (7397, '[Message content here]', '2015-03-20 03:31:00', 12, 33);
INSERT INTO messages VALUES (2435, '[Message content here]', '2015-02-20 03:33:00', 50, 44);
INSERT INTO messages VALUES (348, '[Message content here]', '2014-06-10 10:37:00', 25, 71);
INSERT INTO messages VALUES (9025, '[Message content here]', '2014-05-21 06:55:00', 23, 74);
INSERT INTO messages VALUES (22651, '[Message content here]', '2015-03-05 00:25:00', 58, 80);
INSERT INTO messages VALUES (2445, '[Message content here]', '2014-08-22 04:30:00', 13, 8);
INSERT INTO messages VALUES (15290, '[Message content here]', '2014-05-25 00:08:00', 15, 27);
INSERT INTO messages VALUES (22660, '[Message content here]', '2014-09-14 08:29:00', 15, 27);
INSERT INTO messages VALUES (4330, '[Message content here]', '2014-08-23 00:50:00', 67, 28);
INSERT INTO messages VALUES (22661, '[Message content here]', '2014-04-05 10:31:00', 15, 27);
INSERT INTO messages VALUES (367, '[Message content here]', '2014-01-07 02:19:00', 25, 71);
INSERT INTO messages VALUES (7426, '[Message content here]', '2014-02-03 05:26:00', 39, 33);
INSERT INTO messages VALUES (23891, '[Message content here]', '2014-09-02 05:10:00', 18, 84);
INSERT INTO messages VALUES (19867, '[Message content here]', '2014-02-03 03:40:00', 57, 5);
INSERT INTO messages VALUES (15309, '[Message content here]', '2014-06-25 05:21:00', 15, 27);
INSERT INTO messages VALUES (386, '[Message content here]', '2014-12-07 01:26:00', 13, 71);
INSERT INTO messages VALUES (5736, '[Message content here]', '2014-02-21 00:55:00', 23, 74);
INSERT INTO messages VALUES (18491, '[Message content here]', '2014-04-26 10:44:00', 7, 14);
INSERT INTO messages VALUES (21734, '[Message content here]', '2014-12-24 07:09:00', 34, 4);
INSERT INTO messages VALUES (9069, '[Message content here]', '2014-09-02 09:37:00', 58, 80);
INSERT INTO messages VALUES (21741, '[Message content here]', '2015-04-07 03:57:00', 34, 4);
INSERT INTO messages VALUES (22690, '[Message content here]', '2014-06-03 04:36:00', 15, 27);
INSERT INTO messages VALUES (4370, '[Message content here]', '2015-01-26 04:58:00', 58, 28);
INSERT INTO messages VALUES (5756, '[Message content here]', '2015-01-29 09:43:00', 37, 74);
INSERT INTO messages VALUES (10713, '[Message content here]', '2014-04-19 08:28:00', 50, 44);
INSERT INTO messages VALUES (15332, '[Message content here]', '2014-09-29 05:17:00', 34, 55);
INSERT INTO messages VALUES (13472, '[Message content here]', '2014-12-01 06:37:00', 65, 58);
INSERT INTO messages VALUES (21754, '[Message content here]', '2015-01-28 03:02:00', 59, 67);
INSERT INTO messages VALUES (4387, '[Message content here]', '2014-07-06 04:04:00', 67, 28);
INSERT INTO messages VALUES (427, '[Message content here]', '2014-06-20 00:22:00', 28, 77);
INSERT INTO messages VALUES (16675, '[Message content here]', '2014-11-16 02:43:00', 40, 88);
INSERT INTO messages VALUES (18529, '[Message content here]', '2014-02-08 08:58:00', 7, 14);
INSERT INTO messages VALUES (7488, '[Message content here]', '2014-03-22 00:36:00', 58, 80);
INSERT INTO messages VALUES (4417, '[Message content here]', '2015-01-03 02:08:00', 54, 81);
INSERT INTO messages VALUES (13512, '[Message content here]', '2014-07-15 05:49:00', 65, 58);
INSERT INTO messages VALUES (22737, '[Message content here]', '2015-01-17 08:42:00', 67, 85);
INSERT INTO messages VALUES (21791, '[Message content here]', '2014-10-30 05:13:00', 53, 67);
INSERT INTO messages VALUES (18561, '[Message content here]', '2014-08-14 10:57:00', 17, 100);
INSERT INTO messages VALUES (13525, '[Message content here]', '2014-03-05 09:14:00', 3, 24);
INSERT INTO messages VALUES (18565, '[Message content here]', '2014-11-01 02:42:00', 17, 100);
INSERT INTO messages VALUES (15394, '[Message content here]', '2014-05-24 09:12:00', 34, 55);
INSERT INTO messages VALUES (22754, '[Message content here]', '2014-11-05 02:02:00', 56, 85);
INSERT INTO messages VALUES (19965, '[Message content here]', '2015-03-24 10:24:00', 57, 5);
INSERT INTO messages VALUES (23986, '[Message content here]', '2014-07-28 05:43:00', 18, 84);
INSERT INTO messages VALUES (499, '[Message content here]', '2014-06-26 00:07:00', 21, 3);
INSERT INTO messages VALUES (22768, '[Message content here]', '2015-02-13 00:52:00', 23, 74);
INSERT INTO messages VALUES (4465, '[Message content here]', '2014-09-17 00:07:00', 41, 18);
INSERT INTO messages VALUES (16740, '[Message content here]', '2014-08-24 09:23:00', 40, 88);
INSERT INTO messages VALUES (4468, '[Message content here]', '2014-09-25 04:19:00', 41, 18);
INSERT INTO messages VALUES (15421, '[Message content here]', '2014-04-17 02:14:00', 34, 55);
INSERT INTO messages VALUES (20812, '[Message content here]', '2014-05-16 03:07:00', 53, 83);
INSERT INTO messages VALUES (22773, '[Message content here]', '2014-07-24 07:20:00', 42, 90);
INSERT INTO messages VALUES (13564, '[Message content here]', '2014-01-07 07:24:00', 57, 29);
INSERT INTO messages VALUES (22775, '[Message content here]', '2014-08-10 04:55:00', 17, 100);
INSERT INTO messages VALUES (2608, '[Message content here]', '2014-05-16 04:26:00', 1, 8);
INSERT INTO messages VALUES (527, '[Message content here]', '2014-10-30 07:56:00', 65, 58);
INSERT INTO messages VALUES (529, '[Message content here]', '2014-10-19 02:12:00', 65, 58);
INSERT INTO messages VALUES (4489, '[Message content here]', '2014-04-02 10:58:00', 71, 31);
INSERT INTO messages VALUES (22787, '[Message content here]', '2015-03-27 04:44:00', 20, 100);
INSERT INTO messages VALUES (9189, '[Message content here]', '2014-07-02 03:18:00', 63, 42);
INSERT INTO messages VALUES (22788, '[Message content here]', '2015-01-05 10:49:00', 1, 8);
INSERT INTO messages VALUES (9192, '[Message content here]', '2014-09-29 07:16:00', 59, 67);
INSERT INTO messages VALUES (16763, '[Message content here]', '2014-04-02 04:40:00', 4, 53);
INSERT INTO messages VALUES (20839, '[Message content here]', '2014-07-23 09:42:00', 53, 83);
INSERT INTO messages VALUES (10839, '[Message content here]', '2014-08-12 00:32:00', 50, 44);
INSERT INTO messages VALUES (13592, '[Message content here]', '2015-01-18 09:28:00', 57, 29);
INSERT INTO messages VALUES (4509, '[Message content here]', '2014-07-16 03:41:00', 71, 31);
INSERT INTO messages VALUES (10845, '[Message content here]', '2014-12-07 05:20:00', 13, 71);
INSERT INTO messages VALUES (20009, '[Message content here]', '2014-07-16 09:52:00', 41, 48);
INSERT INTO messages VALUES (7606, '[Message content here]', '2014-08-19 09:50:00', 58, 80);
INSERT INTO messages VALUES (13605, '[Message content here]', '2014-07-08 04:45:00', 23, 10);
INSERT INTO messages VALUES (5907, '[Message content here]', '2014-12-10 02:57:00', 13, 8);
INSERT INTO messages VALUES (15473, '[Message content here]', '2014-06-28 02:14:00', 1, 39);
INSERT INTO messages VALUES (5913, '[Message content here]', '2015-03-19 05:43:00', 13, 8);
INSERT INTO messages VALUES (9230, '[Message content here]', '2014-06-26 04:42:00', 59, 67);
INSERT INTO messages VALUES (20861, '[Message content here]', '2014-07-28 00:04:00', 49, 93);
INSERT INTO messages VALUES (21873, '[Message content here]', '2015-01-10 00:14:00', 53, 67);
INSERT INTO messages VALUES (20875, '[Message content here]', '2015-01-05 08:11:00', 49, 93);
INSERT INTO messages VALUES (21886, '[Message content here]', '2014-04-03 03:47:00', 10, 32);
INSERT INTO messages VALUES (595, '[Message content here]', '2015-02-10 10:08:00', 9, 49);
INSERT INTO messages VALUES (9251, '[Message content here]', '2014-12-02 00:48:00', 59, 67);
INSERT INTO messages VALUES (5937, '[Message content here]', '2014-10-06 06:22:00', 13, 8);
INSERT INTO messages VALUES (10886, '[Message content here]', '2014-01-18 09:30:00', 25, 71);
INSERT INTO messages VALUES (7644, '[Message content here]', '2014-09-20 01:15:00', 56, 85);
INSERT INTO messages VALUES (2692, '[Message content here]', '2015-01-11 01:10:00', 18, 84);
INSERT INTO messages VALUES (10892, '[Message content here]', '2014-04-28 09:26:00', 25, 71);
INSERT INTO messages VALUES (4574, '[Message content here]', '2014-03-30 09:31:00', 71, 31);
INSERT INTO messages VALUES (7655, '[Message content here]', '2015-01-24 03:29:00', 56, 85);
INSERT INTO messages VALUES (20896, '[Message content here]', '2015-01-16 05:09:00', 7, 14);
INSERT INTO messages VALUES (20067, '[Message content here]', '2014-09-06 09:34:00', 15, 75);
INSERT INTO messages VALUES (5968, '[Message content here]', '2014-10-08 04:00:00', 28, 77);
INSERT INTO messages VALUES (21916, '[Message content here]', '2014-05-13 02:05:00', 17, 100);
INSERT INTO messages VALUES (22860, '[Message content here]', '2014-07-26 08:25:00', 49, 93);
INSERT INTO messages VALUES (4596, '[Message content here]', '2014-10-25 03:51:00', 39, 19);
INSERT INTO messages VALUES (20911, '[Message content here]', '2014-11-14 05:04:00', 33, 99);
INSERT INTO messages VALUES (15527, '[Message content here]', '2015-02-27 01:07:00', 51, 39);
INSERT INTO messages VALUES (22865, '[Message content here]', '2015-01-10 08:17:00', 49, 93);
INSERT INTO messages VALUES (22866, '[Message content here]', '2014-01-16 06:39:00', 21, 98);
INSERT INTO messages VALUES (13672, '[Message content here]', '2014-05-28 01:54:00', 23, 10);
INSERT INTO messages VALUES (16854, '[Message content here]', '2014-04-08 07:22:00', 52, 7);
INSERT INTO messages VALUES (21928, '[Message content here]', '2014-08-18 01:07:00', 17, 100);
INSERT INTO messages VALUES (4614, '[Message content here]', '2014-05-08 08:39:00', 20, 14);
INSERT INTO messages VALUES (21940, '[Message content here]', '2014-03-11 01:36:00', 20, 100);
INSERT INTO messages VALUES (21941, '[Message content here]', '2014-06-25 01:05:00', 4, 6);
INSERT INTO messages VALUES (20934, '[Message content here]', '2014-09-10 10:45:00', 71, 31);
INSERT INTO messages VALUES (13701, '[Message content here]', '2014-08-19 05:33:00', 64, 73);
INSERT INTO messages VALUES (18732, '[Message content here]', '2015-01-24 03:42:00', 11, 77);
INSERT INTO messages VALUES (20957, '[Message content here]', '2014-04-30 08:58:00', 71, 31);
INSERT INTO messages VALUES (6025, '[Message content here]', '2015-01-30 07:28:00', 28, 77);
INSERT INTO messages VALUES (15575, '[Message content here]', '2014-04-01 10:21:00', 17, 86);
INSERT INTO messages VALUES (9348, '[Message content here]', '2015-03-04 05:15:00', 17, 100);
INSERT INTO messages VALUES (16907, '[Message content here]', '2015-01-25 06:00:00', 52, 7);
INSERT INTO messages VALUES (13741, '[Message content here]', '2015-04-05 07:54:00', 60, 73);
INSERT INTO messages VALUES (22927, '[Message content here]', '2014-03-11 07:52:00', 21, 98);
INSERT INTO messages VALUES (13753, '[Message content here]', '2014-05-18 04:38:00', 60, 73);
INSERT INTO messages VALUES (20161, '[Message content here]', '2014-03-14 06:14:00', 38, 2);
INSERT INTO messages VALUES (7774, '[Message content here]', '2014-10-04 06:39:00', 24, 38);
INSERT INTO messages VALUES (4704, '[Message content here]', '2015-01-12 08:12:00', 7, 14);
INSERT INTO messages VALUES (4705, '[Message content here]', '2014-01-26 00:23:00', 7, 14);
INSERT INTO messages VALUES (13772, '[Message content here]', '2014-07-08 02:04:00', 68, 97);
INSERT INTO messages VALUES (758, '[Message content here]', '2014-12-03 00:10:00', 48, 37);
INSERT INTO messages VALUES (15634, '[Message content here]', '2014-07-25 03:28:00', 14, 86);
INSERT INTO messages VALUES (4720, '[Message content here]', '2014-09-25 10:15:00', 59, 93);
INSERT INTO messages VALUES (15637, '[Message content here]', '2014-11-19 06:47:00', 27, 97);
INSERT INTO messages VALUES (768, '[Message content here]', '2014-08-15 09:53:00', 48, 37);
INSERT INTO messages VALUES (16956, '[Message content here]', '2014-01-09 05:04:00', 50, 64);
INSERT INTO messages VALUES (20184, '[Message content here]', '2014-07-05 09:56:00', 35, 2);
INSERT INTO messages VALUES (15646, '[Message content here]', '2014-04-10 00:48:00', 27, 97);
INSERT INTO messages VALUES (23601, '[Message content here]', '2015-01-21 03:07:00', 6, 63);
INSERT INTO messages VALUES (4733, '[Message content here]', '2014-05-10 04:10:00', 60, 73);
INSERT INTO messages VALUES (9416, '[Message content here]', '2014-01-12 07:31:00', 30, 36);
INSERT INTO messages VALUES (21025, '[Message content here]', '2014-03-01 03:37:00', 53, 83);
INSERT INTO messages VALUES (20196, '[Message content here]', '2015-02-17 04:06:00', 35, 2);
INSERT INTO messages VALUES (7813, '[Message content here]', '2014-09-17 02:39:00', 45, 24);
INSERT INTO messages VALUES (23607, '[Message content here]', '2014-02-19 09:37:00', 6, 63);
INSERT INTO messages VALUES (23615, '[Message content here]', '2014-04-11 10:20:00', 19, 9);
INSERT INTO messages VALUES (21039, '[Message content here]', '2014-07-16 09:43:00', 62, 83);
INSERT INTO messages VALUES (11064, '[Message content here]', '2014-12-25 07:42:00', 41, 48);
INSERT INTO messages VALUES (7830, '[Message content here]', '2015-02-18 06:22:00', 45, 24);
INSERT INTO messages VALUES (15671, '[Message content here]', '2014-12-18 05:25:00', 27, 97);
INSERT INTO messages VALUES (23623, '[Message content here]', '2014-04-03 06:36:00', 19, 9);
INSERT INTO messages VALUES (7836, '[Message content here]', '2014-10-24 08:48:00', 45, 24);
INSERT INTO messages VALUES (18838, '[Message content here]', '2014-12-11 09:54:00', 67, 85);
INSERT INTO messages VALUES (20229, '[Message content here]', '2014-10-16 09:17:00', 40, 64);
INSERT INTO messages VALUES (4781, '[Message content here]', '2014-07-10 10:22:00', 60, 73);
INSERT INTO messages VALUES (15692, '[Message content here]', '2015-01-21 01:41:00', 40, 88);
INSERT INTO messages VALUES (18855, '[Message content here]', '2014-06-21 05:52:00', 56, 85);
INSERT INTO messages VALUES (20237, '[Message content here]', '2014-12-19 07:40:00', 40, 64);
INSERT INTO messages VALUES (15700, '[Message content here]', '2014-05-18 10:40:00', 61, 88);
INSERT INTO messages VALUES (4794, '[Message content here]', '2014-06-10 06:08:00', 50, 44);
INSERT INTO messages VALUES (841, '[Message content here]', '2015-04-01 08:22:00', 36, 26);
INSERT INTO messages VALUES (13852, '[Message content here]', '2014-10-19 01:33:00', 30, 36);
INSERT INTO messages VALUES (21074, '[Message content here]', '2014-09-29 09:52:00', 4, 53);
INSERT INTO messages VALUES (18869, '[Message content here]', '2014-04-29 10:23:00', 10, 66);
INSERT INTO messages VALUES (13858, '[Message content here]', '2014-02-28 00:56:00', 11, 52);
INSERT INTO messages VALUES (6178, '[Message content here]', '2014-11-16 02:32:00', 28, 77);
INSERT INTO messages VALUES (20254, '[Message content here]', '2014-07-29 07:09:00', 40, 64);
INSERT INTO messages VALUES (868, '[Message content here]', '2015-03-29 04:30:00', 36, 26);
INSERT INTO messages VALUES (7890, '[Message content here]', '2014-02-19 02:21:00', 58, 28);
INSERT INTO messages VALUES (22095, '[Message content here]', '2015-01-21 07:27:00', 35, 6);
INSERT INTO messages VALUES (878, '[Message content here]', '2014-02-21 00:55:00', 26, 26);
INSERT INTO messages VALUES (22103, '[Message content here]', '2014-10-28 10:18:00', 2, 56);
INSERT INTO messages VALUES (17063, '[Message content here]', '2014-11-18 03:42:00', 50, 64);
INSERT INTO messages VALUES (2984, '[Message content here]', '2015-02-11 01:34:00', 18, 84);
INSERT INTO messages VALUES (20303, '[Message content here]', '2015-02-28 02:10:00', 8, 22);
INSERT INTO messages VALUES (15769, '[Message content here]', '2014-07-01 03:57:00', 40, 88);
INSERT INTO messages VALUES (2997, '[Message content here]', '2014-03-23 03:11:00', 49, 50);
INSERT INTO messages VALUES (3001, '[Message content here]', '2015-01-25 08:55:00', 49, 50);
INSERT INTO messages VALUES (6244, '[Message content here]', '2014-12-07 07:30:00', 7, 14);
INSERT INTO messages VALUES (6247, '[Message content here]', '2014-01-24 01:05:00', 20, 14);
INSERT INTO messages VALUES (6251, '[Message content here]', '2014-09-17 01:23:00', 7, 14);
INSERT INTO messages VALUES (11185, '[Message content here]', '2014-08-12 00:44:00', 45, 12);
INSERT INTO messages VALUES (7959, '[Message content here]', '2014-02-05 04:36:00', 28, 72);
INSERT INTO messages VALUES (6276, '[Message content here]', '2015-03-18 07:09:00', 34, 55);
INSERT INTO messages VALUES (15808, '[Message content here]', '2014-01-26 01:40:00', 46, 29);
INSERT INTO messages VALUES (15813, '[Message content here]', '2014-08-16 10:56:00', 46, 29);
INSERT INTO messages VALUES (7982, '[Message content here]', '2014-07-29 00:54:00', 47, 96);
INSERT INTO messages VALUES (3051, '[Message content here]', '2014-04-01 01:50:00', 29, 60);
INSERT INTO messages VALUES (6293, '[Message content here]', '2014-12-13 00:38:00', 62, 37);
INSERT INTO messages VALUES (4928, '[Message content here]', '2014-02-11 02:25:00', 43, 18);
INSERT INTO messages VALUES (20356, '[Message content here]', '2015-04-01 10:30:00', 8, 22);
INSERT INTO messages VALUES (983, '[Message content here]', '2014-01-18 09:45:00', 56, 59);
INSERT INTO messages VALUES (7995, '[Message content here]', '2014-06-26 05:12:00', 47, 96);
INSERT INTO messages VALUES (17139, '[Message content here]', '2014-12-23 02:21:00', 40, 64);
INSERT INTO messages VALUES (20375, '[Message content here]', '2014-10-03 05:33:00', 19, 19);
INSERT INTO messages VALUES (6323, '[Message content here]', '2015-02-21 08:12:00', 62, 37);
INSERT INTO messages VALUES (9623, '[Message content here]', '2014-09-17 02:30:00', 30, 36);
INSERT INTO messages VALUES (4960, '[Message content here]', '2014-07-09 06:09:00', 43, 18);
INSERT INTO messages VALUES (20392, '[Message content here]', '2014-07-23 03:36:00', 71, 31);
INSERT INTO messages VALUES (11263, '[Message content here]', '2014-11-30 04:01:00', 45, 12);
INSERT INTO messages VALUES (6348, '[Message content here]', '2015-03-05 06:55:00', 46, 60);
INSERT INTO messages VALUES (11275, '[Message content here]', '2015-01-09 05:07:00', 24, 12);
INSERT INTO messages VALUES (3121, '[Message content here]', '2014-06-23 05:53:00', 29, 60);
INSERT INTO messages VALUES (21240, '[Message content here]', '2015-02-18 07:48:00', 4, 53);
INSERT INTO messages VALUES (6371, '[Message content here]', '2014-09-18 06:27:00', 46, 60);
INSERT INTO messages VALUES (9672, '[Message content here]', '2014-03-18 03:10:00', 41, 18);
INSERT INTO messages VALUES (5014, '[Message content here]', '2014-07-16 05:10:00', 41, 18);
INSERT INTO messages VALUES (19054, '[Message content here]', '2014-04-18 06:52:00', 10, 66);
INSERT INTO messages VALUES (8074, '[Message content here]', '2014-11-06 10:27:00', 63, 16);
INSERT INTO messages VALUES (6386, '[Message content here]', '2015-02-11 00:39:00', 29, 60);
INSERT INTO messages VALUES (8085, '[Message content here]', '2014-09-22 04:24:00', 63, 16);
INSERT INTO messages VALUES (6399, '[Message content here]', '2014-05-05 01:22:00', 27, 35);
INSERT INTO messages VALUES (1090, '[Message content here]', '2015-01-28 00:43:00', 26, 15);
INSERT INTO messages VALUES (3166, '[Message content here]', '2014-09-24 05:43:00', 38, 2);
INSERT INTO messages VALUES (14067, '[Message content here]', '2014-08-24 05:21:00', 11, 52);
INSERT INTO messages VALUES (22276, '[Message content here]', '2015-01-03 10:46:00', 2, 56);
INSERT INTO messages VALUES (3172, '[Message content here]', '2014-11-02 02:48:00', 35, 2);
INSERT INTO messages VALUES (17232, '[Message content here]', '2015-04-06 10:20:00', 60, 73);
INSERT INTO messages VALUES (22279, '[Message content here]', '2014-10-18 05:45:00', 11, 77);
INSERT INTO messages VALUES (9700, '[Message content here]', '2014-12-14 01:54:00', 41, 18);
INSERT INTO messages VALUES (19079, '[Message content here]', '2014-02-21 08:08:00', 10, 66);
INSERT INTO messages VALUES (19092, '[Message content here]', '2014-04-12 07:05:00', 62, 37);
INSERT INTO messages VALUES (1124, '[Message content here]', '2014-05-29 07:31:00', 26, 15);
INSERT INTO messages VALUES (21301, '[Message content here]', '2014-07-31 04:27:00', 4, 53);
INSERT INTO messages VALUES (1131, '[Message content here]', '2014-02-10 07:19:00', 65, 58);
INSERT INTO messages VALUES (22312, '[Message content here]', '2014-05-31 02:31:00', 28, 77);
INSERT INTO messages VALUES (17271, '[Message content here]', '2014-10-30 04:09:00', 60, 73);
INSERT INTO messages VALUES (19124, '[Message content here]', '2014-03-09 01:02:00', 62, 37);
INSERT INTO messages VALUES (5096, '[Message content here]', '2014-04-16 10:47:00', 58, 80);
INSERT INTO messages VALUES (3234, '[Message content here]', '2014-09-30 08:06:00', 16, 47);
INSERT INTO messages VALUES (21336, '[Message content here]', '2014-06-10 02:53:00', 4, 53);
INSERT INTO messages VALUES (9766, '[Message content here]', '2014-07-02 04:12:00', 59, 93);
INSERT INTO messages VALUES (22339, '[Message content here]', '2014-09-06 10:34:00', 63, 42);
INSERT INTO messages VALUES (9768, '[Message content here]', '2014-08-21 07:52:00', 59, 93);
INSERT INTO messages VALUES (3248, '[Message content here]', '2014-04-10 09:26:00', 36, 26);
INSERT INTO messages VALUES (17301, '[Message content here]', '2014-12-03 06:24:00', 12, 33);
INSERT INTO messages VALUES (1178, '[Message content here]', '2014-01-10 00:52:00', 22, 21);
INSERT INTO messages VALUES (9775, '[Message content here]', '2014-09-20 07:09:00', 59, 93);
INSERT INTO messages VALUES (14147, '[Message content here]', '2014-02-27 00:39:00', 11, 52);
INSERT INTO messages VALUES (3263, '[Message content here]', '2015-03-27 04:24:00', 41, 18);
INSERT INTO messages VALUES (22351, '[Message content here]', '2014-12-18 03:53:00', 63, 42);
INSERT INTO messages VALUES (17318, '[Message content here]', '2014-02-18 07:17:00', 39, 33);
INSERT INTO messages VALUES (3271, '[Message content here]', '2014-01-04 03:28:00', 41, 18);
INSERT INTO messages VALUES (21357, '[Message content here]', '2015-01-03 03:54:00', 49, 93);
INSERT INTO messages VALUES (22359, '[Message content here]', '2015-03-02 01:27:00', 63, 42);
INSERT INTO messages VALUES (19163, '[Message content here]', '2014-05-22 01:02:00', 57, 29);
INSERT INTO messages VALUES (1207, '[Message content here]', '2015-03-21 01:56:00', 13, 8);
INSERT INTO messages VALUES (1212, '[Message content here]', '2014-05-09 10:34:00', 1, 8);
INSERT INTO messages VALUES (3286, '[Message content here]', '2014-06-22 01:39:00', 41, 18);
INSERT INTO messages VALUES (16029, '[Message content here]', '2014-10-21 07:41:00', 29, 76);
INSERT INTO messages VALUES (16031, '[Message content here]', '2014-04-28 06:36:00', 14, 86);
INSERT INTO messages VALUES (17340, '[Message content here]', '2014-12-16 03:04:00', 39, 33);
INSERT INTO messages VALUES (16043, '[Message content here]', '2015-04-03 06:15:00', 14, 86);
INSERT INTO messages VALUES (9825, '[Message content here]', '2014-07-18 06:26:00', 6, 63);
INSERT INTO messages VALUES (21390, '[Message content here]', '2014-02-27 10:30:00', 59, 93);
INSERT INTO messages VALUES (14205, '[Message content here]', '2014-02-28 06:01:00', 2, 92);
INSERT INTO messages VALUES (1258, '[Message content here]', '2015-01-15 07:12:00', 13, 71);
INSERT INTO messages VALUES (17371, '[Message content here]', '2014-10-02 08:08:00', 1, 8);
INSERT INTO messages VALUES (16084, '[Message content here]', '2014-05-25 07:13:00', 29, 76);
INSERT INTO messages VALUES (5215, '[Message content here]', '2014-02-04 04:54:00', 58, 80);
INSERT INTO messages VALUES (5221, '[Message content here]', '2015-01-05 03:21:00', 58, 80);
INSERT INTO messages VALUES (3365, '[Message content here]', '2015-02-26 02:23:00', 45, 12);
INSERT INTO messages VALUES (17407, '[Message content here]', '2014-06-21 10:02:00', 1, 8);
INSERT INTO messages VALUES (16103, '[Message content here]', '2014-04-19 10:05:00', 29, 76);
INSERT INTO messages VALUES (5243, '[Message content here]', '2014-09-03 05:45:00', 4, 6);
INSERT INTO messages VALUES (19258, '[Message content here]', '2014-10-06 08:51:00', 2, 92);
INSERT INTO messages VALUES (11526, '[Message content here]', '2014-07-10 07:48:00', 15, 75);
INSERT INTO messages VALUES (3392, '[Message content here]', '2014-03-20 05:47:00', 45, 12);
INSERT INTO messages VALUES (17427, '[Message content here]', '2014-04-18 02:20:00', 13, 8);
INSERT INTO messages VALUES (8318, '[Message content here]', '2014-06-29 10:48:00', 63, 16);
INSERT INTO messages VALUES (9923, '[Message content here]', '2014-08-26 06:37:00', 6, 63);
INSERT INTO messages VALUES (1349, '[Message content here]', '2015-03-14 06:08:00', 13, 71);
INSERT INTO messages VALUES (17460, '[Message content here]', '2014-09-14 05:49:00', 17, 100);
INSERT INTO messages VALUES (17467, '[Message content here]', '2014-05-11 06:21:00', 20, 100);
INSERT INTO messages VALUES (16164, '[Message content here]', '2014-04-04 03:02:00', 29, 76);
INSERT INTO messages VALUES (6668, '[Message content here]', '2014-08-24 01:43:00', 27, 35);
INSERT INTO messages VALUES (5312, '[Message content here]', '2014-11-12 01:09:00', 4, 6);
INSERT INTO messages VALUES (6677, '[Message content here]', '2014-04-30 09:48:00', 27, 35);
INSERT INTO messages VALUES (5317, '[Message content here]', '2014-08-21 06:55:00', 35, 6);
INSERT INTO messages VALUES (9967, '[Message content here]', '2014-08-21 04:20:00', 62, 83);
INSERT INTO messages VALUES (14333, '[Message content here]', '2014-10-16 04:08:00', 2, 92);
INSERT INTO messages VALUES (8372, '[Message content here]', '2015-01-17 03:29:00', 16, 79);
INSERT INTO messages VALUES (9971, '[Message content here]', '2014-10-23 06:48:00', 62, 83);
INSERT INTO messages VALUES (17489, '[Message content here]', '2014-09-04 07:36:00', 41, 18);
INSERT INTO messages VALUES (3481, '[Message content here]', '2015-04-02 01:51:00', 16, 47);
INSERT INTO messages VALUES (8389, '[Message content here]', '2014-02-28 08:16:00', 68, 97);
INSERT INTO messages VALUES (6703, '[Message content here]', '2014-03-07 03:51:00', 5, 69);
INSERT INTO messages VALUES (19338, '[Message content here]', '2014-05-02 03:32:00', 56, 59);
INSERT INTO messages VALUES (1421, '[Message content here]', '2014-07-13 06:36:00', 18, 84);
INSERT INTO messages VALUES (3490, '[Message content here]', '2015-01-27 00:31:00', 16, 47);
INSERT INTO messages VALUES (11617, '[Message content here]', '2014-07-25 00:00:00', 15, 75);
INSERT INTO messages VALUES (14365, '[Message content here]', '2014-06-06 07:26:00', 2, 92);
INSERT INTO messages VALUES (5359, '[Message content here]', '2014-06-05 04:04:00', 15, 27);
INSERT INTO messages VALUES (8408, '[Message content here]', '2014-09-26 10:53:00', 27, 97);
INSERT INTO messages VALUES (11628, '[Message content here]', '2015-03-20 09:34:00', 15, 75);
INSERT INTO messages VALUES (10009, '[Message content here]', '2014-05-10 09:12:00', 1, 8);
INSERT INTO messages VALUES (6733, '[Message content here]', '2014-08-16 10:23:00', 39, 33);
INSERT INTO messages VALUES (1450, '[Message content here]', '2015-04-07 07:42:00', 18, 84);
INSERT INTO messages VALUES (14387, '[Message content here]', '2014-10-03 00:19:00', 19, 9);
INSERT INTO messages VALUES (16237, '[Message content here]', '2014-12-27 06:04:00', 4, 53);
INSERT INTO messages VALUES (3533, '[Message content here]', '2014-08-17 07:25:00', 16, 47);
INSERT INTO messages VALUES (17554, '[Message content here]', '2014-10-28 09:52:00', 41, 18);
INSERT INTO messages VALUES (3548, '[Message content here]', '2015-03-01 07:43:00', 67, 28);
INSERT INTO messages VALUES (6772, '[Message content here]', '2014-10-08 10:54:00', 12, 33);
INSERT INTO messages VALUES (1493, '[Message content here]', '2015-02-25 08:18:00', 18, 84);
INSERT INTO messages VALUES (14421, '[Message content here]', '2014-07-24 04:45:00', 69, 9);
INSERT INTO messages VALUES (16278, '[Message content here]', '2014-10-18 05:31:00', 4, 53);
INSERT INTO messages VALUES (6809, '[Message content here]', '2015-01-04 09:32:00', 12, 33);
INSERT INTO messages VALUES (14452, '[Message content here]', '2014-03-03 02:11:00', 69, 9);
INSERT INTO messages VALUES (14453, '[Message content here]', '2014-09-20 03:50:00', 26, 15);
INSERT INTO messages VALUES (16297, '[Message content here]', '2014-06-30 02:37:00', 15, 75);
INSERT INTO messages VALUES (11714, '[Message content here]', '2014-05-10 05:41:00', 27, 35);
INSERT INTO messages VALUES (11715, '[Message content here]', '2014-10-04 04:55:00', 27, 35);
INSERT INTO messages VALUES (8503, '[Message content here]', '2015-03-04 06:05:00', 62, 83);
INSERT INTO messages VALUES (6821, '[Message content here]', '2014-05-07 02:15:00', 47, 91);
INSERT INTO messages VALUES (3614, '[Message content here]', '2014-10-06 07:34:00', 58, 28);
INSERT INTO messages VALUES (6836, '[Message content here]', '2014-04-21 06:01:00', 64, 73);
INSERT INTO messages VALUES (8523, '[Message content here]', '2014-09-19 09:37:00', 16, 79);
INSERT INTO messages VALUES (6847, '[Message content here]', '2014-06-29 02:34:00', 60, 73);
INSERT INTO messages VALUES (3639, '[Message content here]', '2014-08-13 09:30:00', 58, 28);
INSERT INTO messages VALUES (11749, '[Message content here]', '2014-02-10 02:32:00', 19, 19);
INSERT INTO messages VALUES (10134, '[Message content here]', '2015-01-15 10:53:00', 8, 22);
INSERT INTO messages VALUES (6889, '[Message content here]', '2015-04-05 07:52:00', 36, 25);
INSERT INTO messages VALUES (1613, '[Message content here]', '2014-01-04 10:56:00', 18, 84);
INSERT INTO messages VALUES (10166, '[Message content here]', '2014-07-19 02:50:00', 8, 22);
INSERT INTO messages VALUES (3683, '[Message content here]', '2014-12-19 05:58:00', 70, 54);
INSERT INTO messages VALUES (19502, '[Message content here]', '2014-07-12 05:51:00', 56, 59);
INSERT INTO messages VALUES (6904, '[Message content here]', '2015-03-19 06:17:00', 36, 25);
INSERT INTO messages VALUES (17676, '[Message content here]', '2014-08-28 06:38:00', 20, 100);
INSERT INTO messages VALUES (10184, '[Message content here]', '2014-02-20 00:16:00', 22, 21);
INSERT INTO messages VALUES (19515, '[Message content here]', '2014-11-10 09:18:00', 56, 59);
INSERT INTO messages VALUES (10209, '[Message content here]', '2014-05-24 10:44:00', 22, 21);
INSERT INTO messages VALUES (6938, '[Message content here]', '2014-02-12 09:58:00', 27, 97);
INSERT INTO messages VALUES (3737, '[Message content here]', '2014-02-14 09:27:00', 1, 39);
INSERT INTO messages VALUES (10222, '[Message content here]', '2014-08-24 00:31:00', 31, 21);
INSERT INTO messages VALUES (3740, '[Message content here]', '2014-03-21 03:42:00', 1, 39);
INSERT INTO messages VALUES (19560, '[Message content here]', '2014-04-03 02:44:00', 3, 38);
INSERT INTO messages VALUES (6963, '[Message content here]', '2014-10-04 10:16:00', 27, 97);
INSERT INTO messages VALUES (17740, '[Message content here]', '2015-01-20 00:41:00', 40, 64);
INSERT INTO messages VALUES (6977, '[Message content here]', '2014-02-28 04:01:00', 27, 97);
INSERT INTO messages VALUES (3775, '[Message content here]', '2014-07-28 05:28:00', 22, 41);
INSERT INTO messages VALUES (17750, '[Message content here]', '2015-03-07 01:45:00', 40, 64);
INSERT INTO messages VALUES (17752, '[Message content here]', '2014-10-20 08:25:00', 40, 64);
INSERT INTO messages VALUES (6988, '[Message content here]', '2014-08-22 07:51:00', 34, 55);
INSERT INTO messages VALUES (14614, '[Message content here]', '2014-05-21 04:17:00', 69, 9);
INSERT INTO messages VALUES (14615, '[Message content here]', '2014-09-02 02:07:00', 19, 9);
INSERT INTO messages VALUES (10267, '[Message content here]', '2014-10-23 10:47:00', 54, 45);
INSERT INTO messages VALUES (11885, '[Message content here]', '2014-01-30 00:53:00', 14, 57);
INSERT INTO messages VALUES (11893, '[Message content here]', '2015-03-18 03:48:00', 65, 3);
INSERT INTO messages VALUES (3804, '[Message content here]', '2014-03-01 01:31:00', 22, 41);
INSERT INTO messages VALUES (10284, '[Message content here]', '2015-03-13 08:54:00', 54, 45);
INSERT INTO messages VALUES (14635, '[Message content here]', '2014-12-30 00:18:00', 19, 9);
INSERT INTO messages VALUES (14636, '[Message content here]', '2014-04-19 08:55:00', 19, 19);
INSERT INTO messages VALUES (7036, '[Message content here]', '2014-05-24 08:55:00', 56, 85);
INSERT INTO messages VALUES (17798, '[Message content here]', '2015-03-16 10:53:00', 33, 99);
INSERT INTO messages VALUES (11931, '[Message content here]', '2015-03-22 04:21:00', 24, 12);
INSERT INTO messages VALUES (17807, '[Message content here]', '2014-11-15 07:19:00', 33, 99);
INSERT INTO messages VALUES (11938, '[Message content here]', '2014-11-13 08:48:00', 24, 12);
INSERT INTO messages VALUES (17830, '[Message content here]', '2014-12-30 04:23:00', 33, 99);
INSERT INTO messages VALUES (17835, '[Message content here]', '2015-01-29 10:29:00', 1, 39);
INSERT INTO messages VALUES (11972, '[Message content here]', '2015-01-06 10:05:00', 47, 91);
INSERT INTO messages VALUES (14736, '[Message content here]', '2014-03-14 02:54:00', 19, 19);
INSERT INTO messages VALUES (3917, '[Message content here]', '2014-10-15 06:50:00', 22, 41);
INSERT INTO messages VALUES (17876, '[Message content here]', '2014-08-29 08:22:00', 51, 39);
INSERT INTO messages VALUES (14757, '[Message content here]', '2014-08-22 05:24:00', 49, 50);
INSERT INTO messages VALUES (1885, '[Message content here]', '2014-12-21 08:38:00', 18, 84);
INSERT INTO messages VALUES (17908, '[Message content here]', '2014-08-12 00:08:00', 67, 28);
INSERT INTO messages VALUES (17923, '[Message content here]', '2014-04-06 02:31:00', 2, 92);
INSERT INTO messages VALUES (1941, '[Message content here]', '2014-05-31 06:55:00', 18, 84);
INSERT INTO messages VALUES (14832, '[Message content here]', '2014-10-10 06:23:00', 62, 37);
INSERT INTO messages VALUES (14835, '[Message content here]', '2014-02-04 02:12:00', 62, 37);
INSERT INTO messages VALUES (17986, '[Message content here]', '2014-06-21 02:46:00', 2, 92);
INSERT INTO messages VALUES (12130, '[Message content here]', '2014-01-15 09:07:00', 59, 67);
INSERT INTO messages VALUES (2005, '[Message content here]', '2014-08-13 05:58:00', 37, 20);
INSERT INTO messages VALUES (14887, '[Message content here]', '2014-03-20 06:48:00', 63, 42);
INSERT INTO messages VALUES (14896, '[Message content here]', '2014-02-14 01:45:00', 61, 91);
INSERT INTO messages VALUES (18036, '[Message content here]', '2014-10-20 10:27:00', 8, 22);
INSERT INTO messages VALUES (2026, '[Message content here]', '2015-01-14 05:12:00', 37, 20);
INSERT INTO messages VALUES (14908, '[Message content here]', '2014-03-13 04:07:00', 47, 91);
INSERT INTO messages VALUES (2035, '[Message content here]', '2014-11-28 01:23:00', 37, 20);
INSERT INTO messages VALUES (14915, '[Message content here]', '2014-02-09 07:21:00', 15, 27);
INSERT INTO messages VALUES (12183, '[Message content here]', '2014-06-23 00:04:00', 23, 10);
INSERT INTO messages VALUES (14924, '[Message content here]', '2014-01-05 08:39:00', 67, 85);
INSERT INTO messages VALUES (14931, '[Message content here]', '2014-11-08 01:27:00', 67, 85);
INSERT INTO messages VALUES (18068, '[Message content here]', '2015-04-01 03:43:00', 35, 6);
INSERT INTO messages VALUES (18072, '[Message content here]', '2014-05-17 10:49:00', 70, 44);
INSERT INTO messages VALUES (2083, '[Message content here]', '2014-04-07 10:09:00', 69, 9);
INSERT INTO messages VALUES (14980, '[Message content here]', '2015-01-29 07:56:00', 56, 85);
INSERT INTO messages VALUES (14989, '[Message content here]', '2014-05-17 10:39:00', 51, 34);
INSERT INTO messages VALUES (12257, '[Message content here]', '2014-04-15 09:16:00', 6, 10);
INSERT INTO messages VALUES (18124, '[Message content here]', '2014-01-19 03:12:00', 50, 44);
INSERT INTO messages VALUES (12294, '[Message content here]', '2015-01-11 09:41:00', 6, 10);
INSERT INTO messages VALUES (12300, '[Message content here]', '2014-07-11 04:08:00', 32, 40);
INSERT INTO messages VALUES (12301, '[Message content here]', '2014-12-22 06:06:00', 22, 41);
INSERT INTO messages VALUES (18159, '[Message content here]', '2014-06-09 00:37:00', 50, 44);
INSERT INTO messages VALUES (12462, '[Message content here]', '2014-08-10 08:09:00', 22, 41);
INSERT INTO messages VALUES (12468, '[Message content here]', '2014-02-07 00:53:00', 64, 73);
INSERT INTO messages VALUES (12477, '[Message content here]', '2014-11-27 08:44:00', 36, 26);
INSERT INTO messages VALUES (12482, '[Message content here]', '2014-09-25 04:11:00', 26, 26);
INSERT INTO messages VALUES (12490, '[Message content here]', '2014-07-06 00:48:00', 36, 26);
INSERT INTO messages VALUES (12694, '[Message content here]', '2015-04-08 01:54:00', 63, 16);
INSERT INTO messages VALUES (12820, '[Message content here]', '2014-02-01 08:21:00', 63, 16);
INSERT INTO messages VALUES (12863, '[Message content here]', '2014-09-06 08:42:00', 56, 59);
INSERT INTO messages VALUES (12917, '[Message content here]', '2014-11-18 03:53:00', 63, 42);
INSERT INTO messages VALUES (12940, '[Message content here]', '2014-11-18 01:16:00', 63, 42);
INSERT INTO messages VALUES (12980, '[Message content here]', '2014-08-07 07:09:00', 63, 42);
INSERT INTO messages VALUES (13054, '[Message content here]', '2014-07-18 04:01:00', 51, 34);



INSERT INTO friends VALUES (50, 57);
INSERT INTO friends VALUES (1, 70);
INSERT INTO friends VALUES (95, 8);
INSERT INTO friends VALUES (59, 71);
INSERT INTO friends VALUES (18, 93);
INSERT INTO friends VALUES (86, 94);
INSERT INTO friends VALUES (71, 32);
INSERT INTO friends VALUES (55, 88);
INSERT INTO friends VALUES (38, 4);
INSERT INTO friends VALUES (9, 37);
INSERT INTO friends VALUES (32, 95);
INSERT INTO friends VALUES (82, 60);
INSERT INTO friends VALUES (80, 1);
INSERT INTO friends VALUES (30, 2);
INSERT INTO friends VALUES (91, 31);
INSERT INTO friends VALUES (95, 53);
INSERT INTO friends VALUES (18, 96);
INSERT INTO friends VALUES (11, 24);
INSERT INTO friends VALUES (69, 11);
INSERT INTO friends VALUES (31, 78);
INSERT INTO friends VALUES (15, 23);
INSERT INTO friends VALUES (35, 88);
INSERT INTO friends VALUES (2, 45);
INSERT INTO friends VALUES (27, 66);
INSERT INTO friends VALUES (86, 71);
INSERT INTO friends VALUES (67, 6);
INSERT INTO friends VALUES (17, 89);
INSERT INTO friends VALUES (63, 99);
INSERT INTO friends VALUES (53, 22);
INSERT INTO friends VALUES (36, 51);
INSERT INTO friends VALUES (97, 63);
INSERT INTO friends VALUES (37, 91);
INSERT INTO friends VALUES (63, 91);
INSERT INTO friends VALUES (56, 45);
INSERT INTO friends VALUES (27, 70);
INSERT INTO friends VALUES (80, 35);
INSERT INTO friends VALUES (10, 80);
INSERT INTO friends VALUES (73, 4);
INSERT INTO friends VALUES (17, 28);
INSERT INTO friends VALUES (90, 98);
INSERT INTO friends VALUES (15, 33);
INSERT INTO friends VALUES (45, 38);
INSERT INTO friends VALUES (12, 98);
INSERT INTO friends VALUES (3, 77);
INSERT INTO friends VALUES (5, 96);
INSERT INTO friends VALUES (56, 12);
INSERT INTO friends VALUES (77, 33);
INSERT INTO friends VALUES (1, 9);
INSERT INTO friends VALUES (67, 87);
INSERT INTO friends VALUES (54, 56);
INSERT INTO friends VALUES (66, 15);
INSERT INTO friends VALUES (37, 40);
INSERT INTO friends VALUES (79, 73);
INSERT INTO friends VALUES (33, 38);
INSERT INTO friends VALUES (76, 16);
INSERT INTO friends VALUES (7, 45);
INSERT INTO friends VALUES (28, 48);
INSERT INTO friends VALUES (28, 89);
INSERT INTO friends VALUES (78, 52);
INSERT INTO friends VALUES (40, 9);
INSERT INTO friends VALUES (11, 20);
INSERT INTO friends VALUES (62, 86);
INSERT INTO friends VALUES (25, 51);
INSERT INTO friends VALUES (30, 64);
INSERT INTO friends VALUES (6, 7);
INSERT INTO friends VALUES (5, 92);
INSERT INTO friends VALUES (55, 25);
INSERT INTO friends VALUES (92, 79);
INSERT INTO friends VALUES (41, 37);
INSERT INTO friends VALUES (78, 48);
INSERT INTO friends VALUES (59, 46);
INSERT INTO friends VALUES (52, 10);
INSERT INTO friends VALUES (27, 51);
INSERT INTO friends VALUES (82, 26);
INSERT INTO friends VALUES (42, 30);
INSERT INTO friends VALUES (66, 2);
INSERT INTO friends VALUES (86, 17);
INSERT INTO friends VALUES (60, 40);
INSERT INTO friends VALUES (58, 31);
INSERT INTO friends VALUES (77, 53);
INSERT INTO friends VALUES (53, 50);
INSERT INTO friends VALUES (89, 4);
INSERT INTO friends VALUES (93, 29);
INSERT INTO friends VALUES (61, 34);
INSERT INTO friends VALUES (5, 56);
INSERT INTO friends VALUES (66, 58);
INSERT INTO friends VALUES (6, 35);
INSERT INTO friends VALUES (55, 32);
INSERT INTO friends VALUES (3, 67);
INSERT INTO friends VALUES (33, 52);
INSERT INTO friends VALUES (94, 39);
INSERT INTO friends VALUES (89, 2);
INSERT INTO friends VALUES (10, 87);
INSERT INTO friends VALUES (1, 13);
INSERT INTO friends VALUES (75, 92);
INSERT INTO friends VALUES (75, 56);
INSERT INTO friends VALUES (12, 23);
INSERT INTO friends VALUES (45, 52);
INSERT INTO friends VALUES (49, 53);
INSERT INTO friends VALUES (44, 49);
INSERT INTO friends VALUES (18, 90);
INSERT INTO friends VALUES (57, 1);
INSERT INTO friends VALUES (80, 3);
INSERT INTO friends VALUES (32, 47);
INSERT INTO friends VALUES (51, 41);
INSERT INTO friends VALUES (41, 61);
INSERT INTO friends VALUES (21, 67);
INSERT INTO friends VALUES (37, 36);
INSERT INTO friends VALUES (69, 18);
INSERT INTO friends VALUES (36, 84);
INSERT INTO friends VALUES (8, 27);
INSERT INTO friends VALUES (67, 30);
INSERT INTO friends VALUES (11, 4);
INSERT INTO friends VALUES (60, 8);
INSERT INTO friends VALUES (59, 77);
INSERT INTO friends VALUES (55, 86);
INSERT INTO friends VALUES (2, 27);
INSERT INTO friends VALUES (56, 100);
INSERT INTO friends VALUES (70, 50);
INSERT INTO friends VALUES (67, 5);
INSERT INTO friends VALUES (86, 44);
INSERT INTO friends VALUES (83, 88);
INSERT INTO friends VALUES (58, 92);
INSERT INTO friends VALUES (15, 93);
INSERT INTO friends VALUES (57, 82);
INSERT INTO friends VALUES (93, 55);
INSERT INTO friends VALUES (21, 8);
INSERT INTO friends VALUES (50, 39);
INSERT INTO friends VALUES (12, 26);
INSERT INTO friends VALUES (96, 41);
INSERT INTO friends VALUES (43, 57);
INSERT INTO friends VALUES (39, 89);
INSERT INTO friends VALUES (45, 71);
INSERT INTO friends VALUES (96, 47);
INSERT INTO friends VALUES (24, 96);
INSERT INTO friends VALUES (19, 29);
INSERT INTO friends VALUES (64, 22);
INSERT INTO friends VALUES (16, 48);
INSERT INTO friends VALUES (38, 98);
INSERT INTO friends VALUES (41, 80);
INSERT INTO friends VALUES (49, 59);
INSERT INTO friends VALUES (100, 11);
INSERT INTO friends VALUES (26, 8);
INSERT INTO friends VALUES (82, 90);
INSERT INTO friends VALUES (28, 51);
INSERT INTO friends VALUES (3, 20);
INSERT INTO friends VALUES (27, 65);
INSERT INTO friends VALUES (51, 42);
INSERT INTO friends VALUES (98, 31);
INSERT INTO friends VALUES (48, 10);
INSERT INTO friends VALUES (22, 81);
INSERT INTO friends VALUES (69, 58);
INSERT INTO friends VALUES (67, 26);
INSERT INTO friends VALUES (84, 98);
INSERT INTO friends VALUES (30, 72);
INSERT INTO friends VALUES (30, 15);
INSERT INTO friends VALUES (34, 40);
INSERT INTO friends VALUES (6, 33);
INSERT INTO friends VALUES (22, 45);
INSERT INTO friends VALUES (52, 73);
INSERT INTO friends VALUES (94, 99);
INSERT INTO friends VALUES (71, 41);
INSERT INTO friends VALUES (69, 45);
INSERT INTO friends VALUES (93, 94);
INSERT INTO friends VALUES (81, 45);
INSERT INTO friends VALUES (85, 84);
INSERT INTO friends VALUES (26, 58);
INSERT INTO friends VALUES (65, 82);
INSERT INTO friends VALUES (36, 98);
INSERT INTO friends VALUES (77, 22);
INSERT INTO friends VALUES (37, 64);
INSERT INTO friends VALUES (21, 23);
INSERT INTO friends VALUES (12, 58);
INSERT INTO friends VALUES (56, 80);
INSERT INTO friends VALUES (87, 55);
INSERT INTO friends VALUES (21, 28);
INSERT INTO friends VALUES (5, 79);
INSERT INTO friends VALUES (38, 24);
INSERT INTO friends VALUES (66, 5);
INSERT INTO friends VALUES (87, 91);
INSERT INTO friends VALUES (21, 88);
INSERT INTO friends VALUES (72, 41);
INSERT INTO friends VALUES (82, 92);
INSERT INTO friends VALUES (30, 45);
INSERT INTO friends VALUES (69, 66);
INSERT INTO friends VALUES (30, 29);
INSERT INTO friends VALUES (95, 87);
INSERT INTO friends VALUES (70, 26);
INSERT INTO friends VALUES (89, 83);
INSERT INTO friends VALUES (88, 86);
INSERT INTO friends VALUES (83, 87);
INSERT INTO friends VALUES (21, 49);
INSERT INTO friends VALUES (14, 92);
INSERT INTO friends VALUES (87, 16);
INSERT INTO friends VALUES (42, 14);
INSERT INTO friends VALUES (75, 84);
INSERT INTO friends VALUES (55, 75);
INSERT INTO friends VALUES (73, 28);
INSERT INTO friends VALUES (20, 65);
INSERT INTO friends VALUES (56, 39);
INSERT INTO friends VALUES (22, 49);
INSERT INTO friends VALUES (21, 54);
INSERT INTO friends VALUES (71, 19);
INSERT INTO friends VALUES (54, 50);
INSERT INTO friends VALUES (81, 24);
INSERT INTO friends VALUES (55, 3);
INSERT INTO friends VALUES (52, 81);
INSERT INTO friends VALUES (31, 25);
INSERT INTO friends VALUES (43, 93);
INSERT INTO friends VALUES (59, 9);
INSERT INTO friends VALUES (21, 24);
INSERT INTO friends VALUES (25, 60);
INSERT INTO friends VALUES (44, 72);
INSERT INTO friends VALUES (50, 72);
INSERT INTO friends VALUES (2, 10);
INSERT INTO friends VALUES (10, 54);
INSERT INTO friends VALUES (76, 61);
INSERT INTO friends VALUES (66, 4);
INSERT INTO friends VALUES (72, 21);
INSERT INTO friends VALUES (8, 30);
INSERT INTO friends VALUES (34, 1);
INSERT INTO friends VALUES (32, 80);
INSERT INTO friends VALUES (87, 18);
INSERT INTO friends VALUES (23, 55);
INSERT INTO friends VALUES (80, 56);
INSERT INTO friends VALUES (7, 87);
INSERT INTO friends VALUES (7, 63);
INSERT INTO friends VALUES (82, 36);
INSERT INTO friends VALUES (39, 72);
INSERT INTO friends VALUES (67, 20);
INSERT INTO friends VALUES (2, 54);
INSERT INTO friends VALUES (3, 78);
INSERT INTO friends VALUES (66, 40);
INSERT INTO friends VALUES (14, 45);
INSERT INTO friends VALUES (42, 69);
INSERT INTO friends VALUES (1, 87);
INSERT INTO friends VALUES (4, 62);
INSERT INTO friends VALUES (24, 10);
INSERT INTO friends VALUES (68, 10);
INSERT INTO friends VALUES (17, 47);
INSERT INTO friends VALUES (16, 32);
INSERT INTO friends VALUES (12, 13);
INSERT INTO friends VALUES (36, 55);
INSERT INTO friends VALUES (86, 35);
INSERT INTO friends VALUES (5, 7);
INSERT INTO friends VALUES (47, 35);
INSERT INTO friends VALUES (57, 21);
INSERT INTO friends VALUES (33, 6);
INSERT INTO friends VALUES (5, 4);
INSERT INTO friends VALUES (82, 15);
INSERT INTO friends VALUES (3, 24);
INSERT INTO friends VALUES (98, 24);
INSERT INTO friends VALUES (67, 89);
INSERT INTO friends VALUES (6, 55);
INSERT INTO friends VALUES (95, 58);
INSERT INTO friends VALUES (15, 74);
INSERT INTO friends VALUES (33, 13);
INSERT INTO friends VALUES (33, 57);
INSERT INTO friends VALUES (3, 29);
INSERT INTO friends VALUES (85, 32);
INSERT INTO friends VALUES (52, 51);
INSERT INTO friends VALUES (40, 10);
INSERT INTO friends VALUES (86, 13);
INSERT INTO friends VALUES (55, 98);
INSERT INTO friends VALUES (69, 47);
INSERT INTO friends VALUES (12, 16);
INSERT INTO friends VALUES (44, 5);
INSERT INTO friends VALUES (40, 98);
INSERT INTO friends VALUES (9, 58);
INSERT INTO friends VALUES (85, 4);
INSERT INTO friends VALUES (99, 31);
INSERT INTO friends VALUES (84, 65);
INSERT INTO friends VALUES (59, 79);
INSERT INTO friends VALUES (41, 26);
INSERT INTO friends VALUES (20, 77);
INSERT INTO friends VALUES (23, 21);
INSERT INTO friends VALUES (93, 72);
INSERT INTO friends VALUES (7, 64);
INSERT INTO friends VALUES (22, 90);
INSERT INTO friends VALUES (7, 39);
INSERT INTO friends VALUES (50, 79);
INSERT INTO friends VALUES (94, 36);
INSERT INTO friends VALUES (35, 84);
INSERT INTO friends VALUES (38, 30);
INSERT INTO friends VALUES (58, 59);
INSERT INTO friends VALUES (29, 100);
INSERT INTO friends VALUES (78, 10);
INSERT INTO friends VALUES (15, 67);
INSERT INTO friends VALUES (24, 21);
INSERT INTO friends VALUES (83, 79);
INSERT INTO friends VALUES (42, 50);
INSERT INTO friends VALUES (86, 9);
INSERT INTO friends VALUES (31, 11);
INSERT INTO friends VALUES (100, 10);
INSERT INTO friends VALUES (59, 75);
INSERT INTO friends VALUES (61, 55);
INSERT INTO friends VALUES (16, 49);
INSERT INTO friends VALUES (7, 59);
INSERT INTO friends VALUES (57, 18);
INSERT INTO friends VALUES (53, 57);


INSERT INTO love_matches VALUES (49, 7);
INSERT INTO love_matches VALUES (58, 91);
INSERT INTO love_matches VALUES (86, 6);
INSERT INTO love_matches VALUES (55, 24);
INSERT INTO love_matches VALUES (91, 33);
INSERT INTO love_matches VALUES (32, 43);
INSERT INTO love_matches VALUES (21, 6);
INSERT INTO love_matches VALUES (9, 6);
INSERT INTO love_matches VALUES (52, 21);
INSERT INTO love_matches VALUES (89, 21);
INSERT INTO love_matches VALUES (27, 89);
INSERT INTO love_matches VALUES (29, 47);
INSERT INTO love_matches VALUES (14, 75);
INSERT INTO love_matches VALUES (25, 1);
INSERT INTO love_matches VALUES (74, 37);
INSERT INTO love_matches VALUES (9, 23);
INSERT INTO love_matches VALUES (30, 96);
INSERT INTO love_matches VALUES (84, 86);
INSERT INTO love_matches VALUES (73, 75);
INSERT INTO love_matches VALUES (16, 70);
INSERT INTO love_matches VALUES (52, 89);
INSERT INTO love_matches VALUES (82, 20);
INSERT INTO love_matches VALUES (40, 81);
INSERT INTO love_matches VALUES (33, 72);
INSERT INTO love_matches VALUES (73, 24);
INSERT INTO love_matches VALUES (13, 4);
INSERT INTO love_matches VALUES (88, 47);
INSERT INTO love_matches VALUES (73, 56);
INSERT INTO love_matches VALUES (21, 49);
INSERT INTO love_matches VALUES (29, 23);
INSERT INTO love_matches VALUES (49, 1);
INSERT INTO love_matches VALUES (84, 1);
INSERT INTO love_matches VALUES (65, 68);
INSERT INTO love_matches VALUES (7, 73);
INSERT INTO love_matches VALUES (33, 84);
INSERT INTO love_matches VALUES (28, 36);
INSERT INTO love_matches VALUES (67, 69);
INSERT INTO love_matches VALUES (2, 6);
INSERT INTO love_matches VALUES (13, 78);
INSERT INTO love_matches VALUES (47, 69);
INSERT INTO love_matches VALUES (53, 8);
INSERT INTO love_matches VALUES (68, 64);
INSERT INTO love_matches VALUES (40, 3);
INSERT INTO love_matches VALUES (5, 98);
INSERT INTO love_matches VALUES (30, 52);
INSERT INTO love_matches VALUES (75, 38);
INSERT INTO love_matches VALUES (97, 52);
INSERT INTO love_matches VALUES (11, 44);
INSERT INTO love_matches VALUES (17, 79);
INSERT INTO love_matches VALUES (94, 16);
INSERT INTO love_matches VALUES (74, 3);
INSERT INTO love_matches VALUES (34, 83);
INSERT INTO love_matches VALUES (20, 31);
INSERT INTO love_matches VALUES (73, 52);
INSERT INTO love_matches VALUES (85, 68);
INSERT INTO love_matches VALUES (83, 90);
INSERT INTO love_matches VALUES (35, 23);
INSERT INTO love_matches VALUES (92, 29);
INSERT INTO love_matches VALUES (7, 10);
INSERT INTO love_matches VALUES (79, 81);
INSERT INTO love_matches VALUES (80, 13);
INSERT INTO love_matches VALUES (94, 37);
INSERT INTO love_matches VALUES (54, 16);
INSERT INTO love_matches VALUES (9, 69);
INSERT INTO love_matches VALUES (57, 24);
INSERT INTO love_matches VALUES (4, 56);
INSERT INTO love_matches VALUES (85, 19);
INSERT INTO love_matches VALUES (50, 22);
INSERT INTO love_matches VALUES (55, 23);
INSERT INTO love_matches VALUES (25, 36);
INSERT INTO love_matches VALUES (37, 81);
INSERT INTO love_matches VALUES (91, 28);
INSERT INTO love_matches VALUES (70, 59);
INSERT INTO love_matches VALUES (19, 100);
INSERT INTO love_matches VALUES (96, 37);
INSERT INTO love_matches VALUES (22, 68);
INSERT INTO love_matches VALUES (65, 50);
INSERT INTO love_matches VALUES (63, 36);
INSERT INTO love_matches VALUES (93, 82);
INSERT INTO love_matches VALUES (11, 95);
INSERT INTO love_matches VALUES (65, 36);
INSERT INTO love_matches VALUES (94, 8);
INSERT INTO love_matches VALUES (38, 29);
INSERT INTO love_matches VALUES (60, 13);
INSERT INTO love_matches VALUES (71, 84);
INSERT INTO love_matches VALUES (31, 64);
INSERT INTO love_matches VALUES (79, 16);
INSERT INTO love_matches VALUES (43, 75);
INSERT INTO love_matches VALUES (14, 43);
INSERT INTO love_matches VALUES (76, 60);
INSERT INTO love_matches VALUES (93, 63);
INSERT INTO love_matches VALUES (28, 72);
INSERT INTO love_matches VALUES (41, 68);
INSERT INTO love_matches VALUES (1, 79);
INSERT INTO love_matches VALUES (15, 3);
INSERT INTO love_matches VALUES (76, 41);
INSERT INTO love_matches VALUES (28, 88);
INSERT INTO love_matches VALUES (15, 67);
INSERT INTO love_matches VALUES (89, 49);
INSERT INTO love_matches VALUES (19, 73);
INSERT INTO love_matches VALUES (25, 38);
INSERT INTO love_matches VALUES (9, 67);
INSERT INTO love_matches VALUES (56, 32);
INSERT INTO love_matches VALUES (30, 28);
INSERT INTO love_matches VALUES (35, 44);
INSERT INTO love_matches VALUES (87, 64);
INSERT INTO love_matches VALUES (73, 87);
INSERT INTO love_matches VALUES (63, 57);
INSERT INTO love_matches VALUES (19, 65);
INSERT INTO love_matches VALUES (76, 35);
INSERT INTO love_matches VALUES (58, 99);
INSERT INTO love_matches VALUES (79, 29);
INSERT INTO love_matches VALUES (41, 92);
INSERT INTO love_matches VALUES (13, 3);
INSERT INTO love_matches VALUES (76, 6);
INSERT INTO love_matches VALUES (53, 66);
INSERT INTO love_matches VALUES (34, 2);
INSERT INTO love_matches VALUES (24, 3);
INSERT INTO love_matches VALUES (47, 55);
INSERT INTO love_matches VALUES (64, 84);
INSERT INTO love_matches VALUES (5, 67);
INSERT INTO love_matches VALUES (73, 46);
INSERT INTO love_matches VALUES (88, 16);
INSERT INTO love_matches VALUES (62, 40);
INSERT INTO love_matches VALUES (68, 25);
INSERT INTO love_matches VALUES (98, 100);
INSERT INTO love_matches VALUES (89, 50);
INSERT INTO love_matches VALUES (18, 81);
INSERT INTO love_matches VALUES (6, 99);
INSERT INTO love_matches VALUES (30, 62);
INSERT INTO love_matches VALUES (19, 41);
INSERT INTO love_matches VALUES (59, 9);
INSERT INTO love_matches VALUES (70, 79);
INSERT INTO love_matches VALUES (85, 56);
INSERT INTO love_matches VALUES (60, 74);
INSERT INTO love_matches VALUES (78, 43);
INSERT INTO love_matches VALUES (8, 58);
INSERT INTO love_matches VALUES (46, 17);
INSERT INTO love_matches VALUES (76, 74);
INSERT INTO love_matches VALUES (31, 89);
INSERT INTO love_matches VALUES (15, 21);
INSERT INTO love_matches VALUES (26, 97);
INSERT INTO love_matches VALUES (11, 3);
INSERT INTO love_matches VALUES (10, 23);
INSERT INTO love_matches VALUES (33, 98);
INSERT INTO love_matches VALUES (3, 68);
INSERT INTO love_matches VALUES (23, 11);
INSERT INTO love_matches VALUES (2, 72);
INSERT INTO love_matches VALUES (66, 64);
INSERT INTO love_matches VALUES (46, 37);
INSERT INTO love_matches VALUES (40, 42);
INSERT INTO love_matches VALUES (31, 1);
INSERT INTO love_matches VALUES (18, 61);
INSERT INTO love_matches VALUES (57, 100);
INSERT INTO love_matches VALUES (47, 23);
INSERT INTO love_matches VALUES (46, 18);
INSERT INTO love_matches VALUES (62, 75);
INSERT INTO love_matches VALUES (58, 68);
INSERT INTO love_matches VALUES (93, 81);
INSERT INTO love_matches VALUES (42, 19);
INSERT INTO love_matches VALUES (65, 21);
INSERT INTO love_matches VALUES (73, 84);
INSERT INTO love_matches VALUES (18, 74);
INSERT INTO love_matches VALUES (9, 38);
INSERT INTO love_matches VALUES (8, 85);
INSERT INTO love_matches VALUES (61, 75);
INSERT INTO love_matches VALUES (75, 68);
INSERT INTO love_matches VALUES (72, 20);
INSERT INTO love_matches VALUES (93, 44);
INSERT INTO love_matches VALUES (34, 45);
INSERT INTO love_matches VALUES (57, 15);
INSERT INTO love_matches VALUES (3, 66);
INSERT INTO love_matches VALUES (53, 11);
INSERT INTO love_matches VALUES (97, 18);
INSERT INTO love_matches VALUES (26, 82);
INSERT INTO love_matches VALUES (58, 30);
INSERT INTO love_matches VALUES (44, 96);
INSERT INTO love_matches VALUES (39, 45);
INSERT INTO love_matches VALUES (16, 88);
INSERT INTO love_matches VALUES (46, 74);
INSERT INTO love_matches VALUES (17, 63);
INSERT INTO love_matches VALUES (34, 56);
INSERT INTO love_matches VALUES (24, 40);
INSERT INTO love_matches VALUES (37, 77);
INSERT INTO love_matches VALUES (72, 4);
INSERT INTO love_matches VALUES (92, 75);
INSERT INTO love_matches VALUES (77, 85);
INSERT INTO love_matches VALUES (88, 7);
INSERT INTO love_matches VALUES (92, 8);
INSERT INTO love_matches VALUES (29, 93);
INSERT INTO love_matches VALUES (85, 55);
INSERT INTO love_matches VALUES (15, 37);
INSERT INTO love_matches VALUES (62, 47);
INSERT INTO love_matches VALUES (72, 92);
INSERT INTO love_matches VALUES (71, 36);
INSERT INTO love_matches VALUES (83, 31);
INSERT INTO love_matches VALUES (62, 63);
INSERT INTO love_matches VALUES (99, 28);
INSERT INTO love_matches VALUES (85, 26);
INSERT INTO love_matches VALUES (37, 75);

INSERT INTO photos VALUES (1, 'Ln06QO5HwaLVcR5cBh5AXjskAUPyECwWITeBjLEXzuvA4eKS2qEJmnglYuO17fndPkTjE1JZLHvwShWz5r1ZWAfVDSo0GZAw4tpGoUXu4wVdIHnQwKht3', 'Dancing at the club woooow', 'www.zoektaal.co.uk', 'GLOBAL', 2);
INSERT INTO photos VALUES (2, '7sZVVLPmEOvthSK2PC3ARrnmFujQ3YVZBRZu40gtKz8rhYSQulpCaW44gzTcOTjs7QoDXTwZ23h6sFbGyhYI03c6ZRMdGvLmSdUNRBVulwDmwg5xjCFOGT5jBwII26frSWitJkZTbmFQJh8niOCvdOljgR3c2MSNTcHPEDn1', 'Just hangin out', 'www.zoekenweb.org', 'SELF', 10);
INSERT INTO photos VALUES (3, 'ZbYLv6rzc6hQASOAURx0SMTqeu7fT8op36y0nwTtzyUBJmXmFBhfUDRZ1Za5YtZ1hl1u7Cqjb2g3lmXYYEHgGutNf0nAoS2JzYl1eadEp1NvO35ofjRKMNZtqWc8ddU0gAfMVC4gc4JDTxvrQ2qJg5skfMFJnDviJQ55M0fpXbf07jMUb3wTlglx5V2qfVZV8BgbrjxB', 'Me and my friends!!', 'www.schoenwerk.org', 'GLOBAL', 10);
INSERT INTO photos VALUES (4, '2NCK2aaezQDe8KVF7blDJIKNKOE48KYMv78diMulOjxD3Ah8yKMmlB8NgMH6ctVBCDVyE5S8wgRQ4XGGxh6XZ2rE1AUn', 'Dancing at the club woooow', 'www.zomaster.ch', 'GLOBAL', 10);
INSERT INTO photos VALUES (5, 'L1bwrFRB1Gawzcg4hd4nBuokPBfqs4wBeScliaF4m3K3fgKQbN2kl6Sbf1FErYwgMMAXC3xK8g0JXw1t6RnMJg7kERhNUULa4BQcFcXN4I4jkZG5fUoXvxdjCkQYTJPtbEmrmxsLc04rKjQcG75XNrhQWazmbPmGHbgN0pZFdcI7Tmt2pW3AFFwDSiJ', 'Dancing at the club woooow', 'www.contactpagina.be', 'GLOBAL', 17);
INSERT INTO photos VALUES (6, 'QPOllfWL00Q1flqWGcRdguiGnMrpWlDlfXfgsrQwsOuxgSX6uwc8BZxB7mSM6wHg5liWP1nSMckveYF8cNnSgUAWA8vmbR2ZmdOffdxld3FiN2RmpEYRyuv3kKc3UWz81bPG7hZWTxuiEMrdHPm0wCJiqo0XB0ef3L8HM6Dq2sMamFfcRCY03PrOAxRKhMoZTycw86UTbTxQBVBwTGwvHhUz', 'Just hangin out', 'www.compatick.es', 'FRIENDS', 17);
INSERT INTO photos VALUES (7, 'ejelKPTt3KlvmqJvrK4pRgGwimHWyo3ml8i3Q0tvjCJVDaXG0Q5BUFT2sGKcVZMPE6TyKUvHatBMYb2jegSJhcGZccVOlIGotMV3kr5Ndno7Z68StTsta2f73VMW0HOVrgQjqO4hMeE7Dn8a1OJq7TNYqZqZvYJzCJI2M456EXM7aQMOLJ6cQoP4ZmKEvDkD6Liz7HiTxLAn66RmlpU0IEoltRH7wXYDJBB5DtorAPkrgu3RDiZrnNGcNr', 'Dancing at the club woooow', 'www.whitemaster.org', 'FRIENDS', 17);
INSERT INTO photos VALUES (8, 'i7jtoxlmCw2dyOdXKxglMvZKWmkh45xZhQmcMHbCpZRCeP0ok0mF1pRW8LmoPImkfdVjm6HHN6pUCTNziJElEsUCgIc', 'Just me', 'www.coolraak.es', 'GLOBAL', 26);
INSERT INTO photos VALUES (9, 'F1OmCLQxxLzPGrRMWzAs1Eug4Y4EgH8DCAxMAGPXjPyMR6eZUozoeX7NRrQOvlrg8cvhswciEBUMFoImPsXGJ34qI55pOcgKRTWiPOhTfsYhYHkGvIfP0fboZbYeVdQNex1ofGr8330IfCgZdsQ7VF3O', 'Just hangin out', NULL, 'SELF', 35);
INSERT INTO photos VALUES (10, 'f0wecjVzHK6Oz6VNuR67SssZxFo7e1IJ3GvFeXndlV2iFWrlhspHbM8XFV1U5lgNEYOiPybSjbz4RBG0CJB40MTIrbVIV8jVPlZlerEEoD2ZTU', 'Just me', 'www.rocktick.nl', 'FRIENDS', 35);
INSERT INTO photos VALUES (11, 'xF700gTSVJGcI36HJxFKvJg3ST2rFySJEp8DipNOVCNOgAzjWEVNAchqQPpaNXcFp26aJkOetmMzFMOvm', 'Just hangin out', 'www.magictarget.com', 'FRIENDS', 35);
INSERT INTO photos VALUES (12, 'KfPEELoek04Cqn8gpgYarPBSIB88XwbIhGKYlucrb4bFteoCGfAVIKKnoDTdIAPjHBRYhxVQ4AwGDt66KJEyRZmWg65jrovyFGvIfGdBhRbhgtK2lkSs55IC', 'Me and my friends!!', 'www.informatievacature.ch', 'FRIENDS', 44);
INSERT INTO photos VALUES (13, 'kl7NOyxm84VAnURl7B3qqLchB4Ud67xiZwo8sUNWvjFuVGRXrhfu66iCVsGsMyehLIlBAK8Vt6TeE0Jm0bDNBE563FVrnvssXl2cAUZ2JZQCJIDH2GXghOBIE063SWyWNjhajgFnNQ7GP84Gaia1j3zPMmDBpKhkTwAj5b1pr4q86kvfjchHeRXgvqACUnhSBJwrf0jfqfCqmx32fOxytPw4WmiV7D1yxePE82zNxv63v0S4DYA7i0', 'Me and my cat', 'www.magicpeople.es', 'LOVE_MATCHES', 54);
INSERT INTO photos VALUES (14, 'sciNceoQGYS0', 'Dancing at the club woooow', 'www.restaurantinfo.be', 'SELF', 56);
INSERT INTO photos VALUES (15, '07BpPDNtFU4RW6LaEKHMs57TRTHP66PrWp5n3pIPqlJ5guVRKUtHUhdfYeru2rN', 'Dancing at the club woooow', 'www.autotrace.be', 'SELF', 56);
INSERT INTO photos VALUES (16, 'sbViLr8ET34UvMXtHm4cfZHZLl4zrLxuuhaxRvC5uLsXvXEZZ53KOo5mNmIR05mMndkebTtHRYCYfkjfiwWBbSEsXDJkrprZ37KbfL7mofvYYBedvKLMEDjHT8vfwLNdvmJtzp7p4m0I4oSdNfPYgT0ZkoNVgnlxkPflDVrowblvgWXRJ0PeDQdOKmRPhk7rwS5NGPG2377CjzfrqowpfIKTJZ7uH2k8CNg0RlHfQLnP5HGZAG', 'Dancing at the club woooow', NULL, 'SELF', 56);
INSERT INTO photos VALUES (17, 'fhEZp4hoGAUFssCGQaXBhAj4LuodCFqsrXSDeFZxRrpFPrCfJrcqnJlXqWpFptdv8fwk30woexlO1kkRZReC7Jdcr17eysItfu4daawGSpRjkoLChl6sjTddvgZOckRNWa1PTdZcmqqLHEtbbDP8X6AfbpEIQdwuCbCsZSztDoeiGS0WLMKUaqVZTe6WUlZgBeBW7r2ynPILYngqxAWNmI', 'Me and my friends!!', 'www.gomars.it', 'FRIENDS', 65);
INSERT INTO photos VALUES (18, 'IFIEmdZwVuv6gd26LVkYSYEnWwGPt3t2RwM5OZs4MkibI3pp0F82e0oiMLFQmKLIOg1gymFdIQjJM4MkpPG2hI3WTA5EAdLxsSdcN2AHMSiCgpQ5EzZr6', 'Me and my cat', 'www.nedvak.gr', 'GLOBAL', 65);
INSERT INTO photos VALUES (19, 'zOeaLF416AISKCPYe5FuNqDt17vaEa26ElcMEmgTxlnLlfhfT7Z1cOWZUtFnsReGFPM458dsBQh8', 'Just hangin out', NULL, 'SELF', 66);
INSERT INTO photos VALUES (20, 'Y8mVqkI04jU41hkA7FOgnSpVWixxbQO1ioCTmf6mwXXCohbauF7zwHajX0NrBTDMDQTKRzN5oB5uJ1WGTeyJ3WJVc05DwtfY6R01ATgF1l2MIrRejnifUfUmCKioMa1b6lWKB3h0gypC0meNWKYeeTIh6wAUhUSYcnukM2qqS6uW3loi7', 'Loved going out with you, guys!!', 'www.voedingwereld.be', 'LOVE_MATCHES', 66);
INSERT INTO photos VALUES (21, 'kzYcA8IZK4888SIXDNlpLxSDSUCbOOvx7rJnYyLDGjUh7seMMdOoRZPoRbFGx', 'Loved going out with you, guys!!', 'www.zieraak.gr', 'GLOBAL', 71);
INSERT INTO photos VALUES (22, 'QtV8hC3oQg1lP650t4tHKDNDCzjee3jkmjbKfP5URmhnvxw04dnG1574UHtY0dywX8fnyLWwl4RSWQFStybZy7JZ11TZe4rPIEKicBGRDlYE8M3GXLcx8G6XTsuEULNtKxPdgIX1P8rxhVTapC1gNoZHWCffW7OsiZCcU6qdHmIEGhP1lcBURHwcdV6vwL5fYjYOJdSPB6jMX1Mui7jGE0N', 'Me and my cat', 'www.inktheek.be', 'FRIENDS', 72);
INSERT INTO photos VALUES (23, '2zdMF1sh4abjeELWFDn7voUBETTCR48Do45J1elLSeZwBdf8NUBNawyUazboiEak2vCGHQghgnW76jDvm0lWbYQCmBQYIUiCO4rZexQx8I1k6mOtVNF5cHZ5i3pMzvq', 'Me and my friends!!', 'www.drinktargets.de', 'SELF', 81);
INSERT INTO photos VALUES (24, 'Ip', 'Me and my friends!!', 'www.restaurantplaats.es', 'GLOBAL', 81);
INSERT INTO photos VALUES (25, 'eAScBol3sLZA6E4BPbqS3bdxt4IE2ODEi2mPPkQ1VZ7aJKjSOvEMkpofYcAIE6ptdaa2MzUwwtMGn5OQa2MC5zJTaP2EhyGC4PeVI8AsvyGYVCmjD5YdzNqD2OZJdPD5ATp2t7bwE1lBIvLbVZ1edO56SwwvaUNzpWG0p1mwuhtasfDmaJvgVmpSQxri1Ez3sdZW5k7RfK1BobIx3mnNJsyODDLg5vvi1LSR6', 'Just hangin out', NULL, 'FRIENDS', 86);
INSERT INTO photos VALUES (26, 'YLkanBKm50D3DOHW', 'Me and my cat', 'www.contactwerk.de', 'SELF', 86);
INSERT INTO photos VALUES (27, '1oZFsjxOFp0QgkA4CGWPQCqubuhLQv7TDVLpsZeXoUEICnlcS1xBW4BiB3R64BD8VStqaVMSIN48l1hfBD2y4NqWvWpACd5pYKokPeZI55YUbttrCEKF', 'Just me', 'www.masterinform.it', 'FRIENDS', 86);
INSERT INTO photos VALUES (28, '871lwPg3KjTUqS6p66PcrxlYDVXl8wg52aDHSrSTHbTewIQoksNHdUpEOaFlt0UJsYPSGWERbm0rV', 'Me and my friends!!', 'www.greensite.com', 'FRIENDS', 94);
INSERT INTO photos VALUES (29, 'en5SYlu76NxxmDb6wSOWuXfTDBvHF4dxOPAyQpbDQpDGme4ft6aZd3LJqpCvQrjnucVdiDfwyBqO5UYlxMKw1CesYUE5Pd1pTVxCycrioCLgnX7fkTCddOhnZNhEKvj2cNpvQYacrvLyiYY7a8i3L5vjPhSsQAoVesldK1VPAdYKUFXW8hF', 'Me and my cat', 'www.autotheek.de', 'LOVE_MATCHES', 94);
INSERT INTO photos VALUES (30, 'PmqI8KQqfwCqoVgk4erJOYqnwkbBLBypjzM3Dh8Sy7oCB3V2LrCZokBUfwXAnkIToMbyLceDoJW8BYECxw2F5AebImWdNTzLhG8zxQGJQNNn0ndramUSLU5bpSrisShY1YisbKZOKY1HyGkEMH5ss3Uo3hVGtoIsEuzLBi7', 'Me and my friends!!', 'www.bluemace.com', 'FRIENDS', 94);
INSERT INTO photos VALUES (31, 'msxWz6xHncUnuPXJKLfpzTQ2PNVAM26f4g0WvFauwXphUfTPGYPxCgi07HjWYqEjHsVmrBOxeuxhq7Axl', 'Dancing at the club woooow', 'www.restaurantvacatures.gr', 'SELF', 100);
INSERT INTO photos VALUES (32, 'nInze6TF1Y4bKSeX6mjATY', 'Me and my friends!!', 'www.zoektaal.gr', 'FRIENDS', 100);
INSERT INTO photos VALUES (33, 'N8qrE5QZV8RhRzWw6LnO8k0V1y3qW6spcfbry7LS4b5d1CYL55jpE', 'Just hangin out', 'www.choicetools.it', 'FRIENDS', 100);
INSERT INTO photos VALUES (34, 'EAl0IfmUnEB3nqDE6VlZKBXTsg5ztHNSxzUZgB3SbOswGPgPB4R0NM0zxYwy0S0RFG1ruwgYmgkHZPuRyQuEOZBF7DJfJMf1jVIAEk34frhOzeKbhSsRCY1AgwHdQJBhNL01oAm7N21Tqe2ROF0ECJsCTYcXqI5qJhjrWPygE6SOWX42rRQLmmVjQdHVl0lYewbrkbpLJ', 'Just hangin out', 'www.downloadactive.org', 'FRIENDS', 10);
INSERT INTO photos VALUES (35, 'jtbDI5OcmjsqrCU5EwXL6lkkkmEE6RLrVD3RAdbD50oT3PrMAUeMMcSxyzsmDjfnHEMyMGXE2emFk4', 'Just hangin out', NULL, 'LOVE_MATCHES', 10);
INSERT INTO photos VALUES (36, 'ovVszDhViiG8ZptUDeueon7CVZcqI2LwUoDbfV2PNV6EmxXfkNXzkGito', 'Loved going out with you, guys!!', 'www.restaurantmarkt.it', 'LOVE_MATCHES', 19);
INSERT INTO photos VALUES (37, 'BUphm58vzsejTQBILkuE5vHwyWQ5gw6GSZPNusZH50OVNgkJCPfxmTqKvDdBr7tBpKHknbbFEqzjaWn2h3ijUSdl3wA7N5RXuPSYibK8ByMxwmdjdcA8x3RLXZWGekWSnMhStp1vGcBlbrFm3zeQ4PW1LLCBL7u6fct0uZmnQ2t', 'Me and my friends!!', 'www.nedbode.ch', 'FRIENDS', 27);
INSERT INTO photos VALUES (38, 'qyOnLZemURBqfsXehjDvFSZlMGDdyXzVICSizFHptTST6bqZ0CAmxYnWLOiLveFlJlC733VRj6lGxeTiMFXxUekkKVj2rvT', 'Just me', NULL, 'GLOBAL', 27);
INSERT INTO photos VALUES (39, '0ZyyYbPjCYfceOqODHAjmAK3chcNpp3in24wBk4zx6A0QQxvI7J16XFxqkhCNVTPqghXYvTZoWYUd6UUpRecTaLxq26ZfZ3GMUtifFFjfy4ylK1lgymFsvruVuqkI120MlUHtKGy3cWQHvr6bYVQGEPlrjAoqoOGL52JZHJ0C8xKzFiG2zkryao', 'Just me', 'www.zietrace.it', 'FRIENDS', 32);
INSERT INTO photos VALUES (40, 'GPunzXFHiEfdbUO20yPaLT5pIj6zgvFKRpPK4NwKsvTcEiCgxSXtqh6Jw3WGvJIrjlT3nJ7h3fjZ0oxOCvtYQ5AasdbasyQZL03Jsy55mNEESdhhewJLw7BSeYIirAUjbBAlvS1KyFgU0zRRNcRWcG5k223MnKe7OkeRUCyKJX2qB45aSzw5PaMZm7GFHjt1EImT36mnl48qZCTwC8A7WSBMAyd1YULBy7ApjQiWlTkPwu', 'Dancing at the club woooow', 'www.koopinform.it', 'LOVE_MATCHES', 32);
INSERT INTO photos VALUES (41, 'AFXpo2YutnhoaK7v41mRK2yEtBJTRx8wvYYCheQwt6FJmq6N81LOmeENA5jd0CeDVr1pIZtJJFZRccLX67AqXaZOsG0xnwEb7aysgbl6NzMWFSbq24ZClWAmRT5K5m4MEqNvLvi7xGiKJiAlxXdQBeCUtLNf5BDmh7wNDpIxetIUSCDjhVvsfpomRnD5DAcQNZ6vXsKwHZ3hs6kniA23L8Oedxcu2GEyykhQo3Vm8SMQ5O8', 'Just me', 'www.elkeschool.fr', 'GLOBAL', 39);
INSERT INTO photos VALUES (42, 'iCqxW3RmBMt7NuhDQBZteLLDwU2HLGekuUsk1dSSX88KQ7KfFu7TkenmuHtEiUgbnegXvEv74kO6fNlxUNtMVtdm1jrwf3Zl2S5W2vTdlGXILd7Tt4onYOuxdF3OztQhgxdV5aGTpvfUTGwdtpbf4C7zgRzWJmPENpt3YJQRw', 'Dancing at the club woooow', 'www.goodoffice.nl', 'GLOBAL', 41);
INSERT INTO photos VALUES (43, 'qeDI7mCFjHbNyMNZDyvhhKmFCyDP2bzusvUWbQN0NAYghg5IRJaQx', 'Me and my friends!!', 'www.anythings.fr', 'FRIENDS', 42);
INSERT INTO photos VALUES (44, 'K35UgrgkAyBmhwJ46J2JdLpqKwDqxx7qMqlhz5rpikHCHnLwPdLrJ1MSbYnU0jKUmqY08RcKxBObXvOzpGZq2Hg5I0ex4GGfYppiEI3SCh0ry0sKSbtNJREOxYpUOD6DcbxbMEKNDmqZHZn35Wkm2u6xYiWvfkRajcFPtqtSZxnW30KTcOTgaBcWWOAuXK5tCmddrQYSOfYgP7Q1pfcxykAEf1lob2ZvYSHzI', 'Loved going out with you, guys!!', 'www.gezondheidplaats.es', 'LOVE_MATCHES', 42);
INSERT INTO photos VALUES (45, '0FfVOqluK1gEvh3KVu5gJdPdyfxQJFVjsaS0QyxIJG5UEf4Xc7Y1pWgIr36SZFoNAS3umNfJzHyv14N4QuSRiSuevUUCFjGMKxBnZ2zFy3OOEijq08ShEnO8Y8xSSw0rrEMCmRUQqFTbzAwan1HICqPDEn5mtEkAo3vvtxb7RbnJKp48VN0BldJeAsjoHiI7QzOTiK6rm4iwoA2WLA8Jwf', 'Just me', 'www.kledinginfo.org', 'SELF', 46);
INSERT INTO photos VALUES (46, 'zoEAAx4vizBUkZ40MXX4DBxibM', 'Loved going out with you, guys!!', 'www.informatiemarkt.co.uk', 'SELF', 46);
INSERT INTO photos VALUES (47, '2hzTTYm25weQy8KGGWTkNQy0HwJd3svRYaX8U6Ygx5TUI82vejJmn0aUDChAm3yJNfyinrodib72Yc1XrPeYE7jABMasy4AHamdJutn3Ve6mz8iQHL8QtfUty8lFwttJJ2Krao5', 'Just hangin out', NULL, 'LOVE_MATCHES', 47);
INSERT INTO photos VALUES (48, 'gqBZoXRV3YdHHJSbSDZb41rZzBckdROqC4q2QhIBzjIo6DVQdvSw6LtUYRFHcee4Jp7SceqMIk1xOIMTMz0', 'Me and my friends!!', 'www.digivacatures.be', 'GLOBAL', 49);
INSERT INTO photos VALUES (49, 'pbGE6vsV', 'Loved going out with you, guys!!', 'www.koopactive.co.uk', 'LOVE_MATCHES', 49);
INSERT INTO photos VALUES (50, 'oRq8WBSv6pRiDlJdxI56OHZCRi5bhtqBhQmQLIzzMU60bN1nlYUQoyjEMobKW01y6Wzbe4TvQIkfYvb08klUBAYs5aFgvRf7IjxQgUiDgoeABpcQY0azD5ic2dcOXyPbTGdPuC7bEXmjTKsPdkpdFFYOUJZoNtZ0RmLsmVFeebabhBGESAxqM87PvDAxft4DnzXw1TPoAUD8fRN0W025oqS', 'Me and my friends!!', 'www.etenbode.com', 'SELF', 49);
INSERT INTO photos VALUES (51, 'YzflgiOebWsLe7eypHirSmYzIgcBQ2iOqI4xnBQuAD14vOrfmQTpwUy2HMHDTXB8gOkwQExKZ1FzJEbgxrySIkt14ZI3TuBv7ypYGDfA6SwISFuOhuq16nOKJqYlj0iRA8QyNCNxEqiNHRP4oznY', 'Loved going out with you, guys!!', 'www.choicetarget.org', 'FRIENDS', 54);
INSERT INTO photos VALUES (52, '7RsmqiqxHcJ0T7g0RxSGmsRTvHkkOuZO3NnDCqLGwMoZjvAeHd5kdOuZKVH0vXUDJlPaHh1i42T3w74Bcf8O8RD1FGZ8GydzbwHLGWL7', 'Me and my friends!!', 'www.maximaalpagina.co.uk', 'LOVE_MATCHES', 64);
INSERT INTO photos VALUES (53, 'xr4CXni0SdSFeNlWzhGh6WxecISUGbypiUhmFBMXPX0PdVqxZNGnDCXb2rhk27Nos6dQXThOeDAFJlcV0VFJdtYBWMY1JLxc8Yw3wMSlOW', 'Loved going out with you, guys!!', NULL, 'FRIENDS', 64);
INSERT INTO photos VALUES (54, 'WjaHDrYWTYnKSpLDpaFO6rLp210SL6mGHY0dSQB6zYhufURs8h0JkuUZ24LsrOp5JALzG2JPvHHsSwnz4SeKznIByws4K8xHv6hiVOdlMHRMTgZmdOdOvbsONRxlm3Wg4xT8yFZqDePGokZlnCqRLUcioRWYEE2QXCfWDnuPWut3z6RkxfXBZmgP0r4QDR4UAKCgYgRMXesIzO3yygnsBJY7PqkFdkLhn0sl0OWIXNjl5', 'Dancing at the club woooow', 'www.kledingplaza.gr', 'LOVE_MATCHES', 66);
INSERT INTO photos VALUES (55, 'zGqgW8MvXmjuVC36mRzNIYJsmCBcWEWHIfIABcFVJixNyQ0m', 'Just me', 'www.masterhelp.it', 'GLOBAL', 74);
INSERT INTO photos VALUES (56, 'FZU1AVPo5xQyBNuzXlL8uUxCByIvNflWPdr2ULhycUKP7dOwAy', 'Loved going out with you, guys!!', 'www.cooltarget.de', 'FRIENDS', 74);
INSERT INTO photos VALUES (57, 'S1y3h6I3F4ktbRLWXJmOwIsmjWOLW1HBZZxM8n0SiW8VLT2RbM6n5QholCYjbL8f034fWKuZE4MjwyEor38osYNZ5VgnMkxtOXBp8ign5Buh382ywzUei2gnXLVBf3x0', 'Me and my friends!!', 'www.startplaza.de', 'FRIENDS', 84);
INSERT INTO photos VALUES (58, 'p2wGRpawyzI5a0UMzA1KWdHBpYrnYKo2nL5ccjiW0eJjFIgnthyP62NnbAOqsKzuvJaymYfAYakX8IrJzFop5BwTEYafC2YNDifDRoW4bOES1UvwQrdMvwbygMTP0I8S2JYIuYIWx60dl0XJ3iEM8KZJiqS1cPae0', 'Me and my cat', 'www.traininfo.ch', 'FRIENDS', 85);
INSERT INTO photos VALUES (59, 'hOYwrg2etbdDxyctsRIJJvpxRGGBFMg4KcuaQrN7h78kUZu10BDmaxnRn4132eruCS4bdrZz0wGFvdcWTgHbuKOoUQLNRACE7G0tZqMqoA8xndoK67pMoPlYg6jC8jS', 'Dancing at the club woooow', NULL, 'SELF', 85);
INSERT INTO photos VALUES (60, 'MWZC8tSMJKez8bOsh3tHZFeZKUnFF572RZdTpECkC3SKia3Ukyno5GtElJ4MPeYqqwvQzNGEcyv7x8sPCv0Ya7l70Axu3MxjFZccjEFKN3cJcX5vB', 'Loved going out with you, guys!!', 'www.perfecthouse.es', 'GLOBAL', 85);
INSERT INTO photos VALUES (61, '3JpzanCBvkmZE7RnYalxJeOFqJxJ5v50XHJ6u7lucQdQDGv2Owp4hGYXNrn1CCuYNYfn8GdvdtH8APu2gJYppzaMDYF0fImPlwdKQmUSQFaqJWPGgURPn1', 'Just me', 'www.coffeemars.gr', 'GLOBAL', 86);
INSERT INTO photos VALUES (62, '2O5bxE6tYPnetVVoXWyphRDUgQsWZRQoBoHbVmCseEYPD74eU6hzfFIUSUgPuM5WVjTyja6AY2euwpjJBpX6ifmdi27YHqh1yunnb8yiUA0qHvhdcJzn8xbhPUL2qDutExkPiQbBGxjGBQRxmNTQpRjIBYhlQgnQdf4WFbX3Yq7bxrFPVvCzdEYdJ8JleoswyG7sEuH0JYrQqJDiBKUZqEdUudOvHYFFWrqEloczN37rz', 'Just hangin out', 'www.restaurantvacatures.ch', 'GLOBAL', 96);
INSERT INTO photos VALUES (63, 'q3WC78cuVuWRWocYp', 'Me and my cat', 'www.zoekenmarkt.nl', 'SELF', 96);
INSERT INTO photos VALUES (64, 'RkzgISWcqiDTcWwFnmKHj8GbUCpSAgd6xdO6zJbExHMz3Rqudi6sfONlEHlXnI1SdlSI4DTWBGRLS2VC0xzRC5Zvqzt3W', 'Just hangin out', 'www.greenoffice.co.uk', 'GLOBAL', 98);
INSERT INTO photos VALUES (65, 'FnKwquwk7lvNRwcCg2y2qrRD', 'Me and my cat', 'www.zoekeninfo.gr', 'LOVE_MATCHES', 98);
INSERT INTO photos VALUES (66, 'OeTcx17TH3pvPkk3VMRPwlM', 'Me and my friends!!', 'www.perfecttool.nl', 'FRIENDS', 99);
INSERT INTO photos VALUES (67, 'ZBK4iJPSSoZFoSVyPtz0teHY84Mb8zfDRTX0Nq1sMnF5EYKn0TLx5w8ibVuJGXnMkTIgZTBUIPDfbRPgETXjlIRIBhEmdFi1aNRtbD7Qf3sKlb6haMNmzgULheygmZRhsObvwFuWNpGrMcexCmGR1ULjaTqkzikLb2RwJGrC0Kkv63HPudWiGHLUSIppgd4fNeb6gS0ccUfdMMvk8dL6oCnTtHn1KKW2Dxr8fhFBNo', 'Loved going out with you, guys!!', 'www.zoeksite.ch', 'LOVE_MATCHES', 99);
INSERT INTO photos VALUES (68, 'QIRZH5A5Q4SKPy2s347pLRzF76607dBQ3mJzJjZq2keSDcKtJYALYmmLIk2fteTuMeTuJDPujzcuPQhUAQ8fSxaH04qUOUf5vMmRt1Jq73ajelGgpvCJJKdzUJBju61x7dJHvIdojcmWk', 'Just me', NULL, 'GLOBAL', 99);
INSERT INTO photos VALUES (69, '2ceFFAre1tge0mBOWbCh2JmxbjvssXgQP2qWwq4Lf1Ux5X5Q0lFU0lmhgirloNdg5cignUR23TSAi2nfJ8', 'Just me', NULL, 'SELF', 2);
INSERT INTO photos VALUES (70, 'PJ2BLYLBWnClq02HPUhtTZ38oBmk7eIvY5UY3tNJYUaeNp1ixMOGQxkgxNfyIqHiSuq20LSCY6cwTuZpKY61mGmXv5F6PCQ44GGe7XhcXHa5m4nAK4s3dZlNlXJmItjh2baHBmwyIExFDlzsVZvIKS6roJhiSisUElUfawjty0vy3RUU003jtzUWM282FHZtqHtGe1nXPoAmZgGuhQIpF4gmzkmDf7prbuEHAl2qpcd2lB3lN4x45YxCm', 'Me and my cat', 'www.choicecompany.de', 'FRIENDS', 2);
INSERT INTO photos VALUES (71, 'bHPgDcr3cf2E8tnBeK6LdfJ8dIOMQ7PdAelIfmU1KW7BtTP6Zbk5ylXXmmzOJA7PisQcWBErBONF1i2nhnOLku5Rdzgp2TRGIydFKxjnZth5KEkVuprE7hAMl26L0KZUCwESb3ZBmhgnQSAdecLJkbRQgDjy71xX6GLH3EyoEk1mmgPpurpankL0lMtKrsDvY3oEJr3dximuTANFAAI0YX2GIHEV', 'Me and my cat', 'www.webschool.de', 'LOVE_MATCHES', 2);
INSERT INTO photos VALUES (72, 'fVpXdPgguY1itKX5NL0zVtDjkwA4KVfS42fUqk0cinuWbnlXPs7jDM4LK0OsY6bflDMaRH7D8fN8Aql38s43nSNRXR0vq0JwMcazutvqDIfUtgIaz1BAdN7I17bZ4tYDrRXUiP8uTUcxEyWTMRuD6XiDww3rkIB1RIUcUi5dO1duF3yyMzKhjYdaG2yEvUgBGoB', 'Me and my friends!!', NULL, 'LOVE_MATCHES', 6);
INSERT INTO photos VALUES (73, '2S8VcpzSL1JOehkVmESKPrGo2lGEk0kLQcPln7VEshyeuWC4qTAuDs0TAi7M3Kmv7e71nOkZg3OSjp', 'Me and my friends!!', NULL, 'FRIENDS', 6);
INSERT INTO photos VALUES (74, '5D2arzrZUzDF0fUdHR8UBBRhsq8BBB5KDF6dyfarDCIWRMLjFxooI8Sp6JReTp8goLJieN4cTCOtBzLIz2ak2JmV23MA4HUucZxmuXvkcZnOtdlgO5KURlJV1G7vQR2', 'Me and my friends!!', 'www.informatiewinkel.es', 'FRIENDS', 16);
INSERT INTO photos VALUES (75, 'XFpgABmJTLrtfmaBLPnrp5hLWNdLg6FPCHd4iayYNRHL2PlYDj0IBoq7sXMVoDbiQZsT10uhRD1DLg6CJbrvuYqe8', 'Just hangin out', NULL, 'SELF', 16);
INSERT INTO photos VALUES (76, '8z6s2X8Y8hAjuXsfPcc6', 'Me and my friends!!', 'www.voedingbaan.fr', 'SELF', 23);
INSERT INTO photos VALUES (77, 'ZXYryf8HddspHlsyrNN83kGGkH1RX3Lav5KFnYZ0IAJvu2Efwwl1uhLbBVBIudbpAxfWCCJbf2zx4G1dixbxUgtUTf2eG5YckKHjrIfQtCEbDRJKAjPuXvfulaq6nmlwI01k8rTcGTyKpYRr0vnljIUj3vHhXogrFOFtxxISkCZQ6pasOaTWYmszJrqJA2o6tF3AXdf03MOUMt8HIhPW4', 'Loved going out with you, guys!!', 'www.marktwereld.be', 'GLOBAL', 31);
INSERT INTO photos VALUES (78, '3Y3Fs3sfBsTf4pdUVNsmS1XW8CWBJP8MH0d6SvDqnUqwbGX', 'Just hangin out', 'www.contactvak.be', 'FRIENDS', 37);
INSERT INTO photos VALUES (79, 'xgczZh5Xfb6qJD5FwpBIOfGqUe2IUqqxASGfEvSd1z31cesXQjewIlrZedT1ZoagQQKF2ExZc2upBRKehpKfLwKWrJVWEqsVWerLD33Zxq5iRxv6JsmRJF1IDd5UNGej5hNULvWfZA1TKXXiFfxtEKruebdX21Yy3WL6KXmBHPWvBHnkKSftUqpNZ52pLdkNeUtNYhPBE3jswCOlahbUMPxunOQpl7VsX2J0Pltp', 'Me and my cat', 'www.bluetarget.gr', 'LOVE_MATCHES', 37);
INSERT INTO photos VALUES (80, 'rOZzCBo418zPOhxqpmtFXSzbMRPAHyMgtECjsgQ3h6uSn3ekeOdJvayLyJKu', 'Loved going out with you, guys!!', 'www.startwerk.org', 'FRIENDS', 41);
INSERT INTO photos VALUES (81, 'vcBfW0HU0o5kHcsfReRfcvNEPVMkKsrTRswUrlaYP0tqK3Wj4BxrY08I5nYSEdEMP6OJLzGwVNxLeYMyppsjzv18wpsbSWhQ00zTcGN54Ab6qgELh1V23omM53yyYpUfVXnX1Yc6j1UGYq5RDcteIZR3DJoLpR0bfitzcmMY2RVnrGk5eGpXUs5VNF6MeQhvkt', 'Just hangin out', NULL, 'GLOBAL', 50);
INSERT INTO photos VALUES (82, 'Lb4sK4q4YRQsk0OBit3Rj4s3WbbLDR50YJxAinwIRamQkUut5b8sOVO7UEFz2KIvNJdHYACaDH0DtvjEYa8taLVMGcJHwtyMtLxeJ4yQwDZ4l4KS3olK4lHqlLMZWt7IRvkXeGjgwkdTJow5qoufhqTjh4AmFSYIiklRT7tZcIAUqcCPIshfpgqGD3JDf7qS0jczF5Lbe5YfplEYynj', 'Dancing at the club woooow', 'www.rocksion.it', 'LOVE_MATCHES', 54);
INSERT INTO photos VALUES (83, 'BHcQlt4OY10WGKCryPn8rQZPihbXzLAsdZrbENE7IGohTpQZgULRPJi88Lon1ikiSzMyqHSiCDQR5wQzDHLAYLDZCc2dTKWI0YPLlbO1NNhEyHr8PcnsO2RXjiq6vhimdn3c1meFgJx2GLO7ZNT6PhmRdTiLkGlVh5dVv6jRndZCr5gGfcOmUPF6sbp5JtyCeiDXP074SKLmklnm', 'Loved going out with you, guys!!', NULL, 'GLOBAL', 60);
INSERT INTO photos VALUES (84, '5BKojxJMYA50iXEJQptt1RwqbCjaYF0ZUhsP48eRGIV8biuCn31O87PWYl6KkA3GgtM6BkrXF7Xv0wKrsY84as1fpF', 'Just me', 'www.startsite.fr', 'LOVE_MATCHES', 60);
INSERT INTO photos VALUES (85, '2q27BDOlQLXEGgO1jvXXYuGYNjbcWqSVb2Qu4fIyCVijQJ2WRYbHKoWXQgdkWZWahh3rJ2Z3ojFuJU7ljR4hxT2gCUiXsIGNvoyUydzQkX2mfIsrpMIt2Sm3rwV4XVSnmiqAF4WQv0iI4fb5PPFuUyENM', 'Me and my cat', 'www.informatiewerk.nl', 'SELF', 60);
INSERT INTO photos VALUES (86, 'o7AAqzK2umD2C5L0a', 'Dancing at the club woooow', 'www.coffeesion.de', 'SELF', 69);
INSERT INTO photos VALUES (87, 'YCZ7BGIgBSifV7OSzuCCsYjl0I47jUvlDBgsePOIXiu3n4azIBkrH77qeVkHwQ4ab4eBtfu1SuT0bxPxtfntznRel1rOIu81SeuntnU2pxId0O2pYzP58eU78mZybiNYhZ3uocRDCVUsz3qwF0CzWFoZyg1ZEqmebvnDdC0nIGLyDSOebhERbvPAgn8oyhOfh5rkp0V2sxWke38aXADhzMnoLiG58kBFgs35KsKywb8hg5Qa8ZVrJsn', 'Me and my friends!!', 'www.leraarvak.co.uk', 'LOVE_MATCHES', 69);
INSERT INTO photos VALUES (88, 'XBUQeUNxzeAKYhNMDbf7113Fo5f6XmfSBGuHDokPf084HUS66nhKjrqRQQyCUzXy1OZsl6IJXZZwW3cfnJVrmc5Y7ZRUogcR1W7ehkeojDjbPPq1e8WPwVAS', 'Just me', 'www.allebaan.it', 'SELF', 69);
INSERT INTO photos VALUES (89, 'InrVgzSJbwab4aT5CaPvngx3ycoQffWxES4ISaSveigWmJJ', 'Me and my friends!!', 'www.newsite.org', 'FRIENDS', 77);
INSERT INTO photos VALUES (90, 'IR1eXfNqEf4lBBmIX', 'Loved going out with you, guys!!', NULL, 'SELF', 77);
INSERT INTO photos VALUES (91, 'WoxUaHIhK5akS3nxIcnWlwVyEQUnpR2QUutsB4Brzk3DOfJaRzKadTpBgd3pDxiSJpNLx60v6epjDUc165BWKYtPjB8CIe8DDiSLAnMIfDuTjI2E3smajLX8sScFLuCQ6S', 'Me and my friends!!', 'www.cooltick.es', 'SELF', 77);
INSERT INTO photos VALUES (92, 'jkpe5csCenexwW8d4Csq3cz7DjhNbzvOTgpCakDgyx1ejjfE81ajNofcyJ2KdrCp6PvTxRwBKzbrWJBIigyWi4sFenLNI7BuoftwkYyY6NViiNa0gBAtbdHYJGiQTMvf2GZa5zYaFRuRmFprPdYKz3yvw4ajKvCUvtyZlh5zTr83qryuKS36ZlBm3VDDQiNbrh7Pmt5k8mNHf', 'Just hangin out', 'www.ziesoftware.ch', 'LOVE_MATCHES', 79);
INSERT INTO photos VALUES (93, 'D86wkRAXcXGUOW4tsWFJA0SlClLgFIncJVl', 'Loved going out with you, guys!!', 'www.bettertrace.co.uk', 'FRIENDS', 79);
INSERT INTO photos VALUES (94, 'eUFlNMr2b6m8EDItkx1DGoSN5hLqIxuyqbkiF3VTnA0mvLpVquPGjdSG7KEcoMediwCK6XR440NgOcxcmD1Xj3Vi1VP', 'Me and my friends!!', NULL, 'LOVE_MATCHES', 87);
INSERT INTO photos VALUES (95, 'vPXyIXC0SWFuqFCNO7OXr5PFMBY8pwlJvgcvdyVBgRBEU4NGDgyVaKF8XmrKtldO5c6enB0rAkKP7n0AT', 'Just me', 'www.kooptick.com', 'LOVE_MATCHES', 87);
INSERT INTO photos VALUES (96, 'jOKFTvWrFWcAwwSuLhMFPMY34mcnCUJlMD0AevtL2yQqJmdJzhupxQRAmhJQTa8jiXE4Jnc35UQAAvgBuvFbB3XzpMhzHyJVLll0IxmFKVnpbxBbLdgQIdMM1aNQtYqxNOA2wGA5gxnpW1C3mdtJb1m6bFo4l21MdGeM2TQMA6v2uZSx6KBMWucWIq4JVX40pJXKfSGQ61kmmBPsKqOVkZXpzQNWNVWcDOxBnQdZOKqYxtARG', 'Me and my friends!!', 'www.keuzewinkels.it', 'GLOBAL', 92);
INSERT INTO photos VALUES (97, 'AEMYMyoBLdwg3ZQvVSHkZzcViV0kU58QvG1NnDiHlGet83QsPB58qVcXHufS2ymdLpqMO1i2Ss0joYRd8vjX0VwKbmXV2WmkQTRHntFMrRQPRoZu2YjlcIrw', 'Me and my cat', 'www.trefplaza.org', 'SELF', 92);
INSERT INTO photos VALUES (98, '6uHdTquvhSb7snCI3zSt', 'Just me', 'www.mastermission.ch', 'LOVE_MATCHES', 92);
INSERT INTO photos VALUES (99, 'czq7Aga56tCVu56AnwIpEHagfxERkvdnGZqzb3AongM1VarNlBcWKl6DlCJFCzbqhhcb', 'Just me', NULL, 'GLOBAL', 2);
INSERT INTO photos VALUES (100, 'cNKcgd', 'Just me', 'www.digiwerk.es', 'SELF', 2);